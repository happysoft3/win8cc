
#include "ccobject.h"
#include "ccobjectThread.h"
#include <Windows.h>

static CCObjectThread s_objectThread;

CCObject::CCObject(CCObject *parent)
{
	if (parent)
		m_parent = parent->m_core;
	auto pr = std::make_unique<std::promise<bool>>();
	
	m_allowDeleter = std::make_unique<std::future<bool>>(pr->get_future());
	m_core = std::make_unique<Core>(std::move(pr), this);
}

CCObject::~CCObject()
{
	m_core.reset();
	m_allowDeleter->wait();
}

std::weak_ptr<CCObject::Core> CCObject::loadCore(CCObject *other) const
{
	if (!other)
		return {};

	return other->m_core;
}

CCObject *CCObject::coreToObject(CCObject::Core &core)
{
	return core.Owner();
}

void CCObject::makeQueueSignal(std::function<void()> &&fn)
{
	s_objectThread.PushQueue(this, std::move(fn));
}

void CCObject::printDebugMessage(const std::string &msg)
{
	std::string put = msg + '\n';
	::OutputDebugStringA(put.c_str());
}
