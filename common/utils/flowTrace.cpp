
#include "flowTrace.h"
#include "stringhelper.h"
#include <thread>
#include <future>
#include <mutex>
#include <ctime>
#include <iostream>

/**

스레드A, B

//B 스레드 - queue 에 elem 이 있는지 확인 -- // 평상시에는 차단상태 //
//!A 스레드 - queue 에 push !// -- // B 스레드에 알림

***/

using namespace _StringHelper;

class FlowTrace::FlowCore
{
private:
	FlowTrace *m_parent;
	std::shared_ptr<std::condition_variable> m_condvar;
	std::unique_ptr<std::future<bool>> m_tracerRet;

public:
	FlowCore(FlowTrace *parent)
		: m_parent(parent)
	{
		m_condvar = std::shared_ptr<std::condition_variable>(new std::condition_variable);
	}
	~FlowCore()
	{
		Notify();
	}
	FlowTrace *GetParent() const
	{
		return m_parent;
	}
	std::shared_ptr<std::condition_variable> GetCondition() const
	{
		return m_condvar;
	}
	void SetTracerResult(std::future<bool> &&result)
	{
		m_tracerRet = std::make_unique<std::future<bool>>(std::forward<std::future<bool>>(result));
	}
	void Notify()
	{
		m_condvar->notify_one();
	}
};

class FlowTrace::TracerState
{
private:
	bool m_runningState;
	std::shared_ptr<std::condition_variable> m_condvar;
	std::shared_ptr<std::mutex> m_parentLock;
	std::weak_ptr<FlowCore> m_parent;
	std::list<std::unique_ptr<AbstractFlowIssue>> m_taskQueue;
	std::unique_ptr<AbstractTraceCore> m_issueHandler;

public:
	TracerState(std::shared_ptr<FlowCore> parentCore)
	{
		m_runningState = true;
		m_condvar = parentCore ? parentCore->GetCondition() : nullptr;
		m_parent = parentCore;
	}
	~TracerState()
	{ }
	bool ParentIsExpired() const
	{
		return m_parent.expired();
	}
	void Shutdown()
	{
		m_runningState = false;
	}
	bool IsRunning() const
	{
		return m_runningState;
	}
	void SetParentLock(std::shared_ptr<std::mutex> parentLock)
	{
		m_parentLock = parentLock;
	}
	void Push(std::unique_ptr<AbstractFlowIssue> iss)
	{
		m_taskQueue.push_back(std::move(iss));
	}
	std::unique_ptr<AbstractFlowIssue> Pop()
	{
		if (m_taskQueue.empty())
			return {};

		std::unique_ptr<AbstractFlowIssue> ret = std::move(m_taskQueue.front());

		m_taskQueue.pop_front();
		return ret;
	}

private:
	bool fetchIssue(std::shared_ptr<FlowCore> core)
	{
		core->GetParent()->PopAll(this);

		return !m_taskQueue.empty();
	}

public:
	bool IsActivated()	//조건이 TRUE 일때, event wait 에서 pass 됩니다
	{
		if (!m_runningState)
			return true;

		std::shared_ptr<FlowCore> core = m_parent.expired() ? nullptr : m_parent.lock();

		if (!core)
			return true;

		return fetchIssue(core);
	}
	void SetIssueHandler(std::unique_ptr<AbstractTraceCore> &&handler)
	{
		m_issueHandler = std::forward<std::unique_ptr<AbstractTraceCore>>(handler);
	}

public:
	static bool TracerProc(std::shared_ptr<TracerState> liveState)
	{
		std::unique_ptr<AbstractFlowIssue> issue;

		for (;;)
		{
			{
				std::unique_lock<std::mutex> innerLock(*liveState->m_parentLock);
				liveState->m_condvar->wait(innerLock, [state = liveState]() {
					return state->IsActivated();
				});

				if (liveState->ParentIsExpired())
					return false;
				if (!liveState->IsRunning())
					break;

				issue = liveState->Pop();
			}

			if (issue)
			{
				issue->SetIssueTime(generateDatetime());
				liveState->m_issueHandler->Issue(std::move(issue));
			}
		}
		return true;
	}
};

FlowTrace::FlowTrace()
	: m_core(new FlowCore(this))
{
	m_lock = std::make_shared<std::mutex>();
}

FlowTrace::~FlowTrace()
{
	m_core.reset();
}

std::unique_ptr<FlowTrace::TracerState> FlowTrace::makeState()
{
	std::unique_ptr<TracerState> state(new TracerState(m_core));

	state->SetParentLock(m_lock);
	return state;
}

std::string FlowTrace::generateDatetime()
{
	std::time_t now = std::time(0);
	std::tm datetime;

	localtime_s(&datetime, &now);
	return stringFormat("%02d-%02d-%04d %02d:%02d:%02d", datetime.tm_mday, datetime.tm_mon + 1, datetime.tm_year + 1900, datetime.tm_hour, datetime.tm_min, datetime.tm_sec);
}

bool FlowTrace::Start(std::unique_ptr<AbstractTraceCore> trace)
{
	if (m_tracerAlive)
		return false;

	m_tracerAlive = makeState();
	m_tracerAlive->SetIssueHandler(std::move(trace));
	std::packaged_task<bool()> task([alive = m_tracerAlive]() { return TracerState::TracerProc(std::move(alive)); });
	m_core->SetTracerResult(task.get_future());
	std::thread tracer(std::move(task));

	tracer.detach();
	return true;
}

void FlowTrace::PushTrace(std::unique_ptr<AbstractFlowIssue> issue)
{
	{
		std::unique_lock<std::mutex> lock(*m_lock);

		m_issueList.push_back(std::move(issue));
	}
	m_core->Notify();
}

std::unique_ptr<AbstractFlowIssue> FlowTrace::PopOne()
{
	if (m_issueList.empty())
		return {};

	std::unique_ptr<AbstractFlowIssue> front = std::move(m_issueList.front());

	m_issueList.pop_front();
	return front;
}

void FlowTrace::PopAll(FlowTrace::TracerState *state)
{
	for (auto &&issue : m_issueList)
		state->Push(std::move(issue));

	m_issueList.clear();
}

void FlowTrace::TraceCore::Issue(std::unique_ptr<AbstractFlowIssue> issue)
{
	FlowIssue *log = static_cast<FlowIssue *>(issue.get());

	std::cout << stringFormat("[%s] %s", log->issueTime(), log->issueName()) << std::endl;
}

FlowTrace::FlowIssue *FlowTrace::TraceCore::CreateNewIssue()
{
	return new FlowIssue({});
}
