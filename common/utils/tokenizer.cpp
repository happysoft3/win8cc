
#include "tokenizer.h"
#include "stringhelper.h"

using namespace _StringHelper;

class Tokenizer::EofToken : public Tokenizer::Token
{
public:
	EofToken()
		: Token({})
	{ }
	~EofToken() override
	{ }

private:
	TokenType::Type Type() const override
	{
		return TokenType::TEOF;
	}
};

class Tokenizer::SpaceToken : public Tokenizer::Token
{
public:
	SpaceToken()
		: Token({})
	{ }

	~SpaceToken() override
	{ }

private:
	TokenType::Type Type() const override
	{
		return TokenType::TSPACE;
	}
};

class Tokenizer::SymbolToken : public Tokenizer::Token
{
public:
	SymbolToken(const char c)
		: Token(std::string(sizeof(c), c))
	{ }

	~SymbolToken() override
	{ }

private:
	TokenType::Type Type() const override
	{
		return TokenType::TSYMB;
	}
};

Tokenizer::Tokenizer()
	: BinaryBuffer()
{
	m_byteRd = 0;
}

Tokenizer::~Tokenizer()
{ }

int Tokenizer::peek()
{
	uint8_t uc = 0;

	if (GetC(uc, m_byteRd))
		return uc;

	return EOF;
}

int Tokenizer::pop()
{
	int tok = peek();

	if (tok != EOF)
	{
		++m_byteRd;
		return tok;
	}
	return tok;
}

void Tokenizer::push(int tok)
{
	if (tok == EOF)
		return;

	uint8_t ret = static_cast<uint8_t>(tok);

	if (SetC(ret, m_byteRd - 1))
		--m_byteRd;
}

std::string Tokenizer::debugLine(const std::string &debugString)
{
	return stringFormat("Tokenizer::%s - file: %s, line: %d - %s", __FUNCTION__, __FILE__, __LINE__, debugString);
}

std::unique_ptr<Tokenizer::Token> Tokenizer::readNumber(int c)
{
	std::string buffer(sizeof(char), c);

	for (;;)
	{
		c = pop();

		if (!isdigit(c))
		{
			push(c);
			return std::make_unique<Token>(buffer);
		}
		buffer.push_back(c);
	}
}

std::unique_ptr<Tokenizer::Token> Tokenizer::readIdent(int c)
{
	std::string buffer(sizeof(char), c);

	for (;;)
	{
		c = pop();
		if (isalnum(c) || c == '-')
		{
			buffer.push_back(c);
			continue;
		}
		push(c);
		return std::make_unique<Token>(buffer);
	}
}

bool Tokenizer::skipWhitespaceImpl()
{
	int c = pop();

	if (c == EOF)
		return false;

	switch (c)
	{
	case ' ': case '\t': case '\f': case '\v': return true;
	default:
		push(c);
		return false;
	}
}

bool Tokenizer::skipWhitespace()
{
	if (!skipWhitespaceImpl())
		return false;

	while (skipWhitespaceImpl());
	return true;
}

std::unique_ptr<Tokenizer::Token> Tokenizer::readToken()
{
	if (skipWhitespace()) return std::make_unique<SpaceToken>();
	int c = peek();

	switch (c)
	{
	case EOF: return std::make_unique<EofToken>();
	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9': return readNumber(pop());
	case '?':
	case '=': return std::make_unique<SymbolToken>(pop());
	case '-': return readIdent(pop());
	default:
		if ((c >= 'a'&&c <= 'z') || (c >= 'A'&&c <= 'Z'))
			return readIdent(pop());

		return {};	//unknown token
	}
}

bool Tokenizer::GetToken(std::string &dest)
{
	auto tok = readToken();

	if (!tok)
		return false;

	dest = tok->Value();
	return true;
}

