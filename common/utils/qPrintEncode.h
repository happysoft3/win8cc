
#ifndef Q_PRINT_ENCODE_H__
#define Q_PRINT_ENCODE_H__

#include "binaryHelper.h"
#include <list>
#include <memory>

class QPrintEncode : public BinaryHelper
{
	struct EncodeToken;
	struct EncodeData;

public:
	class Error;
private:
	std::list<std::unique_ptr<EncodeToken>> m_tokList;
	std::unique_ptr<Error> m_lastError;
	std::unique_ptr<EncodeData> m_data;

public:
	QPrintEncode();
	~QPrintEncode() override;

private:
	int hexaChar(int c) const;
	int readHexaExpr(int c);
	void readKeyword();
	void readSubExpr();
	bool readExpr();
	void makeEncodeData();
	bool readQPrint();

public:
	bool Decode();
	std::string GetError() const;
	operator bool() const
	{
		return m_data ? true : false;
	}

	std::string Type() const;
	std::string Symbol() const;
	std::string Value() const;
};

#endif

