
#include "pathManager.h"
#include "stringhelper.h"

using namespace _StringHelper;
static auto s_slashConvert = [](const char &c) { return (c == '/') ? '\\' : c; };

PathManager::PathManager()
{ }

PathManager::~PathManager()
{ }

void PathManager::SetSubName(const std::string &subName)
{
	if (subName.empty())
		return;

	m_subTerm.resize(subName.size());
	std::transform(subName.cbegin(), subName.cend(), m_subTerm.begin(), s_slashConvert);
}

void PathManager::SetUrl(const std::string &url)
{
    if (url.empty())
    {
        if (m_targetUrl.size())
            m_targetUrl.clear();
        return;
    }

    m_targetUrl.resize(url.size());
    std::transform(url.cbegin(), url.cend(), m_targetUrl.begin(), s_slashConvert);
    size_t lastSlash = m_targetUrl.find_last_of('\\');

	if (lastSlash != std::string::npos)
	{
		m_subTerm = m_targetUrl.substr(lastSlash + 1);
		m_targetUrl.resize(lastSlash);
	}
}

const char *PathManager::BasePath() const
{
    if (m_targetUrl.empty())
        return "";

    return m_targetUrl.c_str();
}

std::string PathManager::FullPath(const std::string &subUrl) const
{
    if (m_targetUrl.empty())
        return subUrl;

    return stringFormat("%s\\%s", m_targetUrl, subUrl);
}

std::string PathManager::FullPath() const
{
	if (m_targetUrl.empty())
		return {};

	if (m_subTerm.empty())
		return m_targetUrl;

	return stringFormat("%s\\%s", m_targetUrl, m_subTerm);
}
