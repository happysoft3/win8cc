
#ifndef FLOW_TRACE_H__
#define FLOW_TRACE_H__

#include <string>
#include <list>
#include <memory>

namespace std
{
	class mutex;
};

class AbstractTraceCore;
class AbstractFlowIssue;

class FlowTrace final
{
	class FlowCore;	//!내부에서만 사용!// 로그 출력을 위한 스레드 관련 객체 입니다
	class TracerState; //!내부에서만 사용!// 로그 출력을 위한 스레드의 상태 그리고 결과를 위한 객체 입니다

public:
	class TraceCore;	//!외부에서 재정의할 수 있음!// 로그 데이터의 출력 방식을 나타냅니다, 이것은 예를 들어 콘솔에 그것을 나타낼 지 또는 파일에 할지를 결정합니다
	class FlowIssue;		//로그 데이터의 형식을 나타냅니다

private:
	std::shared_ptr<FlowCore> m_core;
	std::shared_ptr<TracerState> m_tracerAlive;
	std::list<std::unique_ptr<AbstractFlowIssue>> m_issueList;

public:
	FlowTrace();
	~FlowTrace();

private:
	std::unique_ptr<TracerState> makeState();
	static std::string generateDatetime();

public:
	bool Start(std::unique_ptr<AbstractTraceCore> trace);
	void PushTrace(std::unique_ptr<AbstractFlowIssue> issue);
	std::unique_ptr<AbstractFlowIssue> PopOne();
	void PopAll(TracerState *state);

private:
	std::shared_ptr<std::mutex> m_lock;
};

class AbstractFlowIssue
{
private:
	std::string m_issueName;
	std::string m_datetime;

public:
	AbstractFlowIssue(const std::string &issue)
	{
		m_issueName = issue;
	}

	virtual ~AbstractFlowIssue()
	{ }

	void SetIssueTime(const std::string &datetime)
	{
		m_datetime = datetime;
	}
	void SetIssueName(const std::string &name)
	{
		m_issueName = name;
	}

	//protected:
	std::string issueName() const
	{
		return m_issueName;
	}
	std::string issueTime() const
	{
		return m_datetime;
	}
};

class FlowTrace::FlowIssue : public AbstractFlowIssue
{
public:
	FlowIssue(const std::string &issue)
		: AbstractFlowIssue(issue)
	{ }
	~FlowIssue() override
	{ }
};

class AbstractTraceCore
{
public:
	AbstractTraceCore()
	{ }

	virtual ~AbstractTraceCore()
	{ }

	virtual void Issue(std::unique_ptr<AbstractFlowIssue> issue) = 0;
	virtual AbstractFlowIssue *CreateNewIssue() = 0;
};

class FlowTrace::TraceCore : public AbstractTraceCore
{
public:
	TraceCore()
	{ }

	~TraceCore() override
	{ }

	void Issue(std::unique_ptr<AbstractFlowIssue> issue) override;

private:
	FlowIssue *CreateNewIssue() override;
};

#endif

