
#ifndef STRING_TOKENIZER_H__
#define STRING_TOKENIZER_H__

#include "common/utils/binaryHelper.h"
#include <memory>

class TokenizerToken;

class StringTokenizer : public BinaryHelper
{
public:
	StringTokenizer();
	~StringTokenizer() override;

private:
	bool isWhitespace(int c) const;
	void skipBlockComment();
	void skipLine();
	bool skipComment(int c);
	bool doSkipSpace();
	bool skipSpace();
	std::unique_ptr<TokenizerToken> readNumber(int c);
	std::unique_ptr<TokenizerToken> readIdent(int c);
	virtual int readEscapedChar();
	std::unique_ptr<TokenizerToken> readString();
	virtual std::unique_ptr<TokenizerToken> doReadToken(int c);

public:
	virtual std::unique_ptr<TokenizerToken> ReadToken();
};

#endif

