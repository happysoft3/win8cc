
#include "tokenizerToken.h"
#include <sstream>

TokenizerToken::TokenizerToken(TokenizerToken::Type ty)
{
	m_ty = ty;
}

TokenizerToken::~TokenizerToken()
{ }

StringStorageToken::StringStorageToken(TokenizerToken::Type ty, const std::string &s)
	: TokenizerToken(ty)
{
	m_str = s;
}

NumericToken::NumericToken(const std::string &s)
	: StringStorageToken(TokenizerToken::Type::NumberType, s)
{
	putNumber(s);
}

void NumericToken::putNumber(const std::string &s)
{
	std::stringstream ss;

	ss << s;
	ss >> m_number;
}

KeywordToken::KeywordToken(int key)
	: TokenizerToken(TokenizerToken::Type::KeywordType)
{
	m_keyword = key;
}

IdentToken::IdentToken(const std::string &s)
	: StringStorageToken(TokenizerToken::Type::IdentType, s)
{ }

StringToken::StringToken(const std::string &s)
	: StringStorageToken(TokenizerToken::Type::StringType, s)
{ }

