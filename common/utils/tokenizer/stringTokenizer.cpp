
#include "stringTokenizer.h"
#include "utils/tokenizer/tokenizerToken.h"
#include "common/utils/myDebug.h"

StringTokenizer::StringTokenizer()
	: BinaryHelper()
{ }

StringTokenizer::~StringTokenizer()
{ }

bool StringTokenizer::isWhitespace(int c) const
{
	switch (c)
	{
	case ' ': case '\t': case '\f': case '\v':	 return true;
	default: return false;
	}
}

void StringTokenizer::skipBlockComment()
{
	bool maybeEnd = false;

	for (;;)
	{
		int c = pop();
		if (c == EOF)
			MY_THROW() << "premature end of block comment";
		if (c == '/' && maybeEnd)
			return;
		maybeEnd = (c == '*');
	}
}

void StringTokenizer::skipLine()
{
	for (;;)
	{
		int c = pop();
		if (c == EOF)
			return;

		if (c == '\n')
		{
			push(c);
			return;
		}
	}

}

bool StringTokenizer::skipComment(int c)
{
	if (c != '/')
		return false;

	if (next('*'))
	{
		skipBlockComment();
		return true;
	}
	if (next('/'))
	{
		skipLine();
		return true;
	}
	return false;
}

bool StringTokenizer::doSkipSpace()
{
	int c = pop();

	if (c == EOF)
		return false;

	if (isWhitespace(c))
		return true;

	if (skipComment(c))
		return true;

	push(c);
	return false;
}

bool StringTokenizer::skipSpace()
{
	while (!doSkipSpace())
		return false;

	while (doSkipSpace());
	return true;
}

std::unique_ptr<TokenizerToken> StringTokenizer::readNumber(int c)
{
	std::string buffer(sizeof(char), static_cast<char>(c));

	for (;;)
	{
		c = pop();
		if (!isdigit(c))
		{
			push(c);
			return std::make_unique<NumericToken>(buffer);
		}
		buffer.push_back(static_cast<char>(c));
	}
}

std::unique_ptr<TokenizerToken> StringTokenizer::readIdent(int c)
{
	std::string b(sizeof(char), static_cast<char>(c));

	for (;;)
	{
		c = pop();
		if (isalnum(c) || (c & 0x80) || c == '_' || c == '$')
		{
			b.push_back(c);
			continue;
		}
		push(c);
		return std::make_unique<IdentToken>(b);
	}
}

int StringTokenizer::readEscapedChar()
{
	int c = pop();
	// This switch-cases is an interesting example of magical aspects
	// of self-hosting compilers. Here, we teach the compiler about
	// escaped sequences using escaped sequences themselves.
	// This is a tautology. The information about their real character
	// codes is not present in the source code but propagated from
	// a compiler compiling the source code.
	// See "Reflections on Trusting Trust" by Ken Thompson for more info.
	// http://cm.bell-labs.com/who/ken/trust.html
	switch (c)
	{
	case '\'': case '"': case '?': case '\\':
		return c;
	case 'a': return '\a';
	case 'b': return '\b';
	case 'f': return '\f';
	case 'n': return '\n';
	case 'r': return '\r';
	case 't': return '\t';
	case 'v': return '\v';
	default:
		MY_THROW() << "unknown escape character: " << static_cast<char>(c);
	}
}

std::unique_ptr<TokenizerToken> StringTokenizer::readString()
{
	std::string b;

	for (;;)
	{
		int c = pop();
		if (c == EOF)
			MY_THROW() << "unterminated string";
		if (c == '"')
			break;
		if (c != '\\')
		{
			b.push_back(c);
			continue;
		}
		c = readEscapedChar();
		b.push_back(c);
	}
	return std::make_unique<StringToken>(b);

}

std::unique_ptr<TokenizerToken> StringTokenizer::doReadToken(int c)
{
	switch (c)
	{
	case EOF: return std::make_unique<EofToken>();
	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9': return readNumber(c);
	case ':':
	case '#':
	case '+': case '-': case '*': case '=':
	case '%':
	case '.':
	case '(':
	case '<':
	case '[':
		return std::make_unique<KeywordToken>(c);
	case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l':
	case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z':
	case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K': case 'L':
	case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T':
	case 'V': case 'W': case 'X': case 'Y': case 'Z': case '_': case '$':
		return readIdent(c);
	case '"': return readString();
	default:
		if (c >= 0x80 && c <= 0xfd)
			return readIdent(c);
		MY_THROW() << "unknown token " << static_cast<char>(c);
	}
}

std::unique_ptr<TokenizerToken> StringTokenizer::ReadToken()
{
	if (skipSpace()) return std::make_unique<SpaceToken>();

	return doReadToken(pop());
}

