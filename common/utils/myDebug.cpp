
#include "myDebug.h"
#include "stringhelper.h"
#include "printUtil.h"
#include <iostream>

using namespace _StringHelper;

MyDebug::MyDebug(const std::string &pos)
	: CFlush::Context()
{
	m_triggered = std::make_unique<bool>(false);
	m_pos = pos;
}

MyDebug::~MyDebug()
{
}

void MyDebug::Put(const std::string &s)
{
	if (!m_triggered)
		return;

	m_triggered.reset();
	throw std::logic_error(stringFormat("code: %s : %s", m_pos, s));
}

MyThrow::MyThrow(CFlush::Context *ctx)
{
	m_flush = std::make_unique<CFlush>(ctx);
}
MyThrow::~MyThrow() noexcept(false)
{
	m_flush->Flush();
	m_flush.reset();
}

MyCOut::MyCOut(MyCOut::PrintColor colr)
    : CFlush::Context()
{
    m_colr = colr;
}

MyCOut::~MyCOut()
{ }

void MyCOut::Put(const std::string &s)
{
    auto colrSelector = [this]() {
        switch (m_colr)
        {
        case PrintColor::Blue: return PrintUtil::ConsoleColor::COLOR_BLUE;
        case PrintColor::Cyan: return PrintUtil::ConsoleColor::COLOR_CYAN;
        case PrintColor::Green: return PrintUtil::ConsoleColor::COLOR_GREEN;
        case PrintColor::Red: return PrintUtil::ConsoleColor::COLOR_RED;
        case PrintColor::White: return PrintUtil::ConsoleColor::COLOR_WHITE;
        case PrintColor::Grey: return PrintUtil::ConsoleColor::COLOR_GREY;
        case PrintColor::Pink: return PrintUtil::ConsoleColor::COLOR_PINK;
        case PrintColor::Normal: 
        default: return PrintUtil::ConsoleColor::COLOR_DARKWHITE;
        }
    };
    PrintUtil::ConsoleColor selColr = colrSelector();

    PrintUtil::PrintMessage(selColr, s);
}
