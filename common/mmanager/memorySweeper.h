
#ifndef MEMORY_SWEEPER_H__
#define MEMORY_SWEEPER_H__

#include "dynamicMemory.h"
#include <memory>
#include <list>
#include <map>

class MemorySweeper : public DynamicMemory
{
private:
	using mem_list_ty = std::list<std::unique_ptr<DynamicMemory>>;
	mem_list_ty m_memList;
	using mem_list_iter = mem_list_ty::iterator;
	std::map<std::string, mem_list_iter> m_memMap;

public:
	MemorySweeper();
	~MemorySweeper() override;

private:
	void putDynamic(std::unique_ptr<DynamicMemory> mem);
	std::unique_ptr<DynamicMemory> releaseDynamic(const std::string &address);

public:
	template <class Ty>
	Ty *AllocateRaw(size_t sz)
	{	///create raw here
		auto pRaw = new DynamicRaw<Ty>(sz);

		putDynamic(std::unique_ptr<DynamicMemory>(pRaw));
		return pRaw->Get();
	}

	template <class Ty, class... Args>
	Ty *AllocateObject(Args&&... args)
	{
		auto pRaw = new DynamicObject<Ty>(std::forward<Args>(args)...);

		putDynamic(std::unique_ptr<DynamicMemory>(pRaw));
		return pRaw->Get();
	}

	template <class Ty>
	void Delete(Ty *ptr)
	{
		auto sptr = loadAddressWithPTR(ptr);
		auto m = releaseDynamic(sptr);

		if (m)
			m.reset();
	}
};

#endif
