
#ifndef BINARY_BUFFER_EX_H__
#define BINARY_BUFFER_EX_H__

#include <vector>
#include <iterator>

template <class BufCharTy>
class BinaryBufferEx
{
	using buffer_char_ty = typename std::enable_if<std::is_integral<BufCharTy>::value, BufCharTy>::type;
	using buffer_ty = std::vector<buffer_char_ty>;
private:
	buffer_ty m_buffer;

public:
	BinaryBufferEx()
	{ }
	virtual ~BinaryBufferEx()
	{ }

private:
	template <class Ty>
	bool checkCapacity(size_t index)
	{
		return (index * sizeof(buffer_char_ty)) + sizeof(Ty) <= m_buffer.size() * sizeof(buffer_char_ty);
	}

	template <class CharT, class Ty, bool Compare>
	struct FieldLoader;

	template <class CharT, class Ty>
	struct FieldLoader<CharT, Ty, true>
	{
		union field_ty
		{
			Ty target;
			CharT buff[sizeof(Ty) / sizeof(CharT)];
		};
	};

	template <class CharT, class Ty>
	struct FieldLoader<CharT, Ty, false>
	{
		using field_ty = union
		{
			Ty target;
			CharT buff;
		};
	};

	template <bool Compare, class CharT, class Ty>
	struct FieldWorker : public FieldLoader<CharT, Ty, Compare>
	{
		template <bool Compare>
		struct FieldMethod;

		template <>
		struct FieldMethod<true> : public std::true_type::type
		{
			static void Get(Ty &dest, typename const std::vector<CharT>::iterator &srcIter)
			{
				typename FieldLoader<CharT, Ty, value>::field_ty f = {};

				std::copy_n(srcIter, sizeof(f.buff), f.buff);	//warnings?
				dest = f.target;
			}

			static void Set(const Ty &src, typename const std::vector<CharT>::iterator &destIter)
			{
				typename FieldLoader<CharT, Ty, value>::field_ty f = { src };

				std::copy_n(f.buff, sizeof(f.buff), destIter);
			}
		};

		template <>
		struct FieldMethod<false> : public std::false_type::type
		{
			static void Get(Ty &dest, typename const std::vector<CharT>::iterator &srcIter)
			{
				typename FieldLoader<CharT, Ty, value>::field_ty f = { };
				f.buff = *srcIter;
				dest = f.target;
			}
			static void Set(const Ty &src, typename const std::vector<CharT>::iterator &destIter)
			{
				typename FieldLoader<CharT, Ty, value>::field_ty f = { src, };
				
				*destIter = f.buff;
			}
		};

		template <class... Args>
		static void Get(Args&&... args)
		{
			FieldMethod<Compare>::Get(std::forward<Args>(args)...);
		}

		template <class... Args>
		static void Set(Args&&... args)
		{
			FieldMethod<Compare>::Set(std::forward<Args>(args)...);
		}
	};

public:
	template <class Ty, typename std::enable_if<std::is_arithmetic<Ty>::value, bool>::type = true>
	bool Getc(Ty &dest, size_t index)
	{
		if (!checkCapacity<Ty>(index))
			return false;

		FieldWorker<(sizeof(Ty) > sizeof(buffer_char_ty)), buffer_char_ty, Ty>::Get(dest, m_buffer.begin() + index);
		return true;
	}

	template <class Ty, typename std::enable_if<std::is_arithmetic<Ty>::value, bool>::type = true>
	bool Setc(const Ty &src, size_t index)
	{
		if (!checkCapacity<Ty>(index))
			return false;

		FieldWorker<(sizeof(Ty) > sizeof(buffer_char_ty)), buffer_char_ty, Ty>::Set(src, m_buffer.begin() + index);
		return true;
	}

protected:
	virtual void onBufferClean()
	{
		if (m_buffer.size())
			m_buffer.clear();
	}

public:
	void Clear()
	{
		onBufferClean();
	}

	size_t Size() const
	{
		return m_buffer.size();
	}

	bool Empty() const
	{
		return m_buffer.empty();
	}

	template <class Container, 
		typename std::enable_if<std::is_same<buffer_char_ty, typename Container::value_type>::value, bool>::type = true>
	void StreamPush(const Container &src)
	{
		if (src.empty())
			return;

		m_buffer.reserve(m_buffer.size() + src.size());
		std::copy(src.cbegin(), src.cend(), std::insert_iterator<buffer_ty>(m_buffer, m_buffer.end()));
	}

	template <class Container, 
		typename std::enable_if<std::is_same<buffer_char_ty, typename Container::value_type>::value, bool>::type = true>
	void StreamPut(const Container &src)
	{
		if (src.empty())
		{
			m_buffer.clear();
			return;
		}
		m_buffer.resize(src.size());
		std::copy(src.cbegin(), src.cend(), m_buffer.begin());
	}
};

#endif

