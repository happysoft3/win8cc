
#ifndef FUNCTION_HELPER_H__
#define FUNCTION_HELPER_H__

namespace _FunctionHelper
{
	template <class Fn>
	struct FunctionHelper;

	template <class RetTy, class Ty>
	struct FunctionHelperBase
	{
		using ret_type = RetTy;
		using object_type = Ty;
	};

	template <class Ret, class Ty, class... Args>
	struct FunctionHelper<Ret(Ty::*)(Args...)> : public FunctionHelperBase<Ret, Ty>
	{ };

	template <class Ret, class Ty, class... Args>
	struct FunctionHelper<Ret(Ty::*)(Args...)const> : public FunctionHelperBase<Ret, Ty>
	{ };
}

#endif

