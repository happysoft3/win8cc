
#ifndef TASK_RESULT_H__
#define TASK_RESULT_H__

#include "taskObject.h"

class TaskUnit;

class TaskResult : public TaskObject
{
private:
	std::string m_taskId;

public:
	TaskResult();
	TaskResult(TaskUnit &task);
	~TaskResult() override;

public:
	const char *const TaskID() const
	{
		return m_taskId.empty() ? "" : m_taskId.c_str();
	}
};

#endif
