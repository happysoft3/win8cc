
#include "makeScript.h"
#include "common/utils/binaryBuffer.h"
#include "common\utils\stringhelper.h"
#include "common\utils\myDebug.h"

using namespace _StringHelper;

#define DEF_CODE_TEMPLATE(...)  __VA_ARGS__
static const std::vector<uint8_t> s_codeTemplate = {
#include "codeTemplate.inc"
    };
#undef DEF_CODE_TEMPLATE

static constexpr size_t register_base_off = 0x12;
static constexpr size_t code_base_off = 0x5f;
static constexpr size_t copy_length_off = 79;   //unused...

MakeScript::MakeScript()
{
    m_buffer = std::make_unique<BinaryBuffer>();
    m_buffer->StreamSet(s_codeTemplate);

    size_t padding = sizeof(int) - (s_codeTemplate.size() % sizeof(int));
    std::vector<uint8_t> rest(padding, 0x90);

    if (rest.size())
        m_buffer->StreamPush(rest);
}

MakeScript::~MakeScript()
{ }

void MakeScript::writeComment(std::ofstream &file)
{
    file << "/*" << std::endl;
    file << stringFormat("***INFO: %s:EUDLoaderScript***", __FUNCDNAME__) << std::endl;
    int codebase = 0;
    m_buffer->GetC(codebase, code_base_off);
    int regbase = 0;
    m_buffer->GetC(regbase, register_base_off);
    size_t copyCount = 0;
    //m_buffer->GetC(copyCount, copy_length_off);
    file << stringFormat("code base: 0x%08x,", codebase) << std::endl;
    file << stringFormat("reg base: 0x%08x,", regbase) << std::endl;
    file << stringFormat("copy count: %d", copyCount) << std::endl;
    file << "*/" << std::endl;
}

void MakeScript::writeFunctionPrototype(std::ofstream &file)
{
    file << "void EUDCoreAPILoader()";
}

void MakeScript::writeFunctionEntry(std::ofstream &file)
{
    file << "int target = 0x979744;" << std::endl;
    file << stringFormat("int arr[%d];", ((s_codeTemplate.size()) / sizeof(int)) + 1) << std::endl;
    file << "if(arr[0]) return;" << std::endl;
}

void MakeScript::writeCodeData(std::ofstream &file)
{
    size_t index = 0, length = m_buffer->Size() / sizeof(int);

    for (; index < length ; ++index)
    {
        size_t val = 0;

        if (!m_buffer->GetC(val, index * sizeof(int)))
            MY_THROW() << "error makescript";
        file << stringFormat("arr[%d] = 0x%08x;", index, val);
        if (!(index % 6))
            file << std::endl;
    }
}

void MakeScript::writeFunctionTail(std::ofstream &file)
{
    file << "UnBlind();" << std::endl;
}

void MakeScript::SetCodebase(int base)
{
    m_buffer->SetC(base, code_base_off);
}

void MakeScript::SetRegisterBase(int base)
{
    m_buffer->SetC(base, register_base_off);
}

void MakeScript::SetCopyLength(size_t length)
{
    //length /= sizeof(int);
    //m_buffer->SetC(length, copy_length_off);
}

void MakeScript::Write(std::ofstream &file)
{
    writeComment(file);
    writeFunctionPrototype(file);
    file.put('{');
    file << std::endl;

    writeFunctionEntry(file);
    writeCodeData(file);
    writeFunctionTail(file);

    file.put('}');
    file << std::endl;
}

