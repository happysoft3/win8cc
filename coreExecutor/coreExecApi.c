
/*** README ***
 * 이 소스에 정의된, 함수는 타겟 엔진에서 실행됩니다
 * 여기에서 타겟에서 실행될, 코드를 생성합니다
 * 주의! 증분링크 옵션을 비활성화 해야합니다
 * 릴리즈 모드로 컴파일 하십시오
***/

#include "coreExecApi.h"
#include "coreRegisterDef.h"
#include "include/script/builtins.h"
#include <stdio.h>

#define CORE_INVOKE __cdecl
#define CORE_INVOKE_SCOPE static

#define DECLARE_CORE_API(_retTy, _identifier, _arg, _code)	\
	CORE_INVOKE_SCOPE _retTy CORE_INVOKE _identifier _arg \
	_code	\
	CORE_INVOKE_SCOPE void CORE_INVOKE _identifier##Dummy() { printf(__FUNCDNAME__); }

#define ENABLE_BUILTIN_ESP

#ifdef ENABLE_BUILTIN_ESP

#define DECLARE_CORE_API_EX(_identifier, _code) \
	DECLARE_CORE_API(int, _identifier, (int *pArgs), _code)

#define DECLARE_CORE_API_EX_UNSAFE(_identifier, _code) \
    DECLARE_CORE_API(__declspec(safebuffers) int, _identifier, (int *pArgs), _code)

#elif
#define DECLARE_CORE_API_EX(_identifier, _code) \
	DECLARE_CORE_API(int, _identifier, (), _code)

#endif

#define CORE_API_RETURN return 0;

static lpCoreExecApi s_core = NULL;
typedef int (CORE_INVOKE *engine_api_scr_pop)();
typedef void (CORE_INVOKE *engine_api_scr_push)(int);
typedef void (CORE_INVOKE *engine_api_scr_invoke)(int, int, int);

typedef struct nox_script_xxx_t {
    char* field_0; // 0, 0
    unsigned int stack_size; // 1, 4
    unsigned int size_28; // 2, 8
    unsigned int field_12; // 3, 12; len field_20 and field_24
    unsigned int field_16; // 4, 16
    unsigned int* field_20; // 5, 20
    unsigned int* field_24; // 6, 24
    unsigned int* field_28; // 7, 28
    void* data; // 8, 32
    char* field_36; // 9, 36
    unsigned int field_40; // 10, 40
    unsigned int field_44; // 11, 44
} nox_script_xxx_t;

/*
 *@brief. addr 주소를 읽은 값을 scr 스택에 push 합니다
*/

#define LOAD_SCR_POP(_alias) engine_api_scr_pop _alias = (engine_api_scr_pop)0x507250;
#define LOAD_SCR_PUSH(_alias) engine_api_scr_push _alias = (engine_api_scr_push)0x507230;

DECLARE_CORE_API_EX(CoreExecApiGetMemory,
{
	LOAD_SCR_POP(scrPop)
	LOAD_SCR_PUSH(scrPush)
	int *addr = (int *)scrPop();

	scrPush(*addr);
    CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiGetMemoryWord,
{
	LOAD_SCR_POP(scrPop)
	short *addr = (short *)scrPop();

LOAD_SCR_PUSH(scrPush)
	scrPush(*addr);
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiGetMemoryByte,
{
	LOAD_SCR_POP(scrPop)
	char *addr = (char *)scrPop();

LOAD_SCR_PUSH(scrPush)
	scrPush(*addr);
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiSetMemory,
{
	LOAD_SCR_POP(scrPop)
	int *addr = (int *)scrPop();
	int value = scrPop();

	*addr = value;
    CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiSetMemoryWord,
{
	LOAD_SCR_POP(scrPop)
	short *addr = (short *)scrPop();
	short value = (short)scrPop();

	*addr = value;
    CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiSetMemoryByte,
{
	LOAD_SCR_POP(scrPop)
	char *addr = (char *)scrPop();
	char value = (char)scrPop();

	*addr = value;
    CORE_API_RETURN
})

//DECLARE_CORE_API_EX(CoreExecApiSetMemoryS,
//{
//	CoreExecApiSetMemory();	//equalvalent?
//CORE_API_RETURN
//})

DECLARE_CORE_API_EX(CoreExecApiSetMemorySWord,
{
	LOAD_SCR_POP(scrPop)
	int *addr = (int *)scrPop();
	short value = (short)scrPop();

	*addr = (int)value;
    CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiSetMemorySByte,
{
	LOAD_SCR_POP(scrPop)
	int *addr = (int *)scrPop();
	char value = (char)scrPop();

	*addr = (int)value;
    CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiSetMemoryZ,
{
	LOAD_SCR_POP(scrPop)
	uint32_t *addr = (uint32_t *)scrPop();
	uint32_t value = (uint32_t)scrPop();

	*addr = value;
    CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiSetMemoryZWord,
{
	LOAD_SCR_POP(scrPop)
	uint32_t *addr = (uint32_t *)scrPop();
	uint16_t value = (uint16_t)scrPop();

	*addr = value;
    CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiSetMemoryZByte,
{
	LOAD_SCR_POP(scrPop)
	uint32_t *addr = (uint32_t *)scrPop();
	uint8_t value = (uint8_t)scrPop();

	*addr = value;
    CORE_API_RETURN
})

#define SCR_DATA_INDEX 11
#define SCR_FUNC_INDEX 2

typedef int(*noxscript_nextCmd)(int **);

    DECLARE_CORE_API_EX(CoreExecComputedGoto,
    {
        LOAD_SCR_POP(scrPop)
        int label = scrPop();
    int *p = (int *)&pArgs;
    int index = *(p + SCR_FUNC_INDEX);
    nox_script_xxx_t *scrtable = (nox_script_xxx_t *)(*(int *)0x75ae28);

    p[SCR_DATA_INDEX] = (int)scrtable[index].data + (label * 4);
    CORE_API_RETURN
    })

DECLARE_CORE_API_EX_UNSAFE(CoreExecJumpIf,
{
    volatile int cmpRes = *(int *)CORE_REG_FLAG;
    LOAD_SCR_POP(scrPop)
noxscript_nextCmd next = (noxscript_nextCmd)(0x507270);
    int index = *(int *)(&pArgs+SCR_FUNC_INDEX);
int label = next((&pArgs)+ SCR_DATA_INDEX);

if (cmpRes)
{
    volatile nox_script_xxx_t *scrtable = (nox_script_xxx_t *)(*(int *)0x75ae28);
    volatile int *p = (int *)&pArgs;

    p[SCR_DATA_INDEX] = (int)(scrtable[index].data) + (label * 4);
}
    CORE_API_RETURN
})

DECLARE_CORE_API_EX_UNSAFE(CoreExecJumpIfNot,
{ 
volatile int *p = (int *)&pArgs;
    LOAD_SCR_POP(scrPop)
    volatile int *cmpRes = (int *)CORE_REG_FLAG;
noxscript_nextCmd next = (noxscript_nextCmd)(0x507270);
volatile int label = next((&pArgs) + SCR_DATA_INDEX);

if (*cmpRes == 0)
{
    volatile int index = *(int *)(&pArgs + SCR_FUNC_INDEX);
    volatile nox_script_xxx_t *scrtable = (nox_script_xxx_t *)(*(int *)0x75ae28);

    p[SCR_DATA_INDEX] = (int)(scrtable[index].data) + (label * 4);
}
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiSignedMod,
{
    LOAD_SCR_POP(scrPop)
    int *addr = (int *)scrPop();
int value = scrPop();

*addr %= value;
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiUnsignedMod,
{
    LOAD_SCR_POP(scrPop)
    uint32_t *addr = (uint32_t *)scrPop();
uint32_t value = (uint32_t)scrPop();

*addr %= value;
    CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiSignedDiv,
{
    LOAD_SCR_POP(scrPop)
    int *addr = (int *)scrPop();
int value = scrPop();

*addr /= value;
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiUnsignedDiv,
{
    LOAD_SCR_POP(scrPop)
        uint32_t *addr = (uint32_t *)scrPop();
    uint32_t value = (uint32_t)scrPop();

    *addr /= value;
    CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiArithmeticAddition,
{
    LOAD_SCR_POP(scrPop)
    int *addr = (int *)scrPop();
int value = scrPop();

*addr += value;
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiArithmeticSubtract,
{
    LOAD_SCR_POP(scrPop)
    int *addr = (int *)scrPop();
int value = scrPop();

*addr -= value;
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiArithmeticMultiply,
{
    LOAD_SCR_POP(scrPop)
    int *addr = (int *)scrPop();
int value = scrPop();

*addr *= value;
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiArithmeticShiftLeft,
{
    LOAD_SCR_POP(scrPop)
    int *addr = (int *)scrPop();
int value = scrPop();

*addr <<= value;
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiArithmeticShiftRight,
{
    LOAD_SCR_POP(scrPop)
    int *addr = (int *)scrPop();
int value = scrPop();

*addr >>= value;
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiArithmeticXOR,
{
    LOAD_SCR_POP(scrPop)
    int *addr = (int *)scrPop();
int value = scrPop();

*addr ^= value;
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiArithmeticAND,
{
    LOAD_SCR_POP(scrPop)
    int *addr = (int *)scrPop();
int value = scrPop();

*addr &= value;
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiArithmeticOR,
{
    LOAD_SCR_POP(scrPop)
    int *addr = (int *)scrPop();
int value = scrPop();

*addr |= value;
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiToFloat,
{
	LOAD_SCR_POP(scrPop)
	float *addr = (float *)scrPop();
	int value = scrPop();

	*addr = (float)value;
    CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiFloatToInt,
{
	LOAD_SCR_POP(scrPop)
	int *addr = (int *)scrPop();
uint32_t value = (uint32_t)scrPop();
float fvalue = *(float *)&value;

	*addr = (int)fvalue;
    CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiLoadCompareResult,
{
    LOAD_SCR_POP(scrPop)
    int *addr = (int *)scrPop();
int *pRes = (int *)CORE_REG_FLAG;

*addr = (*pRes) & 1;
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiCompareEqual,
	{
		LOAD_SCR_POP(scrPop)
		int a = scrPop();
	int b = scrPop();
	int *pRes = (int *)CORE_REG_FLAG;
	*pRes = b == a;
	CORE_API_RETURN
	})

DECLARE_CORE_API_EX(CoreExecApiCompareNotEqual,
{
	LOAD_SCR_POP(scrPop)
	int a = scrPop();
	int b = scrPop();
	int *pRes = (int *)CORE_REG_FLAG;
	*pRes = b != a;
	CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiCompareFloatLess,
{
	LOAD_SCR_POP(scrPop)
	int ia = scrPop();
	int ib = scrPop();
	float a = *((float *)&ia);
	float b = *((float *)&ib);
	int *pRes = (int *)CORE_REG_FLAG;
	*pRes = b < a;
	CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiCompareFloatLessEqual,
{
	LOAD_SCR_POP(scrPop)
	int ia = scrPop();
int ib = scrPop();
float a = *((float *)&ia);
float b = *((float *)&ib);
	int *pRes = (int *)CORE_REG_FLAG;
	*pRes = b <= a;
	CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiCompareLess,
{	//a 와 b 을 less 비교하여, 결과를 flag 레지스터에 대입합니다
	LOAD_SCR_POP(scrPop)
	int a = scrPop();
int b = scrPop();

int *pRes = (int *)CORE_REG_FLAG;
*pRes = b < a;
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiCompareLessEqual,
	{
		LOAD_SCR_POP(scrPop)
		int a = scrPop();
	int b = scrPop();
	int *pRes = (int *)CORE_REG_FLAG;
	*pRes = b <= a;
	CORE_API_RETURN
	})

DECLARE_CORE_API_EX(CoreExecApiUnsignedCompareB,
{
	LOAD_SCR_POP(scrPop)
	uint32_t a = (uint32_t)scrPop();
	uint32_t b = (uint32_t)scrPop();
	int *pRes = (int *)CORE_REG_FLAG;
	*pRes = b < a;
    CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiUnsignedCompareEqualB,
{
	LOAD_SCR_POP(scrPop)
	uint32_t a = (uint32_t)scrPop();
	uint32_t b = (uint32_t)scrPop();
	int *pRes = (int *)CORE_REG_FLAG;
	*pRes = b <= a;
    CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiCompareBitAnd,
	{
        int a = *(int *)CORE_REG_EAX;
	int *pRes = (int *)CORE_REG_FLAG;
    *pRes = (a != 0) & 1;
	CORE_API_RETURN
	})

DECLARE_CORE_API_EX(CoreExecApiCompareBitAndNot,
	{
        int a = *(int *)CORE_REG_EAX;
	int *pRes = (int *)CORE_REG_FLAG;
    *pRes = (a == 0) & 1;
	CORE_API_RETURN
	})

DECLARE_CORE_API_EX(CoreExecApiUserPtrCall,
{
	LOAD_SCR_POP(scrPop)
	int *pCaller = (int *)0x979720;
	int *pTrigger = (int *)0x979724;
	int n = scrPop();

	engine_api_scr_invoke scrCall = (engine_api_scr_invoke)0x507310;
	scrCall(n, *pCaller, *pTrigger);
    CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiLoadGlobalPtr,
{
	LOAD_SCR_POP(scrPop)
	int off = scrPop();
int *global = (int *)CORE_REG_GLOBAL;

LOAD_SCR_PUSH(scrPush)
scrPush(*global + off);
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiLoadGlobalValue,
{
    LOAD_SCR_POP(scrPop)
    int off = scrPop();
int *global = (int *)CORE_REG_GLOBAL;

LOAD_SCR_PUSH(scrPush)
scrPush(*(int *)(*global + off));
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiZeroFiller,
{
    LOAD_SCR_POP(scrPop)
    int start = scrPop();
int end = scrPop();
char **ebp = (char **)CORE_REG_EBP;
char *local = *ebp;

for (; start <= end - 4; start += 4)
{
    int *p = (int *)(local + start);

    *p = 0;
}
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecStoreLocal,
{
    LOAD_SCR_POP(scrPop)
    int off = scrPop();
int value = scrPop();
char **ebp = (char **)CORE_REG_EBP;
int *p = (int *)(*ebp + off);

*p = value;
    CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiEudPush,
{
	LOAD_SCR_POP(scrPop)
	int value = scrPop();
	int **esp = (int **)CORE_REG_ESP;
    
	////내부적으로 이렇게 동작합니다: esp-4 한 다음에, esp 주소에 값을 대입해줌
    *esp -= 1;
	**esp = value;
    CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiEudPop,
{
	LOAD_SCR_POP(scrPop)
	int *addr = (int *)scrPop();
	int **esp = (int **)CORE_REG_ESP;

	//esp 주소에서 값을 가져온 다음에, esp+4 함
	*addr = **esp;
    *esp += 1;
    CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiCopyMemory,
{
	LOAD_SCR_POP(scrPop)
	int count = scrPop();
	int *dest = (int *)scrPop();
	int *src = (int *)scrPop();

	while (--count >= 0)
		dest[count] = src[count];
    CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiFloatAdd,
{
    LOAD_SCR_POP(scrPop)
    float *addr = (float *)scrPop();
int value = scrPop();

*addr += *((float *)&value);
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiFloatSub,
{
    LOAD_SCR_POP(scrPop)
    float *addr = (float *)scrPop();
int value = scrPop();

*addr -= *((float *)&value);
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiFloatMul,
{
    LOAD_SCR_POP(scrPop)
    float *addr = (float *)scrPop();
int value = scrPop();

*addr *= *((float *)&value);
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiFloatDiv,
{
    LOAD_SCR_POP(scrPop)
    float *addr = (float *)scrPop();
int value = scrPop();

*addr /= *((float *)&value);
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiEBP,
{
    LOAD_SCR_PUSH(scrPush)
    int *ebp = (int *)CORE_REG_EBP;
    scrPush(*ebp);
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiEAX,
{
    LOAD_SCR_PUSH(scrPush)
    int *eax = (int *)CORE_REG_EAX;
scrPush(*eax);
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiECX,
{
    LOAD_SCR_PUSH(scrPush)
    int *ecx = (int *)CORE_REG_ECX;
scrPush(*ecx);
CORE_API_RETURN
})

DECLARE_CORE_API_EX(CoreExecApiLeave,
{
	///이 코드를 생성해야함: mov esp, ebp; pop ebp
	int **ebp = (int **)CORE_REG_EBP;
	int **esp = (int **)CORE_REG_ESP;

	*esp = *ebp;	//ebp이 가진 값을 esp 주소에 대입
	//그러면, 
	*ebp = (int *)**esp;
    *esp += 1;
    CORE_API_RETURN
})

	typedef int (CORE_INVOKE *engine_api_invoke_builtin)(int, int);
DECLARE_CORE_API_EX(CoreExecApiCallBuiltin,
	{	//esp 에 구조체 포인터 들어있음.

	int **esp = (int **)CORE_REG_ESP;
	CallBuiltinParam *param = (CallBuiltinParam *)**esp;
	engine_api_invoke_builtin callBuiltin = (engine_api_invoke_builtin)0x508b70;

    LOAD_SCR_PUSH(scrPush);
    for (int i = 0 ; i < param->argCount ; ++i)
        scrPush(param->args[i]);

	callBuiltin(param->caller, param->targetFunctionId);
	if (param->retValue)
	{
		LOAD_SCR_POP(scrPop);
		param->retValue = scrPop();
	}

CORE_API_RETURN
	})

#define CODE_MAX_LENGTH (size_t)1280
static size_t skipPadding(const uint8_t *codeStream, size_t length)
{
    if (length >= CODE_MAX_LENGTH)
        return 0;

    size_t spot = length;

    while ((--spot) != -1)
    {
        if (codeStream[spot] != 0xcc)
        {
            size_t padcount = sizeof(int) - ((++spot) % sizeof(int));

            return spot + padcount;
        }
    }
    return 0;
}

static void insertCode(const char *id, uint32_t beginFn, uint32_t endFn)
{
	size_t pick = s_core->tableCount++;
	
    if (pick >= CORE_API_TABLE_SIZE)
    {
        printf("assert %s%d", __FILE__, __LINE__);
        return;
    }

	CoreExecImpl *impl = &s_core->table[pick];

	impl->codePtr = (uint8_t *)beginFn;
	impl->length = skipPadding(impl->codePtr, endFn - beginFn);
	impl->keyLiteral = id;
	s_core->fieldLength += impl->length;
}

static void computeCodeOffset()
{
	size_t count = s_core->tableCount;
	uint32_t off = 0;

	for (size_t u = 0; u < count; ++u)
	{
		s_core->table[u].codeOffset = off;
		off += s_core->table[u].length;
	}
}

lpCoreExecApi ReleaseCoreApi()
{
	static CoreExecApi core;

    printf("%p\n", CoreExecJumpIf);
    printf("%p", CoreExecJumpIfNot);
	if (!s_core)
	{
		s_core = &core;
#define FETCH_CORE_API(fn) (uint32_t)fn, (uint32_t)fn##Dummy
		insertCode("BUILTINS_GetMemory", FETCH_CORE_API(CoreExecApiGetMemory));
		insertCode("BUILTINS_GetMemoryWord", FETCH_CORE_API(CoreExecApiGetMemoryWord));
		insertCode("BUILTINS_GetMemoryByte", FETCH_CORE_API(CoreExecApiGetMemoryByte));
		insertCode("BUILTINS_SetMemory", FETCH_CORE_API(CoreExecApiSetMemory));
		insertCode("BUILTINS_SetMemoryWord", FETCH_CORE_API(CoreExecApiSetMemoryWord));
		insertCode("BUILTINS_SetMemoryByte", FETCH_CORE_API(CoreExecApiSetMemoryByte));
		insertCode("BUILTINS_SetMemorySWord", FETCH_CORE_API(CoreExecApiSetMemorySWord));
		insertCode("BUILTINS_SetMemorySByte", FETCH_CORE_API(CoreExecApiSetMemorySByte));
		insertCode("BUILTINS_SetMemoryZWord", FETCH_CORE_API(CoreExecApiSetMemoryZWord));
		insertCode("BUILTINS_SetMemoryZByte", FETCH_CORE_API(CoreExecApiSetMemoryZByte));

        insertCode("BUILTINS_ComputedGoto", FETCH_CORE_API(CoreExecComputedGoto));
        insertCode("BUILTINS_JumpIf", FETCH_CORE_API(CoreExecJumpIf));
        insertCode("BUILTINS_JumpIfNot", FETCH_CORE_API(CoreExecJumpIfNot));

        insertCode("BUILTINS_SignedMod", FETCH_CORE_API(CoreExecApiSignedMod));
		insertCode("BUILTINS_UnsignedMod", FETCH_CORE_API(CoreExecApiUnsignedMod));
        insertCode("BUILTINS_SignedDiv", FETCH_CORE_API(CoreExecApiSignedDiv));
		insertCode("BUILTINS_UnsignedDiv", FETCH_CORE_API(CoreExecApiUnsignedDiv));
        insertCode("BUILTINS_Addition", FETCH_CORE_API(CoreExecApiArithmeticAddition));
        insertCode("BUILTINS_Subtract", FETCH_CORE_API(CoreExecApiArithmeticSubtract));
        insertCode("BUILTINS_Multiply", FETCH_CORE_API(CoreExecApiArithmeticMultiply));
        insertCode("BUILTINS_ShiftLeft", FETCH_CORE_API(CoreExecApiArithmeticShiftLeft));
        insertCode("BUILTINS_ShiftRight", FETCH_CORE_API(CoreExecApiArithmeticShiftRight));
        insertCode("BUILTINS_XOR", FETCH_CORE_API(CoreExecApiArithmeticXOR));
        insertCode("BUILTINS_AND", FETCH_CORE_API(CoreExecApiArithmeticAND));
        insertCode("BUILTINS_OR", FETCH_CORE_API(CoreExecApiArithmeticOR));
		insertCode("BUILTINS_IntToFloat", FETCH_CORE_API(CoreExecApiToFloat));
		insertCode("BUILTINS_FloatToInt", FETCH_CORE_API(CoreExecApiFloatToInt));
        insertCode("BUILTINS_LoadCompareResult", FETCH_CORE_API(CoreExecApiLoadCompareResult));
		insertCode("BUILTINS_CompareEqual", FETCH_CORE_API(CoreExecApiCompareEqual));
		insertCode("BUILTINS_CompareNotEqual", FETCH_CORE_API(CoreExecApiCompareNotEqual));
		insertCode("BUILTINS_CompareFloatLess", FETCH_CORE_API(CoreExecApiCompareFloatLess));
		insertCode("BUILTINS_CompareFloatLessEqual", FETCH_CORE_API(CoreExecApiCompareFloatLessEqual));
		insertCode("BUILTINS_CompareLess", FETCH_CORE_API(CoreExecApiCompareLess));
		insertCode("BUILTINS_CompareLessEqual", FETCH_CORE_API(CoreExecApiCompareLessEqual));
		insertCode("BUILTINS_CompareB", FETCH_CORE_API(CoreExecApiUnsignedCompareB));
		insertCode("BUILTINS_CompareEqualB", FETCH_CORE_API(CoreExecApiUnsignedCompareEqualB));
		insertCode("BUILTINS_TEST", FETCH_CORE_API(CoreExecApiCompareBitAnd));
		insertCode("BUILTINS_TEST_NOT", FETCH_CORE_API(CoreExecApiCompareBitAndNot));
		insertCode("BUILTINS_UserCall", FETCH_CORE_API(CoreExecApiUserPtrCall));
		insertCode("BUILTINS_LoadGlobalAddress", FETCH_CORE_API(CoreExecApiLoadGlobalPtr));
        insertCode("BUILTINS_LoadGlobalValue", FETCH_CORE_API(CoreExecApiLoadGlobalValue));
        insertCode("BUILTINS_ZeroFiller", FETCH_CORE_API(CoreExecApiZeroFiller));
        insertCode("BUILTINS_StoreLocal", FETCH_CORE_API(CoreExecStoreLocal));
		insertCode("BUILTINS_EUDPush", FETCH_CORE_API(CoreExecApiEudPush));
		insertCode("BUILTINS_EUDPop", FETCH_CORE_API(CoreExecApiEudPop));
		insertCode("BUILTINS_Memcopy", FETCH_CORE_API(CoreExecApiCopyMemory));
        insertCode("BUILTINS_FloatAdd", FETCH_CORE_API(CoreExecApiFloatAdd));
        insertCode("BUILTINS_FloatSub", FETCH_CORE_API(CoreExecApiFloatSub));
        insertCode("BUILTINS_FloatMul", FETCH_CORE_API(CoreExecApiFloatMul));
        insertCode("BUILTINS_FloatDiv", FETCH_CORE_API(CoreExecApiFloatDiv));
        insertCode("BUILTINS_EBP", FETCH_CORE_API(CoreExecApiEBP));
        insertCode("BUILTINS_EAX", FETCH_CORE_API(CoreExecApiEAX));
        insertCode("BUILTINS_ECX", FETCH_CORE_API(CoreExecApiECX));
		insertCode("BUILTINS_FunctionReturn", FETCH_CORE_API(CoreExecApiLeave));
		insertCode("BUILTINS_CallBuitin", FETCH_CORE_API(CoreExecApiCallBuiltin));
#undef FETCH_CORE_API
		computeCodeOffset();
	}
	return s_core;
}
