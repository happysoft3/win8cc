
#ifndef CORE_GEN_H__
#define CORE_GEN_H__

#include <string>
#include <map>
#include <fstream>

struct CoreExecApi;
struct CoreExecImpl;
class GenOption;

class CoreGen
{
private:
	CoreExecApi *m_core;
	std::map<uint32_t, CoreExecImpl *> m_fnMap;
	int m_baseFnOffset;

public:
	CoreGen();
	~CoreGen();

private:
	void sortCoreApi();
	void writeCodeStreamImpl(std::ofstream &file);
    void writeApiTableImpl(std::ofstream &file);

public:
	void LoadCore(const GenOption &opt);
	void WriteAsINC(std::ofstream &file);
	void WriteCodestream(std::ofstream &file);
    void WriteLoaderScript(std::ofstream &file);
    void WriteAPITable(std::ofstream &file);

	using write_task_ty = void(CoreGen::*)(std::ofstream &);
	void WriteBinary(write_task_ty task, const std::string &url = {});
};

#endif
