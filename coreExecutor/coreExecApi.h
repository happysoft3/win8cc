
#ifndef CORE_EXEC_API_H__
#define CORE_EXEC_API_H__

#include <stdint.h>

typedef struct CoreExecImpl
{
	uint32_t codeOffset;
	uint8_t *codePtr;
	size_t length;
	const char *keyLiteral;
} CoreExecImpl, *pCoreExecImpl;

#define CORE_API_TABLE_SIZE 128
typedef struct CoreExecApi
{
	size_t fieldLength;
	CoreExecImpl table[CORE_API_TABLE_SIZE];
	size_t tableCount;
} CoreExecApi, *lpCoreExecApi;

lpCoreExecApi ReleaseCoreApi();

#endif

