
#ifndef GEN_OPTION_H__
#define GEN_OPTION_H__

#include "systemFileParser\iniFileMan.h"

class GenOption : public IniFileMan
{
private:
    std::string m_incPath;
    std::string m_binPath;
    std::string m_loaderScrPath;
    std::string m_apiTablePath;
    int m_tablebaseOffset;

public:
    GenOption();
    ~GenOption() override;

private:
    int determineTableOffset();
    void loadParams();

public:
    bool ReadIni(const std::string &inifile) override;
    const char *const IncPath() const
    {
        return m_incPath.c_str();
    }
    const char *const BinaryPath() const
    {
        return m_binPath.c_str();
    }
    const char *const ScriptPath() const
    {
        return m_loaderScrPath.c_str();
    }
    const char *const TablePath() const
    {
        return m_apiTablePath.c_str();
    }
    int TableOffset() const
    {
        return m_tablebaseOffset;
    }
};

#endif

