
#ifndef BUILTINS_H__
#define BUILTINS_H__

#define BUILTIN_MAX_ARG 8

typedef struct _CallBuiltinParam
{
	int retValue;
	int targetFunctionId;
	int argCount;
	int caller;
	int args[BUILTIN_MAX_ARG];
} CallBuiltinParam;

void AudioEvent(char *audio, int locationId);
int CreateObject(char *unitName, int locationId);
void BuiltinNoArg(int targetID);
int BuiltinNoArgReturn(int targetID);
void BuiltinSingleArg(int arg, int targetID);
int BuiltinSingleArgReturn(int arg, int targetID);
float BuiltinSingleArgReturnFloat(int arg, int targetID);
void BuiltinTwoArg(int arg1, int arg2, int targetID);
int BuiltinTwoArgReturn(int arg1, int arg2, int targetID);
void BuiltinThreeArg(int arg1, int arg2, int arg3, int targetID);
void BuiltinsInitialize();

#define NoxWall(x, y)		BuiltinTwoArgReturn(x, y, 0)
#define NoxWallOpen(wall)	BuiltinSingleArg(wall, 1)
#define NoxWallGroupOpen(wallGroup)	BuiltinSingleArg(wallGroup, 2)
#define NoxWallClose(wall)	BuiltinSingleArg(wall, 3)
#define NoxWallGroupClose(wallGroup)	BuiltinSingleArg(wallGroup, 4)
#define NoxWallToggle(wall)	BuiltinSingleArg(wall, 5)
#define NoxWallGroupToggle(wallGroup)	BuiltinSingleArg(wallGroup, 6)
#define NoxWallBreak(wall)	BuiltinSingleArg(wall, 7)
#define NoxWallGroupBreak(wallGroup)	BuiltinSingleArg(wallGroup, 8)
#define NoxMove(unit, destination)		BuiltinTwoArg(unit, destination, 11)
#define NoxGroupMove(objectGroup, destination)	BuiltinTwoArg(unit, destination, 12)
#define NoxLookAtDirection(unit, direction)	BuiltinTwoArg(unit, direction, 13)
#define NoxGroupLookAtDirection(objectGroup, direction)	BuiltinTwoArg(objectGroup, direction, 14)
#define NoxObjectOn(unit)	BuiltinSingleArg(unit, 15)
#define NoxObjectGroupOn(group)	BuiltinSingleArg(group, 16)
#define NoxWaypointOn(waypoint)	BuiltinSingleArg(waypoint, 17)
#define NoxWaypointGroupOn(waypointGroup)	BuiltinSingleArg(waypointGroup, 18)
#define NoxObjectOff(unit)	BuiltinSingleArg(unit, 19)
#define NoxObjectGroupOff(unit)	BuiltinSingleArg(unit, 20)
#define NoxWaypointOff(waypoint)	BuiltinSingleArg(waypoint, 21)
#define NoxWaypointGroupOff(waypointGroup)	BuiltinSingleArg(waypointGroup, 22)
#define NoxObjectToggle(unit)	BuiltinSingleArg(unit, 23)
#define NoxObjectGroupToggle(group)	BuiltinSingleArg(group, 24)
#define NoxWaypointToggle(waypoint)	BuiltinSingleArg(waypoint, 25)
#define NoxWaypointGroupToggle(waypointGroup)	BuiltinSingleArg(waypointGroup, 26)
#define NoxDeleteObject(unit)	BuiltinSingleArg(unit, 27)
#define NoxDeleteObjectGroup(group)	BuiltinSingleArg(group, 28)
#define NoxWander(unit)	BuiltinSingleArg(unit, 29)
#define NoxGroupWander(group)	BuiltinSingleArg(group, 30)
#define NoxGoBackHome(unit)	BuiltinSingleArg(unit, 33)

void BuiltinsSingleCharArg(char *s, int targetID);

#define NoxPrintMessage(msg) BuiltinsSingleCharArg(msg, 35)
#define NoxPrintToAll(msg) BuiltinsSingleCharArg(msg, 36)

typedef void(*timer_callback)(int);

int BuiltinRunTimer(int delay, int arg, timer_callback cb, int target);

#define NoxFrameTimer(frames, arg, callBack)	BuiltinRunTimer(frames, arg, callBack, 47)
#define NoxSecondTimer(seconds, arg, callBack)	BuiltinRunTimer(seconds, arg, callBack, 46)

void BuiltinSayUnit(int unit, const char *msg, int frames, int seconds);

#define NoxSay(unit, message) BuiltinSayUnit(unit, message, 0, 0)
#define NoxSayTimer(unit, message, frames) BuiltinSayUnit(unit, message, frames, 0)
#define NoxSaySeconds(unit, message, seconds) BuiltinSayUnit(unit, message, 0, seconds)
#define NoxUnlockDoor(door)	BuiltinSingleArg(door, 39)
#define NoxLockDoor(door)	BuiltinSingleArg(door, 40)
#define NoxIsObjectOn(unit) BuiltinSingleArgReturn(unit, 41)
#define NoxIsWaypointOn(waypoint) BuiltinSingleArgReturn(waypoint, 42)
#define NoxIsLocked(door) BuiltinSingleArgReturn(door, 43)

float NoxRandomFloat(float min, float max);

#define NoxRandomInteger(min, max) BuiltinTwoArgReturn(min, max, 45)

char *IntToString(int iValue);
char *FloatToString(float fValue);

typedef enum
{
	DAMAGE_BLADE,
	DAMAGE_FLAME,
	DAMAGE_CRUSH,
	DAMAGE_IMPALE,
	DAMAGE_DRAIN = 4,
	DAMAGE_POISON = 5,
	DAMAGE_DISPEL_UNDEAD = 6,
	DAMAGE_EXPLOSION = 7,
	DAMAGE_BITE = 8,
	DAMAGE_ELECTRIC = 9,
	DAMAGE_CLAW = 10,
	DAMAGE_IMPACT = 11,
	DAMAGE_LAVA = 12,
	DAMAGE_DEATH_MAGIC = 13,
	DAMAGE_PLASMA = 14,
	DAMAGE_MANA_BOMB = 15,
	DAMAGE_ZAP_RAY = 16,
	DAMAGE_AIRBORNE_ELECTRIC = 17,
} DamageType;

void NoxDamage(int target, int source, int amount, DamageType type);

int CharPointerToNoxString(char *ptr);

#define NoxAwardSpell(unit, spellName) BuiltinTwoArgReturn(unit, CharPointerToNoxString(spellName), 55)
#define NoxGroupAwardSpell(group, spellName) BuiltinTwoArgReturn(group, CharPointerToNoxString(spellName), 56)

typedef enum
{
	ENCHANT_INVISIBLE,
	ENCHANT_MOONGLOW,
	ENCHANT_BLINDED,
	ENCHANT_CONFUSED,
	ENCHANT_SLOWED,
	ENCHANT_HELD,
	ENCHANT_DETECTING,
	ENCHANT_ETHEREAL,
	ENCHANT_RUN,
	ENCHANT_HASTED,
	ENCHANT_VILLAIN,
	ENCHANT_AFRAID,
	ENCHANT_BURNING,
	ENCHANT_VAMPIRISM,
	ENCHANT_ANCHORED,
	ENCHANT_LIGHT,
	ENCHANT_DEATH,
	ENCHANT_PROTECT_FROM_FIRE,
	ENCHANT_PROTECT_FROM_POISON,
	ENCHANT_PROTECT_FROM_MAGIC,
	ENCHANT_PROTECT_FROM_ELECTRICITY,
	ENCHANT_INFRAVISION,
	ENCHANT_SHOCK,
	ENCHANT_INVULNERABLE,
	ENCHANT_TELEKINESIS,
	ENCHANT_FREEZE,
	ENCHANT_SHIELD,
	ENCHANT_REFLECTIVE_SHIELD,
	ENCHANT_CHARMING,
	ENCHANT_ANTI_MAGIC,
	ENCHANT_CROWN,
	ENCHANT_SNEAK
} Enchantment;

void NoxEnchant(int unit, Enchantment enchant, float duration);
int NoxHasEnchant(int unit, Enchantment enchant);
void NoxEnchantOff(int unit, Enchantment enchant);
#define NoxGetHost()	BuiltinNoArgReturn(59)
#define NoxGetObject(objectName) BuiltinSingleArgReturn(CharPointerToNoxString(objectName), 60)
#define NoxGetObjectX(unit)		BuiltinSingleArgReturnFloat(unit, 61)
#define NoxGetWaypointX(waypoint)	BuiltinSingleArgReturnFloat(waypoint, 62)
#define NoxGetObjectY(unit)		BuiltinSingleArgReturnFloat(unit, 63)
#define NoxGetWaypointY(waypoint)	BuiltinSingleArgReturnFloat(waypoint, 64)
#define NoxGetObjectZ(unit)		BuiltinSingleArgReturnFloat(unit, 65)
#define NoxGetDirection(unit) 	BuiltinSingleArgReturn(unit, 66)

void BuiltinIntFloatFloat(int arg, float farg1, float farg2, int targetId);

#define NoxMoveObject(unit, xpos, ypos) BuiltinIntFloatFloat(unit, xpos, ypos, 67)
#define NoxMoveWaypoint(waypoint, xpos, ypos) BuiltinIntFloatFloat(waypoint, xpos, ypos, 68)

void BuiltinIntFloat(int arg, int farg, int targetId);

#define NoxRaiseObject(unit, zpos)	BuiltinIntFloat(unit, zpos, 69)
#define NoxLookWithAngle(unit, angle)	BuiltinTwoArg(unit, angle, 70)
#define NoxPushObjectTo(unit, vectorX, vectorY)	BuiltinIntFloatFloat(unit, vectorX, vectorY, 71)
void NoxPushObject(int unit, float magnitude, float x, float y);

#define NoxGetLastItem(unit) 	BuiltinSingleArgReturn(unit, 73)
#define NoxGetPreviousItem(unit) 	BuiltinSingleArgReturn(unit, 74)
#define NoxHasItem(holder, item)	BuiltinTwoArgReturn(holder, item, 75)
#define NoxGetHolder(item) 	BuiltinSingleArgReturn(item, 76)
#define NoxPickup(unit, item)	BuiltinTwoArgReturn(unit, item, 77)
#define NoxDrop(unit, item)	BuiltinTwoArgReturn(unit, item, 78)
#define NoxHasClass(unit, className)	BuiltinTwoArgReturn(unit, CharPointerToNoxString(className), 79)
#define NoxCurrentHealth(unit) 	BuiltinSingleArgReturn(unit, 83)
#define NoxMaxHealth(unit) 	BuiltinSingleArgReturn(unit, 84)
#define NoxRestoreHealth(unit, amount)		BuiltinTwoArg(unit, angle, 85)
float NoxDistance(float x1, float y1, float x2, float y2);
#define NoxIsVisible(unit1, unit2)	BuiltinTwoArgReturn(unit1, unit2, 87)
#define NoxGetCharacterData(index) 	BuiltinSingleArgReturn(index, 95)
#define NoxLookAtObject(unit, target)	BuiltinTwoArg(unit, target, 96)
#define NoxWalkTo(unit, xProfile, yProfile)	BuiltinIntFloatFloat(unit, xProfile, yProfile, 97)
#define NoxCancelTimer(timerId) 	BuiltinSingleArgReturn(timerId, 99)
void NoxPlayFX(const char *fxName, float x1, float y1, float x2, float y2);
#define NoxSetOwner(owner, target)		BuiltinTwoArg(owner, target, 101)
#define NoxIsOwnedBy(owner, target)	BuiltinTwoArgReturn(owner, target, 105)
#define NoxClearOwner(unit)	BuiltinSingleArg(unit, 109)
#define NoxGetWaypoint(waypoint) BuiltinSingleArgReturn(CharPointerToNoxString(waypoint), 110)
#define NoxGetWaypointGroup(groupName) BuiltinSingleArgReturn(CharPointerToNoxString(groupName), 111)
#define NoxGetObjectGroup(groupName) BuiltinSingleArgReturn(CharPointerToNoxString(groupName), 112)
#define NoxGetWallGroup(groupName) BuiltinSingleArgReturn(CharPointerToNoxString(groupName), 113)
#define NoxDestroyChat(unit)	BuiltinSingleArg(unit, 117)
#define NoxDestroyEveryChat()	BuiltinNoArg(118)
#define NoxSetQuestStatus(status, name)	BuiltinTwoArg(status, CharPointerToNoxString(name), 119)
#define NoxSetQuestStatusFloat(fStatus, name)	BuiltinTwoArg(*(int *)&fStatus, CharPointerToNoxString(name), 120)
#define NoxGetQuestStatus(name) 	BuiltinSingleArgReturn(CharPointerToNoxString(name), 121)
float NoxGetQuestStatusFloat(const char *name);
#define NoxResetQuestStatus(name)	BuiltinSingleArg(CharPointerToNoxString(name), 123)
#define NoxIsTrigger(unit) 	BuiltinSingleArgReturn(unit, 124)
#define NoxIsCaller(unit) 	BuiltinSingleArgReturn(unit, 125)

typedef void(*dialog_callback)(void);
typedef enum
{
	NORMAL,
	NEXT,
	YESNO,
	YESNO_NEXT
} DialogType;
void NoxSetupDialog(int unit, DialogType type, dialog_callback startFn, dialog_callback endFn);
#define NoxCancelDialog(unit)	BuiltinSingleArg(unit, 127)
#define NoxSetStoryPic(unit, imageName)	BuiltinTwoArg(unit, CharPointerToNoxString(imageName), 128)
#define NoxTellStory(audio, story)	BuiltinTwoArg(CharPointerToNoxString(audio), CharPointerToNoxString(imageName), 129)
#define NoxStartDialog(npc, user)	BuiltinTwoArg(npc, user, 130)
#define NoxCastSpellObjectObject(spell, source, target) BuiltinThreeArg(CharPointerToNoxString(spell), source, target, 131)
void NoxCastSpellObjectLocation(char *spell, int source, float x, float y);
void NoxCastSpellLocationObject(char *spell, float x, float y, int target);
void NoxCastSpellLocationLocation(char *spell, float x1, float y1, float x2, float y2);
#define NoxUnblind()	BuiltinNoArg(135)
#define NoxBlind()	BuiltinNoArg(136)
#define NoxDoWideScreen(value)	BuiltinSingleArg(value, 137)
#define NoxGetElevatorStatus(elevator) 	BuiltinSingleArgReturn(elevator, 138)
void NoxCreatureGuard(int id, float x1, float y1, float x2, float y2, float distance);
#define NoxCreatureHunt(unit)	BuiltinSingleArg(unit, 141)
#define NoxCreatureIdle(unit)	BuiltinSingleArg(unit, 143)
#define NoxCreatureFollow(unit, target)	BuiltinTwoArg(unit, target, 145)
#define NoxAggressionLevel(unit, level) BuiltinIntFloat(unit, level, 147)
#define NoxHitLocation(unit, xpos, ypos) BuiltinIntFloatFloat(unit, xpos, ypos, 149)
#define NoxHitFarLocation(unit, xpos, ypos) BuiltinIntFloatFloat(unit, xpos, ypos, 151)
#define NoxSetRoamFlag(unit, flags)	BuiltinTwoArg(unit, flags, 153)
#define NoxAttack(unit, target)	BuiltinTwoArg(unit, target, 155)
#define NoxJournalEntry(user, message, type) BuiltinThreeArg(user, CharPointerToNoxString(message), type, 157)
#define NoxJournalDelete(unit, message)	BuiltinTwoArg(unit, CharPointerToNoxString(message), 158)
#define NoxJournalModify(user, message, type) BuiltinThreeArg(user, CharPointerToNoxString(message), type, 159)
#define NoxRetreatLevel(unit, level) BuiltinIntFloat(unit, level, 160)
#define NoxResumeLevel(unit, level) BuiltinIntFloat(unit, level, 162)
#define NoxRunAway(unit, target, duration) BuiltinThreeArg(unit, target, duration, 164)
#define NoxPauseObject(unit, duration)	BuiltinTwoArg(unit, duration, 166)
#define NoxIsAttackedBy(unit1, unit2)	BuiltinTwoArgReturn(unit1, unit2, 168)
#define NoxGetGold(user) 	BuiltinSingleArgReturn(user, 169)
#define NoxChangeGold(user, amount)	BuiltinTwoArg(user, amount, 170)
#define NoxGetAnswer(unit) 	BuiltinSingleArgReturn(unit, 171)
#define NoxGiveExp(unit, xp) BuiltinIntFloat(unit, xp, 172)
#define NoxHasSubclass(unit, subclassName)	BuiltinTwoArgReturn(unit, CharPointerToNoxString(subclassName), 173)
#define NoxAutoSave()	BuiltinNoArg(174)
#define NoxPlayMusic(musicID, volume)	BuiltinTwoArg(musicID, volume, 175)
#define NoxStartupScreen(value)	BuiltinSingleArg(value, 176)
#define NoxIsTalking()	BuiltinNoArgReturn(177)
#define NoxGetTrigger()	BuiltinNoArgReturn(178)
#define NoxGetCaller()	BuiltinNoArgReturn(179)
#define NoxMakeFriendly(unit)	BuiltinSingleArg(unit, 180)
#define NoxMakeHostile(unit)	BuiltinSingleArg(unit, 181)
#define NoxBecomePet(unit)	BuiltinSingleArg(unit, 182)
#define NoxBecomeEnemy(unit)	BuiltinSingleArg(unit, 183)
#define NoxSetHalberd(level)	BuiltinSingleArg(level, 186)
#define NoxDeathScreen(scene)	BuiltinSingleArg(scene, 187)
#define NoxFrozen(unit, enable)	BuiltinTwoArg(unit, enable, 188)
#define NoxNoWallSound(enable)	BuiltinSingleArg(enable, 189)

typedef enum
{
	EnemySighted = 3,
	LookingForEnemy = 4,
	OnDeath = 5,
	OnFocusChanged = 6,
	OnHit = 7,
	OnRetreat = 8,
	Collision = 9,
	EnemyHeard = 10,
	EndOfWaypoint = 11,
	LostSightOfEnemy = 13
} UnitCallbackType;
typedef void(*UnitCallback)(void);
void NoxSetCallback(int mon, UnitCallbackType type, UnitCallback callback);

#define NoxDeleteObjectTimer(unit, delay)	BuiltinTwoArg(unit, delay, 191)
void NoxSetTrapSpells(int unit, const char *spell1, const char *spell2, const char *spell3);
#define NoxIsTrading()	BuiltinNoArgReturn(193)
#define NoxClearMessage(user)	BuiltinSingleArg(user, 194)
#define NoxSetShopkeeperText(unit, txt)	BuiltinTwoArg(unit, CharPointerToNoxString(txt), 195)
#define NoxIsSummoned(unit) 	BuiltinSingleArgReturn(unit, 197)
#define NoxZombieStayDown(zombie)	BuiltinSingleArg(zombie, 198)
#define NoxZombieWakeup(zombie)	BuiltinSingleArg(zombie, 200)
#define NoxPushMusic()	BuiltinNoArg(202)
#define NoxPopMusic()	BuiltinNoArg(203)
#define NoxMusicEvent()	BuiltinNoArg(204)
#define NoxIsGameball(ball) 	BuiltinSingleArgReturn(ball, 205)
#define NoxIsCrown(crown) 	BuiltinSingleArgReturn(crown, 206)
#define NoxEndGame(type)	BuiltinSingleArg(type, 207)
#define NoxImmediateBlind()	BuiltinNoArg(208)
#define NoxChangeScore(unit, amount)	BuiltinTwoArg(unit, amount, 209)
#define NoxGetScore(user) 	BuiltinSingleArgReturn(user, 210)

#endif

