
#include "stringUtil.h"

#define P_TO_STRING_BUFFER 64
int CharPointerToNoxString(char *ptr)
{
	static int arr[P_TO_STRING_BUFFER], cursor;

	if (cursor >= P_TO_STRING_BUFFER)
		cursor = 0;

	arr[cursor] = (int)ptr;
	return ((int)(&arr[cursor++]) - 0x97bb40) >> 2;
}
#undef P_TO_STRING_BUFFER
#define MAX_LENGTH 1024

void CopyString(const char* src, char* dest)
{
	int rep = -1;

	while (++rep < MAX_LENGTH)
	{
		if (!src[rep])
			break;

		dest[rep] = src[rep];
	}
}

void CopyWideString(const short *src, short *dest)
{
	int rep = -1;

	while (++rep < MAX_LENGTH)
	{
		if (!src[rep])
			break;

		dest[rep] = src[rep];
	}
}

void Utf8ToUnicodeString(const char *src, short *dest)
{
    int i = 0;

    while (i < MAX_LENGTH)
    {
        if (!src[i]) break;
        if (!(src[i] & 0x80))
        {
            dest[0] = (short)src[i];
            i += 1;
        }
        else if ((src[i] & 0xe0) == 0xc0)
        {
            dest[0] = (short)( ((src[i]&0x1f)<<6) | (src[i+1]&0x3f) );
            i+=2;
        }
        else if ((src[i] & 0xf0) == 0xe0)
        {
            dest[0]=(short) ( ((src[i]&0xf)<<12) | ((src[i+1]&0x3f) << 6) | (src[i+2]&0x3f) );
            i+=3;
        }
        dest++;
    }
    dest[0] = 0;
}

void UnicodeToUtf8String(const short* src, char* dest)
{
	int rep = -1;
	short front;
	char* oldDest = dest;

	while (++rep < MAX_LENGTH)
	{
		front = src[rep];
		if (!front) break;
		if (front < 0x80)
		{
			dest[0] = (char)front;
			dest += 1;
		}
		else if (front < 0x800)
		{
			dest[0] = (char)( ((front >> 6) & 0x1f) | 0xc0 );
			dest[1] = (char)( (front & 0x3f) | 0x80 );
			dest += 2;
		}
		else
		{
			dest[0] = (char)( ((front >> 12) & 0xf) | 0xe0 );
			dest[1] = (char) ( ((front >> 6) & 0x3f) | 0x80 );
			dest[2] = (char) ( (front & 0x3f) | 0x80 );
			dest += 3;
		}
	}
	dest[0] = 0;
}

#undef MAX_LENGTH

int StringGetLength(const char* str)
{
	char* oldStr = str;

	for ( ; *str; ++str)
	{ }
	return (int)( str - oldStr );
}

int WideStringGetLength(const short* wstr)
{
	short* oldWstr = wstr;

	for ( ; *wstr; ++wstr)
	{ }
	return (int) ( wstr - oldWstr );
}

int StringCompare(const char *s1, const char *s2)
{
	while (*s1 && (*s1 == *s2))
	{
		++s1;
		++s2;
	}
	return s1[0] - s2[0];
}

int WideStringCompare(const short *ws1, const short *ws2)
{
	while (*ws1 && (*ws1 == *ws2))
	{
		++ws1;
		++ws2;
	}
	return ws1[0] - ws2[0];
}

void ConcatenateString(char *dest, const char *src)
{
	int length = StringGetLength(dest);

	while (*src)
	{
		dest[length++] = *src;
		++src;
	}
	dest[length] = 0;
}
