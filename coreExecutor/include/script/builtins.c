
#include "builtins.h"
#include "stringUtil.h"

#define BUILTIN_FALSE	0
#define BUILTIN_TRUE	1

typedef struct _NoxScriptFunction
{
    char *fName;    //28
    int hasReturnVar;   //1- return {n}, 0- return nothing
    int argCount;
    int varCount;
    int varFieldSize;
    int *varSizeTable;
    int *field24;
    int *varTable;
    int *codeTable;
    int field36;
    int field40;
    int field44;
} NoxScriptFunction;

static void invokeBuiltin(CallBuiltinParam *param)
{ }	//CODE: 0x45 + {special} + 0x48 ���� ����.

typedef void(*Invokable_Ptr)(CallBuiltinParam*);
static Invokable_Ptr s_callerFunction = &invokeBuiltin;

void AudioEvent(char *audio, int locationId)
{
	CallBuiltinParam param = { BUILTIN_FALSE, 50, 2, (int)s_callerFunction, CharPointerToNoxString(audio), locationId };

	invokeBuiltin(&param);
}

int CreateObject(char *unitName, int locationId)
{
	CallBuiltinParam param = { BUILTIN_TRUE, 50, 2, (int)s_callerFunction, CharPointerToNoxString(unitName), locationId };

	invokeBuiltin(&param);
	return param.retValue;
}

void BuiltinNoArg(int targetID)
{
	CallBuiltinParam param = { BUILTIN_FALSE, targetID, 0, (int)s_callerFunction, };

	invokeBuiltin(&param);
}

int BuiltinNoArgReturn(int targetID)
{
	CallBuiltinParam param = { BUILTIN_TRUE, targetID, 0, (int)s_callerFunction, };

	invokeBuiltin(&param);
	return param.retValue;
}

void BuiltinSingleArg(int arg, int targetID)
{
	CallBuiltinParam param = { BUILTIN_FALSE, targetID, 1, (int)s_callerFunction, arg };

	invokeBuiltin(&param);
}

int BuiltinSingleArgReturn(int arg, int targetID)
{
	CallBuiltinParam param = { BUILTIN_TRUE, targetID, 1, (int)s_callerFunction, arg };

	invokeBuiltin(&param);
	return param.retValue;
}

float BuiltinSingleArgReturnFloat(int arg, int targetID)
{
	int ret = BuiltinSingleArgReturn(arg, targetID);

	return *(float *)&ret;
}

void BuiltinTwoArg(int arg1, int arg2, int targetID)
{
	CallBuiltinParam param = { BUILTIN_FALSE, targetID, 2, (int)s_callerFunction, arg1, arg2 };

	invokeBuiltin(&param);
}

int BuiltinTwoArgReturn(int arg1, int arg2, int targetID)
{
	CallBuiltinParam param = { BUILTIN_TRUE, targetID, 2, (int)s_callerFunction, arg1, arg2 };

	invokeBuiltin(&param);
	return param.retValue;
}

void BuiltinThreeArg(int arg1, int arg2, int arg3, int targetID)
{
	CallBuiltinParam param = { BUILTIN_FALSE, targetID, 3, (int)s_callerFunction, arg1, arg2, arg3 };

	invokeBuiltin(&param);
}

void BuiltinsInitialize()
{
	int targetId = (int)s_callerFunction;
	NoxScriptFunction *scrTable = (NoxScriptFunction *)(*(int *)0x75ae28);
	int *pCode = scrTable[targetId].codeTable;

	pCode[0] = 0x45;
	pCode[1] = (0x750af8-0x5c308c)/4;
	pCode[2] = 0x48;
}

void BuiltinsSingleCharArg(char *s, int targetID)
{
	CallBuiltinParam param = { BUILTIN_FALSE, targetID, 1, (int)s_callerFunction, CharPointerToNoxString(s), };

	invokeBuiltin(&param);
}

typedef void(*timer_impl_fn)(void);

typedef struct _NoxScriptTimer
{
	int timeStamp;
	timer_impl_fn invokeTarget;
	int argument;
	int timerId;
	// int field16;
	timer_callback timerCb;
	int field20;
	struct _NoxScriptTimer *pNext;
} NoxScriptTimer;

static void ScriptTimerInvokable()
{
	NoxScriptTimer *pTimer = *(NoxScriptTimer **)0x83395c;

	pTimer->timerCb(pTimer->argument);
}

int BuiltinRunTimer(int delay, int arg, timer_callback cb, int target)
{
	CallBuiltinParam param = { BUILTIN_TRUE, target, 3, (int)s_callerFunction, delay, arg, (int)&ScriptTimerInvokable, };

	NoxScriptTimer *pTimer = *(NoxScriptTimer **)0x83395c;
	invokeBuiltin(&param);

	pTimer->timerCb = cb;
	return param.retValue;
}

void BuiltinSayUnit(int unit, const char *msg, int frames, int seconds)
{
	CallBuiltinParam param = { BUILTIN_FALSE, 37, 2, (int)s_callerFunction, unit, CharPointerToNoxString(msg), };

	do
	{
		if (frames)
			param.targetFunctionId = 115;
		else if (seconds)
			param.targetFunctionId = 114;
		else
			break;

		param.args[param.argCount++] = frames ? frames : seconds;
	} while (0);

	invokeBuiltin(&param);
}

float NoxRandomFloat(float min, float max)
{
	CallBuiltinParam param = { BUILTIN_TRUE, 44, 2, (int)s_callerFunction, *((int *)&min), *((int *)&max), };

	invokeBuiltin(&param);
	return *(float *)&param.retValue;
}

char *noxImplToString(CallBuiltinParam *pParam)
{
	char **strTable = (char **)0x97bb40;

	invokeBuiltin(pParam);
	return strTable[pParam->retValue];
}

char *IntToString(int iValue)
{
	CallBuiltinParam param = { BUILTIN_TRUE, 48, 1, (int)s_callerFunction, iValue, };
	
	return noxImplToString(&param);
}

char *FloatToString(float fValue)
{
	CallBuiltinParam param = { BUILTIN_TRUE, 49, 1, (int)s_callerFunction, *(int *)&fValue, };
	
	return noxImplToString(&param);
}

void NoxDamage(int target, int source, int amount, DamageType type)
{
	CallBuiltinParam param = { BUILTIN_FALSE, 51, 4, (int)s_callerFunction, target, source, amount, type };

	invokeBuiltin(&param);
}

static char *s_enchantList[] = {
	"ENCHANT_INVISIBLE",
	"ENCHANT_MOONGLOW",
	"ENCHANT_BLINDED",
	"ENCHANT_CONFUSED",
	"ENCHANT_SLOWED",
	"ENCHANT_HELD",
	"ENCHANT_DETECTING",
	"ENCHANT_ETHEREAL",
	"ENCHANT_RUN",
	"ENCHANT_HASTED",
	"ENCHANT_VILLAIN",
	"ENCHANT_AFRAID",
	"ENCHANT_BURNING",
	"ENCHANT_VAMPIRISM",
	"ENCHANT_ANCHORED",
	"ENCHANT_LIGHT",
	"ENCHANT_DEATH",
	"ENCHANT_PROTECT_FROM_FIRE",
	"ENCHANT_PROTECT_FROM_POISON",
	"ENCHANT_PROTECT_FROM_MAGIC",
	"ENCHANT_PROTECT_FROM_ELECTRICITY",
	"ENCHANT_INFRAVISION",
	"ENCHANT_SHOCK",
	"ENCHANT_INVULNERABLE",
	"ENCHANT_TELEKINESIS",
	"ENCHANT_FREEZE",
	"ENCHANT_SHIELD",
	"ENCHANT_REFLECTIVE_SHIELD",
	"ENCHANT_CHARMING",
	"ENCHANT_ANTI_MAGIC",
	"ENCHANT_CROWN",
	"ENCHANT_SNEAK"
};

void NoxEnchant(int unit, Enchantment enchant, float duration)
{
	CallBuiltinParam param = { BUILTIN_FALSE, 57, 3, (int)s_callerFunction, unit, CharPointerToNoxString(s_enchantList[enchant]), *(int *)&duration, };

	invokeBuiltin(&param);
}

int NoxHasEnchant(int unit, Enchantment enchant)
{
	CallBuiltinParam param = { BUILTIN_TRUE, 81, 2, (int)s_callerFunction, unit, CharPointerToNoxString(s_enchantList[enchant]), };

	invokeBuiltin(&param);
	return param.retValue;
}
void NoxEnchantOff(int unit, Enchantment enchant)
{
	CallBuiltinParam param = { BUILTIN_FALSE, 82, 2, (int)s_callerFunction, unit, CharPointerToNoxString(s_enchantList[enchant]), };

	invokeBuiltin(&param);
}

void BuiltinIntFloatFloat(int arg, float farg1, float farg2, int targetId)
{
	CallBuiltinParam param = { BUILTIN_FALSE, targetId, 3, (int)s_callerFunction, arg, *(int *)&farg1, *(int *)&farg2 };

	invokeBuiltin(&param);
}

void NoxPushObject(int unit, float magnitude, float x, float y)
{
	CallBuiltinParam param = { BUILTIN_FALSE, 72, 4, (int)s_callerFunction, unit, *(int *)&magnitude, *(int *)&x, *(int *)&y };

	invokeBuiltin(&param);
}

typedef struct _CallBuiltinFloatParam
{
	float retValue;
	int targetFunctionId;
	int argCount;
	int caller;
	float args[BUILTIN_MAX_ARG];
} CallBuiltinFloatParam;

float NoxDistance(float x1, float y1, float x2, float y2)
{
	CallBuiltinFloatParam fParam = { BUILTIN_TRUE, 86, 4, (int)s_callerFunction, x1, y1, x2, y2, };

	invokeBuiltin(&fParam);
	return fParam.retValue;
}

void NoxPlayFX(const char *fxName, float x1, float y1, float x2, float y2)
{
	int strNumber = CharPointerToNoxString(fxName);
	CallBuiltinFloatParam fParam = { BUILTIN_FALSE, 100, 5, (int)s_callerFunction, *(float *)&strNumber, x1, y1, x2, y2 };

	invokeBuiltin(&fParam);
}

float NoxGetQuestStatusFloat(const char *name)
{
	CallBuiltinParam param = { BUILTIN_TRUE, 122, 1, (int)s_callerFunction, CharPointerToNoxString(name), };

	invokeBuiltin(&param);
	return *(float *)&param.retValue;
}

char *s_dialogTypeStr[] =
{
	"NORMAL",
	"NEXT",
	"YESNO",
	"YESNO_NEXT"
};

void NoxSetupDialog(int unit, DialogType type, dialog_callback startFn, dialog_callback endFn)
{
	CallBuiltinParam param = { BUILTIN_FALSE, 126, 4, (int)s_callerFunction, unit, type, (int)startFn, (int)endFn, };

	invokeBuiltin(&param);
}

void NoxCastSpellObjectLocation(char *spell, int source, float x, float y)
{
	CallBuiltinParam param = { BUILTIN_FALSE, 132, 4, (int)s_callerFunction, CharPointerToNoxString(spell), source, *(int *)&x, *(int *)&y, };

	invokeBuiltin(&param);
}

void NoxCastSpellLocationObject(char *spell, float x, float y, int target)
{
	CallBuiltinParam param = { BUILTIN_FALSE, 133, 4, (int)s_callerFunction, CharPointerToNoxString(spell), *(int *)&x, *(int *)&y, target, };

	invokeBuiltin(&param);
}
void NoxCastSpellLocationLocation(char *spell, float x1, float y1, float x2, float y2)
{
	CallBuiltinParam param = { BUILTIN_FALSE, 134, 5, (int)s_callerFunction, CharPointerToNoxString(spell), *(int *)&x1, *(int *)&y1, *(int *)&x2, *(int *)&y2 };

	invokeBuiltin(&param);
}

void NoxCreatureGuard(int id, float x1, float y1, float x2, float y2, float distance)
{
	CallBuiltinFloatParam fParam = { BUILTIN_FALSE, 139, 6, (int)s_callerFunction, *(float *)&id, x1, y1, x2, y2, };

	invokeBuiltin(&fParam);
}

void NoxSetCallback(int mon, UnitCallbackType type, UnitCallback callback)
{
	BuiltinThreeArg(mon, type, (int)callback, 190);
}

void NoxSetTrapSpells(int unit, const char *spell1, const char *spell2, const char *spell3)
{
	CallBuiltinParam param = { BUILTIN_FALSE, 192, 4, (int)s_callerFunction, unit, CharPointerToNoxString(spell1), CharPointerToNoxString(spell2), CharPointerToNoxString(spell3) };

	invokeBuiltin(&param);
}
