
#ifndef CORE_REGISTER_DEF_H__
#define CORE_REGISTER_DEF_H__

#define CORE_REG_BASE	(int)0x750F9C
#define CORE_REG_GLOBAL (int)(CORE_REG_BASE)
#define CORE_REG_ESP	(int)(CORE_REG_GLOBAL+4)
#define CORE_REG_EBP	(int)(CORE_REG_ESP+4)
#define CORE_REG_EAX	(int)(CORE_REG_EBP+4)
#define CORE_REG_EBX	(int)(CORE_REG_EAX+4)
#define CORE_REG_ECX	(int)(CORE_REG_EBX+4)
#define CORE_REG_EDX	(int)(CORE_REG_ECX+4)
#define CORE_REG_ESI	(int)(CORE_REG_EDX+4)
#define CORE_REG_EDI	(int)(CORE_REG_ESI+4)
#define CORE_REG_FLAG	(int)(CORE_REG_EDI+4)

#endif
