
#ifndef MAKE_SCRIPT_H__
#define MAKE_SCRIPT_H__

#include <fstream>
#include <memory>

class BinaryBuffer;

class MakeScript
{
private:
    std::unique_ptr<BinaryBuffer> m_buffer;

public:
    MakeScript();
    ~MakeScript();

private:
    void writeComment(std::ofstream &file);
    void writeFunctionPrototype(std::ofstream &file);
    void writeFunctionEntry(std::ofstream &file);
    void writeCodeData(std::ofstream &file);
    void writeFunctionTail(std::ofstream &file);

public:
    void SetCodebase(int base);
    void SetRegisterBase(int base);
    void SetCopyLength(size_t length);
    void Write(std::ofstream &file);
};

#endif

