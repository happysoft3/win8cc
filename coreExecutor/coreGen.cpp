
#include "coreGen.h"
extern "C" {
#include "coreExecApi.h"
}
#include "makeScript.h"
#include "genOption.h"
#include "coreRegisterDef.h"
#include "common\utils\myDebug.h"

constexpr auto def_impl_builtins = "DEF_IMPL_BUILTINS_FUNCTION";

CoreGen::CoreGen()
{
	m_core = nullptr;
	m_baseFnOffset = 0;
}

CoreGen::~CoreGen()
{ }

void CoreGen::sortCoreApi()
{
	int count = static_cast<int>(m_core->tableCount);

	while (--count >= 0)
	{
		auto fn = &m_core->table[count];

		m_fnMap[reinterpret_cast<uint32_t>(fn->codePtr)] = fn;
	}
}

void CoreGen::writeCodeStreamImpl(std::ofstream &file)
{
	constexpr size_t allowed_max_length = 2560;
	size_t checkLength = 0;
	const char *code = nullptr;

	for (auto &fn : m_fnMap)
	{
		checkLength = fn.second->length;
		code = reinterpret_cast<const char *>(fn.second->codePtr);

        if (checkLength > allowed_max_length)
            MY_THROW() << "stream too long: " << checkLength << ", " << fn.second->keyLiteral;

        if (checkLength)
            file.write(code, checkLength);
        else
            MY_PRINT() << "no stream...";
	}
}

void CoreGen::writeApiTableImpl(std::ofstream &file)
{
    union
    {
        uint32_t off;
        char raw[sizeof(uint32_t)];
    } iConv = { };

    for (auto &fn : m_fnMap)
    {
        iConv.off = fn.second->codeOffset + sizeof(int);    //길이 필드가 있으므로 +4해야함
        file.write(iConv.raw, sizeof(int));
    }
}

void CoreGen::LoadCore(const GenOption &opt)
{
    m_baseFnOffset = opt.TableOffset();
	m_core = ReleaseCoreApi();
	sortCoreApi();
}

void CoreGen::WriteAsINC(std::ofstream &file)
{
    int table = m_baseFnOffset;

	for (auto &item : m_fnMap)
	{
		file << def_impl_builtins;
		file.put('(');
		file << item.second->keyLiteral;
		file.put(',');
        file << table;      //file << (m_baseFnOffset + item.second->codeOffset);
        table += 4;
		file.put(')');
		file << "\r\n";
	}
}

void CoreGen::WriteCodestream(std::ofstream &file)
{
    if (m_fnMap.empty())
        MY_THROW() << "no stream";

    union
    {
        size_t len;
        char raw[sizeof(size_t)];
    } field = { m_core->fieldLength };

    file.write(field.raw, sizeof(field.raw));
	writeCodeStreamImpl(file);
}

void CoreGen::WriteLoaderScript(std::ofstream &file)
{
    MakeScript scr;

    scr.SetCodebase(m_baseFnOffset);
    scr.SetRegisterBase(CORE_REG_BASE);
    scr.SetCopyLength(m_core->fieldLength);
    scr.Write(file);
}

void CoreGen::WriteAPITable(std::ofstream &file)
{
    union
    {
        size_t len;
        char raw[sizeof(size_t)];
    } field = { m_core->tableCount * sizeof(int) };

    file.write(field.raw, sizeof(field.raw));
    writeApiTableImpl(file);
}

void CoreGen::WriteBinary(CoreGen::write_task_ty task, const std::string &url)
{
    if (!m_core)
        MY_THROW() << "core is null";

	const char *fname = url.empty() ? "eudcore_impl.bin" : url.c_str();
	std::ofstream file(fname, std::ios::binary | std::ios::out);

    if (!file.is_open())
        MY_THROW() << "cannot create code file";

	try
	{
		(this->*task)(file);
	}
	catch (const std::exception &writeException)
	{
		remove(fname);
		throw writeException;
	}
}


