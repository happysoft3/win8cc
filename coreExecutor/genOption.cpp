
#include "genOption.h"
#include "common\utils\myDebug.h"

static constexpr auto gen_main_key = "CORE_API";
static constexpr int default_api_table_off = 0x750a80;

GenOption::GenOption()
    : IniFileMan()
{ }

GenOption::~GenOption()
{ }

int GenOption::determineTableOffset()
{
    std::string tbOff;

    if (!GetItemValue(gen_main_key, "TABLE_OFFSET", tbOff))
        return default_api_table_off;

    std::stringstream ss(tbOff);
    int off;

    ss >> off;
    return off;
}

void GenOption::loadParams()
{
    if (!GetItemValue(gen_main_key, "INC_PATH", m_incPath))
        MY_THROW() << "need inc_path";

    if (!GetItemValue(gen_main_key, "BIN_PATH", m_binPath))
        MY_THROW() << "need bin_path";

    if (!GetItemValue(gen_main_key, "LOADER_SCR_PATH", m_loaderScrPath))
        MY_THROW() << "missing key 'LOADER_SCR_PATH'";

    if (!GetItemValue(gen_main_key, "API_TABLE_PATH", m_apiTablePath))
        MY_THROW() << "api table";

    m_tablebaseOffset = determineTableOffset();
}

bool GenOption::ReadIni(const std::string &inifile)
{
    try
    {
        IniFileMan::ReadIni(inifile);
        loadParams();
    }
    catch (const std::exception &ex)
    {
        MY_PRINT() << "error: " << ex.what();
        return false;
    }
    return true;
}

