// Copyright 2014 Rui Ueyama. Released under the MIT license.

#include "8cc.h"
#include "error.h"
#include "buffer.h"

#include <errno.h>
#include <limits.h>
#include <string.h>
//#include <unistd.h>

#include <filesystem>
#include <string>

#include "common\utils\stringhelper.h"

using namespace _StringHelper;

#define PATH_MAX 1024

// Returns the shortest path for the given full path to a file.
static char *clean(char *p) {
    assert(*p == '/');
    char buf[PATH_MAX];
    char *q = buf;
    *q++ = '/';
    for (;;) {
        if (*p == '/') {
            p++;
            continue;
        }
        if (!memcmp("./", p, 2)) {
            p += 2;
            continue;
        }
        if (!memcmp("../", p, 3)) {
            p += 3;
            if (q == buf + 1)
                continue;
            for (q--; q[-1] != '/'; q--);
            continue;
        }
        while (*p != '/' && *p != '\0')
            *q++ = *p++;
        if (*p == '/') {
            *q++ = *p++;
            continue;
        }
        *q = '\0';
        return strdup(buf);
    }
}

// Returns the shortest absolute path for the given path.
std::string fullpath(char *path) {
    //static char cwd[PATH_MAX];
    if (path[0] == '/')
        return clean(path);

    auto cwdpath = std::experimental::filesystem::current_path();
    auto cwd = cwdpath.string();

    //if (*cwd == '\0' && !getcwd(cwd, PATH_MAX))
    if (cwd.empty())
        throw std::logic_error(stringFormat("getcwd failed: %s", strerror(errno)));
    return clean(toArray(stringFormat("%s/%s", cwd.c_str(), path)));
}