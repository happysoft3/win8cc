
#ifndef BUFFER_H__
#define BUFFER_H__

//typedef struct Buffer
//{
//	char *body;
//	int nalloc;
//	int len;
//} Buffer;
//
//// buffer.c
//Buffer *make_buffer(void);
//char *buf_body(Buffer *b);
//int buf_len(Buffer *b);
//void buf_write(Buffer *b, char c);
//void buf_append(Buffer *b, char *s, int len);
//void buf_printf(Buffer *b, char *fmt, ...);
//char *vformat(char *fmt, va_list ap);
//char *format(char *fmt, ...);
//char *quote_cstring(char *p);
//char *quote_cstring_len(char *p, int len);
//char *quote_char(char c);

#include <memory>
#include <list>

class CCBuffer
{
private:
	char *m_body;
	int m_nalloc;
	int m_length;

public:
	CCBuffer();
	virtual ~CCBuffer();

private:
	void reallocBody();

public:
	char *Body() const
	{
		return m_body;
	}
	int Length() const
	{
		return m_length;
	}
	void Write(char c);
	void Append(const char *s, int length);
	void Printf(const char *fmt, ...);

private:
	char *vFormatImpl(const char *fmt, va_list ap);

protected:
	char *loadVFormat(CCBuffer &buff, const char *fmt, va_list ap);
	static void print(CCBuffer &buff, char c);

public:
	void WriteUtf8(uint32_t rune);
};

class CCBufferAllocator : public CCBuffer
{
private:
	std::list<std::unique_ptr<CCBuffer>> m_bufferList;

public:
	CCBufferAllocator();
	~CCBufferAllocator() override;

private:
	CCBuffer *makeChild();

public:
	CCBuffer *MakeBuffer();
	char *VFormat(const char *fmt, va_list ap);
	char *Format(const char *fmt, ...);
	char *QuoteString(const char *p);
	char *QuoteStringWithLength(const char *p, int length);
	char *QuoteChar(char c);

private:
	static int readRune(uint32_t &r, const char *s, const char *end);

public:
	CCBuffer *ToUtf16(const char *p, int len);
	CCBuffer *ToUtf32(const char *p, int len);
};

#endif
