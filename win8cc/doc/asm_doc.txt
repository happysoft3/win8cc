/*
할일.
1. global variable 찾아서 global reg에 넣기
2. ebp, esp 레지스터에 global field base 넣기
3. 00751000 으로 memcopy
*/

//시작
push esi
push edi
push ecx
push ebp
mov ebp, esp
sub esp, 08
mov eax, [0075ae28]
mov [ebp-04], eax

//144+0x20 = 176		//3번 fn 코드 필드 오프셋
mov ecx, 00750F9C
mov eax, [eax+b0]
mov [ecx], eax			//global reg 에 값을 대입 합니다
add ecx, 04

//+144+0x1c = 172	//3번 fn 데이터 필드 오프셋
mov eax, [ebp-04]
push ecx
mov ecx, [eax+a4]
mov ecx, [ecx]		//스택크기/sizeof(int)
shl ecx, 02			//크기*sizeof(int)
sub ecx, 04		//마지막 주소를 가리켜야 하므로 -4
mov eax, [eax+ac]
lea eax, [eax+ecx]
pop ecx
mov [ecx], eax		//esp 에 값을 대입(스택의 시작주소)
mov [ecx+04], eax		//ebp 에 값을 대입

mov eax, [ebp-04]
push eax
mov eax, [eax+110]		//실제 api 구현 주소다
mov [ebp-08], eax
pop eax

mov esi, [eax+e0]		//복사대상-함수테이블
mov ecx, [esi]		//개수 get
shr ecx, 02
add esi, 04
mov edi, 00750a80		//api code table

l2:
test ecx, ecx
je l1

mov eax, [esi]	//코드 오프셋 가져옴
push esi
mov esi, [ebp-08]
lea eax, [eax+esi]
pop esi
mov [edi], eax
add esi, 04
add edi, 04
dec ecx
jmp l2
l1:
//종료

//20 dec 2022, 22:07추가
//320 bytes
mov eax, [ebp-04]	//script table 가져옴
mov esi, [eax+140]	//pointer offset table
mov eax, [eax+b0]	//global field get
push eax
mov ecx, [esi]	//get repeat count
lea esi, [esi+04]
l4:
test ecx, ecx
je l3
mov eax, [esp]
add eax, [esi]
mov edi, eax

mov eax, [esp]
add eax, [edi]
mov [edi], eax

add esi, 04
dec ecx
jmp l4
l3:
pop eax
//20 dec 2022, 22:07추가, 여기까지

xor eax, eax
mov esp, ebp
pop ebp
pop ecx
pop edi
pop esi
ret


bytecode =
56 57 51 55 8B EC 50 A1 28 AE 75 00 89 45 FC B9 9C 0F 75 00 8B 80 B0 00 00 00 89 01 83 C1 04 8B 45 FC 8B 80 AC 00 00 00 8D 80 FC FF 01 00 89 01 89 41 04 8B 45 FC 8B B0 E0 00 00 00 BF 80 0A 75 00 B9 FF 00 00 00 F3 A5 31 C0 8B E5 5D 59 5F 5E C3 90 90 90

56 57 51 55 8B EC 83 EC 08 A1 28 AE 75 00 89 45 FC B9 9C 0F 75 00 8B 80 B0 00 00 00 89 01 83 C1 04 8B 45 FC 51 8B 88 A4 00 00 00 8B 09 C1 E1 02 83 E9 04 8B 80 AC 00 00 00 8D 04 08 59 89 01 89 41 04 8B 45 FC 50 8B 80 10 01 00 00 89 45 F8 58 8B B0 E0 00 00 00 8B 0E 83 C6 04 BF 80 0A 75 00 85 C9 74 15 8B 06 56 8B 75 F8 8D 04 30 5E 89 07 83 C6 04 83 C7 04 49 EB E7 31 C0 8B E5 5D 59 5F 5E C3 90 90



