// Copyright 2012 Rui Ueyama. Released under the MIT license.

/*
* Vectors are containers of void pointers that can change in size.
*/

#include "vector.h"
#include <stdlib.h>
#include <string.h>
#include <cassert>
#include <stdexcept>

#define MIN_SIZE 8

static int vectormax(int a, int b) {
    return a > b ? a : b;
}

static int roundup(int n) {
    if (n == 0)
        return 0;
    int r = 1;
    while (n > r)
        r *= 2;
    return r;
}
/////Vector 을 클래스 화////

CCVector::CCVector(size_t sz)
{
	initialize(sz);
}

CCVector::CCVector(void *elem)
{
	initialize(0);
	Push(elem);
}

CCVector::~CCVector()
{
	if (m_body)
		free(m_body);
}

void CCVector::initialize(size_t size)
{
	size = roundup(size);
	if (size > 0)
		m_body = reinterpret_cast<void **>(malloc(sizeof(void *) * size));
	m_length = 0;
	m_nalloc = size;

	m_ignore = false;
}

void CCVector::vectorExtend(int delta)
{
	if (m_length + delta <= m_nalloc)
		return;
	int nelem = vectormax(roundup(m_length + delta), MIN_SIZE);
	void *newbody = malloc(sizeof(void *) * nelem);
	memcpy(newbody, m_body, sizeof(void *) * m_length);
	if (m_body)
		free(m_body);
	m_body = reinterpret_cast<void **>(newbody);
	m_nalloc = nelem;
}

CCVector *CCVector::Copy() const
{
	auto r = new CCVector(m_length);

	memcpy(r->m_body, m_body, sizeof(void *) * m_length);
	r->m_length = m_length;
	return r;
}

void CCVector::Push(void *elem)
{
	vectorExtend(1);
	m_body[m_length++] = elem;
}

void CCVector::Append(const CCVector &other)
{
	vectorExtend(other.m_length);
	memcpy(m_body + m_length, other.m_body, sizeof(void *) * other.m_length);
	m_length += other.m_length;
}

void *CCVector::Pop()
{
	if (m_length <= 0)
	{
		if (!m_ignore)
			throw std::exception("assert(vec->len > 0)");

		return nullptr;
	}
	return m_body[--m_length];
}

void *CCVector::Get(int index) const
{
	if (index < 0 || index >= m_length)
	{
		if (!m_ignore)
			throw std::exception("assert(0 <= index && index < vec->len)");
		return nullptr;
	}
	return m_body[index];
}

void CCVector::Set(int index, void *value)
{
	if (index < 0 || index >= m_length)
	{
		if (!m_ignore)
			throw std::exception("assert(0 <= index && index < vec->len)");
		return;
	}
	m_body[index] = value;
}

void *CCVector::Head() const
{
	if (m_length <= 0)
	{
		if (!m_ignore)
			throw std::exception("assert(vec->len > 0)");
		return nullptr;
	}
	return m_body[0];
}

void *CCVector::Tail() const
{
	if (m_length <= 0)
	{
		if (!m_ignore)
			throw std::exception("assert(vec->len > 0)");
		return nullptr;
	}
	return m_body[m_length - 1];
}

CCVector *CCVector::Reverse() const
{
	auto r = new CCVector(m_length);

	for (int i = 0; i < m_length; i++)
		r->m_body[i] = m_body[m_length - i - 1];
	r->m_length = m_length;
	return r;
}

bool CCVector::EqualLength(const CCVector &other) const
{
	return m_length == other.m_length;
}

