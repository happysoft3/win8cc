// Copyright 2012 Rui Ueyama. Released under the MIT license.

#include "buffer.h"
#include "error.h"
#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <cstdio>

#define INIT_SIZE 8

/////////Buffer Class//////

CCBuffer::CCBuffer()
{
	m_body = reinterpret_cast<char *>(malloc(INIT_SIZE));
	m_nalloc = INIT_SIZE;
	m_length = 0;
}

CCBuffer::~CCBuffer()
{
	free(m_body);
}

void CCBuffer::reallocBody()
{
	int newsize = m_nalloc * 2;
	char *body = reinterpret_cast<char *>(malloc(newsize));

	memcpy(body, m_body, m_length);
	free(m_body);
	m_body = body;
	m_nalloc = newsize;
}

void CCBuffer::Write(char c)
{
	if (m_nalloc == (m_length + 1))
		reallocBody();
	m_body[m_length++] = c;
}

void CCBuffer::Append(const char *s, int length)
{
	for (int i = 0; i < length; ++i)
		Write(s[i]);
}

void CCBuffer::Printf(const char *fmt, ...)
{
	va_list args;
	for (;;)
	{
		int avail = m_nalloc - m_length;
		va_start(args, fmt);
		int written = vsnprintf(m_body + m_length, avail, fmt, args);
		va_end(args);
		if (avail <= written)
		{
			reallocBody();
			continue;
		}
		m_length += written;
		return;
	}
}

char *CCBuffer::vFormatImpl(const char *fmt, va_list ap)
{
	va_list aq;

	for (;;)
	{
		int avail = m_nalloc - m_length;
		va_copy(aq, ap);
		int written = vsnprintf(m_body + m_length, avail, fmt, aq);
		va_end(aq);
		if (avail <= written)
		{
			reallocBody();
			continue;
		}
		m_length += written;
		return m_body;
	}
}

char *CCBuffer::loadVFormat(CCBuffer &buff, const char *fmt, va_list ap)
{
	return buff.vFormatImpl(fmt, ap);
}

static char *quoteT(char c)
{
    switch (c)
	{
    case '"': return "\\\"";
    case '\\': return "\\\\";
    case '\b': return "\\b";
    case '\f': return "\\f";
    case '\n': return "\\n";
    case '\r': return "\\r";
    case '\t': return "\\t";
    }
    return nullptr;
}

void CCBuffer::print(CCBuffer &buff, char c)
{
	char *q = quoteT(c);

	if (q)
	{
		buff.Printf("%s", q);
	}
	else if (isprint(c))
	{
		buff.Printf("%c", c);
	}
	else
	{
		buff.Printf("\\x%02x", c);
	}
}

void CCBuffer::WriteUtf8(uint32_t rune)
{
	if (rune < 0x80)
	{
		Write(rune);
		return;
	}
	if (rune < 0x800)
	{
		Write(0xC0 | (rune >> 6));
		Write(0x80 | (rune & 0x3F));
		return;
	}
	if (rune < 0x10000)
	{
		Write(0xE0 | (rune >> 12));
		Write(0x80 | ((rune >> 6) & 0x3F));
		Write(0x80 | (rune & 0x3F));
		return;
	}
	if (rune < 0x200000)
	{
		Write(0xF0 | (rune >> 18));
		Write(0x80 | ((rune >> 12) & 0x3F));
		Write(0x80 | ((rune >> 6) & 0x3F));
		Write(0x80 | (rune & 0x3F));
		return;
	}
	THROW_ERROR_NOPOS("invalid UCS character: \\U%08x", rune);
}

////

CCBufferAllocator::CCBufferAllocator()
{ }

CCBufferAllocator::~CCBufferAllocator()
{ }

CCBuffer *CCBufferAllocator::makeChild()
{
	auto child = std::make_unique<CCBuffer>();
	auto ret = child.get();

	m_bufferList.push_back(std::move(child));
	return ret;
}

CCBuffer *CCBufferAllocator::MakeBuffer()
{
	return makeChild();
}

char *CCBufferAllocator::VFormat(const char *fmt, va_list ap)
{
	auto b = makeChild();

	if (!b)
		throw std::exception(__FUNCDNAME__);
	return loadVFormat(*b, fmt, ap);
}

char *CCBufferAllocator::Format(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	char *r = VFormat(fmt, ap);
	va_end(ap);
	return r;
}

char *CCBufferAllocator::QuoteString(const char *p)
{
	auto b = makeChild();

	while (*p)
		print(*b, *p++);
	return b->Body();
}

char *CCBufferAllocator::QuoteStringWithLength(const char *p, int length)
{
	auto b = makeChild();

	for (int i = 0; i < length; i++)
		print(*b, p[i]);
	return b->Body();
}

char *CCBufferAllocator::QuoteChar(char c)
{
	if (c == '\\') return "\\\\";
	if (c == '\'') return "\\'";
	return Format("%c", c);
}

//Encoding

static int count_leading_ones(char c)
{
	for (int i = 7; i >= 0; i--)
		if ((c & (1 << i)) == 0)
			return 7 - i;
	return 8;
}

int CCBufferAllocator::readRune(uint32_t &r, const char *s, const char *end)
{
	int len = count_leading_ones(s[0]);
	if (len == 0)
	{
		r = s[0];
		return 1;
	}
	if (s + len > end)
		THROW_ERROR_NOPOS("invalid UTF-8 sequence");
	for (int i = 1; i < len; i++)
		if ((s[i] & 0xC0) != 0x80)
			THROW_ERROR_NOPOS("invalid UTF-8 continuation byte");
	switch (len)
	{
	case 2:
		r = ((s[0] & 0x1F) << 6) | (s[1] & 0x3F);
		return 2;
	case 3:
		r = ((s[0] & 0xF) << 12) | ((s[1] & 0x3F) << 6) | (s[2] & 0x3F);
		return 3;
	case 4:
		r = ((s[0] & 0x7) << 18) | ((s[1] & 0x3F) << 12) | ((s[2] & 0x3F) << 6) | (s[3] & 0x3F);
		return 4;
	}
	THROW_ERROR_NOPOS("invalid UTF-8 sequence");
}

static void write16(CCBuffer &b, uint16_t x)
{
	b.Write(x & 0xFF);
	b.Write(x >> 8);
}

static void write32(CCBuffer &b, uint32_t x)
{
	write16(b, x & 0xFFFF);
	write16(b, x >> 16);
}

CCBuffer *CCBufferAllocator::ToUtf16(const char *p, int len)
{
	CCBuffer *b = makeChild();
	const char *end = p + len;
	while (p != end)
	{
		uint32_t rune;
		p += readRune(rune, p, end);
		if (rune < 0x10000)
		{
			write16(*b, rune);
		}
		else
		{
			write16(*b, (rune >> 10) + 0xD7C0);
			write16(*b, (rune & 0x3FF) + 0xDC00);
		}
	}
	return b;
}

CCBuffer *CCBufferAllocator::ToUtf32(const char *p, int len)
{
	CCBuffer *b = makeChild();
	const char *end = p + len;
	while (p != end)
	{
		uint32_t rune;
		p += readRune(rune, p, end);
		write32(*b, rune);
	}
	return b;
}
