
#include "typePool.h"
#include "hook/ast/astType.h"

TypePool::TypePool()
{ }

TypePool::~TypePool()
{
}

AstType *TypePool::createType(AstType *ty)
{
	auto n = std::unique_ptr<AstType>(ty);

	m_typeList.push_back(std::move(n));
	return ty;
}
