
#ifndef PARSE_H__
#define PARSE_H__

#include "cpp.h"

class CCVector;
class CCMap;
class SourceLocation;
class Case;
class CCDict;
class CCBufferAllocator;
class TypePool;

//==parse.cpp ���۾�==//
class AstType;
class AstArrayType;
class AstNode;
class AstStructType;
class AstMap;
class AstDict;
class AstVector;

// parse.c
// The last source location we want to point to when we find an error in the
// source code.

// Objects representing various scopes. Did you know C has so many different
// scopes? You can use the same name for global variable, local variable,
// struct/union/enum tag, and goto label!

class CCParse : public CCPreprocessor
{
private:
    int m_tempnameCounter;
    int m_labelCounter;
    int m_staticLabelCounter;
    AstMap *m_globalenv;
    AstMap *m_localenv;
    AstMap *m_tags;
	std::unique_ptr<SourceLocation> m_sourceLocator;
    AstVector *m_localvars;

    AstVector *m_toplevels;
    AstMap *m_labels;
    AstVector *m_gotos;
    CCVector *m_cases;
    AstType *m_current_func_type;

    char *m_defaultcase;
    char *m_lbreak;
    char *m_lcontinue;

	std::unique_ptr<TypePool> m_typegen;

private:
	std::unique_ptr<CCBufferAllocator> m_parseBuffer;
	std::unique_ptr<MemorySweeper> m_parseSweep;

public:
    CCParse();
    ~CCParse();

public:
    char *make_tempname(void);
    char *make_label(void);

private:
    char *make_static_label(const char *name);
    AstMap *env();
    template <class Ty, class... Args>
    Ty *makeAstNode(Args&&... args);
    AstNode *astLvarNode(AstType *ty, const std::string &name, AstVector *init = nullptr);
    AstNode *astStaticLvarNode(AstType *ty, const char *name);

private:
    void expect(char id);
	AstType *copy_incomplete_type(AstType *ty);
    void typeChecking(AstType *ty, AstNode *target, int cop = 0);
    AstType *get_typedef(const char *name);
    bool is_type(Token *tok);
    bool next_token(int kind, Token **discard = nullptr);
    void *make_pair(void *first, void *second);

private:
    AstNode *conv(AstNode *node);
    AstNode *wrap(AstType *t, AstNode *node);
    AstType *usual_arith_conv(AstType *t, AstType *u);
    AstNode *binopLiteral(int op, AstNode *lhs, AstNode *rhs, AstType *altTy);
    AstNode *binopLiteralFloat(int op, AstNode *lhs, AstNode *rhs, AstType *altTy);
    AstNode *binop(int op, AstNode *lhs, AstNode *rhs);

private:
    int read_intexpr();
    AstNode *read_int(Token *tok);
    AstNode *read_float(Token *tok);
    AstNode *read_number(Token *tok);
    AstType *read_sizeof_operand_sub();
    AstNode *read_sizeof_operand();
    AstNode *read_alignof_operand();
    AstVector *read_func_args(AstVector *params);
    AstNode *read_funcall(AstNode *fp);
    CCVector *read_generic_list(AstNode **defaultexpr);
    AstNode *read_generic();
    void read_static_assert();
    AstNode *read_var_or_func(const char *name);
    AstNode *read_stmt_expr();
    AstNode *read_primary_expr();
    AstNode *read_subscript_expr(AstNode *node);
    AstNode *read_postfix_expr_tail(AstNode *node);
    AstNode *read_postfix_expr();
    AstNode *read_unary_incdec(int op);
    AstNode *read_label_addr(Token *tok);
    AstNode *read_unary_addr();
    AstNode *read_unary_deref(Token *tok);
    AstNode *read_unary_minus();
    AstNode *read_unary_bitnot(Token *tok);
    AstNode *read_unary_lognot();
    AstNode *read_unary_expr();
    AstNode *read_compound_literal(AstType *ty);
    AstType *read_cast_type();
    AstNode *read_cast_expr();
    AstNode *read_multiplicative_expr();
    AstNode *read_additive_expr();
    AstNode *read_shift_expr();
    AstNode *read_relational_expr();
    AstNode *read_equality_expr();
    AstNode *read_bitand_expr();
    AstNode *read_bitxor_expr();
    AstNode *read_bitor_expr();
    AstNode *read_logand_expr();
    AstNode *read_logor_expr();
    AstNode *do_read_conditional_expr(AstNode *cond);
    AstNode *read_conditional_expr();
    AstNode *read_assignment_expr();
    AstNode *read_comma_expr();

public:
    AstNode *read_expr(void) override;

private:
    AstNode *read_expr_opt();
    AstNode *read_struct_field(AstNode *struc);
    char *read_rectype_tag();
	void squash_unnamed_struct(AstDict *dict, AstType *unnamed, int offset);
    int read_bitsize(const char *name, AstType *ty);
    CCVector *read_rectype_fields_sub();
	AstDict *update_struct_offset(int *rsize, int *align, CCVector *fields);
	AstDict *update_union_offset(int *rsize, int *align, CCVector *fields);
    AstDict *read_rectype_fields(int *rsize, int *align, bool is_struct);
    AstType *read_rectype_def(bool is_struct);
    AstType *read_struct_def();
    AstType *read_union_def();
    AstType *read_enum_def();
    void assign_string(AstVector *inits, AstType *ty, const char *p, int off);
	template <class T>
	void makeStringInitializer(AstVector *inits, const AstNode *str, int off);
	void assignStringEx(AstVector *inits, AstType *ty, const AstNode *str, int off);
    bool maybe_read_brace();
    void maybe_skip_comma();
    void skip_to_brace();
    void read_initializer_elem(AstVector *inits, AstType *ty, int off, bool designated);
    void read_struct_initializer_sub(AstVector *inits, AstStructType *ty, int off, bool designated);
    void read_struct_initializer(AstVector *inits, AstStructType *ty, int off, bool designated);
    void read_array_initializer_sub(AstVector *inits, AstArrayType *ty, int off, bool designated);
    void read_array_initializer(AstVector *inits, AstArrayType *ty, int off, bool designated);
    void read_initializer_list(AstVector *inits, AstType *ty, int off, bool designated);
    AstVector *read_decl_init(AstType *ty);
    AstType *read_func_param(char **name, bool optional);
    void read_declarator_params(AstVector *types, AstVector *vars, bool *ellipsis);
    void read_declarator_params_oldstyle(AstVector *vars);
    AstType *read_func_param_list(AstVector *paramvars, AstType *rettype);
    AstType *read_declarator_array(AstType *basety);
    AstType *read_declarator_func(AstType *basety, AstVector *param);
    AstType *read_declarator_tail(AstType *basety, AstVector *params);
    void skip_type_qualifiers();
    AstType *read_declarator(char **rname, AstType *basety, AstVector *params, int ctx);
    AstType *read_abstract_declarator(AstType *basety);
    AstType *read_typeof();
    int read_alignas();
    AstType *read_decl_spec(int *rsclass);
    void read_static_local_var(AstType *ty, const char *name);
    AstType *read_decl_spec_opt(int *sclass);
    void read_decl(AstVector *block, bool isglobal);
    AstVector *read_oldstyle_param_args();
    void read_oldstyle_param_type(AstVector *params);
	AstVector *param_types(AstVector *params);
    AstNode *read_func_body(AstType *functype, char *fname, AstVector *params);
    void skip_parentheses(CCVector *buf);
    bool is_funcdef();
    void backfill_labels();
    AstNode *read_funcdef();
    AstNode *read_boolean_expr();
    AstNode *read_if_stmt();
    AstNode *read_opt_decl_or_stmt();
    AstNode *read_for_stmt();
    AstNode *read_while_stmt();
    AstNode *read_do_stmt();
    AstNode *make_switch_jump(AstNode *var, Case *c);
    AstNode *read_switch_stmt();
    AstNode *read_label_tail(AstNode *label);
    AstNode *read_case_label(Token *tok);
    AstNode *read_default_label(Token *tok);
    AstNode *read_break_stmt(Token *tok);
    AstNode *read_continue_stmt(Token *tok);
    AstNode *read_return_stmt();
    AstNode *read_goto_stmt();
    AstNode *read_label(Token *tok);
    AstNode *read_stmt();
    AstNode *read_compound_stmt();
    void read_decl_or_stmt(AstVector *list);

public:
    AstVector *read_toplevels(void) override;

private:
    void concatenate_string(Token *tok);
    Token *get();
    Token *peek();
    void define_builtin(const char *name, AstType *rettype, AstVector *paramtypes);

public:
	void InitializeUnit() override;

private:
    void parse_init(void);
};

#endif
