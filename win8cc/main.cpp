// Copyright 2012 Rui Ueyama. Released under the MIT license.

#include "hook/entry/eudCompiler.h"
#include <iostream>

#pragma comment(lib, "common")
#pragma comment(lib, "systemFileParser")

int main(int argc, char **argv)
{
	EudCompiler comp;

	if (argc < 2)
	{
        comp.ShowInfo();
		std::cout << "too less input params";
		return 0;
	}
	comp.DoCompile(argv[1]);
    return 0;
}

