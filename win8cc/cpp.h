
#ifndef CPP_H__
#define CPP_H__

#include "lex.h"

struct Token;
class CCFile;
struct File;
struct Set;
struct Macro;
class CCMap;
struct CondIncl;
class PathManager;
class CCBufferAllocator;
class MemorySweeper;
class CCSetAllocator;

class AstVector;
class AstNode;

class CCPreprocessor : public CCLex
{
    class CCTime;
private:
    CCFile *m_filestack;
    std::unique_ptr<CCMap> m_macros;
    std::unique_ptr<CCMap> m_include_guard;
    std::unique_ptr<CCVector> m_cond_incl_stack;
    std::unique_ptr<CCVector> m_std_include_path;
    std::unique_ptr<CCMap> m_once;
    std::shared_ptr<PathManager> m_pathMan;
    std::unique_ptr<CCTime> m_buildTime;

	std::unique_ptr<CCSetAllocator> m_makeSet;
	std::unique_ptr<CCBufferAllocator> m_cppBuffer;
	std::unique_ptr<MemorySweeper> m_cppSweep;

public:
    CCPreprocessor();
    ~CCPreprocessor() override;

	void SetInitialFilename(const std::string &fileName) override;
private:
    void onInitFileStack(CCFile *file) override;
	CondIncl *make_cond_incl(bool wastrue);
    Macro *make_macro(Macro *tmpl);
    Macro *make_obj_macro(CCVector *body);
    Macro *make_func_macro(CCVector *body, int nargs, bool is_varg);

    using SpecialMacroHandler = void(CCPreprocessor::*)(Token *);
    Macro *make_special_macro(SpecialMacroHandler fn);
	Token *make_macro_token(int position, bool is_vararg);
	Token *copy_token(Token *tok);

private:
    void expect(char id);
    bool next(int id);
	void propagate_space(CCVector *tokens, Token *tmpl);
    Token *read_ident();

public:
    void expect_newline(void);

private:
    CCVector *read_one_arg(Token *ident, bool *end, bool readall);
    CCVector *do_read_args(Token *ident, Macro *macro);
    CCVector *read_args(Token *tok, Macro *macro);
	CCVector *add_hide_set(CCVector *tokens, Set *hideset);
    Token *glue_tokens(Token *t, Token *u);
    void glue_push(CCVector *tokens, Token *tok);
	Token *stringize(Token *tmpl, CCVector *args);
    CCVector *expand_all(CCVector *tokens, Token *tmpl);
    CCVector *subst(Macro *macro, CCVector *args, Set *hideset);
    void unget_all(CCVector *tokens);
    Token *read_expand_newline();
    Token *read_expand();
    bool read_funclike_macro_params(Token *name, CCMap *param);
    static void hashhash_check(CCVector *v);
    CCVector *read_funclike_macro_body(CCMap *param);
    void read_funclike_macro(Token *name);
    void read_obj_macro(char *name);
    void read_define();
    void read_undef();
    Token *read_defined_op();
    CCVector *read_intexpr_line();
    bool read_constexpr();
    void do_read_if(bool istrue);
    void read_if();
    void read_ifdef();
    void read_ifndef();
    void read_else(Token *hash);
    void read_elif(Token *hash);
    Token *skip_newlines();
    void read_endif(Token *hash);
    char *read_error_message();
    void read_error(Token *hash);
    void read_warning(Token *hash);
    char *read_cpp_header_name(Token *hash, bool *std);
    bool guarded(const char *path);
    bool try_include(const char *dir, const char *filename, bool isimport);
    void read_include(Token *hash, File *file, bool isimport);
    void read_include_next(Token *hash, File *file);
    void parse_pragma_operand(Token *tok);
    void read_pragma();
    void read_line();
    void read_linemarker(Token *tok);
    void read_directive(Token *hash);
    void make_token_pushback(Token *tmpl, int kind, char *sval);
    void handle_date_macro(Token *tmpl);
    void handle_time_macro(Token *tmpl);
    void handle_timestamp_macro(Token *tmpl);
    void handle_file_macro(Token *tmpl);
    void handle_line_macro(Token *tmpl);
    void handle_pragma_macro(Token *tmpl);
    void handle_base_file_macro(Token *tmpl);
    void handle_counter_macro(Token *tmpl);
    void handle_include_level_macro(Token *tmpl);
    void define_obj_macro(char *name, Token *value);
    void define_special_macro(char *name, SpecialMacroHandler fn);
    void init_predefined_macros();

private:  //override
	virtual AstVector *read_toplevels(void) = 0;
	virtual AstNode *read_expr(void) = 0;

public:
    void add_include_path(char *path);

private:
    void cpp_init(void);
	Token *maybe_convert_keyword(Token *tok);

protected:
	void InitializeUnit() override;

public:
    Token *peek_token(void);
    Token *read_token(void);
	void RegistPath(std::shared_ptr<PathManager> &pathMan)
	{
		m_pathMan = pathMan;
	}
};

#endif
