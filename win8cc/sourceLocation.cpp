
#include "sourceLocation.h"
#include "8cc.h"
#include "file.h"
#include "error.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

struct SourceLocation::Location
{
    std::string m_furl;
    int m_line;
    int m_column;
};

SourceLocation::SourceLocation()
{ }

SourceLocation::~SourceLocation()
{ }

std::unique_ptr<SourceLocation::Location> SourceLocation::makeLocation(Token *tok)
{
    auto loc = std::make_unique<Location>();

    loc->m_column = tok->column;
    loc->m_line = tok->line;
    loc->m_furl = tok->file->name;
    return loc;
}

void SourceLocation::Markup(Token *tok)
{
	if (!tok)
		THROW_ERROR_NOPOS("maybe error: token is null");

    m_sourcePosList.push_back(makeLocation(tok));
    if (m_sourcePosList.size() > 5)
        m_sourcePosList.pop_front();
}

std::string SourceLocation::LoadProfileString() const
{
    Location *p = m_sourcePosList.empty() ? nullptr : m_sourcePosList.back().get();

	if (!p)
		return "null";

	return stringFormat("%s::%d::%d", p->m_furl, p->m_line, p->m_column);
}

std::unique_ptr<SourceLocation> SourceLocation::Clone() const
{
    auto clone = std::make_unique<SourceLocation>();

    if (!m_sourcePosList.empty())
    {
        clone->m_sourcePosList.push_back(std::make_unique<Location>(*m_sourcePosList.back()));
    }
    return clone;
}

void SourceLocation::CopyPush(const SourceLocation &src)
{
    if (src.m_sourcePosList.empty())
        return;

    m_sourcePosList.push_back(std::make_unique<Location>(*src.m_sourcePosList.back()));
}
