
#ifndef VECTOR_H__
#define VECTOR_H__

class CCVector
{
private:
	void **m_body;
	int m_length;
	int m_nalloc;
	bool m_ignore;

public:
	explicit CCVector(size_t sz = 0);
    explicit CCVector(void *elem);
	~CCVector();

private:
	void initialize(size_t size);
	void vectorExtend(int delta);

public:
	CCVector *Copy() const;
	void Push(void *elem);
	void Append(const CCVector &other);
	void *Pop();
	void *Get(int index) const;
	void Set(int index, void *value);
	void *Head() const;
	void *Tail() const;
	CCVector *Reverse() const;
	void *Body() const
	{
		return m_body;
	}
	int Length() const
	{
		return m_length;
	}
	bool EqualLength(const CCVector &other) const;
};

#endif
