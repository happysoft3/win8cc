
#if 0   //unused
#ifndef GEN_H__
#define GEN_H__

#include <cstdio>

struct Node;

// gen.c
void set_output_file(FILE *fp);
void close_output_file(void);
void emit_toplevel(Node *v);

#endif

#endif

