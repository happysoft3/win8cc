
#ifndef SOURCE_LOCATION_H__
#define SOURCE_LOCATION_H__

#include <list>
#include <memory>
#include <string>

struct Token;

class SourceLocation
{
    struct Location;

private:
	std::list<std::unique_ptr<Location>> m_sourcePosList;

public:
	SourceLocation();
	~SourceLocation();

private:
    std::unique_ptr<Location> makeLocation(Token *tok);

public:
	void Markup(Token *tok);
	std::string LoadProfileString() const;
    std::unique_ptr<SourceLocation> Clone() const;
    void CopyPush(const SourceLocation &src);
};

#endif

