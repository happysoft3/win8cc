
#ifndef ENCODING_H__
#define ENCODING_H__

#include <cstdint>

class CCBuffer;

// encoding.c
namespace ObsoleteStuff
{
    CCBuffer *to_utf16(char *p, int len);
    CCBuffer *to_utf32(char *p, int len);
    void write_utf8(CCBuffer *b, uint32_t rune);
}

#endif
