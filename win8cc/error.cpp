// Copyright 2012 Rui Ueyama. Released under the MIT license.

#include "error.h"
#include "hook/ast/astNode.h"
#include "8cc.h"
#include "file.h"
#include "buffer.h"
#include "common/utils/printUtil.h"

void CCError::pronounceError()
{
#ifdef STOP_IF_GET_AN_ERROR_
	exit(1);
#else
	throw std::exception("compile error");
#endif
}

void CCError::printMessage(const char *line, const std::string &pos, const std::string &detail, bool canIgnore)
{
	auto colr = canIgnore ? PrintUtil::ConsoleColor::COLOR_GREY : PrintUtil::ConsoleColor::COLOR_RED;

	PrintUtil::PrintMessage(colr, canIgnore ? "WARNING>" : "ERROR>", line, ':', pos.empty() ? "(null)" : pos, ':', detail);
	if (!canIgnore)
		pronounceError();
}

std::string CCError::TokenPosition(Token *tok)
{
	File *f = tok->file;
	
	if (!f)
		return "(unknown)";

	char *name = f->name ? f->name : "(unknown)";

	return _StringHelper::stringFormat("%s:%d:%d", name, tok->line, tok->column);
}

std::string CCError::NodePosition(const AstNode *node)
{
    return node->Location();
}
