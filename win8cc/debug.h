
#ifndef DEBUG_H__
#define DEBUG_H__

#include <string>

struct Token;
class CCBuffer;
class CCBufferAllocator;
class CCDict;
class AstType;

class CCHelper
{
private:
	CCHelper() = delete;
	~CCHelper() = delete;
	CCHelper(const CCHelper &) = delete;
	CCHelper(CCHelper &&) = delete;
	CCHelper &operator=(const CCHelper &) = delete;
	CCHelper &operator=(CCHelper &&) = delete;

private:
	static std::string decorate_int(const char *name, const AstType *ty);
	static std::string doTypeToString(CCBufferAllocator &factory, CCDict &dict, const AstType *ty);

public:
	static std::string ToString(const Token *tok);
	static std::string ToString(const AstType *ty);
};


#endif
