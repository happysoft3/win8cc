// Copyright 2014 Rui Ueyama. Released under the MIT license.

/*
* This file provides character input stream for C source code.
* An input stream is either backed by stdio's FILE * or
* backed by a string.
* The following input processing is done at this stage.
*
* - C11 5.1.1.2p1: "\r\n" or "\r" are canonicalized to "\n".
* - C11 5.1.1.2p2: A sequence of backslash and newline is removed.
* - EOF not immediately following a newline is converted to
*   a sequence of newline and EOF. (The C spec requires source
*   files end in a newline character (5.1.1.2p2). Thus, if all
*   source files are comforming, this step wouldn't be needed.)
*
* Trigraphs are not supported by design.
*/

#include "file.h"
#include "vector.h"
#include "error.h"
#include "buffer.h"

#include <errno.h>
#include <cassert>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdio>

//static Vector s_empty_files = { 0, };
//static Vector *files = &s_empty_files;
//
//static Vector s_empty_stashed = { 0, };
//static Vector *stashed = &s_empty_stashed;

CCFile::CCFile()
{
    m_files = new CCVector;
    m_stashed = new CCVector;
	m_buffer = std::make_unique<CCBufferAllocator>();
}

CCFile::~CCFile()
{
    delete m_files;
    delete m_stashed;
}

File *CCFile::make_file(FILE *file, const char *name) {
	File *r = new File({}); // calloc(1, sizeof(File));
    r->file = file;
    r->name = const_cast<char *>(name);
    r->line = 1;
    r->column = 1;
    struct stat st;
    if (fstat(_fileno(file), &st) == -1)
		THROW_ERROR_NOPOS("fstat failed: %s", strerror(errno));
    r->mtime = st.st_mtime;
    return r;
}

File *CCFile::make_file_string(char *s) {
	File *r = new File({}); //calloc(1, sizeof(File));
    r->line = 1;
    r->column = 1;
    r->p = s;
    return r;
}

void CCFile::close_file(File *f)
{
    if (f->file)
        fclose(f->file);
}

int CCFile::readc_file(File *f)
{
    int c = getc(f->file);
    if (c == EOF) {
        c = (f->last == '\n' || f->last == EOF) ? EOF : '\n';
    }
    else if (c == '\r') {
        int c2 = getc(f->file);
        if (c2 != '\n')
            ungetc(c2, f->file);
        c = '\n';
    }
    f->last = c;
    return c;
}

int CCFile::readc_string(File *f)
{
    int c;
    if (*f->p == '\0') {
        c = (f->last == '\n' || f->last == EOF) ? EOF : '\n';
    }
    else if (*f->p == '\r') {
        f->p++;
        if (*f->p == '\n')
            f->p++;
        c = '\n';
    }
    else {
        c = *f->p++;
    }
    f->last = c;
    return c;
}

int CCFile::get()
{
    File *f = reinterpret_cast<File *>(m_files->Tail());
    int c;
    if (f->buflen > 0) {
        c = f->buf[--f->buflen];
    }
    else if (f->file) {
        c = readc_file(f);
    }
    else {
        c = readc_string(f);
    }
    if (c == '\n') {
        f->line++;
        f->column = 1;
    }
    else if (c != EOF) {
        f->column++;
    }
    return c;
}

int CCFile::readc()
{
    for (;;) {
        int c = get();
        if (c == EOF) {
            if (m_files->Length() == 1)
                return c;
            close_file(reinterpret_cast<File *>(m_files->Pop()));
            continue;
        }
        if (c != '\\')
            return c;
        int c2 = get();
        if (c2 == '\n')
            continue;
        unreadc(c2);
        return c;
    }
}

void CCFile::unreadc(int c) {
    if (c == EOF)
        return;
    File *f = reinterpret_cast<File *>(m_files->Tail());
    assert(f->buflen < sizeof(f->buf) / sizeof(f->buf[0]));
    f->buf[f->buflen++] = c;
    if (c == '\n') {
        f->column = 1;
        f->line--;
    }
    else {
        f->column--;
    }
}

File *CCFile::current_file() {
    return reinterpret_cast<File *>(m_files->Tail());
}

void CCFile::stream_push(File *f) {
    m_files->Push( f);
}

int CCFile::stream_depth() {
    return m_files->Length();
}

char *CCFile::input_position() {
    if (m_files->Length() == 0)
        return "(unknown)";
    File *f = reinterpret_cast<File *>(m_files->Tail());
    return m_buffer->Format("%s:%d:%d", f->name, f->line, f->column);
}

void CCFile::stream_stash(File *f) {
    m_stashed->Push( m_files);
    m_files = new CCVector(f);
}

void CCFile::stream_unstash() {
    m_files = reinterpret_cast<CCVector *>(m_stashed->Pop());
}
