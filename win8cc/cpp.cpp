// Copyright 2012 Rui Ueyama. Released under the MIT license.

/*
* This implements Dave Prosser's C Preprocessing algorithm, described
* in this document: https://github.com/rui314/8cc/wiki/cpp.algo.pdf
*/

#include "cpp.h"
#include "hook/ast/astNode.h"
#include "gen.h"
#include "parse.h"
#include "error.h"
#include "debug.h"
#include "file.h"
#include "8cc.h"
#include "buffer.h"
#include "map.h"
#include "vector.h"
#include "set.h"
#include "common/mmanager/memorySweeper.h"
#include "common\utils\pathManager.h"
#include "common/utils/myDebug.h"
#include "common/utils/stringhelper.h"
#include <ctype.h>
//#include <libgen.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
//#include <unistd.h>

using namespace _StringHelper;

#pragma warning(disable : 4996)

static CCMap s_empty_keywords;
static CCMap *keywords = &s_empty_keywords;

//static struct tm now;
//static struct tm *s_nowDateTime;

static Token s_cpp_token_zero = { TNUMBER };
static Token *cpp_token_zero = &s_cpp_token_zero;

static Token s_cpp_token_one = { TNUMBER };
static Token *cpp_token_one = &s_cpp_token_one;

class InitLiteral
{
public:
	InitLiteral()
	{
		s_cpp_token_zero.sval = "0";
		s_cpp_token_one.sval = "1";
        init_keywords();
		initialLocale();
	}

    static void init_keywords() {
#define op(id, str)         keywords->Put(str, (void *)id);
#define keyword(id, str, _) keywords->Put(str, (void *)id);
#include "keyword.inc"
#undef keyword
#undef op
    }

	static void initialLocale()
	{
		setlocale(LC_ALL, "C");
	}
};

static InitLiteral s_literalInitializer;

using SpecialMacroHandler = void(CCPreprocessor::*)(Token *);
typedef enum {
    IN_THEN, IN_ELIF, IN_ELSE
} CondInclCtx;
typedef enum {
    MACRO_OBJ, MACRO_FUNC, MACRO_SPECIAL
} MacroType;

struct CondIncl
{
    CondInclCtx ctx;
    char *include_guard;
    File *file;
    bool wastrue;
};

typedef struct Macro {
    MacroType kind;
    int nargs;
    CCVector *body;
    bool is_varg;
    SpecialMacroHandler fn;
    CCPreprocessor *owner;
} Macro;

class CCPreprocessor::CCTime
{
private:
    time_t m_rawtime;
    struct tm *m_now;

public:
    CCTime()
    {
        time(&m_rawtime);
        m_now = localtime(&m_rawtime);
    }

    operator const struct tm*() const
    {
        return m_now;
    }
};

CCPreprocessor::CCPreprocessor()
    : CCLex()
{
    m_macros = std::make_unique<CCMap>();
    m_include_guard = std::make_unique<CCMap>();
    m_cond_incl_stack = std::make_unique<CCVector>();
    m_std_include_path = std::make_unique<CCVector>();
    m_once = std::make_unique<CCMap>();
	m_makeSet = std::make_unique<CCSetAllocator>();
	m_cppBuffer = std::make_unique<CCBufferAllocator>();
	m_cppSweep = std::make_unique<MemorySweeper>();
}

CCPreprocessor::~CCPreprocessor()
{ }

void CCPreprocessor::SetInitialFilename(const std::string &fileName)
{
	CCLex::SetInitialFilename(stringFormat("%s\\%s", m_pathMan->BasePath(), fileName));
}

void CCPreprocessor::onInitFileStack(CCFile *file)
{
    m_filestack = file;
}

/*
* Constructors
*/

CondIncl *CCPreprocessor::make_cond_incl(bool wastrue) {
	CondIncl *r = m_cppSweep->AllocateObject<CondIncl>(CondIncl{});
    r->ctx = IN_THEN;
    r->wastrue = wastrue;
    return r;
}

Macro *CCPreprocessor::make_macro(Macro *tmpl)
{
    Macro *n = m_cppSweep->AllocateObject<Macro>(*tmpl);

    n->owner = this;
    return n;
}

Macro *CCPreprocessor::make_obj_macro(CCVector *body) {
	Macro m = { MACRO_OBJ, 0, body };

	return make_macro(&m);
}

Macro *CCPreprocessor::make_func_macro(CCVector *body, int nargs, bool is_varg) {
	Macro m = { MACRO_FUNC , nargs, body, is_varg, 0 };
	return make_macro(&m);
}

Macro *CCPreprocessor::make_special_macro(SpecialMacroHandler fn) {
	Macro m = { MACRO_SPECIAL };

	m.fn = fn;
	return make_macro(&m);
}

Token *CCPreprocessor::make_macro_token(int position, bool is_vararg)
{
	Token *r = m_cppSweep->AllocateObject<Token>(Token({}));
    r->kind = TMACRO_PARAM;
    r->is_vararg = is_vararg;
    r->hideset = nullptr;
    r->position = position;
    r->space = false;
    r->bol = false;
    return r;
}

Token *CCPreprocessor::copy_token(Token *tok)
{
	return m_cppSweep->AllocateObject<Token>(*tok);
}

void CCPreprocessor::expect(char id) {
    Token *tok = lex();
    if (!is_keyword(tok, id))
		THROW_ERROR_TOKEN(tok, "%c expected, but got %s", id, CCHelper::ToString(tok));
}

/*
* Utility functions
*/

bool CCPreprocessor::next(int id) {
    Token *tok = lex();
    if (is_keyword(tok, id))
        return true;
    unget_token(tok);
    return false;
}

void CCPreprocessor::propagate_space(CCVector *tokens, Token *tmpl) {
    if (tokens->Length() == 0)
        return;
    Token *tok = copy_token(reinterpret_cast<Token *>(tokens->Head()));
    tok->space = tmpl->space;
	tokens->Set(0, tok);
}

/*
* Macro expander
*/

Token *CCPreprocessor::read_ident() {
    Token *tok = lex();
    if (tok->kind != TIDENT)
		THROW_ERROR_TOKEN(tok, "identifier expected, but got %s", CCHelper::ToString(tok));
    return tok;
}

void CCPreprocessor::expect_newline() {
    Token *tok = lex();
    if (tok->kind != TNEWLINE)
		THROW_ERROR_TOKEN(tok, "newline expected, but got %s", CCHelper::ToString(tok));
}

CCVector *CCPreprocessor::read_one_arg(Token *ident, bool *end, bool readall) {
	auto r = m_cppSweep->AllocateObject<CCVector>();
    int level = 0;
    for (;;) {
        Token *tok = lex();
        if (tok->kind == TEOF)
			THROW_ERROR_TOKEN(ident, "unterminated macro argument list");
        if (tok->kind == TNEWLINE)
            continue;
        if (tok->bol && is_keyword(tok, '#')) {
            read_directive(tok);
            continue;
        }
        if (level == 0 && is_keyword(tok, ')')) {
            unget_token(tok);
            *end = true;
            return r;
        }
        if (level == 0 && is_keyword(tok, ',') && !readall)
            return r;
        if (is_keyword(tok, '('))
            level++;
        if (is_keyword(tok, ')'))
            level--;
        // C11 6.10.3p10: Within the macro argument list,
        // newline is considered a normal whitespace character.
        // I don't know why the standard specifies such a minor detail,
        // but the difference of newline and space is observable
        // if you stringize tokens using #.
        if (tok->bol) {
            tok = copy_token(tok);
            tok->bol = false;
            tok->space = true;
        }
		r->Push(tok);
    }
}

CCVector *CCPreprocessor::do_read_args(Token *ident, Macro *macro) {
	auto r = m_cppSweep->AllocateObject<CCVector>();
    bool end = false;
    while (!end) {
        bool in_ellipsis = (macro->is_varg && r->Length() + 1 == macro->nargs);
		r->Push(read_one_arg(ident, &end, in_ellipsis));
    }
    if (macro->is_varg && r->Length() == macro->nargs - 1)
		r->Push(m_cppSweep->AllocateObject<CCVector>());
    return r;
}

CCVector *CCPreprocessor::read_args(Token *tok, Macro *macro) {
    if (macro->nargs == 0 && is_keyword(peek_token(), ')')) {
        // If a macro M has no parameter, argument list of M()
        // is an empty list. If it has one parameter,
        // argument list of M() is a list containing an empty list.
		return m_cppSweep->AllocateObject<CCVector>();
    }
    CCVector *args = do_read_args(tok, macro);
    if (args->Length() != macro->nargs)
		THROW_ERROR_TOKEN(tok, "macro argument number does not match");
    return args;
}

CCVector *CCPreprocessor::add_hide_set(CCVector *tokens, Set *hideset) {
	auto r = m_cppSweep->AllocateObject<CCVector>();
    for (int i = 0; i < tokens->Length() ; i++) {
        Token *t = copy_token(reinterpret_cast<Token *>(tokens->Get(i)));
        t->hideset = m_makeSet->SetUnion(t->hideset, hideset);
		r->Push(t);
    }
    return r;
}

Token *CCPreprocessor::glue_tokens(Token *t, Token *u) {
	CCBuffer *b = m_cppBuffer->MakeBuffer();
    std::string tstr = CCHelper::ToString(t);
    std::string ustr = CCHelper::ToString(u);

    b->Printf( "%s", toArray(tstr));
    b->Printf( "%s", toArray(ustr));
    Token *r = lex_string(b->Body());
    return r;
}

void CCPreprocessor::glue_push(CCVector *tokens, Token *tok) {
    Token *last = reinterpret_cast<Token *>(tokens->Pop());
	tokens->Push(glue_tokens(last, tok));
}

Token *CCPreprocessor::stringize(Token *tmpl, CCVector *args)
{
	CCBuffer *b = m_cppBuffer->MakeBuffer();
    for (int i = 0; i < args->Length() ; i++) {
        Token *tok = reinterpret_cast<Token *>(args->Get(i));
        if (b->Length() && tok->space)
            b->Printf( " ");

        std::string tokString = CCHelper::ToString(tok);

        b->Printf( "%s", tokString.c_str());
    }
    b->Write( '\0');
    Token *r = copy_token(tmpl);
    r->kind = TSTRING;
    r->sval = b->Body();
    r->slen = b->Length();
    r->enc = ENC_NONE;
    return r;
}

CCVector *CCPreprocessor::expand_all(CCVector *tokens, Token *tmpl) {
    token_buffer_stash(tokens->Reverse());
	auto r = m_cppSweep->AllocateObject<CCVector>();

    for (;;) {
        Token *tok = read_expand();
        if (tok->kind == TEOF)
            break;
		r->Push(tok);
    }
    propagate_space(r, tmpl);
    token_buffer_unstash();
    return r;
}

CCVector *CCPreprocessor::subst(Macro *macro, CCVector *args, Set *hideset) {
	auto r = m_cppSweep->AllocateObject<CCVector>();
    int len = macro->body->Length();

    for (int i = 0; i < len; i++) {
        Token *t0 = reinterpret_cast<Token *>(macro->body->Get(i));
        Token *t1 = (i == len - 1) ? nullptr : reinterpret_cast<Token *>(macro->body->Get( i + 1));
        bool t0_param = (t0->kind == TMACRO_PARAM);
        bool t1_param = (t1 && t1->kind == TMACRO_PARAM);

        if (is_keyword(t0, '#') && t1_param) {
			r->Push(stringize(t0, reinterpret_cast<CCVector *>(args->Get(t1->position))));
            i++;
            continue;
        }
        if (is_keyword(t0, KHASHHASH) && t1_param) {
			CCVector *arg = reinterpret_cast<CCVector *>(args->Get(t1->position));
            // [GNU] [,##__VA_ARG__] is expanded to the empty token sequence
            // if __VA_ARG__ is empty. Otherwise it's expanded to
            // [,<tokens in __VA_ARG__>].
            if (t1->is_vararg && r->Length() > 0 && is_keyword(reinterpret_cast<Token *>(r->Tail()), ',')) {
                if (arg->Length() > 0)
                    r->Append(*arg);
                else
                    r->Pop();
            }
            else if (arg->Length() > 0) {
                glue_push(r, reinterpret_cast<Token *>(arg->Head()));
                for (int i = 1; i < arg->Length(); i++)
                    r->Push(arg->Get(i));
            }
            i++;
            continue;
        }
        if (is_keyword(t0, KHASHHASH) && t1) {
            hideset = t1->hideset;
            glue_push(r, t1);
            i++;
            continue;
        }
        if (t0_param && t1 && is_keyword(t1, KHASHHASH)) {
            hideset = t1->hideset;
			CCVector *arg = reinterpret_cast<CCVector *>(args->Get(t0->position));
            if (arg->Length() == 0)
                i++;
            else
                r->Append(*arg);
            continue;
        }
        if (t0_param) {
            CCVector *arg = reinterpret_cast<CCVector *>(args->Get(t0->position));
			r->Append(*expand_all(arg, t0));
            continue;
        }
		r->Push(t0);
    }
    return add_hide_set(r, hideset);
}

void CCPreprocessor::unget_all(CCVector *tokens) {
    for (int i = tokens->Length() - 1; i >= 0; i--)
        unget_token(reinterpret_cast<Token *>(tokens->Get(i)));
}

// This is "expand" function in the Dave Prosser's document.
Token *CCPreprocessor::read_expand_newline() {
    Token *tok = lex();
    if (tok->kind != TIDENT)
        return tok;
    char *name = tok->sval;
    Macro *macro = reinterpret_cast<Macro *>(m_macros->Get(name));
    if (!macro || m_makeSet->SetHas(tok->hideset, name))
        return tok;

    switch (macro->kind) {
    case MACRO_OBJ:
    {
        Set *hideset = m_makeSet->SetAdd(tok->hideset, name);
        CCVector *tokens = subst(macro, NULL, hideset);
        propagate_space(tokens, tok);
        unget_all(tokens);
        return read_expand();
    }
    case MACRO_FUNC:
    {
        if (!next('('))
            return tok;
        CCVector *args = read_args(tok, macro);
        Token *rparen = peek_token();
        expect(')');
        Set *hideset = m_makeSet->SetAdd(m_makeSet->SetIntersection(tok->hideset, rparen->hideset), name);
        CCVector *tokens = subst(macro, args, hideset);
        propagate_space(tokens, tok);
        unget_all(tokens);
        return read_expand();
    }
    case MACRO_SPECIAL:
    {
        auto macroFn = macro->fn;
        ((macro->owner)->*macroFn)(tok);
        return read_expand();
    }
    default:
		THROW_ERROR({}, "internal error");
        return NULL;
    }
}

Token *CCPreprocessor::read_expand() {
    for (;;) {
        Token *tok = read_expand_newline();
        if (tok->kind != TNEWLINE)
            return tok;
    }
}

bool CCPreprocessor::read_funclike_macro_params(Token *name, CCMap *param) {
    int pos = 0;
    for (;;) {
        Token *tok = lex();
        if (is_keyword(tok, ')'))
            return false;
        if (pos) {
            if (!is_keyword(tok, ','))
                THROW_ERROR_TOKEN(tok, ", expected, but got %s", CCHelper::ToString(tok));
            tok = lex();
        }
        if (tok->kind == TNEWLINE)
            THROW_ERROR_TOKEN(name, "missing ')' in macro parameter list");
        if (is_keyword(tok, KELLIPSIS)) {
            param->Put("__VA_ARGS__", make_macro_token(pos++, true));
            expect(')');
            return true;
        }
        if (tok->kind != TIDENT)
            THROW_ERROR_TOKEN(tok, "identifier expected, but got %s", CCHelper::ToString(tok));
        char *arg = tok->sval;
        if (next(KELLIPSIS)) {
            expect(')');
            param->Put( arg, make_macro_token(pos++, true));
            return true;
        }
        param->Put( arg, make_macro_token(pos++, false));
    }
}

void CCPreprocessor::hashhash_check(CCVector *v) {
    if (v->Length() == 0)
        return;
    if (is_keyword(reinterpret_cast<Token *>(v->Head()), KHASHHASH))
        THROW_ERROR_TOKEN(reinterpret_cast<Token *>(v->Head()), "'##' cannot appear at start of macro expansion");
    if (is_keyword(reinterpret_cast<Token *>(v->Tail()), KHASHHASH))
        THROW_ERROR_TOKEN(reinterpret_cast<Token *>(v->Tail()), "'##' cannot appear at end of macro expansion");
}

CCVector *CCPreprocessor::read_funclike_macro_body(CCMap *param) {
	auto r = m_cppSweep->AllocateObject<CCVector>();
    for (;;) {
        Token *tok = lex();
        if (tok->kind == TNEWLINE)
            return r;
        if (tok->kind == TIDENT) {
            Token *subst = reinterpret_cast<Token *>(param->Get( tok->sval));
            if (subst) {
                subst = copy_token(subst);
                subst->space = tok->space;
                r->Push(subst);
                continue;
            }
        }
        r->Push(tok);
    }
}

void CCPreprocessor::read_funclike_macro(Token *name) {
	CCMap *param = m_cppSweep->AllocateObject<CCMap>();
    bool is_varg = read_funclike_macro_params(name, param);
    CCVector *body = read_funclike_macro_body(param);
    hashhash_check(body);
    Macro *macro = make_func_macro(body, param->Length(), is_varg);
    m_macros->Put( name->sval, macro);
}

void CCPreprocessor::read_obj_macro(char *name) {
	auto body = m_cppSweep->AllocateObject<CCVector>();
    for (;;) {
        Token *tok = lex();
        if (tok->kind == TNEWLINE)
            break;
        body->Push( tok);
    }
    hashhash_check(body);
    m_macros->Put( name, make_obj_macro(body));
}

/*
* #define
*/

void CCPreprocessor::read_define() {
    Token *name = read_ident();
    Token *tok = lex();
    if (is_keyword(tok, '(') && !tok->space) {
        read_funclike_macro(name);
        return;
    }
    unget_token(tok);
    read_obj_macro(name->sval);
}

/*
* #undef
*/

void CCPreprocessor::read_undef() {
    Token *name = read_ident();
    expect_newline();
    m_macros->Remove( name->sval);
}

/*
* #if and the like
*/

Token *CCPreprocessor::read_defined_op() {
    Token *tok = lex();
    if (is_keyword(tok, '(')) {
        tok = lex();
        expect(')');
    }
    if (tok->kind != TIDENT)
        THROW_ERROR_TOKEN(tok, "identifier expected, but got %s", CCHelper::ToString(tok));
    return m_macros->Get( tok->sval) ? cpp_token_one : cpp_token_zero;
}

CCVector *CCPreprocessor::read_intexpr_line() {
	auto r = m_cppSweep->AllocateObject<CCVector>();
    for (;;) {
        Token *tok = read_expand_newline();
        if (tok->kind == TNEWLINE)
            return r;
        if (is_ident(tok, "defined")) {
            r->Push( read_defined_op());
        }
        else if (tok->kind == TIDENT) {
            // C11 6.10.1.4 says that remaining identifiers
            // should be replaced with pp-number 0.
            r->Push( cpp_token_zero);
        }
        else {
            r->Push( tok);
        }
    }
}

bool CCPreprocessor::read_constexpr() {
    token_buffer_stash(read_intexpr_line()->Reverse());
    AstNode *expr = read_expr();
    Token *tok = lex();
    if (tok->kind != TEOF)
        THROW_ERROR_TOKEN(tok, "stray token: %s", CCHelper::ToString(tok));
    token_buffer_unstash();
    return expr->EvaluateIntExpr(nullptr) !=0 ;
}

void CCPreprocessor::do_read_if(bool istrue) {
    m_cond_incl_stack->Push( make_cond_incl(istrue));
    if (!istrue)
        skip_cond_incl();
}

void CCPreprocessor::read_if() {
    do_read_if(read_constexpr());
}

void CCPreprocessor::read_ifdef() {
    Token *tok = lex();
    if (tok->kind != TIDENT)
        THROW_ERROR_TOKEN(tok, "identifier expected, but got %s", CCHelper::ToString(tok));
    expect_newline();
    do_read_if(m_macros->Get( tok->sval) != nullptr);
}

void CCPreprocessor::read_ifndef() {
    Token *tok = lex();
    if (tok->kind != TIDENT)
        THROW_ERROR_TOKEN(tok, "identifier expected, but got %s", CCHelper::ToString(tok));
    expect_newline();
    do_read_if(!m_macros->Get( tok->sval));
    if (tok->count == 2) {
        // "ifndef" is the second token in this file.
        // Prepare to detect an include guard.
        CondIncl *ci = reinterpret_cast<CondIncl *>(m_cond_incl_stack->Tail());
        ci->include_guard = tok->sval;
        ci->file = tok->file;
    }
}

void CCPreprocessor::read_else(Token *hash) {
    if (m_cond_incl_stack->Length() == 0)
        THROW_ERROR_TOKEN(hash, "stray #else");
    CondIncl *ci = reinterpret_cast<CondIncl *>(m_cond_incl_stack->Tail());
    if (ci->ctx == IN_ELSE)
        THROW_ERROR_TOKEN(hash, "#else appears in #else");
    expect_newline();
    ci->ctx = IN_ELSE;
    ci->include_guard = NULL;
    if (ci->wastrue)
        skip_cond_incl();
}

void CCPreprocessor::read_elif(Token *hash) {
    if (m_cond_incl_stack->Length() == 0)
        THROW_ERROR_TOKEN(hash, "stray #elif");
    CondIncl *ci = reinterpret_cast<CondIncl *>(m_cond_incl_stack->Tail());
    if (ci->ctx == IN_ELSE)
		THROW_ERROR_TOKEN(hash, "#elif after #else");
    ci->ctx = IN_ELIF;
    ci->include_guard = NULL;
    if (ci->wastrue || !read_constexpr()) {
        skip_cond_incl();
        return;
    }
    ci->wastrue = true;
}

// Skips all newlines and returns the first non-newline token.
Token *CCPreprocessor::skip_newlines() {
    Token *tok = lex();
    while (tok->kind == TNEWLINE)
        tok = lex();
    unget_token(tok);
    return tok;
}

void CCPreprocessor::read_endif(Token *hash) {
    if (m_cond_incl_stack->Length() == 0)
		THROW_ERROR_TOKEN(hash, "stray #endif");
    CondIncl *ci = reinterpret_cast<CondIncl *>(m_cond_incl_stack->Pop());
    expect_newline();

    // Detect an #ifndef and #endif pair that guards the entire
    // header file. Remember the macro name guarding the file
    // so that we can skip the file next time.
    if (!ci->include_guard || ci->file != hash->file)
        return;
    Token *last = skip_newlines();
    if (ci->file != last->file)
        m_include_guard->Put(ci->file->name, ci->include_guard);
}

/*
* #error and #warning
*/

char *CCPreprocessor::read_error_message() {
	CCBuffer *b = m_cppBuffer->MakeBuffer();
    for (;;) {
        Token *tok = lex();
        if (tok->kind == TNEWLINE)
            return b->Body();
        if (b->Length() != 0 && tok->space)
            b->Write( ' ');

        std::string tokString = CCHelper::ToString(tok);

        b->Printf( "%s", tokString.c_str());
    }
}

void CCPreprocessor::read_error(Token *hash) {
	THROW_ERROR_TOKEN(hash, "#error: %s", read_error_message());
}

void CCPreprocessor::read_warning(Token *hash) {
    THROW_WARNING_TOKEN(hash, "#warning: %s", read_error_message());
}

/*
* #include
*/

static char *join_paths(CCBuffer &b, CCVector *args) {
    for (int i = 0; i < args->Length(); i++)
    {
        std::string s = CCHelper::ToString(reinterpret_cast<Token *>(args->Get(i)));
        b.Printf("%s", s.c_str());
    }
    return b.Body();
}

char *CCPreprocessor::read_cpp_header_name(Token *hash, bool *std) {
    // Try reading a filename using a special tokenizer for #include.
    char *path = read_header_file_name(std);
    if (path)
        return path;

    // If a token following #include does not start with < nor ",
    // try to read the token as a regular token. Macro-expanded
    // form may be a valid header file path.
    Token *tok = read_expand_newline();
    if (tok->kind == TNEWLINE)
		THROW_ERROR_TOKEN(hash, "expected filename, but got newline");
    if (tok->kind == TSTRING) {
        *std = false;
        return tok->sval;
    }
    if (!is_keyword(tok, '<'))
		THROW_ERROR_TOKEN(tok, "< expected, but got %s", CCHelper::ToString(tok));
	CCVector *tokens = m_cppSweep->AllocateObject<CCVector>();
    for (;;) {
        Token *tok = read_expand_newline();
        if (tok->kind == TNEWLINE)
			THROW_ERROR_TOKEN(hash, "premature end of header name");
        if (is_keyword(tok, '>'))
            break;
        tokens->Push( tok);
    }
    *std = true;
    return join_paths(*m_cppBuffer->MakeBuffer(), tokens);
}

bool CCPreprocessor::guarded(const char *path) {
    char *guard = reinterpret_cast<char *>(m_include_guard->Get(path));
    bool r = (guard && m_macros->Get( guard));
    define_obj_macro("__8cc_include_guard", r ? cpp_token_one : cpp_token_zero);
    return r;
}

bool CCPreprocessor::try_include(const char *dir, const char *filename, bool isimport) {
	std::string *path = m_cppSweep->AllocateObject<std::string>(stringFormat("%s\\%s", dir, filename));

    if (m_once->Get( path->data()))
        return true;
    if (guarded(path->c_str()))
        return true;
    FILE *fp = fopen(path->c_str(), "r");
    if (!fp)
        return false;
    if (isimport)
        m_once->Put( path->c_str(), (void *)1);
	
    m_filestack->stream_push(CCFile::make_file(fp, path->c_str()));
    return true;
}

void CCPreprocessor::read_include(Token *hash, File *file, bool isimport) {
    bool std;
    char *filename = read_cpp_header_name(hash, &std);
    expect_newline();
    if (filename[0] == '/') {
        if (try_include("/", filename, isimport))
            return;
        goto err;
    }
    if (!std) {
        PathManager currentPath;

        currentPath.SetUrl(ThisFileName());
        if (try_include(currentPath.BasePath(), filename, isimport))
            return;
    }
    for (int i = 0; i < m_std_include_path->Length(); i++)
        if (try_include(reinterpret_cast<char *>(m_std_include_path->Get(i)), filename, isimport))
            return;
err:
	THROW_ERROR_TOKEN(hash, "cannot find header file: %s", filename);
}

void CCPreprocessor::read_include_next(Token *hash, File *file) {
    // [GNU] #include_next is a directive to include the "next" file
    // from the search path. This feature is used to override a
    // header file without getting into infinite inclusion loop.
    // This directive doesn't distinguish <> and "".
    bool std;
    char *filename = read_cpp_header_name(hash, &std);

    do
    {
        expect_newline();
        if (filename[0] == '/') {
            if (try_include("/", filename, false))
                return;            
            break;  //goto err;
        }
        std::string curstr = m_pathMan->FullPath(file->name);
        //char *cur = fullpath(file->name);
        int i = 0;
        for (; i < m_std_include_path->Length(); i++) {
            char *dir = reinterpret_cast<char *>(m_std_include_path->Get( i));
			std::string fullPath = m_pathMan->FullPath(stringFormat("%s/%s", dir, filename));

			if (curstr == fullPath)	//if (!strcmp(curstr.c_str(), fullPath.c_str()))
                break;
        }
        for (i++; i < m_std_include_path->Length(); i++)
            if (try_include(reinterpret_cast<char *>(m_std_include_path->Get( i)), filename, false))
                return;
    }
    while (false);
//err:
	THROW_ERROR_TOKEN(hash, "cannot find header file: %s", filename);
}

/*
* #pragma
*/

void CCPreprocessor::parse_pragma_operand(Token *tok) {
    char *s = tok->sval;
    if (!strcmp(s, "once")) {
        std::string *path = m_cppSweep->AllocateObject<std::string>( m_pathMan->FullPath(tok->file->name) );

        m_once->Put( path->c_str(), (void *)1);
    }
    else if (!strcmp(s, "enable_warning")) {
        //enable_warning = true;
		//Todo. need implementation
		MY_PRINT() << "no enable_warning implementation";
    }
    else if (!strcmp(s, "disable_warning")) {
        //enable_warning = false;
		MY_PRINT() << "no disable_warning implementation";
    }
    else {
		THROW_ERROR_TOKEN(tok, "unknown #pragma: %s", s);
    }
}

void CCPreprocessor::read_pragma() {
    Token *tok = read_ident();
    parse_pragma_operand(tok);
}

/*
* #line
*/

static bool is_digit_sequence(char *p) {
    for (; *p; p++)
        if (!isdigit(*p))
            return false;
    return true;
}

void CCPreprocessor::read_line() {
    Token *tok = read_expand_newline();
    if (tok->kind != TNUMBER || !is_digit_sequence(tok->sval))
		THROW_ERROR_TOKEN(tok, "number expected after #line, but got %s", CCHelper::ToString(tok));
    int line = atoi(tok->sval);
    tok = read_expand_newline();
    char *filename = NULL;
    if (tok->kind == TSTRING) {
        filename = tok->sval;
        expect_newline();
    }
    else if (tok->kind != TNEWLINE) {
		THROW_ERROR_TOKEN(tok, "newline or a source name are expected, but got %s", CCHelper::ToString(tok));
    }
    File *f = m_filestack->current_file();
    f->line = line;
    if (filename)
        f->name = filename;
}

// GNU CPP outputs "# linenum filename flags" to preserve original
// source file information. This function reads them. Flags are ignored.
void CCPreprocessor::read_linemarker(Token *tok) {
    if (!is_digit_sequence(tok->sval))
		THROW_ERROR_TOKEN(tok, "line number expected, but got %s", CCHelper::ToString(tok));
    int line = atoi(tok->sval);
    tok = lex();
    if (tok->kind != TSTRING)
		THROW_ERROR_TOKEN(tok, "filename expected, but got %s", CCHelper::ToString(tok));
    char *filename = tok->sval;
    do {
        tok = lex();
    }
    while (tok->kind != TNEWLINE);
    File *file = m_filestack->current_file();
    file->line = line;
    file->name = filename;
}

/*
* #-directive
*/

void CCPreprocessor::read_directive(Token *hash) {
    Token *tok = lex();
    if (tok->kind == TNEWLINE)
        return;
    if (tok->kind == TNUMBER) {
        read_linemarker(tok);
        return;
    }
    if (tok->kind != TIDENT)
        goto err;
    char *s = tok->sval;
    if (!strcmp(s, "define"))            read_define();
    else if (!strcmp(s, "elif"))         read_elif(hash);
    else if (!strcmp(s, "else"))         read_else(hash);
    else if (!strcmp(s, "endif"))        read_endif(hash);
    else if (!strcmp(s, "error"))        read_error(hash);
    else if (!strcmp(s, "if"))           read_if();
    else if (!strcmp(s, "ifdef"))        read_ifdef();
    else if (!strcmp(s, "ifndef"))       read_ifndef();
    else if (!strcmp(s, "import"))       read_include(hash, tok->file, true);
    else if (!strcmp(s, "include"))      read_include(hash, tok->file, false);
    else if (!strcmp(s, "include_next")) read_include_next(hash, tok->file);
    else if (!strcmp(s, "line"))         read_line();
    else if (!strcmp(s, "pragma"))       read_pragma();
    else if (!strcmp(s, "undef"))        read_undef();
    else if (!strcmp(s, "warning"))      read_warning(hash);
    else goto err;
    return;

err:
	THROW_ERROR_TOKEN(hash, "unsupported preprocessor directive: %s", CCHelper::ToString(tok));
}

/*
* Special macros
*/

void CCPreprocessor::make_token_pushback(Token *tmpl, int kind, char *sval) {
    Token *tok = copy_token(tmpl);
    tok->kind = kind;
    tok->sval = sval;
    tok->slen = strlen(sval) + 1;
    tok->enc = ENC_NONE;
    unget_token(tok);
}

void CCPreprocessor::handle_date_macro(Token *tmpl) {
    char buf[20];
    strftime(buf, sizeof(buf), "%b %e %Y", *m_buildTime/* &now*/);
    make_token_pushback(tmpl, TSTRING, strdup(buf));
}

void CCPreprocessor::handle_time_macro(Token *tmpl) {
    char buf[10];
    strftime(buf, sizeof(buf), "%T", *m_buildTime);
    make_token_pushback(tmpl, TSTRING, strdup(buf));
}

void CCPreprocessor::handle_timestamp_macro(Token *tmpl) {
    // [GNU] __TIMESTAMP__ is expanded to a string that describes the date
    // and time of the last modification time of the current source file.
    char buf[30];
    strftime(buf, sizeof(buf), "%a %b %e %T %Y", localtime(&tmpl->file->mtime));
    make_token_pushback(tmpl, TSTRING, strdup(buf));
}

void CCPreprocessor::handle_file_macro(Token *tmpl) {
    make_token_pushback(tmpl, TSTRING, tmpl->file->name);
}

void CCPreprocessor::handle_line_macro(Token *tmpl) {
    make_token_pushback(tmpl, TNUMBER, m_cppBuffer->Format("%d", tmpl->file->line));
}

void CCPreprocessor::handle_pragma_macro(Token *tmpl) {
    expect('(');
    Token *operand = read_token();
    if (operand->kind != TSTRING)
		THROW_ERROR_TOKEN(operand, "_Pragma takes a string literal, but got %s", CCHelper::ToString(operand));
    expect(')');
    parse_pragma_operand(operand);
    make_token_pushback(tmpl, TNUMBER, "1");
}

void CCPreprocessor::handle_base_file_macro(Token *tmpl) {
    char *url = const_cast<char *>(m_pathMan->BasePath());

    make_token_pushback(tmpl, TSTRING, url);
}

void CCPreprocessor::handle_counter_macro(Token *tmpl) {
    static int counter = 0;
    make_token_pushback(tmpl, TNUMBER, m_cppBuffer->Format("%d", counter++));
}

void CCPreprocessor::handle_include_level_macro(Token *tmpl) {
    make_token_pushback(tmpl, TNUMBER, m_cppBuffer->Format("%d", m_filestack->stream_depth() - 1));
}

/*
* Initializer
*/

void CCPreprocessor::add_include_path(char *path) {
    m_std_include_path->Push( path);
}

void CCPreprocessor::define_obj_macro(char *name, Token *value) {
    m_macros->Put( name, make_obj_macro(m_cppSweep->AllocateObject<CCVector>(value)));
}

void CCPreprocessor::define_special_macro(char *name, SpecialMacroHandler fn) {
    m_macros->Put( name, make_special_macro(fn));
}

#define BUILD_DIR ""

void CCPreprocessor::init_predefined_macros() {
    m_std_include_path->Push(BUILD_DIR "/include");
    m_std_include_path->Push("/usr/local/lib/8cc/include");
    m_std_include_path->Push("/usr/local/include");
    m_std_include_path->Push("/usr/include");
    m_std_include_path->Push("/usr/include/linux");
    m_std_include_path->Push("/usr/include/x86_64-linux-gnu");

    define_special_macro("__DATE__", &CCPreprocessor::handle_date_macro);
    define_special_macro("__TIME__", &CCPreprocessor::handle_time_macro);
    define_special_macro("__FILE__", &CCPreprocessor::handle_file_macro);
    define_special_macro("__LINE__", &CCPreprocessor::handle_line_macro);
    define_special_macro("_Pragma", &CCPreprocessor::handle_pragma_macro);
    // [GNU] Non-standard macros
    define_special_macro("__BASE_FILE__", &CCPreprocessor::handle_base_file_macro);
    define_special_macro("__COUNTER__", &CCPreprocessor::handle_counter_macro);
    define_special_macro("__INCLUDE_LEVEL__", &CCPreprocessor::handle_include_level_macro);
    define_special_macro("__TIMESTAMP__", &CCPreprocessor::handle_timestamp_macro);

    //read_from_string("#include <" BUILD_DIR "/include/8cc.h>");
}

void CCPreprocessor::cpp_init()
{
    m_buildTime = std::make_unique<CCTime>();
    init_predefined_macros();
}

/*
* Public intefaces
*/

Token *CCPreprocessor::maybe_convert_keyword(Token *tok) {
    if (tok->kind != TIDENT)
        return tok;
    int id = (intptr_t)keywords->Get( tok->sval);
    if (!id)
        return tok;
    Token *r = copy_token(tok);
    r->kind = TKEYWORD;
    r->id = id;
    return r;
}

void CCPreprocessor::InitializeUnit()
{
	CCLex::InitializeUnit();

	cpp_init();
}

// Reads from a string as if the string is a content of input file.
// Convenient for evaluating small string snippet contaiing preprocessor macros.
//void CCPreprocessor::read_from_string(char *buf) {
//    m_filestack->stream_stash(m_filestack->make_file_string(buf));
//    Vector *toplevels = read_toplevels();
//    for (int i = 0; i < vec_len(toplevels); i++)
//        emit_toplevel(reinterpret_cast<Node *>(vec_get(toplevels, i)));
//    m_filestack->stream_unstash();
//}

Token *CCPreprocessor::peek_token() {
    Token *r = read_token();
    unget_token(r);
    return r;
}

Token *CCPreprocessor::read_token() {
    Token *tok;
    for (;;) {
        tok = read_expand();
        if (tok->bol && is_keyword(tok, '#') && tok->hideset == NULL) {
            read_directive(tok);
            continue;
        }
		LOGIC_ASSERT(tok->kind < MIN_CPP_TOKEN);
        return maybe_convert_keyword(tok);
    }
}
