// Copyright 2012 Rui Ueyama. Released under the MIT license.

#include "8cc.h"
#include "hook/ast/astNode.h"
#include "hook/ast/astType.h"
#include "error.h"
#include "debug.h"
#include "buffer.h"
#include "dict.h"
#include "hook/ast/astVector.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

#define _MODIFICATION_

std::string CCHelper::decorate_int(const char *name, const AstType *ty)
{
    const char *u = (ty->Unsigned()) ? "u" : "";
    
    if (ty->BitSize() > 0)
        return stringFormat("%s%s:%d:%d", u, name, ty->BitOffset(), ty->BitOffset() + ty->BitSize());
    return stringFormat("%s%s", u, name);
}

std::string CCHelper::doTypeToString(CCBufferAllocator &factory, CCDict &dict, const AstType *ty)
{
    if (!ty)
        return "(nil)";
    switch (ty->Kind()) {
    case KIND_VOID: return "void";
    case KIND_BOOL: return "_Bool";
    case KIND_CHAR: return decorate_int("char", ty);
    case KIND_SHORT: return decorate_int("short", ty);
    case KIND_INT:  return decorate_int("int", ty);
    case KIND_LONG: return decorate_int("long", ty);
    case KIND_LLONG: return decorate_int("llong", ty);
    case KIND_FLOAT: return "float";
    case KIND_DOUBLE: return "double";
    case KIND_LDOUBLE: return "long_double";
    case KIND_PTR:
        return stringFormat("*%s", doTypeToString(factory, dict, static_cast<const AstPointerType *>(ty)->GetPTR()));
    case KIND_ARRAY:
        return stringFormat("[%d]%s", static_cast<const AstArrayType *>(ty)->GetLength(), doTypeToString(factory, dict, static_cast<const AstPointerType *>(ty)->GetPTR()));
    case KIND_STRUCT:
    {
        const AstStructType *strtTy = static_cast<const AstStructType *>(ty);
        char *kind = strtTy->IsStruct() ? "struct" : "union";

		if (dict.Get(factory.Format("%p", ty)))
            return stringFormat("(%s)", kind);
        dict.Put( factory.Format("%p", ty), (void *)1);
        if (strtTy->ValidField())
		{
            CCBuffer *b = factory.MakeBuffer();
            
			b->Printf( "(%s", kind);
            auto keys = strtTy->GetFieldKeys();
            for (uint32_t i = 0; i < keys.size() ; i++) {
                //const char *key =  //reinterpret_cast<const char *>(keys->Get( i));
                AstType *fieldtype = strtTy->GetFieldType(keys[i].c_str());
                b->Printf( " (%s)", toArray(doTypeToString(factory, dict, fieldtype)));
            }
            b->Printf( ")");
            return b->Body();
        }
    }
    case KIND_FUNC:
    {
        CCBuffer *b = factory.MakeBuffer();
        const AstFunctionType *fnTy = static_cast<const AstFunctionType *>(ty);
        AstVector *params = fnTy->Params();

        b->Printf( "(");
        if (params) {
            for (int i = 0; i < params->Length(); i++) {
                if (i > 0)
                    b->Printf( ",");
                AstType *t = reinterpret_cast<AstType *>(params->Get( i));
                b->Printf("%s", toArray(doTypeToString(factory, dict, t)));
            }
        }
        b->Printf(")=>%s", toArray(doTypeToString(factory, dict, fnTy->ReturnType())));
        return b->Body();
    }
    default:
        return stringFormat("(Unknown ty: %d)", ty->Kind());
    }
}

static char *encoding_prefix(int enc) {
    switch (enc) {
    case ENC_CHAR16: return "u";
    case ENC_CHAR32: return "U";
    case ENC_UTF8:   return "u8";
    case ENC_WCHAR:  return "L";
    }
    return "";
}

std::string CCHelper::ToString(const Token *tok)
{
	if (!tok)
		return "(null)";

	switch (tok->kind)
	{
	case TIDENT:
		return tok->sval;
	case TKEYWORD:
		switch (tok->id)
		{
#define op(id, str)         case id: return str;
#define keyword(id, str, _) case id: return str;
#include "keyword.inc"
#undef keyword
#undef op
		default: return stringFormat("%c", tok->id);
		}
	case TCHAR:
	{
		CCBufferAllocator buff;

		return stringFormat("%s'%s'", encoding_prefix(tok->enc), buff.QuoteChar(tok->c));
	}
	case TNUMBER:
		return tok->sval;
	case TSTRING:
	{
		CCBufferAllocator buff;

		return stringFormat("%s\"%s\"", encoding_prefix(tok->enc), buff.QuoteString(tok->sval));
	}
	case TEOF:
		return "(eof)";
	case TINVALID:
		return stringFormat("%c", tok->c);
	case TNEWLINE:
		return "(newline)";
	case TSPACE:
		return "(space)";
	case TMACRO_PARAM:
		return "(macro-param)";
	default:
		THROW_ERROR_NOPOS("internal error: unknown token kind: %d", tok->kind);
		return "(error)";
	}
}

std::string CCHelper::ToString(const AstType *ty)
{
	CCBufferAllocator factory;
	CCDict dict;

	return doTypeToString(factory, dict, ty);
}
