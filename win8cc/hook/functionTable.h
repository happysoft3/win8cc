
#ifndef FUNCTION_TABLE_H__
#define FUNCTION_TABLE_H__

#include <list>
#include <map>
#include <string>
#include <memory>
#include <functional>

class NoxFunction;

class FunctionTable
{
private:
	using function_list_ty = std::list<std::unique_ptr<NoxFunction>>;
	using function_list_iter = function_list_ty::iterator;
	function_list_ty m_functionList;
	std::map<std::string, function_list_iter> m_functionMap;
	std::weak_ptr<FunctionTable> m_topTable;

public:
	FunctionTable();
    virtual ~FunctionTable();

protected:
	void pushImpl(const char *const fnKey, std::unique_ptr<NoxFunction> fn, bool prohibitCover);

public:
	void SetParentTable(std::weak_ptr<FunctionTable> parent);
	virtual NoxFunction *Push(std::unique_ptr<NoxFunction> fn);
	NoxFunction *Push(const std::string &fnKey, std::unique_ptr<NoxFunction> fn);
	std::unique_ptr<NoxFunction> Pop();
	NoxFunction *Retrieve(const std::string &name, bool recursion = false) const;
	std::unique_ptr<NoxFunction> PopWithName(const std::string &name);
	int Count() const
	{
		return m_functionList.size();
	}
	void Preorder(std::function<void(NoxFunction&)> &&fn);
    NoxFunction *Front() const;
};

#endif

