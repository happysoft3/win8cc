
#ifndef PORTABLE_LINK_DATA_H__
#define PORTABLE_LINK_DATA_H__

#include "common/include/functionHelper.h"
#include <memory>

class FunctionTable;
class CDataSection;

template <class Ty>
class WrappedLinkData
{
private:
	std::unique_ptr<Ty> m_data;

public:
	WrappedLinkData() { }
	~WrappedLinkData() { }

	void Put(std::unique_ptr<Ty> data)
	{
		m_data = std::move(data);
	}
	Ty *Get() const
	{
		if (!m_data)
			throw std::exception(__FUNCSIG__);
		return m_data.get();
	}
	template <class MemberFn, class... Args>
	auto InvokeImpl(MemberFn &&fn, Args&&... args)
		-> typename decltype((std::declval<Ty>().*std::forward<MemberFn>(std::declval<Args>()...)))
	{
		if (!m_data)
			throw std::exception(__FUNCSIG__);
		return ((*m_data).*fn)(std::forward<Args>(args)...);
	}
};

class PortableLinkData
{
private:
	WrappedLinkData<FunctionTable> m_fnTable;
	WrappedLinkData<CDataSection> m_dataField;

public:
	PortableLinkData();
	~PortableLinkData();

private:
	template <class Ty>
	WrappedLinkData<Ty> &loadData();

	template <>
	WrappedLinkData<FunctionTable> &loadData()
	{
		return m_fnTable;
	}

	template <>
	WrappedLinkData<CDataSection> &loadData()
	{
		return m_dataField;
	}

public:
	template <class Ty>
	void Put(std::unique_ptr<Ty> data)
	{
		auto &obj = loadData<Ty>();

		obj.Put(std::move(data));
	}
	template <class Ty, class... Args>
	void Emplace(Args&&... args)
	{
		auto &obj = loadData<Ty>();

		obj.Put(std::make_unique<Ty>(std::forward<Args>(args)...));
	}
	template <class Ty>
	Ty *Get() const
	{
		auto &obj = loadData<Ty>();

		return obj.Get();
	}
	template <class MemberFn, class... Args>
	auto Invoke(MemberFn &&fn, Args&&... args)
		-> typename _FunctionHelper::FunctionHelper<typename std::decay<decltype(fn)>::type>::ret_type
	{
		using my_obj_ty = typename _FunctionHelper::FunctionHelper<std::remove_reference<decltype(fn)>::type>::object_type;
		auto &obj = loadData<my_obj_ty>();	//인라인이 될 것이라 기대합니다

		return obj.InvokeImpl(std::forward<Args>(args)...);
	}
};

#endif

