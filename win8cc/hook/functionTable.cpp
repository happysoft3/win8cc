
#include "functionTable.h"
#include "noxFunction.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

FunctionTable::FunctionTable()
{ }

FunctionTable::~FunctionTable()
{ }

void FunctionTable::pushImpl(const char *const fnKey, std::unique_ptr<NoxFunction> fn, bool prohibitCover)
{
	if (!fnKey)
		throw std::logic_error("key is null");

	auto fnIterator = m_functionMap.find(fnKey);

	if (fnIterator == m_functionMap.cend())
	{
		m_functionList.push_back(std::move(fn));
		m_functionMap[fnKey] = std::prev(m_functionList.end());
	}
	else if (!prohibitCover)
		*fnIterator->second = std::move(fn);
	else
	{
		throw std::logic_error(
			stringFormat("%s:conflict %s function '%s'", __FUNCDNAME__, fn->IsStatic() ? "static" : "global", fn->Name()));
	}
}

void FunctionTable::SetParentTable(std::weak_ptr<FunctionTable> parent)
{
	auto top = parent.lock();

	if (!top)
		return;

	if (this != top.get())
		m_topTable = parent;
}

NoxFunction *FunctionTable::Push(std::unique_ptr<NoxFunction> fn/*, bool dontDuplicate*/)
{
	NoxFunction *ret = fn.get();
	auto fName = fn->Name();			//25, Oct 2022-14:02

	pushImpl(fName, std::move(fn), true);	//Crashed after NoxFunction object was moved into calling function
	return ret;
}

NoxFunction *FunctionTable::Push(const std::string &fnKey, std::unique_ptr<NoxFunction> fn)
{
	NoxFunction *ret = fn.get();

	pushImpl(fnKey.c_str(), std::move(fn), true);
	return ret;
}

std::unique_ptr<NoxFunction> FunctionTable::Pop()
{
	if (m_functionList.empty())
		return {};

	auto ret = std::move(m_functionList.front());

	m_functionList.pop_front();
	return ret;
}

NoxFunction *FunctionTable::Retrieve(const std::string &name, bool recursion) const
{
	auto functionIterator = m_functionMap.find(name);

	if (functionIterator == m_functionMap.cend())
	{
		if (recursion)
		{
			auto parent = m_topTable.lock();

			if (parent)
				return parent->Retrieve(name, recursion);
		}
		return nullptr;
	}

	return (*functionIterator->second).get();
}

std::unique_ptr<NoxFunction> FunctionTable::PopWithName(const std::string &name)
{
	auto functionIterator = m_functionMap.find(name);

	if (functionIterator == m_functionMap.cend())
		return {};

	std::unique_ptr<NoxFunction> ret = std::move(*functionIterator->second);

	m_functionList.erase(functionIterator->second);
	return ret;
}

void FunctionTable::Preorder(std::function<void(NoxFunction&)> &&fn)
{
	for (auto &noxFn : m_functionList)
		fn(*noxFn);
}

NoxFunction *FunctionTable::Front() const
{
    if (m_functionList.empty())
        return nullptr;

    return m_functionList.front().get();
}
