
#include "cCResult.h"
#include "hook/noxCodegen.h"
#include "parse.h"
#include "hook/functionTable.h"
#include "hook/cdata/cDataSection.h"
#include "common/utils/mydebug.h"

CCResult::CCResult()
    : TaskResult()
{ }

CCResult::CCResult(TaskUnit &task)
    : TaskResult(task)
{ }

CCResult::~CCResult()
{ }

void CCResult::MakeResult(NoxCodegen &gen)
{
	m_fnTable = gen.ReleaseFunctionTable();
	m_field = gen.ReleaseField();
}

std::unique_ptr<FunctionTable> CCResult::DetachTable()
{
	return m_fnTable ? std::move(m_fnTable) : nullptr;
}

std::unique_ptr<CDataSection> CCResult::DetachField()
{
	return m_field ? std::move(m_field) : nullptr;
}

const char *const CCResult::SourceFile() const
{
    if (m_sourceFileName.empty())
        MY_THROW() << "name is empty";

    return m_sourceFileName.c_str();
}
