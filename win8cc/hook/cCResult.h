
#ifndef C_C_RESULT_H__
#define C_C_RESULT_H__

#include "common/taskthread/taskResult.h"
#include <memory>
#include <stdexcept>

class CDataSection;
class FunctionTable;
class NoxCodegen;

class CCResult : public TaskResult
{
private:
	std::unique_ptr<CDataSection> m_field;
	std::unique_ptr<FunctionTable> m_fnTable;
    std::string m_sourceFileName;

public:
	CCResult();
    CCResult(TaskUnit &task);
	~CCResult() override;

	void MakeResult(NoxCodegen &gen);
	std::unique_ptr<FunctionTable> DetachTable();
	std::unique_ptr<CDataSection> DetachField();
    const char *const SourceFile() const;
    void PutSourceFile(const std::string &fn)
    {
        m_sourceFileName = fn;
    }
};

#endif

