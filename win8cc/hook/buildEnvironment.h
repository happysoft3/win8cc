
#ifndef BUILD_ENVIRONMENT_H__
#define BUILD_ENVIRONMENT_H__

#include "systemFileParser/iniFileMan.h"
#include <list>
#include <functional>
#include <map>
#include <set>

class PathManager;
class BuildSourceFile;

class BuildEnvironment : public IniFileMan
{
    class EnvParser;
private:
	std::shared_ptr<PathManager> m_pathman;
	std::unique_ptr<PathManager> m_outTargetPath;
	std::list<std::unique_ptr<BuildSourceFile>> m_sourceFiles;
	std::function<void(const std::exception &)> m_exceptCapture;
	std::map<std::string, std::string> m_optionMap;
    std::set<std::string> m_eudloaderCallFunctions;    //EUD 로더를 호출하는, 코드를 삽입할 함수 이름입니다//

public:
	BuildEnvironment();
	~BuildEnvironment() override;

private:
    std::unique_ptr<IniParser> loadParser() override;
	std::unique_ptr<PathManager> loadBasepath(const std::string &altPath);
	std::unique_ptr<PathManager> loadOutTarget();
	void loadSourceFiles();
    void putSourceFile(const std::string &sourceUrl);
    void putInitEudCallFunctionName(const std::string &fname);

public:
	int SourceFileCount() const
	{
		return m_sourceFiles.size();
	}

private:
	void catchException(const std::exception &except) override;

public:
	void PutExceptCapture(std::function<void(const std::exception &)> &&capture);
	bool ReadIni(const std::string &inifile) override;
	void DoSourceFiles(std::function<void(const BuildSourceFile &)> &&proc);
	std::shared_ptr<PathManager> GetPath() const
	{
		return m_pathman;
	}
	std::string OutTargetPath() const;
	const char *const GetOption(const std::string &s) const;
    bool GetOptionAsInt(const std::string &s, int &dest) const;

private:
    void setOption(const std::string &key, const std::string &val);

public:
	bool UseDebug() const;
    void DoPreorderEudLoaderCallTarget(std::function<void(const std::string &)> &&f);
};

#endif

