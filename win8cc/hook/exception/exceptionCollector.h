
#ifndef EXCEPTION_COLLECTOR_H__
#define EXCEPTION_COLLECTOR_H__

#include <list>
#include <memory>

class ExceptionCollector
{
private:
	std::list<std::unique_ptr<std::exception>> m_exceptions;

public:
	ExceptionCollector(bool warnIsError = false);
	~ExceptionCollector();
};

#endif

