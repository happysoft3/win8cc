
#ifndef CODE_OFFSET_H__
#define CODE_OFFSET_H__

#include "cclink.h"

class CodeOffset : public CCLink
{
public:
	CodeOffset();
	~CodeOffset() override;

private:
	bool Linked() const override;
};

#endif

