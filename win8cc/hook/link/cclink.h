
#ifndef CCLINK_H__
#define CCLINK_H__

#include <memory>

class CCLink
{
private:
	std::unique_ptr<int> m_link;

public:
	CCLink();
	virtual ~CCLink();

protected:
	bool getLinkDirect(int &link) const;

public:
	virtual bool Linked() const
	{
		return m_link ? true : false;
	}
	int Link() const;
	void Change(int want);
	void MakeLink(int link);
};

#endif

