
#include "cclink.h"
#include "common/utils/myDebug.h"

CCLink::CCLink()
{ }

CCLink::~CCLink()
{ }

bool CCLink::getLinkDirect(int &link) const
{
	if (!m_link)
		return false;

	link = *m_link;
	return true;
}

int CCLink::Link() const
{
	if (!Linked())
		MY_THROW() << "has no link";

	return *m_link;
}

void CCLink::Change(int want)
{
	if (!m_link)
		MY_THROW() << "has no link";
	*m_link = want;
}

void CCLink::MakeLink(int link)
{
	if (m_link)
		MY_THROW() << "this object had already linked";
	m_link = std::make_unique<int>(link);
}
