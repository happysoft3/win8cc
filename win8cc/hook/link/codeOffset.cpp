
#include "codeOffset.h"

static int unsolved_value = -1;

CodeOffset::CodeOffset()
	: CCLink()
{
	MakeLink(unsolved_value);
}

CodeOffset::~CodeOffset()
{ }

bool CodeOffset::Linked() const
{
	if (!CCLink::Linked())
		return false;

	int link = unsolved_value;

	if (!getLinkDirect(link))
		return false;

	return (link != unsolved_value);
}
