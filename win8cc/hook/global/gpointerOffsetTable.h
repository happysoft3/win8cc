
#ifndef G_POINTER_OFFSET_TABLE_H__
#define G_POINTER_OFFSET_TABLE_H__

#include <memory>
#include <list>
#include <functional>

class CCLink;

class GPointerOffsetTable
{
private:
	std::list<std::shared_ptr<CCLink>> m_offsetList;

public:
	GPointerOffsetTable();
	~GPointerOffsetTable();

	int Count() const
	{
		return static_cast<int>(m_offsetList.size());
	}
	void Push(std::shared_ptr<CCLink> &link);
	void Preorder(std::function<void(CCLink &)> &&fn);
};

#endif

