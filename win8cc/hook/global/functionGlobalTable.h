
#ifndef FUNCTION_GLOBAL_TABLE_H__
#define FUNCTION_GLOBAL_TABLE_H__

#include "hook/functiontable.h"

class FunctionGlobalTable : public FunctionTable
{
public:
    FunctionGlobalTable();
    ~FunctionGlobalTable() override;

private:
    NoxFunction *Push(std::unique_ptr<NoxFunction> fn) override;
};

#endif

