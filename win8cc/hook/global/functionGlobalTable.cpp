
#include "functionGlobalTable.h"
#include "hook/noxfunction.h"

FunctionGlobalTable::FunctionGlobalTable()
    : FunctionTable()
{ }

FunctionGlobalTable::~FunctionGlobalTable()
{ }

NoxFunction *FunctionGlobalTable::Push(std::unique_ptr<NoxFunction> fn)
{
    NoxFunction *ret = fn.get();
    auto fName = ret->IsStatic() ? ret->StaticTag() : fn->Name();			//25, Oct 2022-14:02

    pushImpl(fName, std::move(fn), true);	//Crashed after NoxFunction object was moved into calling function
    return ret;
}
