
#ifndef GLOBAL_DATA_H__
#define GLOBAL_DATA_H__

#include <memory>

class FunctionTable;
class CDataSection;

class GlobalData
{
private:
	std::unique_ptr<FunctionTable> m_globalFnTable;
	std::unique_ptr<CDataSection> m_globalField;

public:
	GlobalData();
	GlobalData(std::unique_ptr<FunctionTable> fnTable, std::unique_ptr<CDataSection> field);
	~GlobalData();
	
public:
	void Attach(std::unique_ptr<FunctionTable> fnTable);
	void Attach(std::unique_ptr<CDataSection> field);
	std::unique_ptr<FunctionTable> DetachFunctionTable();
	std::unique_ptr<CDataSection> DetachDataField();
	FunctionTable *GetFunctionTable();
	CDataSection *GetDataField();
};

#endif

