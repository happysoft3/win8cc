
#include "globalData.h"
#include "hook/functionTable.h"
#include "hook/cdata/cDataSection.h"

GlobalData::GlobalData()
{ }

GlobalData::GlobalData(std::unique_ptr<FunctionTable> fnTable, std::unique_ptr<CDataSection> field)
	: m_globalField(std::move(field)), m_globalFnTable(std::move(fnTable))
{ }

GlobalData::~GlobalData()
{ }

void GlobalData::Attach(std::unique_ptr<FunctionTable> fnTable)
{
	m_globalFnTable = std::move(fnTable);
}

void GlobalData::Attach(std::unique_ptr<CDataSection> field)
{
	m_globalField = std::move(field);
}

std::unique_ptr<FunctionTable> GlobalData::DetachFunctionTable()
{
	return m_globalFnTable ? std::move(m_globalFnTable) : nullptr;
}
std::unique_ptr<CDataSection> GlobalData::DetachDataField()
{
	return m_globalField ? std::move(m_globalField) : nullptr;
}
