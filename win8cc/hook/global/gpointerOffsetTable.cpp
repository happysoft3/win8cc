
#include "gpointerOffsetTable.h"
#include "hook/link/cclink.h"

GPointerOffsetTable::GPointerOffsetTable()
{ }

GPointerOffsetTable::~GPointerOffsetTable()
{ }

void GPointerOffsetTable::Push(std::shared_ptr<CCLink> &link)
{
	m_offsetList.push_back(link);
}

void GPointerOffsetTable::Preorder(std::function<void(CCLink &)> &&fn)
{
	for (auto &off : m_offsetList)
		fn(*off);
}
