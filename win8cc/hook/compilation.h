
#ifndef COMPILATION_H__
#define COMPILATION_H__

#include <memory>
#include <string>
#include <vector>

class BuildEnvironment;
class BuildSourceFile;
class CompileLinker;
class ScrOutFormat;
class TaskQueue;
class JoinCompilation;
class GPointerOffsetTable;

class Compilation
{
private:
	std::unique_ptr<TaskQueue> m_taskQueue;
	std::unique_ptr<BuildEnvironment> m_env;
	std::unique_ptr<CompileLinker> m_linker;
	std::unique_ptr<JoinCompilation> m_joinThread;
	std::shared_ptr<GPointerOffsetTable> m_gpointerTable;
    int m_unitCount;

public:
	Compilation();
	~Compilation();

private:
	void readyToCompile(const BuildSourceFile &sourceFile);

public:
	void PutEnvironment(std::unique_ptr<BuildEnvironment> env);

private:
	void preLinking(CompileLinker &linker);
	void postLinking(CompileLinker &linker);

public:
	void CompileAll();
	void Linking();

private:
	void createOutputImpl(ScrOutFormat &form);

public:
	void CreateOutput();
};

#endif

