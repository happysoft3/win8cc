
#ifndef AST_VECTOR_H__
#define AST_VECTOR_H__

#include <vector>
#include <memory>

class AstObject;

class AstVector
{
private:
    std::vector<AstObject *> m_vt;
    std::unique_ptr<AstVector> m_copiedVector;

public:
    explicit AstVector(size_t sz = 0);
    explicit AstVector(AstObject *elem);
    ~AstVector();

private:
    AstVector *deepCopy(AstVector &other) const;

public:
    AstVector *Copy();
    void Push(AstObject *elem);
    void Append(const AstVector &other);
    AstObject *Pop();
    AstObject *Get(int index) const;
    void Set(int index, AstObject *value);
    AstObject *Head() const;
    AstObject *Tail() const;
    AstVector *Reverse();

    using sort_function_ty = int(*)(const void*, const void*);
    void Sorting(sort_function_ty fn);
    int Length() const;
    bool EqualLength(const AstVector &other) const;
};

#endif

