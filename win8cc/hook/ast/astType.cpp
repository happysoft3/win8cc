
#include "astType.h"
#include "astDict.h"
#include "astVector.h"
#include "error.h"
#include "debug.h"
#include "astNode.h"
#include "8cc.h"
#include "typepool.h"

class StructGuard
{
private:
    uint64_t m_ptrValue;
    bool m_result;

public:
    explicit StructGuard(const AstType *ty1, const AstType *ty2)
    {
        m_ptrValue = reinterpret_cast<uint64_t>(ty1) |
            (reinterpret_cast<uint64_t>(ty2) << 32);
        m_result = true;
    }

    ~StructGuard()
    { }

    uint64_t PTR() const
    {
        return m_ptrValue;
    }

    bool Result() const
    {
        return m_result;
    }
};

AstType::AstType()
{ }

AstType::AstType(int kind)
    : m_kind(kind)
{ }

AstType::AstType(int kind, int size, int align, bool unsig)
    : m_kind(kind)
{
    m_align = align;
    m_size = size;
    m_usig = unsig;
}

AstType::~AstType()
{ }

void AstType::SetSize(int size, int *pAlign)
{
    m_size = size;
    if (pAlign)
        m_align = *pAlign;
}

bool AstType::IsKind(int kind) const
{
    return m_kind == kind;
}

void AstType::EnsureNotVoid()
{
    if (m_kind==KIND_VOID)
        THROW_ERROR_NOPOS("void is not allowed");
}

void AstType::EnsureAssignable(AstType *fromTy)
{
    if ((IsArithtype() || m_kind == KIND_PTR) &&
        (fromTy->IsArithtype() || fromTy->m_kind == KIND_PTR))
        return;
    //if (TypePool::IsSameStruct(totype, fromtype))
    if (Compatible(fromTy))
        return;
    THROW_ERROR_NOPOS("incompatible kind: <%s> <%s>", Explain(), fromTy->Explain());
}

bool AstType::IsString() const
{
    return false;
}

bool AstType::IsIntType() const
{
    switch (m_kind)
    {
    case KIND_BOOL: case KIND_CHAR: case KIND_SHORT: case KIND_INT:
    case KIND_LONG: case KIND_LLONG:
        return true;
    default:
        return false;
    }
}

bool AstType::IsFloatType() const
{
    switch (m_kind)
    {
    case KIND_FLOAT: case KIND_DOUBLE: case KIND_LDOUBLE:
        return true;
    default:
        return false;
    }
}

bool AstType::IsArithtype() const
{
    return IsIntType() || IsFloatType();
}

bool AstType::SameArithType(const AstType &t) const
{
    if (m_kind == t.m_kind)
    {
        if (m_usig == t.m_usig)
            return true;
    }
    return false;
}

AstType *AstType::createCloneType()
{
    return new AstType(m_kind);
}

void AstType::cloneData(AstType *cloneTy)
{
    cloneTy->m_align = m_align;
    cloneTy->m_size = m_size;
    cloneTy->m_isstatic = m_isstatic;
    cloneTy->m_usig = m_usig;
    cloneTy->m_offset = m_offset;
}

//FIXME. 이 메서드는 "메모리 누수"에 취약합니다, 그러므로 할당한 메모리를 어떻게 처리할 지 검토해야합니다
AstType *AstType::Clone()
{
    AstType *ty = createCloneType();

    cloneData(ty);
    putAllocatedObject(ty);
    return ty;
}

bool AstType::Compatible(const AstType *ty) const
{
    if (m_kind == KIND_VOID)    //void vs non ptr
    {
        if (!ty->GetPTR())
            return true;
    }

    if (m_kind != ty->m_kind)
    {
        if (ty->m_kind == KIND_VOID)    //non ptr vs void
        {
            if (!GetPTR())
                return true;
        }
        return false;
    }

    return true;
}

bool AstType::EvalSpec(const AstNode *node, int cop) const
{
    if (!node->IsKind(AST_LITERAL))
        return Compatible(node->GetType());
    
    if (node->GetType()->IsIntType())
    {
        int value = static_cast<const AstLiteralNode *>(node)->GetIntValue();

        switch (m_kind)
        {
        case KIND_BOOL: case KIND_CHAR: case KIND_SHORT: case KIND_INT:
        case KIND_LONG: case KIND_LLONG: case KIND_ENUM:
            //int vs int
            return true;

        case KIND_FLOAT: case KIND_DOUBLE: case KIND_LDOUBLE:
            //int vs float
            return value == 0;

        default:
            return false;
        }
    }
    return Compatible(node->GetType());
}

std::string AstType::Explain() const
{
    return CCHelper::ToString(this);
}

AstPointerType::AstPointerType(AstType *ptr)
    : AstType(KIND_PTR)
{
    int size = sizeof(int);
    m_ptr = ptr;
    SetSize(size, &size);
}

AstPointerType::~AstPointerType()
{ }

AstType *AstPointerType::createCloneType()
{
    return new AstPointerType(nullptr);
}

bool AstPointerType::IsString() const
{
    if (m_kind != KIND_ARRAY)
        return false;

    switch (m_ptr->Kind())
    {
    case KIND_CHAR:
    case KIND_SHORT:
        return true;
    default:
        return false;
    }
}

void AstPointerType::cloneData(AstType *cloneTy)
{
    AstPointerType *ptrTy = static_cast<AstPointerType *>(cloneTy);

    AstType::cloneData(cloneTy);
    ptrTy->m_ptr = m_ptr;
}

bool AstPointerType::Compatible(const AstType *ty) const
{
    switch (ty->Kind())
    {
    case KIND_PTR: case KIND_ARRAY:
        break;

    default:
        return false;
    }
    return m_ptr->Compatible(ty->GetPTR());
}

bool AstPointerType::allowVoidZeroPtr(const AstNode *node) const
{
    if (!node->IsKind(OP_CAST))
        return false;
    const AstNode *subNode = static_cast<const AstConvNode *>(node)->Operand();
    if (!subNode->IsKind(AST_LITERAL))
        return false;

    int ival = static_cast<const AstLiteralNode *>(subNode)->GetIntValue();

    const AstType *c = this;
    const AstType *ty = node->GetType();

    while (c)
    {
        c = c->GetPTR();
        if (!ty->GetPTR())
            break;
        ty = ty->GetPTR();
    }
    if (ty->IsKind(KIND_VOID) && ival == 0)
        return true;
    return false;
}

bool AstPointerType::EvalSpec(const AstNode *node, int cop) const
{
    if (node->GetType()->IsIntType())
    {
        //ptr vs int(0)
        if (!cop)
        {
            if (node->IsKind(AST_LITERAL))
                return static_cast<const AstLiteralNode *>(node)->GetIntValue() == 0;
        }
        switch (cop)
        { //ptr + int or ptr - int
        case '+': case '-': return true;
        default: return false;
        }
    }
    if (allowVoidZeroPtr(node))
        return true;
    return Compatible(node->GetType());
}

AstArrayType::AstArrayType(AstType *ptr, int length)
    : AstPointerType(ptr)
{
    int size, align = ptr->GetAlign();

    if (length < 0)
        size = -1;
    else
        size = ptr->GetSize() * length;

    SetSize(size, &align);
    m_length = length;
    m_kind = KIND_ARRAY;
}

AstArrayType::~AstArrayType()
{ }

AstType *AstArrayType::createCloneType()
{
    return new AstArrayType(m_ptr, m_length);
}

bool AstArrayType::Compatible(const AstType *ty) const
{
    if (!AstType::Compatible(ty))
        return false;

    if (m_length != static_cast<const AstArrayType *>(ty)->m_length)
        return false;

    return m_ptr->Compatible(static_cast<const AstArrayType *>(ty)->GetPTR());
}

void AstArrayType::cloneData(AstType *cloneTy)
{
    AstPointerType::cloneData(cloneTy);
    AstArrayType *realTy = static_cast<AstArrayType *>(cloneTy);
    
    realTy->m_length = m_length;
}

void AstArrayType::SetLengthIfArray(const AstType *ty)
{
    LOGIC_ASSERT(ty->IsKind(KIND_ARRAY));
    const AstArrayType *realTy = static_cast<const AstArrayType *>(ty);

    m_length = realTy->m_length;
}

class StructMap
{
private:
    std::map<uint64_t, std::unique_ptr<StructGuard>> m_hash;

public:
    StructMap()
    { }
    ~StructMap()
    { }

    std::unique_ptr<bool> GetResult(const uint64_t &u) const
    {
        auto iter= m_hash.find(u);
        if (iter == m_hash.cend())
            return{ };
        return std::make_unique<bool>(iter->second->Result());
    }

    void Add(std::unique_ptr<StructGuard> guard)
    {
        auto u = guard->PTR();

        m_hash[u] = std::move(guard);
    }
};

AstStructType::AstStructType(bool isStruct)
    : AstType(KIND_STRUCT)
{
    m_is_struct = isStruct;
}

AstStructType::~AstStructType()
{ }

void AstStructType::cloneData(AstType *cloneTy)
{
    AstType::cloneData(cloneTy);
    AstStructType *ty = static_cast<AstStructType *>(cloneTy);

    ty->m_fields = m_fields;
}

AstType *AstStructType::createCloneType()
{
    return new AstStructType(m_is_struct);
}

bool AstStructType::compatibleImpl(const AstType *ty) const
{
    if (this == ty)
        return true;

    if (!AstType::Compatible(ty))
        return false;

    const AstStructType *otherStrtTy = static_cast<const AstStructType *>(ty);

    if (m_is_struct != otherStrtTy->m_is_struct)	//union 일 수도 있다
        return false;

    AstDict *fieldA = m_fields;
    AstDict *fieldB = otherStrtTy->m_fields;

    if (!fieldA)
        return false;

    if (!fieldA->Equal(*fieldB))
        return false;

    StructGuard stGuard(this, ty);
    std::unique_ptr<bool> res = m_structMap->GetResult(stGuard.PTR());

    if (res)
        return *res;

    m_structMap->Add(std::make_unique<StructGuard>(std::move(stGuard)));
    for (size_t u = 0; u < fieldA->Count(); ++u)
    {
        if (!reinterpret_cast<AstType *>(fieldA->GetWithIndex(u))->Compatible(reinterpret_cast<const AstType *>(fieldB->GetWithIndex(u))))
            return false;
    }
    return true;
}

bool AstStructType::Compatible(const AstType *ty) const
{
    if (!m_structMap)
    {
        m_structMap = std::make_unique<StructMap>();
        bool ret = compatibleImpl(ty);
        m_structMap.reset();
        return ret;
    }
    return compatibleImpl(ty);
}

bool AstStructType::EvalSpec(const AstNode *node, int cop) const
{
    LOGIC_ASSERT(node != nullptr);
    return Compatible(node->GetType());
}

bool AstStructType::ValidField() const
{
    if (!m_fields)
        return false;

    return true;
}

std::vector<std::string> AstStructType::GetFieldKeys() const
{
    if (m_fields)
        return m_fields->Keys();

    return{ };
}

AstType *AstStructType::GetFieldType(const char *key) const
{
    if (!m_fields || !key)
        return nullptr;

    return reinterpret_cast<AstType *>( m_fields->Get(key) );
}

AstFunctionType::AstFunctionType(AstType *retType, AstVector *paramtypes, bool has_varargs, bool oldstyle)
    : AstType(KIND_FUNC)
{
    m_rettype = retType;
    m_hasva = has_varargs;
    m_oldstyle = oldstyle;
    SetParams(paramtypes);
}

AstFunctionType::~AstFunctionType()
{ }

bool AstFunctionType::Compatible(const AstType *ty) const
{
    if (!AstType::Compatible(ty))
        return false;

    const AstFunctionType *fty = static_cast<const AstFunctionType *>(ty);

    if (!m_rettype->Compatible(fty->m_rettype))
        return false;
    if (Params()->Length() != fty->Params()->Length())
        return false;

    AstVector *otherParam = fty->Params();

    for (int u = 0; u < Params()->Length() ; ++u)
    {
        if (!reinterpret_cast<AstType *>(Params()->Get(u))->Compatible(reinterpret_cast<const AstType *>(otherParam->Get(u))) )
            return false;
    }
    return true;
}

bool AstFunctionType::EvalSpec(const AstNode *node, int cop) const
{
    LOGIC_ASSERT(node != nullptr);
    return Compatible(node->GetType());
}

void AstFunctionType::cloneData(AstType *cloneTy)
{
    AstType::cloneData(cloneTy);
}

AstType *AstFunctionType::createCloneType()
{
    return new AstFunctionType(m_rettype, Params(), m_hasva, m_oldstyle);
}

AstStubType::AstStubType()
    : AstType(KIND_STUB)
{ }

AstStubType::~AstStubType()
{ }


