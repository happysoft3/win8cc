
#ifndef AST_MAP_H__
#define AST_MAP_H__

#include <map>
#include <string>

class AstObject;

class AstMap
{
private:
    std::map<std::string, AstObject *> m_objectMap;
    const AstMap *m_parent;

public:
    explicit AstMap(const AstMap *parent = nullptr);
    ~AstMap();

    AstObject *Get(const std::string &key) const;
    bool Put(const std::string &key, AstObject *value, bool force = true);
    bool Remove(const std::string &key);
    size_t Length() const;
};

#endif

