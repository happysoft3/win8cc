
#include "astBuiltinType.h"
#include "astType.h"
#include "8cc.h"

static AstBuiltinType s_builtType;

static AstType *s_voidTy;
static AstType *s_boolTy;
static AstType *s_charTy;
static AstType *s_shortTy;
static AstType *s_intTy;
static AstType *s_longTy;
static AstType *s_llongTy;
static AstType *s_ucharTy;
static AstType *s_ushortTy;
static AstType *s_uintTy;
static AstType *s_ulongTy;
static AstType *s_ullongTy;
static AstType *s_floatTy;
static AstType *s_doubleTy;
static AstType *s_ldoubleTy;
static AstType *s_enumTy;

AstBuiltinType::AstBuiltinType()
{
    s_voidTy = new AstType(KIND_VOID, 0, 0, false);
    s_boolTy = new AstType(KIND_BOOL, sizeof(char), sizeof(char), true);
    s_charTy = new AstType(KIND_CHAR, sizeof(char), sizeof(char), false);
    s_shortTy = new AstType(KIND_SHORT, sizeof(short), sizeof(short), false);
    s_intTy = new AstType(KIND_INT, sizeof(int), sizeof(int), false);
    s_longTy = new AstType(KIND_LONG, sizeof(long), sizeof(long), false);
    s_llongTy = new AstType(KIND_LLONG, sizeof(long long), sizeof(long long), false);
    s_ucharTy = new AstType(KIND_CHAR, sizeof(char), sizeof(char), true);
    s_ushortTy = new AstType(KIND_SHORT, sizeof(short), sizeof(short), true);
    s_uintTy = new AstType(KIND_INT, sizeof(int), sizeof(int), true);
    s_ulongTy = new AstType(KIND_LONG, sizeof(long), sizeof(long), true); //DECLARE_TYPE_INSTANCE(type_ulong, KIND_LONG, 8, 8, true)
    s_ullongTy = new AstType(KIND_LLONG, sizeof(long long), sizeof(long long), true);
    s_floatTy = new AstType(KIND_FLOAT, sizeof(float), sizeof(float), false);
    s_doubleTy = new AstType(KIND_DOUBLE, sizeof(double), sizeof(double), false);
    s_ldoubleTy = new AstType(KIND_LDOUBLE, sizeof(long double), sizeof(long double), false);
    s_enumTy = new AstType(KIND_ENUM, sizeof(int), sizeof(int), false);
}

AstBuiltinType::~AstBuiltinType()
{ }

AstType *AstBuiltinType::VoidType()
{
    return s_voidTy;
}

AstType *AstBuiltinType::BoolType()
{
    return s_boolTy;
}
AstType *AstBuiltinType::CharType() {
    return s_charTy;
}
AstType *AstBuiltinType::ShortType() {
    return s_shortTy;
}
AstType *AstBuiltinType::IntType() {
    return s_intTy;
}
AstType *AstBuiltinType::LongType() {
    return s_longTy;
}
AstType *AstBuiltinType::LLongType() {
    return s_llongTy;
}
AstType *AstBuiltinType::UCharType() {
    return s_ucharTy;
}
AstType *AstBuiltinType::UShortType() {
    return s_ushortTy;
}
AstType *AstBuiltinType::UIntType() {
    return s_uintTy;
}
AstType *AstBuiltinType::ULongType() {
    return s_ulongTy;
}
AstType *AstBuiltinType::ULLongType() {
    return s_ullongTy;
}
AstType *AstBuiltinType::FloatType() {
    return s_floatTy;
}
AstType *AstBuiltinType::DoubleType() {
    return s_doubleTy;
}
AstType *AstBuiltinType::LDoubleType() {
    return s_ldoubleTy;
}
AstType *AstBuiltinType::EnumType() {
    return s_enumTy;
}
