
#include "astDict.h"
#include "astMap.h"
#include "error.h"

AstDict::AstDict()
{
    m_map = std::make_unique<AstMap>();
}

AstDict::~AstDict()
{ }

AstObject *AstDict::Get(const std::string &key)
{
    return m_map->Get(key);
}

AstObject *AstDict::GetWithIndex(size_t index)
{
    LOGIC_ASSERT(index < m_keys.size());
    return m_map->Get(m_keys[index] );
}

void AstDict::Put(const std::string &key, AstObject *val)
{
    m_map->Put(key, val);
    m_keys.push_back(key);
}

bool AstDict::Equal(const AstDict &other) const
{
    if (m_map->Length() != other.m_map->Length())
        return false;

    return m_keys.size() == other.m_keys.size();
}

bool AstDict::CompareKey(const AstDict &other) const
{
    int u = 0;

    for (const auto &s : m_keys)
    {
        if (other.m_keys[u] != s)
            return false;
    }
    return true;
}

size_t AstDict::Count() const
{
    size_t keyLength = m_keys.size();
    size_t mapLength = m_map->Length();

    LOGIC_ASSERT(keyLength == mapLength);
    return mapLength;
}

std::vector<std::string> AstDict::Keys() const
{
    return m_keys;
}
