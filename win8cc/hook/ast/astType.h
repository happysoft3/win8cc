
#ifndef AST_TYPE_H__
#define AST_TYPE_H__

#include "astObject.h"
#include <map>
#include <vector>

//class CCDict;
//class CCVector;
class StructMap;
class StructGuard;
class AstNode;
class AstDict;
class AstVector;

class AstType : public AstObject
{
protected:
    int m_kind;

private:
    int m_size;
    int m_align;
    bool m_usig;
    bool m_isstatic;
    // bitfield
    int m_bitoff;
    int m_bitsize;

    int m_offset;

    //Function
    AstVector *m_params;

public:
    AstType();
    explicit AstType(int kind);
    explicit AstType(int kind, int size, int align, bool unsig);
    ~AstType() override;

    int BitOffset() const
    {
        return m_bitoff;
    }

    void SetBitOffset(int off)
    {
        m_bitoff = off;
    }

    int BitSize() const
    {
        return m_bitsize;
    }
    void SetBitSize(int bitsize)
    {
        m_bitsize = bitsize;
    }

    void SetAlign(int align)
    {
        m_align = align;
    }
    int GetAlign() const
    {
        return m_align;
    }
    int GetSize() const
    {
        return m_size;
    }
    bool IsStatic() const
    {
        return m_isstatic;
    }
    void SwitchingStatic(bool isstatic)
    {
        m_isstatic = isstatic;
    }
    int Kind() const
    {
        return m_kind;
    }
    bool Unsigned() const
    {
        return m_usig;
    }
    void SwitchingUnsigned(bool usig)
    {
        m_usig = usig;
    }
    int Offset() const
    {
        return m_offset;
    }

    void SetOffset(int off)
    {
        m_offset = off;
    }
    void IncreaseOffset(int off)
    {
        m_offset += off;
    }
    AstVector *Params() const
    {
        return m_params;
    }
    void SetParams(AstVector *params)
    {
        m_params = params;
    }
    void SetSize(int size, int *pAlign = nullptr);
    bool IsKind(int kind) const;
    void EnsureNotVoid();
    void EnsureAssignable(AstType *fromTy);
    virtual bool IsString() const;
    bool IsIntType() const;
    bool IsFloatType() const;
    bool IsArithtype() const;
    bool SameArithType(const AstType &t) const;
    virtual AstType *GetPTR() const
    {
        return nullptr;
    }
    virtual void SetPTR(AstType *ptr)
    { }
    
private:
    virtual AstType *createCloneType();

protected:
    virtual void cloneData(AstType *cloneTy);

public:
    AstType *Clone();
    virtual bool Compatible(const AstType *ty) const;
    virtual bool EvalSpec(const AstNode *node, int cop) const;
    std::string Explain() const override;
};

class AstPointerType : public AstType
{
protected:
    AstType *m_ptr;

public:
    explicit AstPointerType(AstType *ptr);
    ~AstPointerType() override;

private:
    AstType *createCloneType() override;
    bool IsString() const override;

protected:
    void cloneData(AstType *cloneTy) override;

public:
    AstType *GetPTR() const override
    {
        return m_ptr;
    }
    void SetPTR(AstType *ptr) override
    {
        m_ptr = ptr;
    }

private:
    bool Compatible(const AstType *ty) const override;
    bool allowVoidZeroPtr(const AstNode *node) const;
    bool EvalSpec(const AstNode *node, int cop) const override;
};

class AstArrayType : public AstPointerType
{
private:
    int m_length;

public:
    explicit AstArrayType(AstType *ptr, int length);
    ~AstArrayType() override;

private:
    AstType *createCloneType() override;
    bool Compatible(const AstType *ty) const override;

protected:
    void cloneData(AstType *cloneTy) override;

public:
    void SetLengthIfArray(const AstType *ty);
    void SetLength(int length)
    {
        m_length = length;
    }
    int GetLength() const
    {
        return m_length;
    }
};

class AstStructType : public AstType
{
private:
    AstDict *m_fields;
    bool m_is_struct; // true if struct, false if union

    mutable std::unique_ptr<StructMap> m_structMap;

public:
    explicit AstStructType(bool isStruct);
    ~AstStructType() override;

private:
    void cloneData(AstType *cloneTy) override;
    AstType *createCloneType() override;
    bool compatibleImpl(const AstType *ty) const;
    bool Compatible(const AstType *ty) const override;
    bool EvalSpec(const AstNode *node, int cop) const override;

public:
    bool IsStruct() const
    {
        return m_is_struct;
    }

    void SetField(AstDict *field)
    {
        m_fields = field;
    }

    bool ValidField() const;
    std::vector<std::string> GetFieldKeys() const;
    AstType *GetFieldType(const char *key) const;
};

class AstFunctionType : public AstType
{
private:
    AstType *m_rettype;
    bool m_hasva;
    bool m_oldstyle;

public:
    explicit AstFunctionType(AstType *retType, AstVector *paramtypes, bool has_varargs, bool oldstyle);
    ~AstFunctionType() override;

private:
    bool Compatible(const AstType *ty) const override;
    bool EvalSpec(const AstNode *node, int cop) const override;
    void cloneData(AstType *cloneTy) override;
    AstType *createCloneType() override;

public:
    bool IsOldStyle() const
    {
        return m_oldstyle;
    }
    bool HasVarArgs() const
    {
        return m_hasva;
    }
    void SetVarArgs(bool set)
    {
        m_hasva = set;
    }
    AstType *ReturnType() const
    {
        return m_rettype;
    }
};

class AstStubType : public AstType
{
public:
    AstStubType();
    ~AstStubType() override;
};

#endif

