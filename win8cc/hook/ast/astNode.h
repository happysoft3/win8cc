
#ifndef AST_NODE_H__
#define AST_NODE_H__

#include "astObject.h"
#include <functional>

class AstVector;
class AstMap;
class AstType;
class SourceLocation;

class AstNode : public AstObject
{
private:
    std::unique_ptr<SourceLocation> m_sourceLoc;
    std::unique_ptr<AstType> m_typeInstance;
    std::unique_ptr<AstNode> m_subNode;

protected:
    AstType *m_type;
    int m_kind;

public:
    AstNode();
    explicit AstNode(int astTy, AstType *ty = nullptr);
    ~AstNode() override;

public:
    void PutSourceLocation(const SourceLocation &src);

protected:
    template <class T, class... Args>
    T *makeType(Args&&... args);

public:
    void EnsureArithtype() const;
    void EnsureLValue() const;
    void EnsureIntType() const;
    void EnsureSafeDereference() const;
    bool IsKind(int kind) const;
    void ErrorIfNotArithtype() const;
    virtual int EvaluateIntExpr(AstNode **addr);
    std::string Explain() const override;
    std::string Location() const;

private:
    template <class T, class... Args>
    T *makeSubNode(Args&&... args);

public:
    AstNode *Conv();
    virtual int evaluateStructRef(int offset);

    int Kind() const
    {
        return m_kind;
    }
    AstType *GetType() const
    {
        return m_type;
    }
    void SetType(AstType *ty)
    {
        m_type = ty;
    }
};

class AstLiteralNode : public AstNode
{
private:
    float m_fvalue;
    long m_iValue;

public:
    explicit AstLiteralNode(AstType *ty);
    ~AstLiteralNode() override;

private:
    int EvaluateIntExpr(AstNode **addr) override;

protected:
    std::string Explain() const override;
    void setValue(float *fv, long *iv);

public:
    float GetFloatValue() const;
    int GetIntValue() const;
};

// Char, int, or long
class AstIntNode : public AstLiteralNode
{
public:
    explicit AstIntNode(AstType *ty, long val);
    ~AstIntNode() override;
};

// Float or double
class AstFloatNode : public AstLiteralNode
{    //std::string m_fLabel;

public:
    explicit AstFloatNode(AstType *ty, float fval);
    ~AstFloatNode() override;

//public:
//    float FValue() const
//    {
//        return m_fvalue;
//    }
};

class AstVariableNode : public AstNode
{
private:
    std::string m_varName;
    int m_lOff;
    AstVector *m_lvarInit;

public:
    explicit AstVariableNode(AstType *ty, const std::string &name, AstMap *scope = nullptr);
    explicit AstVariableNode(AstType *ty, const std::string &name, AstVector *init, AstMap *scope = nullptr);
    ~AstVariableNode() override;

private:
    std::string Explain() const override;
    void commonInit(AstType *ty, const std::string &name, AstVector *init, AstMap *scope);

public:
    const char *VarName() const;
    void AdjustLocalOffset(int off)
    {
        m_lOff = off;
    }
    void FetchLvarData(AstVector **lvarinit, int *lOff = nullptr , const char **varName = nullptr) const;
    void SetLvarInit(AstVector *init)
    {
        m_lvarInit = init;
    }
};

class AstGlobalVariableNode : public AstVariableNode
{
private:
    std::string m_glabel;

public:
    explicit AstGlobalVariableNode(AstType *ty, const std::string &name, AstMap *scope = nullptr);
    explicit AstGlobalVariableNode(AstType *ty, const std::string &name, const std::string &glabel, AstMap *scope);
    ~AstGlobalVariableNode() override;

private:
    std::string Explain() const override;
    int EvaluateIntExpr(AstNode **addr) override;

public:
    const char *GlobalLabel() const;
};

class AstStringNode : public AstNode
{
private:
    std::string m_svalue;
    std::string m_slabel;
    std::wstring m_wsvalue;

public:
    explicit AstStringNode(int enc, const char *sval, int length);
    ~AstStringNode() override;

private:
    AstType *initString(int enc, const char *sval, int length);
    std::string Explain() const override;

public:
    const char *StringValue() const;
    const wchar_t *WideStringValue() const;
    const char *StringLabel() const;
    void PutStringLabel(const std::string &slabel)
    {
        m_slabel = slabel;
    }
};

class AstBinaryOpNode : public AstNode
{
private:
    AstNode *m_left;
    AstNode *m_right;

public:
    explicit AstBinaryOpNode(AstType *ty, int kind, AstNode *left, AstNode *right);
    ~AstBinaryOpNode() override;

private:
    int EvaluateIntExpr(AstNode **addr) override;
    std::string explainConcat(const char *op) const;
    std::string Explain() const override;

public:
    void GetChild(AstNode **left, AstNode **right) const;
};

class AstUnaryOpNode : public AstNode
{
private:
    AstNode *m_operand;

public:
    explicit AstUnaryOpNode(int kind, AstType *ty, AstNode *operand);
    ~AstUnaryOpNode() override;

private:
    int EvaluateIntExpr(AstNode **addr) override;
    std::string castingInfo() const;
    std::string Explain() const override;

public:
    AstNode *Operand() const
    {
        return m_operand;
    }
};

class AstComputedGoto : public AstUnaryOpNode
{
public:
    explicit AstComputedGoto(AstNode *expr);
    ~AstComputedGoto() override;
};

class AstConvNode : public AstUnaryOpNode
{
public:
    explicit AstConvNode(AstType *totype, AstNode *value);
    ~AstConvNode() override;

private:
    std::string Explain() const override;
};

// Function call or function declaration
class AstFunctionNode : public AstNode
{
private:
    std::string m_fname;

protected:
    AstVector *m_args;
    AstType *m_ftype;
    // Function pointer or function designator
    AstNode *m_fptr;

private:
    // Function declaration
    AstVector *m_params;
    AstVector *m_localvars;
    AstNode *m_body;

public:
    explicit AstFunctionNode(AstType *ty, const char *name, AstVector *params, AstNode *body, AstVector *localvars);
    ~AstFunctionNode() override;

private:
    std::string Explain() const override;

public:
    AstType *FTYPE() const
    {
        return m_ftype;
    }
    AstNode *FunctionPTR() const
    {
        return m_fptr;
    }
    AstNode *FunctionBody() const
    {
        return m_body;
    }
    const char *FunctionName() const;

private:
    int fetchLength(AstVector *v) const;
    AstNode *fetchNode(AstVector *v, int n) const;

public:
    int ArgLength() const;
    AstNode *GetArg(int n) const;
    int ParamLength() const;
    AstNode *GetParam(int n) const;
    int LocalVarsCount() const;
    AstNode *GetLocalVars(int n) const;
};

class AstFunctionCall : public AstFunctionNode
{
public:
    explicit AstFunctionCall(AstType *ty, const char *name, AstVector *args);
    ~AstFunctionCall() override;
};

class AstFunctionPtrCall : public AstFunctionNode
{
public:
    explicit AstFunctionPtrCall(AstNode *fptr, AstVector *args);
    ~AstFunctionPtrCall() override;
};

class AstFunctionDesgNode : public AstFunctionNode
{
public:
    AstFunctionDesgNode(AstType *ty, const char *name);
    ~AstFunctionDesgNode() override;
};

class AstDeclNode : public AstNode
{
private:
    AstNode *m_declvar;
    AstVector *m_declinit;

public:
    explicit AstDeclNode(AstNode *var, AstVector *init);
    ~AstDeclNode() override;

private:
    void a2sDeclinit(std::string &b, AstVector *initlist) const;
    std::string Explain() const override;

public:
    AstVector *DeclInit() const
    {
        return m_declinit;
    }
    AstNode *DeclVar() const
    {
        return m_declvar;
    }
};

class AstInitializerNode : public AstNode
{
private:
    AstNode *m_initval;
    int m_initoff;
    AstType *m_totype;

public:
    explicit AstInitializerNode(AstNode *value, AstType *totype, int off);
    ~AstInitializerNode() override;

private:
    std::string Explain() const override;

public:
    AstType *ToType() const
    {
        return m_totype;
    }
    int InitOffset() const
    {
        return m_initoff;
    }
    AstNode *InitValue() const
    {
        return m_initval;
    }
};

class AstIfNode : public AstNode
{
protected:
    AstNode *m_cond;
    AstNode *m_then;
    AstNode *m_els;

public:
    explicit AstIfNode(AstNode *cond, AstNode *then, AstNode *els);
    ~AstIfNode() override;

private:
    std::string Explain() const override;
    int EvaluateIntExpr(AstNode **addr) override;

public:
    void FetchBranches(AstNode **cond, AstNode **then, AstNode **els =nullptr) const;
};

class AstTernaryNode : public AstIfNode
{
public:
    explicit AstTernaryNode(AstType *ty, AstNode *cond, AstNode *then, AstNode *els);
    ~AstTernaryNode() override;

private:
    std::string Explain() const override;
};

class AstGotoNode : public AstNode
{
protected:
    std::string m_label;
    std::string m_newLabel;

public:
    explicit AstGotoNode(const char *label, const char *newLabel = nullptr);
    ~AstGotoNode() override;

private:
    std::string Explain() const override;

public:
    const char *LabelName() const;
    const char *NewLabel() const;
    void BackfillLabel(const AstMap &labels, std::function<std::string()> &&makeLabel);
};

class AstJumpNode : public AstGotoNode
{
public:
    explicit AstJumpNode(const char *label);
    ~AstJumpNode() override;
};

class AstReturnNode : public AstNode
{
private:
    AstNode *m_retval;

public:
    explicit AstReturnNode(AstNode *retval);
    ~AstReturnNode() override;

private:
    std::string Explain() const override;

public:
    AstNode *ReturnValue() const
    {
        return m_retval;
    }
};

class AstCompoundStmtNode : public AstNode
{
private:
    AstVector *m_stmts;

public:
    explicit AstCompoundStmtNode(AstVector *stmts);
    ~AstCompoundStmtNode() override;

private:
    std::string Explain() const override;

public:
    AstVector *Statements() const
    {
        return m_stmts;
    }
    int StatementLength() const;
    AstNode *GetStatement(int n) const;
};

class AstStructNode : public AstNode
{
private:
    AstNode *m_struc;
    std::string m_field;
    AstType *m_fieldtype;

public:
    explicit AstStructNode(AstType *ty, AstNode *struc, const char *name);
    ~AstStructNode() override;

private:
    int evaluateStructRef(int offset) override;
    std::string Explain() const override;

public:
    AstNode *Struc() const
    {
        return m_struc;
    }
};

class AstTypedefNode : public AstNode
{
public:
    explicit AstTypedefNode(AstType *ty, const char *name, AstMap *field = nullptr);
    ~AstTypedefNode() override;
};

class AstLabelNode : public AstGotoNode
{
public:
    explicit AstLabelNode(const char *label);   //Node *CCParse::ast_label(char *label) {
    explicit AstLabelNode(const char *label, const char *newLabel); //Node *CCParse::ast_dest(char *label) {
    ~AstLabelNode() override;

private:
    std::string Explain() const override;
};

class AstDestNode : public AstLabelNode
{
public:
    explicit AstDestNode(const char *label);
    ~AstDestNode() override;
};

class AstLabelAddrNode : public AstLabelNode
{
public:
    explicit AstLabelAddrNode(const char *label);
    ~AstLabelAddrNode() override;
};

#endif

