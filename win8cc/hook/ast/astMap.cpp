
#include "astMap.h"
#include "astObject.h"

AstMap::AstMap(const AstMap *parent)
    : m_parent(parent)
{ }

AstMap::~AstMap()
{ }

AstObject *AstMap::Get(const std::string &key) const
{
    auto iter = m_objectMap.find(key);

    if (iter != m_objectMap.cend())
        return iter->second;

    if (!m_parent)
        return nullptr;

    return m_parent->Get(key);
}

bool AstMap::Put(const std::string &key, AstObject *value, bool forced)
{
    if (forced)
    {
        m_objectMap[key] = value;
        return true;
    }

    if (m_objectMap.find(key) == m_objectMap.cend())
    {
        m_objectMap[key] = value;
        return true;
    }
    return false;
}

bool AstMap::Remove(const std::string &key)
{
    auto iter = m_objectMap.find(key);

    if (iter != m_objectMap.cend())
    {
        m_objectMap.erase(iter);
        return true;
    }
    return false;
}

size_t AstMap::Length() const
{
    if (m_objectMap.empty())
        return 0;

    return m_objectMap.size();
}

