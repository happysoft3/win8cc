
#include "astVector.h"
#include "astObject.h"

AstVector::AstVector(size_t sz)
{
    if (sz)
        m_vt.reserve(sz);
}

AstVector::AstVector(AstObject *elem)
    : m_vt({ elem })
{ }

AstVector::~AstVector()
{ }

AstVector *AstVector::deepCopy(AstVector &other) const
{
    other.m_vt = m_vt;
    return &other;
}

AstVector *AstVector::Copy()
{
    auto p = std::make_unique<AstVector>();
    auto ret = deepCopy(*p);

    m_copiedVector = std::move(p);
    return ret;
}

void AstVector::Push(AstObject *elem)
{
    m_vt.push_back(elem);
}

void AstVector::Append(const AstVector &other)
{
    for (const auto &p : other.m_vt)
        m_vt.push_back(p);
}

AstObject *AstVector::Pop()
{
    if (m_vt.empty())
        return nullptr;

    auto ret = m_vt.back();

    m_vt.pop_back();
    return ret;
}

AstObject *AstVector::Get(int index) const
{
    AstObject *ret;

    try
    {
        ret = m_vt.at(index);
    }
    catch (const std::exception &)
    {
        return nullptr;
    }
    return ret;
}

void AstVector::Set(int index, AstObject *value)
{
    m_vt[index] = value;
}

AstObject *AstVector::Head() const
{
    return m_vt.front();
}

AstObject *AstVector::Tail() const
{
    return m_vt.back();
}

AstVector *AstVector::Reverse()
{
    auto p = std::make_unique<AstVector>();
    
    if (!m_vt.empty())
    {
        p->m_vt.resize(m_vt.size());
        size_t r = m_vt.size();
        for (const auto &elem : m_vt)
            p->m_vt[--r] = elem;
    }
    m_copiedVector = std::move(p);
    return m_copiedVector.get();
}

void AstVector::Sorting(AstVector::sort_function_ty fn)
{
    qsort(m_vt.data() , m_vt.size() , sizeof(void *), fn);
}

int AstVector::Length() const
{
    if (m_vt.empty())
        return 0;

    return static_cast<int>(m_vt.size());
}

bool AstVector::EqualLength(const AstVector &other) const
{
    return m_vt.size() == other.m_vt.size();
}
