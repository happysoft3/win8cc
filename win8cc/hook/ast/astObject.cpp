
#include "astObject.h"

AstObject::AstObject()
{ }

AstObject::~AstObject()
{ }

void AstObject::putAllocatedObject(AstObject *pObject)
{
    std::unique_ptr<AstObject> object(pObject);

    m_objects.push_back(std::move(object));
}

AstObject *AstObject::releaseObject()
{
    if (m_objects.empty())
        return{ };

    std::unique_ptr<AstObject> object = std::move( m_objects.front() );

    m_objects.pop_front();
    return object.release();
}

