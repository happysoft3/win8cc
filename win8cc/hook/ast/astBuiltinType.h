
#ifndef AST_BUILTIN_TYPE_H__
#define AST_BUILTIN_TYPE_H__

class AstType;

class AstBuiltinType
{
public:
    AstBuiltinType();
    ~AstBuiltinType();
    AstBuiltinType(const AstBuiltinType&) = delete;
    AstBuiltinType(AstBuiltinType &&) = delete;

    static AstType *VoidType();
    static AstType *BoolType();
    static AstType *CharType();
    static AstType *ShortType();
    static AstType *IntType();
    static AstType *LongType();
    static AstType *LLongType();
    static AstType *UCharType();
    static AstType *UShortType();
    static AstType *UIntType();
    static AstType *ULongType();
    static AstType *ULLongType();
    static AstType *FloatType();
    static AstType *DoubleType();
    static AstType *LDoubleType();
    static AstType *EnumType();
};

#endif

