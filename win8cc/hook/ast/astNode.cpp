
#include "astNode.h"
#include "astMap.h"
#include "astVector.h"
#include "sourceLocation.h"
#include "buffer.h"
#include "astBuiltinType.h"
#include "astType.h"
#include "error.h"
#include "debug.h"
#include "8cc.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

AstNode::AstNode()
    : m_sourceLoc(nullptr)
{ }

AstNode::AstNode(int astTy, AstType *ty)
    : m_kind(astTy), m_sourceLoc(nullptr), m_type(ty)
{ }

AstNode::~AstNode()
{ }

void AstNode::PutSourceLocation(const SourceLocation &src)
{
    if (!m_sourceLoc)
    {
        m_sourceLoc = src.Clone();
    }
    m_sourceLoc->CopyPush(src);
}

template <class T, class... Args>
T *AstNode::makeType(Args&&... args)
{
    T *ty = new T(std::forward<Args>(args)...);

    m_typeInstance = std::unique_ptr<AstType>(ty);
    //m_type = ty;
    return ty;
}

void AstNode::EnsureArithtype() const
{
    if (!m_type->IsArithtype())
        THROW_ERROR_NOPOS("arithmetic type expected, but got %s", Explain());
}

void AstNode::EnsureLValue() const
{
    switch (m_kind) {
    case AST_LVAR: case AST_GVAR: case AST_DEREF: case AST_STRUCT_REF:
        return;
    default:
        THROW_ERROR_NOPOS("lvalue expected, but got %s", Explain());
    }
}

void AstNode::EnsureIntType() const
{
    if (!m_type->IsIntType())
        THROW_ERROR_NOPOS("integer type expected, but got %s", Explain());
}

void AstNode::EnsureSafeDereference() const
{	/////배열이나 포인터가 아닌 변수를 배열로 참조할 때, 충돌나는 문제로 추가된 함수입니다//

    if (m_type->IsArithtype())
    {
        //auto loc = m_sourceLoc;
        //const char *filePos = "null";
        //int line = -1;

        //if (loc)
        //{
        //    filePos = loc->file;
        //    line = loc->line;
        //}
        //THROW_ERROR_NOPOS("dereferncing error %s: %s:%d", Explain(), filePos, line);
        THROW_ERROR_NODE(this, "dereferncing error %s", Explain());
    }
}

bool AstNode::IsKind(int kind) const
{
    return m_kind == kind;
}

void AstNode::ErrorIfNotArithtype() const
{
    /*char buff[1024] = { 0, };

    sprintf(buff, "expect arithtype, but %s, file: %s, line: %d",
        m_type->Explain()
        toArray(CCHelper::ToString(m_type)), m_sourceLoc->file, m_sourceLoc->line);
    THROW_ERROR_NOPOS(buff);*/
    THROW_ERROR_NODE(this, "expect arithtype, but %s", m_type->Explain());
}

int AstNode::EvaluateIntExpr(AstNode **)
{
    THROW_ERROR_NODE(this, "Integer expression expected, but got %s", Explain());
    return 0;
}

std::string AstNode::Explain() const
{
    std::string b = stringFormat("[ASTNode- %d]", m_kind);

    if (m_type)
        b += stringFormat("[Type: %s]", m_type->Explain());
    return b;
}

std::string AstNode::Location() const
{
    return m_sourceLoc->LoadProfileString();
}

template <class T, class... Args>
T *AstNode::makeSubNode(Args&&... args)
{
    LOGIC_ASSERT(m_subNode == nullptr);
    T *p = new T(std::forward<Args>(args)...);

    m_subNode = std::unique_ptr<AstNode>(p);
    return p;
}

AstNode *AstNode::Conv()
{
    switch (m_type->Kind())
    {
    case KIND_ARRAY:
        // C11 6.3.2.1p3: An array of T is converted to a pointer to T.
        return makeSubNode<AstUnaryOpNode>(AST_CONV, makeType<AstPointerType>(m_type->GetPTR()), this);
    case KIND_FUNC:
        // C11 6.3.2.1p4: A function designator is converted to a pointer to the function.
        return makeSubNode<AstUnaryOpNode>(AST_ADDR, makeType<AstPointerType>(m_type), this);
    case KIND_SHORT: case KIND_CHAR: case KIND_BOOL:
        // C11 6.3.1.1p2: The integer promotions
        return makeSubNode<AstConvNode>(AstBuiltinType::IntType(), this);
    case KIND_INT:
        if (m_type->BitSize() > 0)
            return makeSubNode<AstConvNode>(AstBuiltinType::IntType(), this);
    }
    return this;
}

int AstNode::evaluateStructRef(int offset)
{
    return EvaluateIntExpr(nullptr) + offset;
}

AstLiteralNode::AstLiteralNode(AstType *ty)
    : AstNode(AST_LITERAL, ty)
{
    m_fvalue = 0.0f;
    m_iValue = 0;
}

AstLiteralNode::~AstLiteralNode()
{ }

int AstLiteralNode::EvaluateIntExpr(AstNode **)
{
    return GetIntValue();
}

std::string AstLiteralNode::Explain() const
{
    if (m_type->IsIntType())
        return stringFormat("ASTLiteral-%s %d", m_type->Explain(), GetIntValue());
    if (m_type->IsFloatType())
        return stringFormat("ASTLiteral-%s %f", m_type->Explain(), GetFloatValue());
    return "ASTLiteral";
}

void AstLiteralNode::setValue(float *fv, long *iv)
{
    if (fv)
        m_fvalue = *fv;
    if (iv)
        m_iValue = *iv;
}
#define IGNORE_LITERAL_TY
float AstLiteralNode::GetFloatValue() const
{
#ifndef IGNORE_LITERAL_TY
    if (!m_type->IsFloatType())
        THROW_ERROR_NODE(this, "not float type");
#endif
    return m_fvalue;
}
int AstLiteralNode::GetIntValue() const {
#ifndef IGNORE_LITERAL_TY
    if (!m_type->IsIntType())
        THROW_ERROR_NODE(this, "not int type");
#endif
    return m_iValue;
}

AstIntNode::AstIntNode(AstType *ty, long val)
    : AstLiteralNode(ty)
{
    setValue(nullptr, &val);
}

AstIntNode::~AstIntNode()
{ }

AstFloatNode::AstFloatNode(AstType *ty, float fval)
    : AstLiteralNode(ty)
{
    setValue(&fval, nullptr);
}

AstFloatNode::~AstFloatNode()
{ }

AstVariableNode::AstVariableNode(AstType *ty, const std::string &name, AstMap *scope)
    : AstNode(AST_LVAR)
{
    commonInit(ty, name, nullptr, scope);
}
AstVariableNode::AstVariableNode(AstType *ty, const std::string &name, AstVector *init, AstMap *scope)
    : AstNode(AST_LVAR)
{
    commonInit(ty, name, init, scope);
}

AstVariableNode::~AstVariableNode()
{ }

std::string AstVariableNode::Explain() const
{
    return stringFormat("var=%s", m_varName);
}

void AstVariableNode::commonInit(AstType *ty, const std::string &name, AstVector *init, AstMap *scope)
{
    m_type = ty;
    m_varName = name;
    m_lOff = 0;
    m_lvarInit = init;
    if (scope)
        scope->Put(name.c_str(), this); //name.c_str 에서 댕글링될 위험이 있음. 나중에 검토//
}

const char *AstVariableNode::VarName() const
{
    if (m_varName.empty())
        return "";

    return m_varName.c_str();
}

void AstVariableNode::FetchLvarData(AstVector **lvarinit, int *lOff, const char **varName) const
{
    if (lvarinit)
        *lvarinit = m_lvarInit;
    if (lOff)
        *lOff = m_lOff;
    if (varName)
        *varName = m_varName.empty() ? nullptr : m_varName.c_str();
}

AstGlobalVariableNode::AstGlobalVariableNode(AstType *ty, const std::string &name, AstMap *scope )
    : AstVariableNode(ty, name, scope)
{
    m_kind = AST_GVAR;
    m_glabel = name;
}

AstGlobalVariableNode::AstGlobalVariableNode(AstType *ty, const std::string &name, const std::string &glabel, AstMap *scope)
    : AstVariableNode(ty, name, scope)
{
    m_kind = AST_GVAR;
    m_glabel = glabel;
}

AstGlobalVariableNode::~AstGlobalVariableNode()
{ }

const char *AstGlobalVariableNode::GlobalLabel() const
{
    if (m_glabel.empty())
        return "";

    return m_glabel.c_str();
}

std::string AstGlobalVariableNode::Explain() const
{
    return stringFormat("gvar=%s", m_glabel);
}

int AstGlobalVariableNode::EvaluateIntExpr(AstNode **addr)
{
    if (addr)
    {
        *addr = Conv();
        return 0;
    }
    return AstNode::EvaluateIntExpr(addr);
}

AstStringNode::AstStringNode(int enc, const char *sval, int length)
    : AstNode(AST_LITERAL)
{
    m_type = initString(enc, sval, length);
}

AstStringNode::~AstStringNode()
{ }

AstType *AstStringNode::initString(int enc, const char *sval, int length)
{
    AstType *ty = nullptr;
    const char *body = nullptr;
    CCBufferAllocator ba;

    switch (enc) {
    case ENC_NONE:
    case ENC_UTF8:
        ty = makeType<AstArrayType>(AstBuiltinType::CharType(), length);
        body = sval;
        break;
    case ENC_CHAR16:
    {
        CCBuffer *b = ba.ToUtf16(sval, length);
        ty = makeType<AstArrayType>(AstBuiltinType::UShortType(), b->Length() / AstBuiltinType::UShortType()->GetSize());
        body = b->Body();
        m_wsvalue = std::wstring( reinterpret_cast<const wchar_t *>(body) );
        break;
    }
    case ENC_CHAR32:
    case ENC_WCHAR:
    {
        CCBuffer *b = ba.ToUtf32(sval, length);
        ty = makeType<AstArrayType>(AstBuiltinType::UIntType(), b->Length() / AstBuiltinType::UIntType()->GetSize());
        body = b->Body();
        break;
    }
    default:
        THROW_ERROR_NOPOS(__FUNCSIG__);
    }
    m_svalue = body;
    return ty;
}

std::string AstStringNode::Explain() const
{
    return stringFormat("\"%s\"", m_svalue);
}

const char *AstStringNode::StringValue() const
{
    if (m_svalue.empty())
        return "";

    return m_svalue.c_str();
}

const wchar_t *AstStringNode::WideStringValue() const
{
    if (m_wsvalue.empty())
        return nullptr;

    return m_wsvalue.c_str();
}

const char *AstStringNode::StringLabel() const
{
    if (m_slabel.empty())
        return nullptr;

    return m_slabel.c_str();
}

AstBinaryOpNode::AstBinaryOpNode(AstType *ty, int kind, AstNode *left, AstNode *right)
    : AstNode(kind, ty)
{
    m_left = left;
    m_right = right;
}

AstBinaryOpNode::~AstBinaryOpNode()
{ }

int AstBinaryOpNode::EvaluateIntExpr(AstNode **addr)
{
    switch (m_kind)
    {
#define L (m_left->EvaluateIntExpr(addr))
#define R (m_right->EvaluateIntExpr(addr))
    case '+': return L + R;
    case '-': return L - R;
    case '*': return L * R;
    case '/': return L / R;
    case '<': return L < R;
    case '^': return L ^ R;
    case '&': return L & R;
    case '|': return L | R;
    case '%': return L % R;
    case OP_EQ: return L == R;
    case OP_LE: return L <= R;
    case OP_NE: return L != R;
    case OP_SAL: return L << R;
    case OP_SAR: return L >> R;
    case OP_SHR: return ((unsigned long)L) >> R;
    case OP_LOGAND: return L && R;
    case OP_LOGOR:  return L || R;
    default:
        return AstNode::EvaluateIntExpr(nullptr); //go to error
#undef L
#undef R
    }
}

std::string AstBinaryOpNode::explainConcat(const char *op) const
{
    return stringFormat("[%s %s %s]", m_left->Explain(), op, m_right->Explain());
}

std::string AstBinaryOpNode::Explain() const
{
    switch (m_kind)
    {
    case OP_SAL: return explainConcat("<<");
    case OP_SAR:
    case OP_SHR: return explainConcat(">>");
    case OP_GE: return explainConcat( ">=");
    case OP_LE: return explainConcat( "<=");
    case OP_NE: return explainConcat( "!=");
    case OP_LOGAND: return explainConcat("and");
    case OP_LOGOR:  return explainConcat("or"); 
    case OP_A_ADD:  return explainConcat("+="); 
    case OP_A_SUB:  return explainConcat("-="); 
    case OP_A_MUL:  return explainConcat("*="); 
    case OP_A_DIV:  return explainConcat("/="); 
    case OP_A_MOD:  return explainConcat("%="); 
    case OP_A_AND:  return explainConcat("&="); 
    case OP_A_OR:   return explainConcat("|="); 
    case OP_A_XOR:  return explainConcat("^="); 
    case OP_A_SAL:  return explainConcat("<<=");
    case OP_A_SAR:
    case OP_A_SHR: return explainConcat(">>=");
    case '&': return explainConcat( "&");
    case '|': return explainConcat( "|");
    default:
        return "";
    }
}

void AstBinaryOpNode::GetChild(AstNode **left, AstNode **right) const
{
    if (left)
        *left = m_left;
    if (right)
        *right = m_right;
}

AstUnaryOpNode::AstUnaryOpNode(int kind, AstType *ty, AstNode *operand)
    : AstNode(kind, ty)
{
    m_operand = operand;
}

AstUnaryOpNode::~AstUnaryOpNode()
{ }

int AstUnaryOpNode::EvaluateIntExpr(AstNode **addr)
{
    switch (m_kind)
    {
    case '!': return !m_operand->EvaluateIntExpr(addr);
    case '~': return ~m_operand->EvaluateIntExpr(addr);
    case OP_CAST: 
    case AST_CONV:
        return m_operand->EvaluateIntExpr(addr);
    case AST_ADDR:
        if (m_operand->IsKind(AST_STRUCT_REF))
            return m_operand->evaluateStructRef(0);
        // fallthrough
        if (addr) {
            *addr = Conv();
            return 0;
        }
        break;

    case AST_DEREF:
        if (m_operand->GetType()->IsKind(KIND_PTR))
            return m_operand->EvaluateIntExpr(addr);
        break;
    }
    return AstNode::EvaluateIntExpr(nullptr);  //go to error
}

std::string AstUnaryOpNode::castingInfo() const
{
    return stringFormat("[Cast: %s=>%s %s]",
        m_operand->GetType()->Explain(),
        m_type->Explain(),
        m_operand->Explain());
}

std::string AstUnaryOpNode::Explain() const
{
    const char *op = "null";

    switch (m_kind)
    {
    case AST_ADDR: op = "addr"; break;
    case AST_DEREF: op = "deref"; break;
    case OP_PRE_INC: op = "++a"; break;
    case OP_PRE_DEC: op = "--a"; break;
    case OP_POST_INC: op = "a++"; break;
    case OP_POST_DEC: op = "a--"; break;
    case '!': op = "! "; break;
    case OP_CAST: return castingInfo();
    }
    return stringFormat("[%s %s]", op, m_operand->Explain());
}

AstComputedGoto::AstComputedGoto(AstNode *expr)
    :AstUnaryOpNode(AST_COMPUTED_GOTO, nullptr, expr)
{ }

AstComputedGoto::~AstComputedGoto()
{ }

AstConvNode::AstConvNode(AstType *totype, AstNode *value)
    : AstUnaryOpNode(AST_CONV, totype, value)
{ }

AstConvNode::~AstConvNode()
{ }

std::string AstConvNode::Explain() const
{
    return stringFormat("[conv %s=>%s]", Operand()->Explain(), m_type->Explain());
}

AstFunctionNode::AstFunctionNode(AstType *ty, const char *name, AstVector *params, AstNode *body, AstVector *localvars)
    : AstNode(AST_FUNC, ty)
{    
    m_fname = (name == nullptr) ? "" : name;
    m_params = params;
    m_localvars = localvars;
    m_body = body;
}

AstFunctionNode::~AstFunctionNode()
{ }

std::string AstFunctionNode::Explain() const
{
    return stringFormat("function: %s", FunctionName());
}

const char *AstFunctionNode::FunctionName() const
{
    if (m_fname.empty())
        return "(empty)";

    return m_fname.c_str();
}

int AstFunctionNode::fetchLength(AstVector *v) const
{
    if (!v)
        return 0;
    return v->Length();
}
AstNode *AstFunctionNode::fetchNode(AstVector *v, int n) const
{
    if (!v)
        return{ };

    return reinterpret_cast<AstNode *>(v->Get(n));
}

int AstFunctionNode::ArgLength() const
{
    return(fetchLength(m_args));
}

AstNode *AstFunctionNode::GetArg(int n) const
{
    return fetchNode(m_args, n);
}

int AstFunctionNode::ParamLength() const
{
    if (!m_params)
        return 0;

    return m_params->Length();
}

AstNode *AstFunctionNode::GetParam(int n) const
{
    return fetchNode(m_params, n);
}

int AstFunctionNode::LocalVarsCount() const
{
    return fetchLength(m_localvars);
}

AstNode *AstFunctionNode::GetLocalVars(int n) const
{
    return fetchNode(m_localvars, n);
}

AstFunctionCall::AstFunctionCall(AstType *ty, const char *name, AstVector *args)
    : AstFunctionNode(static_cast<AstFunctionType *>(ty)->ReturnType(), name, nullptr, nullptr, nullptr)
{
    m_args = args;
    m_ftype = ty;
    m_kind = AST_FUNCALL;
}

AstFunctionCall::~AstFunctionCall()
{ }

AstFunctionPtrCall::AstFunctionPtrCall(AstNode *fptr, AstVector *args)
    : AstFunctionNode(nullptr, nullptr, nullptr, nullptr, nullptr)
{
    SetType(static_cast<AstFunctionType *>(static_cast<AstPointerType *>(fptr->GetType())->GetPTR())->ReturnType());
    m_kind = AST_FUNCPTR_CALL;
    m_args = args;
    m_fptr = fptr;
}

AstFunctionPtrCall::~AstFunctionPtrCall()
{ }

AstFunctionDesgNode::AstFunctionDesgNode(AstType *ty, const char *name)
    : AstFunctionNode(ty, name, nullptr, nullptr, nullptr)
{
    m_kind = AST_FUNCDESG;
}

AstFunctionDesgNode::~AstFunctionDesgNode()
{ }

AstDeclNode::AstDeclNode(AstNode *var, AstVector *init)
    : AstNode(AST_DECL)
{
    m_declvar = var;
    m_declinit = init;
}

AstDeclNode::~AstDeclNode()
{ }

void AstDeclNode::a2sDeclinit(std::string &b, AstVector *initlist) const
{
    for (int i = 0; i < initlist->Length(); i++) {
        if (i > 0)
            b += ' ';
        const AstNode *init = reinterpret_cast<const AstNode *>(initlist->Get(i));

        b += init->Explain();
    }
}

std::string AstDeclNode::Explain() const
{
    std::string s = stringFormat("decl %s %s", m_declvar->GetType()->Explain(), static_cast<AstVariableNode *>(m_declvar)->VarName());

    if (m_declinit)
        a2sDeclinit(s, m_declinit);
    return s;
}

AstInitializerNode::AstInitializerNode(AstNode *value, AstType *totype, int off)
    : AstNode(AST_INIT)
{
    m_initval = value;
    m_initoff = off;
    m_totype = totype;
}

AstInitializerNode::~AstInitializerNode()
{ }

std::string AstInitializerNode::Explain() const
{
    return stringFormat("%s@%d", m_initval->Explain(), m_totype->Explain());
}

AstIfNode::AstIfNode(AstNode *cond, AstNode *then, AstNode *els)
    : AstNode(AST_IF)
{
    m_cond = cond;
    m_then = then;
    m_els = els;
}

AstIfNode::~AstIfNode()
{ }

std::string AstIfNode::Explain() const
{
    std::string s = stringFormat("[IF %s then %s", m_cond->Explain(), m_then->Explain());

    if (m_els)
        s += stringFormat(" %s", m_els->Explain());
    return s + ']';
}

int AstIfNode::EvaluateIntExpr(AstNode **addr)
{
    if (m_kind == AST_TERNARY)
    {
        long cond = m_cond->EvaluateIntExpr(addr);

        if (cond)
            return m_then ? m_then->EvaluateIntExpr(addr) : cond;
        return m_els->EvaluateIntExpr( addr);
    }
    return AstNode::EvaluateIntExpr(nullptr);
}

void AstIfNode::FetchBranches(AstNode **cond, AstNode **then, AstNode **els) const
{
    if (cond)
        *cond = m_cond;
    if (then)
        *then = m_then;
    if (els)
        *els = m_els;
}

AstTernaryNode::AstTernaryNode(AstType *ty, AstNode *cond, AstNode *then, AstNode *els)
    : AstIfNode(cond, then, els)
{
    m_type = ty;
    m_kind = AST_TERNARY;
}

AstTernaryNode::~AstTernaryNode()
{ }

std::string AstTernaryNode::Explain() const
{
    return stringFormat("[ternary %s %s %s]",
        m_cond->Explain(),
        m_then->Explain(),
        m_els->Explain()
        );
}

AstGotoNode::AstGotoNode(const char *label, const char *newLabel)
    : AstNode(AST_GOTO)
{
    m_label = label;
    if (newLabel)
        m_newLabel = label;
}

AstGotoNode::~AstGotoNode()
{ }

std::string AstGotoNode::Explain() const
{
    return stringFormat("goto %s", LabelName());
}

const char *AstGotoNode::LabelName() const
{
    std::string s = m_label.empty() ? m_newLabel : m_label;

    if (s.empty())
        return "";

    return s.c_str();
}

const char *AstGotoNode::NewLabel() const
{
    if (m_newLabel.empty())
        return nullptr;

    return m_newLabel.c_str();
}

void AstGotoNode::BackfillLabel(const AstMap &labels, std::function<std::string()> &&makeLabel)
{
    AstNode *dst = static_cast<AstNode *>( labels.Get(m_label) );

    if (!dst)
        THROW_ERROR_NODE(this, "stray %s: %s", m_kind == AST_GOTO ? "goto" : "unary &&", m_label);

    AstLabelNode *dstLabel = static_cast<AstLabelNode *>(dst);

    if (!dstLabel->m_newLabel.empty())
        m_newLabel = dstLabel->m_newLabel;
    else
    {
        std::string createdLabel = makeLabel();

        m_newLabel = createdLabel;
        dstLabel->m_newLabel = createdLabel;
    }
}

AstJumpNode::AstJumpNode(const char *label)
    : AstGotoNode(label, label)
{ }

AstJumpNode::~AstJumpNode()
{ }

AstReturnNode::AstReturnNode(AstNode *retval)
    : AstNode(AST_RETURN)
{
    m_retval = retval;
}

AstReturnNode::~AstReturnNode()
{ }

std::string AstReturnNode::Explain() const
{
    return stringFormat("[return %s]", m_retval->Explain());
}

AstCompoundStmtNode::AstCompoundStmtNode(AstVector *stmts)
    : AstNode(AST_COMPOUND_STMT)
{
    m_stmts = stmts;
}

AstCompoundStmtNode::~AstCompoundStmtNode()
{ }

std::string AstCompoundStmtNode::Explain() const
{
    std::string bf;

    for (int i = 0 ; i < m_stmts->Length() ; ++i)
    {
        AstNode *node = reinterpret_cast<AstNode *>(m_stmts->Get(i));

        bf += stringFormat(" {%s} ", node->Explain());
    }
    return stringFormat("[%s]", bf);
}

int AstCompoundStmtNode::StatementLength() const
{
    if (!m_stmts)
        return 0;

    return m_stmts->Length();
}

AstNode *AstCompoundStmtNode::GetStatement(int n) const
{
    if (!m_stmts)
        return nullptr;

    return static_cast<AstNode *>(m_stmts->Get(n) );
}

AstStructNode::AstStructNode(AstType *ty, AstNode *struc, const char *name)
    : AstNode(AST_STRUCT_REF, ty)
{
    m_struc = struc;
    m_field = name;
}

AstStructNode::~AstStructNode()
{ }

int AstStructNode::evaluateStructRef(int offset)
{
    return m_struc->evaluateStructRef(m_type->Offset() + offset);
}

std::string AstStructNode::Explain() const
{
    return stringFormat("%s.%s", m_struc->Explain(), m_field);
}

AstTypedefNode::AstTypedefNode(AstType *ty, const char *name, AstMap *field)
    : AstNode(AST_TYPEDEF, ty)
{
    if (field)
        field->Put(name, this);
}

AstTypedefNode::~AstTypedefNode()
{ }

AstLabelNode::AstLabelNode(const char *label)   //Node *CCParse::ast_label(char *label) {
    : AstGotoNode(label)
{
    m_kind = AST_LABEL;
}

AstLabelNode::AstLabelNode(const char *label, const char *newLabel) //Node *CCParse::ast_dest(char *label) {
    : AstGotoNode(label, newLabel)
{
    m_kind = AST_LABEL;
}

AstLabelNode::~AstLabelNode()
{ }

std::string AstLabelNode::Explain() const
{
    return stringFormat("[label:%s]", LabelName());
}

AstDestNode::AstDestNode(const char *label)
    : AstLabelNode(label, label)
{ }

AstDestNode::~AstDestNode()
{ }

AstLabelAddrNode::AstLabelAddrNode(const char *label)
    :AstLabelNode(label)
{
    SetType(makeType<AstPointerType>(AstBuiltinType::VoidType()));
}

AstLabelAddrNode::~AstLabelAddrNode()
{ }
