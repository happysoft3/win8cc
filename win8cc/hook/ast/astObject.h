
#ifndef AST_OBJECT_H__
#define AST_OBJECT_H__

#include <string>
#include <list>
#include <memory>

class AstObject
{
private:
    std::list<std::unique_ptr<AstObject>> m_objects;

public:
    AstObject();
    virtual ~AstObject();

    AstObject(const AstObject &) = delete;
    AstObject &operator=(const AstObject &) = delete;

protected:
    void putAllocatedObject(AstObject *pObject);
    AstObject *releaseObject();

public:
    virtual std::string Explain() const = 0;
};

#endif

