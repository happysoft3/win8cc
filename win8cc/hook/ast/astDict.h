
#ifndef AST_DICT_H__
#define AST_DICT_H__

#include <memory>
#include <vector>
#include <string>

class AstMap;
class AstObject;

class AstDict
{
private:
    std::unique_ptr<AstMap> m_map;
    std::vector<std::string> m_keys;

public:
    AstDict();
    ~AstDict();

    AstObject *Get(const std::string &key);
    AstObject *GetWithIndex(size_t index);
    void Put(const std::string &key, AstObject *val);
    bool Equal(const AstDict &other) const;
    bool CompareKey(const AstDict &other) const;
    size_t Count() const;
    std::vector<std::string> Keys() const;
};

#endif

