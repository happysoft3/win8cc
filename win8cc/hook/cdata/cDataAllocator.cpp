
#include "cDataAllocator.h"
#include "cDataSection.h"
#include "cDataLabel.h"
#include "hook/global/gpointerOffsetTable.h"
#include "hook/functionTable.h"
#include "hook/noxfunction.h"
#include "hook/link/cclink.h"
#include "common/utils/mydebug.h"
#include "common/utils/stringhelper.h"
#include <vector>

using namespace _StringHelper;

CDataAllocator::CDataAllocator(int size)
{
	m_size = size;
	m_depth = 0;
}

CDataAllocator::~CDataAllocator()
{ }

void CDataAllocator::MakeOffset(int off)
{
    if (m_offset)
        MY_THROW() << "detected try to re-align the offset";
	m_offset = std::make_shared<CCLink>();
    m_offset->MakeLink(off);
}

std::shared_ptr<CCLink> CDataAllocator::GetOffset() const
{
    if (!m_offset)
        MY_THROW() << "has no offset";

    return m_offset;
}

BssDataAllocator::BssDataAllocator(int size)
    : CDataAllocator(size)
{ }

BssDataAllocator::~BssDataAllocator()
{ }

void BssDataAllocator::EmitData(ScrOutStream &scrOs)
{
	int sz = Size();

	if (!sz)
		throw std::logic_error("invalid size");

	std::vector<uint8_t> data(sz);

	for (const auto &c : data)
		scrOs.Write(c);
}

AddressAllocator::AddressAllocator(const std::string &addressTag)
	: CDataAllocator(sizeof(int))
{
	m_addressTag = addressTag;
}

AddressAllocator::~AddressAllocator()
{ }

void AddressAllocator::EmitData(ScrOutStream &scrOs)
{
	if (!m_link)
		throw std::logic_error("unlinked field");

	scrOs.Write(m_link->Link() + linkAdder());
}

const char *AddressAllocator::Label() const
{
	if (m_addressTag.empty())
		throw std::logic_error("unnamed address tag");

	return m_addressTag.c_str();
}

void AddressAllocator::setLink(std::shared_ptr<CCLink> &link)
{
    if (m_link)
        MY_THROW() << "the field was linked twice";

    m_link = link;
}

PointerAllocator::PointerAllocator(const std::string &tag, int adder)
	: AddressAllocator(tag)
{
	m_adder = adder;
}

PointerAllocator::PointerAllocator(const std::string &tag, std::shared_ptr<GPointerOffsetTable> &table, int adder)
	: AddressAllocator(tag), m_adder(adder)
{
	m_gpointerTable = table;
}

PointerAllocator::~PointerAllocator()
{ }

int PointerAllocator::linkAdder() const
{
	return m_adder;
}

void PointerAllocator::PostLink(FunctionTable &fnTable, CDataSection &field)
{
    auto gLabel = field.Retrieve(Label());

    if (!gLabel)
        MY_THROW() << stringFormat("unknown v-link '%s'", Label());
        
    auto off = gLabel->TryLoadOffset();

    if (!off)
        MY_THROW() << "error";
    setLink(off);
}

void PointerAllocator::MakeOffset(int off)
{
	AddressAllocator::MakeOffset(off);
	if (m_gpointerTable)
		m_gpointerTable->Push(GetOffset());
}

FunctionPointerAllocator::FunctionPointerAllocator(const std::string &functionTag)
	: AddressAllocator(functionTag)
{ }

FunctionPointerAllocator::~FunctionPointerAllocator()
{ }

void FunctionPointerAllocator::PreLink(FunctionTable &fnTable, CDataSection &field)
{
    auto fn = fnTable.Retrieve(Label(), true);

    if (fn)
        setLink(fn->UniqueId());
}

void FunctionPointerAllocator::PostLink(FunctionTable &fnTable, CDataSection &field)
{
    if (isLinked())
        return;

    auto fn = fnTable.Retrieve(Label());

    if (!fn)
        MY_THROW() << stringFormat("unknown f-link '%s'", Label());

    setLink(fn->UniqueId());
}

