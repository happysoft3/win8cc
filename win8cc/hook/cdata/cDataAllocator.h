
#ifndef C_DATA_ALLOCATOR_H__
#define C_DATA_ALLOCATOR_H__

#include "hook/output/scrOutStream.h"
#include <string>
#include <memory>

class FunctionTable;
class CDataSection;
class CCLink;

class CDataAllocator
{
private:
	int m_size;
	int m_depth;
    std::shared_ptr<CCLink> m_offset;
	std::string m_uniqId;

public:
	CDataAllocator(int size);
	virtual ~CDataAllocator();

public:
	int Size() const
	{
		return m_size;
	}
	int Depth() const
	{
		return m_depth;
	}
	void SetDepth(int depth)
	{
		m_depth = depth;
	}
	void SetUniqueString(const std::string &s)
	{
		m_uniqId = s;
	}
	const char *const UniqueId() const
	{
		return m_uniqId.c_str();
	}

	virtual bool IsLabel() const
	{
		return false;
	}

    virtual void MakeOffset(int off);
	//데이터 필드의 이름입니다, 기본적으로 공백입니다
	//포인터, 함수주소 할당자와 공유합니다
	virtual const char *Label() const
	{
		return "";
	}
    std::shared_ptr<CCLink> GetOffset() const;
    virtual void PreLink(FunctionTable &fnTable, CDataSection &field)
    { }
    virtual void PostLink(FunctionTable &fnTable, CDataSection &field)
    { }

	virtual void EmitData(ScrOutStream &scrOs) = 0;
};

//초기화 되지 않은 전역변수입니다
class BssDataAllocator : public CDataAllocator
{
public:
    BssDataAllocator(int size);
    ~BssDataAllocator() override;

private:
	void EmitData(ScrOutStream &scrOs) override;
};

template <class Ty, typename std::enable_if<std::is_arithmetic<Ty>::value, bool>::type = true>
class ArithAllocator : public CDataAllocator
{
private:
	Ty m_value;

public:
	ArithAllocator(Ty value)
		: CDataAllocator(sizeof(Ty))
	{
		m_value = value;
	}

	~ArithAllocator() override
	{ }

	void SetValue(Ty value)
	{
		m_value = value;
	}

	void EmitData(ScrOutStream &scrOs) override
	{
		scrOs.Write(m_value);
	}
};

template <class CharT, typename std::enable_if<std::is_integral<CharT>::value, bool>::type = true>
class StringAllocator : public CDataAllocator
{
private:
	using my_char_ty = CharT;
	std::basic_string<CharT> m_str;

public:
	StringAllocator(const std::basic_string<CharT> &sValue)
		: CDataAllocator((sValue.length() + 1) * sizeof(CharT))
	{
		m_str = sValue;
	}
	~StringAllocator() override
	{ }

private:
	void EmitData(ScrOutStream &scrOs) override
	{
		for (const auto &c : m_str)
			scrOs.Write(c);

		scrOs.Write(static_cast<my_char_ty>(0));
	}
};

class AddressAllocator : public CDataAllocator
{
private:
	std::string m_addressTag;
	std::shared_ptr<CCLink> m_link;

public:
	AddressAllocator(const std::string &addressTag);
	~AddressAllocator() override;

private:
	virtual int linkAdder() const { return 0; }
	void EmitData(ScrOutStream &scrOs) override;

protected:
	const char *Label() const override;
    bool isLinked() const {
        return m_link ? true : false;
    }
    void setLink(std::shared_ptr<CCLink> &link);
};

class GPointerOffsetTable;

//TODO. 변수의 주소로 링크해 줘야합니다
class PointerAllocator : public AddressAllocator
{
private:
	int m_adder;
	std::shared_ptr<GPointerOffsetTable> m_gpointerTable;

public:
	explicit PointerAllocator(const std::string &globalTag, int adder = 0);
	explicit PointerAllocator(const std::string &globalTag, std::shared_ptr<GPointerOffsetTable> &table, int adder = 0);
	~PointerAllocator() override;

private:
	int linkAdder() const override;
    void PostLink(FunctionTable &fnTable, CDataSection &field) override;
	void MakeOffset(int off) override;
};

//TODO. 함수의 번지를 밸류값으로서 지정해야 합니다
class FunctionPointerAllocator : public AddressAllocator
{
public:
	FunctionPointerAllocator(const std::string &functionTag);
	~FunctionPointerAllocator() override;

private:
    void PreLink(FunctionTable &fnTable, CDataSection &field) override;
    void PostLink(FunctionTable &fnTable, CDataSection &field) override;
};

#endif

