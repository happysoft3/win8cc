
#ifndef C_DATA_LIST_H__
#define C_DATA_LIST_H__

#include <memory>
#include <list>
#include <functional>

class CDataAllocator;

class CDataList
{
private:
	std::unique_ptr<CDataAllocator> m_label;
	std::list<std::unique_ptr<CDataAllocator>> m_dataList;
    int m_vStaticId;

public:
	explicit CDataList(std::unique_ptr<CDataAllocator> label);
	~CDataList();

public:
	int Depth() const;
	const char *Label() const;
	void AttachLabel(std::unique_ptr<CDataAllocator> label);
	std::unique_ptr<CDataAllocator> DetachLabel();
	void Push(std::unique_ptr<CDataAllocator> data);
	std::unique_ptr<CDataAllocator> Pop();
	CDataAllocator *Front() const;
	void MoveData(std::function<void(std::unique_ptr<CDataAllocator>)> &&fn);
};

#endif

