
#include "cDataList.h"
#include "cDataAllocator.h"
#include "common/utils/myDebug.h"

CDataList::CDataList(std::unique_ptr<CDataAllocator> label)
	: m_label(label ? std::move(label) : nullptr)
{
}

CDataList::~CDataList()
{ }

int CDataList::Depth() const
{
	if (!m_label)
		MY_THROW() << "label is empty";

	return m_label->Depth();
}

const char *CDataList::Label() const
{
	if (!m_label)
		MY_THROW() << "label is empty";

	return m_label->Label();
}

void CDataList::AttachLabel(std::unique_ptr<CDataAllocator> label)
{
	if (m_label)
		MY_THROW() << "tried to attach a label twice";

	m_label = std::move(label);
}

std::unique_ptr<CDataAllocator> CDataList::DetachLabel()
{
	std::unique_ptr<CDataAllocator> label = std::move(m_label);

	m_label.reset();
	return label;
}

void CDataList::Push(std::unique_ptr<CDataAllocator> data)
{
	m_dataList.push_back(std::move(data));
}

std::unique_ptr<CDataAllocator> CDataList::Pop()
{
	if (m_dataList.empty())
		return {};

	std::unique_ptr<CDataAllocator> data = std::move(m_dataList.front());

	m_dataList.pop_front();
	return data;
}

CDataAllocator *CDataList::Front() const
{
	if (m_dataList.empty())
		return nullptr;

	return m_dataList.front().get();
}

void CDataList::MoveData(std::function<void(std::unique_ptr<CDataAllocator>)> &&fn)
{
	for (auto &data : m_dataList)
		fn(std::move(data));

	m_dataList.clear();
}
