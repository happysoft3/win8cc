
#ifndef C_DATA_LABEL_H__
#define C_DATA_LABEL_H__

#include <string>
#include <memory>

class CDataAllocator;
class CCLink;

class CDataLabel
{
private:
	std::string m_labelName;
	bool m_useGlobalScope;
	CDataAllocator *m_pointing;

public:
	CDataLabel(const std::string &labelName);
	~CDataLabel();

	void SetGlobal()
	{
		m_useGlobalScope = true;
	}
	bool IsGlobal() const
	{
		return m_useGlobalScope;
	}
	void SetTarget(CDataAllocator *target)
	{
		m_pointing = target;
	}
	const char * const Label() const
	{
		return m_labelName.c_str();
	}
	CDataAllocator *Merge(CDataAllocator *diff);
	std::shared_ptr<CCLink> TryLoadOffset() const;
    const char *const UniqueLabel() const;
};

#endif

