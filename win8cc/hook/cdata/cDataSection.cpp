
#include "cDataSection.h"
#include "cDataList.h"
#include "cDataLabel.h"
#include "cDataAllocator.h"
#include "hook/functionTable.h"
#include "common/utils/myDebug.h"
#include "common/utils/stringhelper.h"
#include "common/utils/randomString.h"

#include <atomic>

using namespace _StringHelper;

class CDataSection::BssData
{
private:
	std::unique_ptr<CDataLabel> m_label;
	std::unique_ptr<CDataAllocator> m_bssData;

public:
	explicit BssData(std::unique_ptr<CDataLabel> label, std::unique_ptr<CDataAllocator> data)
		: m_label(std::move(label)), m_bssData(std::move(data))
	{ }
	~BssData()
	{ }

public:
	bool CompareSize(int src) const
	{
		if (!m_bssData)
			return false;

		return m_bssData->Size() == src;
	}

	std::unique_ptr<CDataLabel> DetachLabel()
	{
		return std::move(m_label);
	}

	std::unique_ptr<CDataAllocator> DetachBss()
	{
		return std::move(m_bssData);
	}
};

//실제 소스라인에 표현되지는 않는다
class LabelAllocator : public CDataAllocator
{
private:
	bool m_isGlobal;
	std::string m_label;
	int m_labelOffset;

public:
	LabelAllocator(const std::string &label, bool global = false)
		: CDataAllocator(0)
	{
		m_isGlobal = global;
		m_label = label;
		m_labelOffset = 0;
	}

	~LabelAllocator() override
	{ }

private:
	bool IsLabel() const override { return true; }
	void EmitData(ScrOutStream &scrOs) override
	{
		MY_THROW() << "label still alive";
	}

public:
	void SetGlobal()
	{
		m_isGlobal = true;
	}

	bool IsGlobal() const
	{
		return m_isGlobal;
	}
	const char *Label() const override
    {
        return m_label.c_str();
    }
};

static std::atomic<int> s_uniqueSeedGenerator = 1;

CDataSection::CDataSection(int baseOffset)
	: m_uniqueSeed(++s_uniqueSeedGenerator)
{
	m_uniqueLabel = 0;
	m_baseOffset = baseOffset;
    m_globalData = nullptr;
	m_lastLength = 0;

    m_uniqId = 0;
}

CDataSection::~CDataSection()
{ }

std::unique_ptr<CDataLabel> CDataSection::makeLabel(const std::string &label, bool isStatic)
{
	auto lb = std::make_unique<CDataLabel>(label);

	if (!isStatic)
		lb->SetGlobal();

	return lb;
}

const char *CDataSection::PutVLabel(int depth)
{
	std::string randgen = RandomString::Generate(8);
	std::string label = stringFormat("-%s%d%d%d-vlabel", randgen, m_uniqueLabel++, m_uniqueSeed, depth);	//must be unique

	if (m_labels.find(label) != m_labels.cend())
		MY_THROW() << stringFormat("vlabel '%s' conflict", label);

	auto labelAlloctor = std::make_unique<LabelAllocator>(label);

	labelAlloctor->SetDepth(depth);
	m_field.push_back(std::move(labelAlloctor));

	auto createdLabel = makeLabel(label);
	auto labelName = createdLabel->Label();

	m_labels[label] = std::move(createdLabel);
	return labelName;
}

//생각을 해 보니까, 광역변수는 굳이 global 테이블에서 찾을 필요가 없을 것 같아요
//함수는 몸체가 없으면 안되지만, 변수는 몸체는 있으니까요
//로컬에서 찾아 링크해 주면 될 것 같고..
void CDataSection::PutLabel(const std::string &label, bool isStatic)
{
	if (m_labels.find(label) != m_labels.cend())
		MY_THROW() << stringFormat("multiple definition of '%s'", label);

	auto bssIterator = m_bssMap.find(label);

	if (bssIterator != m_bssMap.cend())
		m_bssMap.erase(bssIterator);	//bss 필드가 있으면, 지워준다

	m_field.push_back(std::make_unique<LabelAllocator>(label, !isStatic));
	m_labels[label] = makeLabel(label, isStatic);
}

void CDataSection::PutBssLabel(const std::string &label, int fieldSize, bool isStatic)
{
	if (m_labels.find(label) != m_labels.cend())
	{
		///!타입이 다를 수가 있음!- Todo. 타입이 다르면 오류로 내보내야 합니다//
		return;	//일단 무시하겠음//
	}
	auto bssIterator = m_bssMap.find(label);

	if (bssIterator != m_bssMap.cend())
	{
		if (!bssIterator->second->CompareSize(fieldSize))
			MY_THROW() << stringFormat("multiple definition of '%s'", label);
		return;
	}
	std::unique_ptr<CDataAllocator> bssChunk = std::make_unique<BssDataAllocator>(fieldSize);

	allocateImpl(*bssChunk, 0);
	m_bssMap[label] = std::make_unique<BssData>(makeLabel(label, isStatic), std::move(bssChunk));
}

void CDataSection::allocateImpl(CDataAllocator &allocator, int depth)
{
	if (depth)
		allocator.SetDepth(depth);

	std::string rndgen = stringFormat("%s%d%d", RandomString::Generate(16), m_uniqueSeed, m_uniqueLabel);

	allocator.SetUniqueString(rndgen);
}

void CDataSection::pushGlobal(std::unique_ptr<CDataAllocator> data)
{
	std::string uniqId = data->UniqueId();

	if (uniqId.empty())
		MY_THROW() << "unique id is empty";

	m_field.push_back(std::move(data));
	auto findIterator = m_fieldHash.find(uniqId);

	if (findIterator != m_fieldHash.cend())
		MY_THROW() << stringFormat("internal hash error- %s", uniqId);
	m_fieldHash[uniqId] = std::prev(m_field.end());
}

void CDataSection::eraseGlobal(const char *uniqId)
{
	auto uniqIdIterator = m_fieldHash.find(uniqId);

	if (uniqIdIterator == m_fieldHash.cend())
		MY_THROW() << stringFormat("no attrib symbol '%s'", uniqId);
	m_field.erase(uniqIdIterator->second);
    m_fieldHash.erase(uniqIdIterator);
}

std::unique_ptr<CDataAllocator> CDataSection::popData()
{
	if (m_field.empty())
		return {};

	std::unique_ptr<CDataAllocator> pop = std::move(m_field.front());

	m_field.pop_front();
	return pop;
}

CDataAllocator *CDataSection::peekData() const
{
	if (m_field.empty())
		return nullptr;

	return m_field.front().get();
}

void CDataSection::ungetData(std::unique_ptr<CDataAllocator> data)
{
	m_field.push_front(std::move(data));
}

void CDataSection::mergeData(CDataLabel &glabel, CDataList &pack)
{
	auto data = pack.Front();
    auto prevAlloc = glabel.Merge(data);

	if (prevAlloc)
	{
		popLabel(glabel.Label());	//머지가 되었으면, 기존 레이블을 뜯어내고(이때, 오직 bss만)
		m_globalData->eraseGlobal(prevAlloc->UniqueId());
		pack.MoveData([this](std::unique_ptr<CDataAllocator> a)	///새 레이블을 끝에 추가한다
		{
			m_globalData->pushGlobal(std::move(a));
		});
	}
}

void CDataSection::pushLabelData(CDataList &pack)
{	//글로벌에 이 레이블이 없어요, 소스에서 레이블을 뜯어와, 글로벌에 삽입합니다
	auto labelName = pack.Label();
	auto newLabel = popLabel(labelName);

	if (!newLabel)
		MY_THROW() << "internal error: newLabel==nullptr";
	newLabel->SetTarget(pack.Front());
	m_globalData->m_labels[labelName] = std::move(newLabel);
	pack.MoveData([this](std::unique_ptr<CDataAllocator> a)
	{
		m_globalData->pushGlobal(std::move(a));
	});
}

void CDataSection::emitFieldLabel(CDataList &pack/*, std::unique_ptr<CDataAllocator> data*/)
{
    //그러나 우선, 글로벌 필드에 이미 존재하는 지
    ///그리고 존재하면, 초기화 값이 있는 지를 살펴야 합니다
    ////초기화 값이 있으면, 중복 오류입니다
	auto labelName = pack.Label();
	auto glabel = m_globalData->loadLabel(labelName);

	if (glabel)	//이 레이블은 이미, 글로벌 테이블에 존재합니다
		mergeData(*glabel, pack);
	else
		pushLabelData(pack);
}

void CDataSection::matchLabelEx(CDataList &pack, std::unique_ptr<CDataAllocator> data)
{
	CDataList inpack(std::move(data));

	matchLabelValue(inpack);
	emitFieldLabel(inpack);
	matchLabelValue(pack);
	return;
}

void CDataSection::moveSameDepth(CDataList &pack, std::unique_ptr<CDataAllocator> data)
{
	pack.Push(std::move(data));

	for (;;)
	{
		data = popData();

		if (!data)
			break;
		if (data->IsLabel())
			break;
		if (data->Depth() != pack.Depth())
			break;
		pack.Push(std::move(data));
	}
	ungetData(data ? std::move(data) : nullptr);
}

void CDataSection::matchLabelValue(CDataList &pack)
{
	std::unique_ptr<CDataAllocator> data = popData();

	if (!data)
		MY_THROW() << __FUNCSIG__ << " stream was ended unclearly";

	if (data->IsLabel())
	{
		matchLabelEx(pack, std::move(data));
		return;
	}
	if (pack.Depth() != data->Depth())
		MY_THROW() << stringFormat("%s stray symbol %d-'%s'", __FUNCDNAME__, pack.Depth(), pack.Label());	
	moveSameDepth(pack, std::move(data));

	auto peek = peekData();

	if (!peek)
		return;
	if (peek->IsLabel() && peek->Depth() != pack.Depth())
		matchLabelValue(pack);
}

//필드값 오프셋 정렬하는 부분
void CDataSection::fieldAlignment()
{
	/// id1 [0,] id2 id2-1 [1,] [0,]
	for (;;)
	{
		std::unique_ptr<CDataAllocator> data = popData();

		if (!data)
			return; //stream EOF

		if (data->IsLabel())
		{
			CDataList pack(std::move(data));

			matchLabelValue(pack);
			emitFieldLabel(pack);
			continue;
		}
		//아마 여기를 흐르면, 레이블 되지 않은, 필드(예를 들어, 패딩같은)일 것입니다//
		m_globalData->pushGlobal(std::move(data));
	}
}

CDataLabel *CDataSection::loadLabel(const std::string &labelName)
{
	auto iter = m_labels.find(labelName);

	if (iter == m_labels.cend())
		return {};

	return iter->second.get();
}

std::unique_ptr<CDataLabel> CDataSection::popLabel(const std::string &labelName)
{
	auto lbIterator = m_labels.find(labelName);

	if (lbIterator != m_labels.cend())
	{
		std::unique_ptr<CDataLabel> outLabel = std::move(lbIterator->second);

		m_labels.erase(lbIterator);
		return outLabel;
	}
	return {};
}

CDataLabel *CDataSection::Retrieve(const std::string &labelName) const
{
    auto labelIterator = m_labels.find(labelName);

    if (labelIterator == m_labels.cend())
        return nullptr;

    return (labelIterator->second).get();
}

void CDataSection::joinBssField()
{
	std::unique_ptr<CDataLabel> label;

	for (auto &bss : m_bssMap)
	{
		label = bss.second->DetachLabel();
		m_field.push_back(std::make_unique<LabelAllocator>(label->Label(), label->IsGlobal()));
		m_field.push_back(bss.second->DetachBss());
		m_labels[label->Label()] = std::move(label);
	}
	m_bssMap.clear();
}

//링크의 첫 관문입니다
void CDataSection::PreLinking(FunctionTable &fnTable)
{
	joinBssField();	//bss 합류//
    for (auto &data : m_field)
        data->PreLink(fnTable, *this);
}

/****
A.c:	g1 v2 v3 g4 v5
B.c:	v1 v2 v3 g1 g4 v6

out(ng):	g1 v2 v3 g4 v5 / v1 v2 v3 g1 g4 v6
out(ok):	g1 v2 v3 g4 v5 / v1 v2 v3 v6

var이 global 인지?
var이 global table에 있는 지?
****/
void CDataSection::MoveToGlobal(CDataSection &global)
{
    m_globalData = &global;
	fieldAlignment();
    m_globalData = nullptr;
}

void CDataSection::PostLinking(FunctionTable &fnTable, int offset)
{
	constexpr int max_padding_count = 8;

	for (auto &data : m_field)
	{
		data->MakeOffset(offset);
		offset += data->Size();
	}
    for (auto &data : m_field)
        data->PostLink(fnTable, *this);

	int padding = offset % max_padding_count;

	if (padding)
	{
		padding = max_padding_count - (padding % max_padding_count);
		Allocate<BssDataAllocator>(padding);
		offset += padding;
	}
	m_lastLength = offset;
}

void CDataSection::Preorder(std::function<void(CDataAllocator&)> &&fn)
{
	for (auto &data : m_field)
		fn(*data);
}

void CDataSection::exchangeField(const std::set<std::string> &lbSet, CDataSection &getData)
{
    std::unique_ptr<CDataAllocator> alloc;
    const char *cuniq;

    for (;;)
    {
        if (getData.m_field.empty())
            return;
        alloc = std::move( getData.m_field.front() );
        cuniq = alloc->UniqueId();
        getData.m_field.pop_front();
        m_field.push_back(std::move(alloc));
        m_fieldHash[cuniq] = std::prev(m_field.end());
        //if (cuniq == uniq)
        //    break;
    }
}

void CDataSection::exchangeLabel(const std::string &key, std::unique_ptr<CDataLabel> lb)
{
    auto lbIter = m_labels.find(key);

    if (lbIter == m_labels.cend())
    {
        m_labels[key] = std::move( lb );
        return;
    }
    if (!lb->IsGlobal())
    {
        m_labels[stringFormat("%d-%s", ++m_uniqId, key)] = std::move(lb);
        return;
    }
    MY_THROW() << "dup key: " << key;  //Todo. extern 붙지 않은 광역변수, 이것은 어떻게 처리한다...
}

void CDataSection::Exchange(CDataSection &getData)
{
    ///getData에서 레이블을 가져오면된다///
    std::set<std::string> labelSet;

    for (auto &pr : getData.m_labels)
    {
        labelSet.insert(pr.second->UniqueLabel());
        exchangeLabel(pr.first, std::move( pr.second) );
    }
    exchangeField(labelSet, getData);
    //TODO;
    getData.m_labels.clear();
}
