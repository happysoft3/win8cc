
#ifndef C_DATA_SECTION_H__
#define C_DATA_SECTION_H__

#include <list>
#include <memory>
#include <map>
#include <functional>
#include <set>

class CDataAllocator;
class CDataLabel;
class FunctionTable;

class CDataList;

//codegen에서 이 클래스에 data을 넣고, 이 클래스가 가진 linking함수에서
//global 클래스로 한번 더 data 을 이동합니다
class CDataSection
{
	class BssData;
private:
	using field_list_ty = std::list<std::unique_ptr<CDataAllocator>>;
	field_list_ty m_field;
	using field_list_iter = field_list_ty::iterator;
	std::map<std::string, field_list_iter> m_fieldHash;
	std::map<std::string, std::unique_ptr<CDataLabel>> m_labels;
	int m_uniqueLabel;
	const int m_uniqueSeed;
	int m_baseOffset;
    CDataSection *m_globalData;
	int m_lastLength;

	std::map<std::string, std::unique_ptr<BssData>> m_bssMap;
    int m_uniqId;

public:
	CDataSection(int baseOffset = 0);
	~CDataSection();

private:
	std::unique_ptr<CDataLabel> makeLabel(const std::string &label, bool isStatic = true);

public:
	int DataLength() const
	{
		return m_lastLength;
	}
	const char *PutVLabel(int depth);
	void PutLabel(const std::string &clabel, bool isStatic = false);
	void PutBssLabel(const std::string &clabel, int fieldSize, bool isStatic = false);

private:
	void allocateImpl(CDataAllocator &allocator, int depth);

public:
	template <class Ty,	
		class... Args,
        typename std::enable_if<std::is_base_of<CDataAllocator, Ty>::value, bool>::type = true
        >	//syntatic sugar
	void AllocateWithDepth(int depth, Args&&... args)
	{
		std::unique_ptr<CDataAllocator> p = std::make_unique<Ty>(std::forward<Args>(args)...);

		allocateImpl(*p, depth);
		m_field.push_back(std::move(p));
	}

	template <class Ty, class... Args>
	void Allocate(Args&&... args)
	{
		AllocateWithDepth<Ty>(0, std::forward<Args>(args)...);
	}

private:	//globals
	void pushGlobal(std::unique_ptr<CDataAllocator> data);
	void eraseGlobal(const char *uniqId);

private:
	std::unique_ptr<CDataAllocator> popData();
	CDataAllocator *peekData() const;
    void ungetData(std::unique_ptr<CDataAllocator> data);
	void mergeData(CDataLabel &glabel, CDataList &pack);
	void pushLabelData(CDataList &pack);
    void emitFieldLabel(CDataList &pack);
	void matchLabelEx(CDataList &pack, std::unique_ptr<CDataAllocator> data);
	void moveSameDepth(CDataList &pack, std::unique_ptr<CDataAllocator> data);
	void matchLabelValue(CDataList &pack);
	void fieldAlignment();
	CDataLabel *loadLabel(const std::string &labelName);
	std::unique_ptr<CDataLabel> popLabel(const std::string &labelName);

public:
    CDataLabel *Retrieve(const std::string &labelName) const;

private:
	void joinBssField();

public:
    void PreLinking(FunctionTable &fnTable);
	void MoveToGlobal(CDataSection &global);
	void PostLinking(FunctionTable &fnTable, int offset = 0);
	void Preorder(std::function<void(CDataAllocator &)> &&fn);

private:
    void exchangeField(const std::set<std::string> &lbSet, CDataSection &getData);
    void exchangeLabel(const std::string &key, std::unique_ptr<CDataLabel> lb);

public:
    void Exchange(CDataSection &getData);
};

#endif

