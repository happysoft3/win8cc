
#include "cDataLabel.h"
#include "cDataAllocator.h"
#include "hook/link/cclink.h"
#include "common/utils/mydebug.h"

CDataLabel::CDataLabel(const std::string &labelName)
{
	m_labelName = labelName;
	m_useGlobalScope = false;
	m_pointing = nullptr;
}

CDataLabel::~CDataLabel()
{ }

CDataAllocator *CDataLabel::Merge(CDataAllocator *diff)
{	//bss이니까, replace 이 필요해도, 하나만 떼면 된다
	bool isBss = dynamic_cast<BssDataAllocator *>(m_pointing) != nullptr;
	bool isDiffBss = dynamic_cast<BssDataAllocator *>(diff) != nullptr;

    if (!isBss && !isDiffBss)
        MY_THROW() << "multiple definition of " << diff->Label() << "--" << m_labelName;

	if (isBss)
	{
        auto ret = m_pointing;
		m_pointing = diff;
		return ret;
	}
	return nullptr;
}

std::shared_ptr<CCLink> CDataLabel::TryLoadOffset() const
{
    if (!m_pointing)
        MY_THROW() << "no target object";
        //return{ };

    return m_pointing->GetOffset();
}

const char *const CDataLabel::UniqueLabel() const
{
    if (!m_pointing)
        return "";

    return m_pointing->UniqueId();
}
