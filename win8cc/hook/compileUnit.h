
#ifndef COMPILE_UNIT_H__
#define COMPILE_UNIT_H__

#include "common/taskthread/taskUnit.h"
#include <string>
#include <memory>
#include <future>

class CCParse;
class NoxCodegen;
class CCResult;
class BuildEnvironment;
class BuildSourceFile;
class GPointerOffsetTable;

class CompileUnit : public TaskUnit
{
private:
	std::unique_ptr<CCParse> m_parser;
	std::unique_ptr<NoxCodegen> m_codegen;
	std::unique_ptr<std::future<std::unique_ptr<CCResult>>> m_compileOutput;

public:
	CompileUnit();
	~CompileUnit() override;

private:
	void compileImpl();
	std::unique_ptr<CCResult> tryCompile();
	void showBeforeCompile(CCResult &res);

public:
	void PutSource(const BuildSourceFile &source, BuildEnvironment &env, int uniqId = 0);
	void PutGPointerTable(std::shared_ptr<GPointerOffsetTable> &table);

private:
	void Run() override;
	std::unique_ptr<TaskResult> GetResult() override;
};

#endif

