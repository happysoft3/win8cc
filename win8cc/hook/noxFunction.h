
#ifndef NOX_FUNCTION_H__
#define NOX_FUNCTION_H__

#include "hook/output/scrOutStream.h"
#include <string>
#include <memory>
#include <list>
#include <map>
#include <functional>

class NoxOpcode;
class NoxOpLabel;
class CCLink;

class NoxFunction : public ScrOutStream
{
private:
    std::string m_name;
    std::list<std::unique_ptr<NoxOpcode>> m_opcodes;
	std::shared_ptr<CCLink> m_uniqId;
	bool m_isStatic;
	std::map<std::string, std::weak_ptr<CCLink>> m_labels;
	NoxOpcode *m_latestOpcode;
	std::string m_staticTag;
	bool m_hasReturn;
	std::list<int> m_nativeVarInfo;
	int m_nativeArgCount;

public:
    NoxFunction(const std::string &fn = { }, bool isStatic = false);
    ~NoxFunction() override;

private:
	void emitNativeVarInfo(ScrOutStream &scrOs);
	void preEmitCode(int &wholeLength);
	virtual void emitCode(ScrOutStream &scrOs);

protected:
    void emitFunctionIdentifier(ScrOutStream &scrOs);

private:
	void onReleaseStream(ScrOutStream &scrOs) override;

public:
    const char *Name() const;
    void ChangeName(const std::string &name);
    void PushOp(std::unique_ptr<NoxOpcode> opcode);
    void PushFrontOp(std::unique_ptr<NoxOpcode> opcode);
	std::unique_ptr<NoxOpcode> PopOp();
    void SetUniqueId(int uniqId);
	std::shared_ptr<CCLink> UniqueId() const;
	bool IsStatic() const
	{
		return m_isStatic;
	}
	void PutLabel(const std::string &label);
	std::weak_ptr<CCLink> GetLabelAddress(const std::string &label) const;
	void OpcodePreorder(std::function<void(NoxOpcode&)> &&fn);

private:
	void generateStaticTag();

public:
	const char *const StaticTag();
	void ChangeReturnFlag(bool tof)
	{
		m_hasReturn = tof;
	}
	void PushNativeVar(int size, bool isArg = false);
};

#endif

