
#include "joinCompilation.h"
#include "common/taskthread/taskResultHash.h"
#include "common/taskthread/taskUnit.h"
#include "buildEnvironment.h"

static constexpr size_t default_numof_thread = 2;
JoinCompilation::JoinCompilation()
	: TaskThread()
{ }

JoinCompilation::~JoinCompilation()
{ }

void JoinCompilation::doMakeThread()
{
	if (m_procVector.empty())
		m_procVector.resize(default_numof_thread);
	for (auto &proc : m_procVector)
		proc = std::make_unique<TaskThread>();
}

bool JoinCompilation::beforeExec(std::shared_ptr<TaskQueue> queue)
{
	doMakeThread();

	for (auto &proc : m_procVector)
		proc->Perform(queue);

	return false;
}

void JoinCompilation::afterExec(TaskResultHash &resultMap) noexcept
{
	for (auto &proc : m_procVector)
	{
		std::unique_ptr<TaskResultHash> res = proc->GetResult();

		if (!res->Empty())
			resultMap.Merge(*res);
	}
}

bool JoinCompilation::setThreadCount(size_t count)
{
	if (!count)
		return false;

	if (count > 8)
		return false;

	m_procVector.resize(count);
	return true;
}

void JoinCompilation::SetDetailOption(BuildEnvironment &buildOpt)
{
	std::string nOfThreads = buildOpt.GetOption("CCNumberOfThreads");

	if (nOfThreads.empty())
		return;

	std::stringstream ss;
	int nThreads = 0;

	ss << nOfThreads;
	ss >> nThreads;
	setThreadCount(nThreads);
}

