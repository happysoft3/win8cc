
#include "compilation.h"
#include "compileLinker.h"
#include "compileUnit.h"
#include "hook/functionTable.h"
#include "hook/cCResult.h"
#include "output/scrDebugFormat.h"
#include "output/scrStringTable.h"
#include "output/scrCodeTable.h"
#include "buildEnvironment.h"
#include "portable/buildSourceFile.h"
#include "joinCompilation.h"
#include "global/gpointerOffsetTable.h"
#include "common/taskthread/taskQueue.h"
#include "common/taskthread/taskResultHash.h"
#include "common/utils/mydebug.h"
#include "common/utils/stringhelper.h"
#include <iostream>

using namespace _StringHelper;

Compilation::Compilation()
{
	m_gpointerTable = std::make_shared<GPointerOffsetTable>();
    m_unitCount = 0;
}

Compilation::~Compilation()
{ }

void Compilation::readyToCompile(const BuildSourceFile &sourceFile)
{
	auto unit = std::make_unique<CompileUnit>();

	unit->PutSource(sourceFile, *m_env, ++m_unitCount);
	unit->PutGPointerTable(m_gpointerTable);
	m_taskQueue->Push(std::move(unit));
}

void Compilation::PutEnvironment(std::unique_ptr<BuildEnvironment> env)
{
	m_env = std::move(env);
	m_taskQueue = std::make_unique<TaskQueue>();
	m_env->DoSourceFiles([this](const BuildSourceFile &s) { readyToCompile(s); });
}

void Compilation::preLinking(CompileLinker &linker)
{
	std::unique_ptr<std::exception> err;
	std::unique_ptr<CCResult> res;
	auto compileResult = m_joinThread->GetResult();

	for (;;)
	{
		res = compileResult->PopAsValidType<CCResult>();

		if (!res)
			break;

		auto err = res->GetError();

		if (err)
			throw *err;
		linker.Prelink(*res);
	}
}

void Compilation::postLinking(CompileLinker &linker)
{
	linker.Postlink();	//마지막 링킹, 오프셋을 여기에서 부여합니다
}

void Compilation::CompileAll()
{
	if (!m_taskQueue)
		throw std::exception(__FUNCSIG__);

	m_joinThread = std::make_unique<JoinCompilation>();
	if (m_env)
		m_joinThread->SetDetailOption(*m_env);
	m_joinThread->Perform(std::move(m_taskQueue));
}

void Compilation::Linking()
{
	auto linker = std::make_unique<CompileLinker>();
	std::string processingName;

	try
	{
		processingName = "PRELINK";
		preLinking(*linker);
		processingName = "POSTLINK";
		postLinking(*linker);
	}
	catch (const std::logic_error &except)
	{
        MY_THROW() << stringFormat("ERROR in %s::%s:%s", __FUNCSIG__, processingName, except.what());
		return;
	}
	m_linker = std::move(linker);
}

void Compilation::createOutputImpl(ScrOutFormat &form)
{
	auto strTable = std::make_unique<ScrStringTable>();

	strTable->Append("NOXSCRIPT03");	//기존 버전처럼, 기본적으로 0번 섹션에 문자열 하나를 넣어둔다
	form << *strTable;

	auto codeTable = std::make_unique<ScrCodeTable>();

	codeTable->PutGPointerTable(m_gpointerTable);
	codeTable->PutOutputParam(*m_linker, *m_env);
	
    form << *codeTable;
	form << "DONE";	//maybe need?
}

void Compilation::CreateOutput()
{
	if (!m_linker)
		MY_THROW() << "linker is missing";

	std::unique_ptr<ScrOutFormat> form = (m_env->UseDebug()) ? std::make_unique<ScrDebugFormat>() : std::make_unique<ScrOutFormat>();

	if (!form->Create(*m_env))
		MY_THROW() << "cannot create an output file";
	createOutputImpl(*form);
}

