
#ifndef NOX_CODEGEN_H__
#define NOX_CODEGEN_H__

#include "noxOpGenerator.h"
#include <string>
#include <memory>

class AstType;
class AstStructType;

class AstVector;

class AstNode;
class AstDeclNode;
class AstFunctionNode;
class AstVariableNode;
class AstGlobalVariableNode;
class AstDeclNode;
class AstUnaryOpNode;
class AstBinaryOpNode;
class AstIfNode;
class AstGotoNode;
class AstReturnNode;
class AstInitializerNode;
class AstCompoundStmtNode;

class CCVector;
class NoxFunction;
class FunctionTable;
class CDataSection;
class AbstractRegister;
class CCParse;
class MemorySweeper;
class GPointerOffsetTable;

class NoxCodegen : public NoxOpGenerator
{
private:
    int m_stackpos;
    std::unique_ptr<FunctionTable> m_sourceFunction;
    std::unique_ptr<CCParse> m_parser;
	std::unique_ptr<CDataSection> m_dataSection;
	std::unique_ptr<MemorySweeper> m_memSweep;
	std::shared_ptr<GPointerOffsetTable> m_gpointerTable;

public:
    explicit NoxCodegen(int uniqId = 0);
    ~NoxCodegen() override;

protected:
	void genOpPush(AbstractRegister &regValue) override;
	void genOpPop(AbstractRegister &regAddress) override;

private:
    void emitToBool(AstType *ty);
    void emitIntCast(AstType *ty);
    void emitToInt(AstType *fTy);
    void emitLoadConvert(AstType *to, AstType *from);
    void maybeConvertBool(AstType *ty);
    void emitConvert(AstUnaryOpNode *node);
    void emitDereference(AstUnaryOpNode *node);
    void emitCast(AstUnaryOpNode *node);

    void doEmitAssignDereference(AstType *ty, int off);
    void emitAssignDeref(AstUnaryOpNode *var);
    void emitAssignStructReference(AstNode *struc, AstType *field, int off);
    void emitStore(AstNode *var);
    void emitCompoundStatement(AstCompoundStmtNode *node);
	void emitCopyStructMemcopy(AstNode *left, AstNode *right);
    void emitCopyStruct(AstNode *left, AstNode *right);
    void emitLoadStructRef(AstNode *struc, AstType *field, int off);
    void emitAssign(AstBinaryOpNode *node);
    void emitComma(AstBinaryOpNode *node);

    //Arithmetic
    void emitPreIncOrDec(AstUnaryOpNode *node, const std::string &op);
    void emitPostIncOrDec(AstUnaryOpNode *node, const std::string &op);

    //bit logical
    void emitLogNot(AstUnaryOpNode *node);
    void emitLogAnd(AstBinaryOpNode *node);
    void emitLogOr(AstBinaryOpNode *node);
    void emitBitAnd(AstBinaryOpNode *node);
    void emitBitOr(AstBinaryOpNode *node);
    void emitBitNot(AstUnaryOpNode *node); //unary??

    //Flow controls
    void emitJMP(const std::string &label);
    void emitJE(const std::string &label);
    void emitTernary(AstIfNode *node);
    void emitGoto(AstGotoNode *node);
    void emitLabel(const std::string &label);
    void emitComputedGoto(AstNode *node);


    //Binary Operator
    void emitIntCompare(AstBinaryOpNode *node, int compareKind);
    void emitFloatCompare(AstNode *node, int compareKind);
    void emitCompare(AstBinaryOpNode *node, int compareKind);
    void emitPointerArith(char kind, AstNode *left, AstNode *right);
    void emitBinaryOpIntArith(AstBinaryOpNode *node);
    void emitBinaryOpFloatArith(AstBinaryOpNode *node);
    void emitBinaryOp(AstNode *node);

    //
    //Declare
    void emitDeclare(AstDeclNode *node);

    //Functions
    void maybeBooleanizeReturnValue(AstType *ty);
    void restoreArgRegs(int nints, int nFloats);
    void popIntArgs(int nints);
    int emitArgs(CCVector *vals);
    void saveArgRegs(int nints, int nfloats);
    void classifyArgs(CCVector *ints, CCVector *floats, CCVector *rest, AstFunctionNode *fnode); //CCVector *args);
    void emitFunctionCall(AstFunctionNode *node);

	void emitLiteral(AstNode *node, bool direct = false);
    void maybeEmitBitshiftSave(AstType *ty, AbstractRegister &addr);
	void maybeEmitBitshiftLoad(AstType *ty);
	void emitLLoad(AstType *ty, AbstractRegister &base, int off);
    void emitGLoad(AstType *ty, const char *label, int off);
	void emitZeroFiller(int start, int end);
	void emitFillHoles(AstVector *inits, int off, int totalsize);
	void emitSaveLiteral(AstNode *node, AstType *totype, int off);
	void emitLsave(AstType *ty, int off);
    void emitGSave(const char *varname, AstType *ty, int off);
	void emitDeclareInit(AstVector *inits, int off, int totalsize);
	void ensureLvarInit(AstVariableNode *node);
	void emitLvar(AstVariableNode *node);
	void emitGvar(AstGlobalVariableNode *node);
	void emitAddress(AstNode *node);
	int pushStructMemcopy(int size);
    int pushStruct(int size);
	void pushFunctionParamsRewritten(const AstFunctionNode &fnode);
    void pushFunctionParams(AstVector *params, int off);
    void emitReturn(AstReturnNode *node);
    void emitRet();
    void emitExpr(AstNode *node);
    void emitFunctionPrologue(AstFunctionNode *func);
    
private:    //Global proc
    void emitZero(int size);
    void emitPadding(AstInitializerNode *node, int off);
    void emitDataCharptr(const AstNode *node, int depth);
    void emitDataAddr(AstNode *operand, int depth);
    void emitDataPrimaryType(AstType *ty, AstNode *val, int depth);
    void doEmitData(AstVector *inits, int size, int off, int depth);
    void emitData(AstDeclNode *v, int off, int depth);
    void emitBss(AstDeclNode *v);
    void emitGlobalVariable(AstDeclNode *v);

public:
	void AttachParser(std::unique_ptr<CCParse> parser)
	{
		m_parser = std::move(parser);
	}
	std::unique_ptr<CCParse> DetachParser()
	{
		return std::move(m_parser);
	}
	void PutGPointerTable(std::shared_ptr<GPointerOffsetTable> &table)
	{
		m_gpointerTable = table;
	}
    void EmitToplevel(AstNode *v);
	std::unique_ptr<FunctionTable> ReleaseFunctionTable();
	std::unique_ptr<CDataSection> ReleaseField();
};

#endif

