
#include "instructionList.h"
#include "instruction.h"
#include "hook/register/abstractregister.h"

#include <functional>

class BranchInst : public Instruction
{
private:
    std::unique_ptr<InstructionList> m_branch;

public:
    explicit BranchInst(std::unique_ptr<InstructionList> branch)
        : Instruction()
    {
        m_branch = std::move(branch);
    }
    ~BranchInst() override{}

private:
    void emitInst(RegisterOutputTarget &target) override
    {
        m_branch->EmitInstAll(target);
    }

    bool MergeInst(InstructionList &iList) override
    {
        bool ret = m_branch->ConvertBranch(iList);

        m_branch->ConvertInst();
        return ret;
    }

    bool TryJoin(InstructionList &iList) override
    {
        return true;
    }

    void TryMerge(InstructionList &list) override
    {
        m_branch->PreorderInst();
    }
};

InstructionList::InstructionList()
{
    m_parent = nullptr;
}

InstructionList::InstructionList(std::unique_ptr<Instruction> inst)
{
    m_parent = nullptr;
    putBranchInst(std::move(inst));
}

InstructionList::~InstructionList()
{}

InstructionList *InstructionList::putBranch(std::unique_ptr<InstructionList> branch)
{
    auto ret = branch.get();

    branch->m_parent = this;

    std::unique_ptr<Instruction> binst = std::make_unique<BranchInst>(std::move(branch));

    PutInst(binst);
    return ret;
}

void InstructionList::putBranchInst(std::unique_ptr<Instruction> inst)
{
    inst->BindParams(*this);
    m_branch = std::move(inst);
}

void InstructionList::PutTarget(const AbstractRegister *address, const AbstractRegister *value)
{
    if (address)
        m_addressTarget = std::unique_ptr<AbstractRegister>(address->Clone());
    if (value)
        m_valueTarget = std::unique_ptr<AbstractRegister>(value->Clone());
}

void InstructionList::ResetValue()
{
    if (m_valueTarget)
        m_valueTarget.reset();
}

void InstructionList::PutInst(std::unique_ptr<Instruction> &inst)
{
    if (inst->TryJoin(*this) || (!m_parent))
        m_instlist.push_back(std::move(inst));
}

void InstructionList::PreorderInst()
{
    for (auto &inst : m_instlist)
    {
        inst->TryMerge(*this);
    }
}

bool InstructionList::CanConv() const
{
    if (!m_branch)
        return false;

    if (!m_valueTarget)
        return false;

    return true;
}

bool InstructionList::ConvertBranch(InstructionList &iList)
{
    auto a = iList.AddressTarget(), v = ValueTarget();

    if (a && v)
    {
        if (a->IsSame(*v))
        {
            return m_branch->MergeInst(iList);
        }
    }
    return false;
}

void InstructionList::ConvertInst()
{
    bool merged = false;

    for (auto &inst : m_instlist)
    {
        if (inst->MergeInst(*this))
        {
            if (!merged)
                merged = true;
        }
    }
    if (!merged && m_branch)
        m_instlist.push_front(std::move(m_branch));
}

void InstructionList::EmitInstAll(RegisterOutputTarget &outctx)
{
    std::unique_ptr<Instruction> inst;

    while (m_instlist.size())
    {
        inst = std::move(m_instlist.front());
        m_instlist.pop_front();
        inst->emitInst(outctx);
    }
}

void InstructionList::CanIgnore(std::unique_ptr<Instruction> &inst)
{
    if (m_instlist.empty())
        return;

    Instruction *pp = m_instlist.back().get();
    Instruction **prev = &pp;

    if (inst->DoIgnore(prev))
        inst.reset();
    if (!(*prev))
        m_instlist.pop_back();
}
