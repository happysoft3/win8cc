
#ifndef INSTRUCTION_H__
#define INSTRUCTION_H__

#include <memory>
#include <string>

class RegisterOutputTarget;
class AbstractRegister;
class InstructionList;

namespace InstructionGuard
{
    using reg_fn_arg_ty = const AbstractRegister &;
}

class Instruction
{
private:
    std::unique_ptr<AbstractRegister> m_addressTarget;

public:
    Instruction();
    explicit Instruction(InstructionGuard::reg_fn_arg_ty r);
    virtual ~Instruction();

protected:
    AbstractRegister *targetAddress() const
    {
        return m_addressTarget.get();
    }
    std::unique_ptr<AbstractRegister> replaceTargetAddress(InstructionGuard::reg_fn_arg_ty r);

public:
    virtual bool IsBranch() const
    {
        return false;
    }
    virtual bool TryJoin(InstructionList &iList)
    {
        return false;
    }
    virtual void BindParams(InstructionList &iList);
    virtual void TryMerge(InstructionList &iList);

protected:
    virtual bool equivalentTo(const Instruction *inst) const;

private:
    virtual bool doMerge(InstructionList &iList)
    {
        return false;
    }

public:
    virtual bool MergeInst(InstructionList &iList);
    virtual void emitInst(RegisterOutputTarget &target) = 0;
    virtual bool DoIgnore(Instruction **prev) {
        return false;
    }
};

class InstPush : public Instruction
{
public:
    explicit InstPush(InstructionGuard::reg_fn_arg_ty r);
    ~InstPush() override;
    
private:
    bool TryJoin(InstructionList &iList) override;
    void TryMerge(InstructionList &iList) override {};
    bool doMerge(InstructionList &iList) override;
    void emitInst(RegisterOutputTarget &target) override;
    bool DoIgnore(Instruction **prev) override;
};

class InstPop : public Instruction
{
public:
    explicit InstPop(InstructionGuard::reg_fn_arg_ty r);
    ~InstPop() override;

private:
    bool TryJoin(InstructionList &iList) override;
    void emitInst(RegisterOutputTarget &target) override;
};

class BinaryInst : public Instruction
{
private:
    std::unique_ptr<AbstractRegister> m_secondary;

public:
    explicit BinaryInst(InstructionGuard::reg_fn_arg_ty first, InstructionGuard::reg_fn_arg_ty secondary);
    ~BinaryInst() override;

protected:
    AbstractRegister *secondary() const
    {
        return m_secondary.get();
    }
    void replaceSecondary(InstructionGuard::reg_fn_arg_ty r);
    bool equivalentTo(const Instruction *inst) const override;
    //bool mergeInst(RegisterOutputTarget &target, Instruction **inst) override;
};

class InstMov : public BinaryInst
{
public:
    explicit InstMov(InstructionGuard::reg_fn_arg_ty src, InstructionGuard::reg_fn_arg_ty dest);
    ~InstMov() override;

private:
    bool IsBranch() const override
    {
        return true;
    }
    bool doMerge(InstructionList &iList) override;
    void BindParams(InstructionList &iList) override;
    void emitInst(RegisterOutputTarget &target) override;
};

class InstMovN : public BinaryInst
{
private:
    int m_instSize;

public:
    explicit InstMovN(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg, int size);
    ~InstMovN() override;

private:
    void emitInst(RegisterOutputTarget &target) override;
};

class InstMovzx : public BinaryInst
{
public:
    explicit InstMovzx(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg);
    ~InstMovzx() override;

private:
    bool DoIgnore(Instruction **prev) override;
    void emitInst(RegisterOutputTarget &target) override;
};

class InstMovsx : public BinaryInst
{
public:
    explicit InstMovsx(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg);
    ~InstMovsx() override;

private:
    bool DoIgnore(Instruction **prev) override;
    void emitInst(RegisterOutputTarget &target) override;
};

class InstAdd : public BinaryInst
{
public:
    explicit InstAdd(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg);
    ~InstAdd() override;

private:
    bool TryJoin(InstructionList &iList) override;
    void emitInst(RegisterOutputTarget &target) override;
};

class InstSub : public BinaryInst
{
public:
    explicit InstSub(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg);
    ~InstSub() override;

private:
    bool TryJoin(InstructionList &iList) override;
    void emitInst(RegisterOutputTarget &target) override;
};

class InstMul : public BinaryInst
{
public:
    explicit InstMul(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg);
    ~InstMul() override;

private:
    void emitInst(RegisterOutputTarget &target) override;
};

class InstDiv : public BinaryInst
{
private:
    bool m_unsigned;

public:
    explicit InstDiv(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg, bool isUnsigned);
    ~InstDiv() override;

private:
    void emitInst(RegisterOutputTarget &target) override;
};

class InstMod : public BinaryInst
{
private:
    bool m_unsigned;

public:
    explicit InstMod(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg, bool isUnsigned);
    ~InstMod() override;

private:
    void emitInst(RegisterOutputTarget &target) override;
};

class InstCallFromReg : public Instruction
{
public:
    explicit InstCallFromReg(InstructionGuard::reg_fn_arg_ty r);
    ~InstCallFromReg() override;

private:
    void emitInst(RegisterOutputTarget &target) override;
};

class InstCall : public Instruction
{
private:
    std::string m_fname;

public:
    explicit InstCall(const std::string &fn);
    ~InstCall() override;

private:
    void emitInst(RegisterOutputTarget &target) override;
};

class InstLea : public BinaryInst
{
public:
    explicit InstLea(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg);
    ~InstLea() override;

private:
    bool TryJoin(InstructionList &iList) override;
    void TryMerge(InstructionList &iList) override{}
    void emitInst(RegisterOutputTarget &target) override;
};

class InstBinaryOp : public BinaryInst
{
private:
    int m_builtinId;

public:
    explicit InstBinaryOp(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg, int id);
    ~InstBinaryOp() override;

private:
    void emitInst(RegisterOutputTarget &target) override;
};

class InstNot : public Instruction
{
public:
    explicit InstNot(InstructionGuard::reg_fn_arg_ty r);
    ~InstNot() override;

private:
    void emitInst(RegisterOutputTarget &target) override;
};

class InstComparison : public BinaryInst
{
private:
    int m_cmpty;

public:
    explicit InstComparison(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg, int id);
    ~InstComparison() override;

private:
    bool TryJoin(InstructionList &iList) override {
        return true;
    }
    void TryMerge(InstructionList &iList) override {};
    bool doMerge(InstructionList &iList) override;
    void emitInst(RegisterOutputTarget &target) override;
};

class InstCmpResult : public Instruction
{
public:
    explicit InstCmpResult(InstructionGuard::reg_fn_arg_ty r);
    ~InstCmpResult() override;

private:
    void emitInst(RegisterOutputTarget &target) override;
};

class InstLabel : public Instruction
{
private:
    std::string m_labelName;

public:
    explicit InstLabel(const std::string &ln);
    ~InstLabel() override;

protected:
    const char *const labelName() const
    {
        return m_labelName.c_str();
    }

private:
    void emitInst(RegisterOutputTarget &target) override;
};

class InstJump : public InstLabel
{
public:
    explicit InstJump(const std::string &ln);
    ~InstJump() override;

private:
    //bool mergeInst(RegisterOutputTarget &target, Instruction **inst) override;
    void emitInst(RegisterOutputTarget &target) override;
};

class InstJumpConditional : public InstLabel
{
private:
    bool m_isEqual;

public:
    explicit InstJumpConditional(InstructionGuard::reg_fn_arg_ty rCmp, const std::string &ln, bool isEqual);
    ~InstJumpConditional() override;

private:
    void emitInst(RegisterOutputTarget &target) override;
};

class InstReturn : public Instruction
{
public:
    InstReturn();
    ~InstReturn() override;

private:
    void emitInst(RegisterOutputTarget &target) override;
};

class InstMemCopy : public Instruction
{
private:
    std::unique_ptr<AbstractRegister> m_src;
    std::unique_ptr<AbstractRegister> m_repeat;

public:
    explicit InstMemCopy(InstructionGuard::reg_fn_arg_ty src, InstructionGuard::reg_fn_arg_ty dest, InstructionGuard::reg_fn_arg_ty repeat);
    ~InstMemCopy() override;

private:
    void emitInst(RegisterOutputTarget &target) override;
};

class InstZeroFiller : public BinaryInst
{
public:
    explicit InstZeroFiller(InstructionGuard::reg_fn_arg_ty src, InstructionGuard::reg_fn_arg_ty dest);
    ~InstZeroFiller() override;

private:
    void emitInst(RegisterOutputTarget &target) override;
};

class InstStoreLocal : public Instruction
{
private:
    int m_off;

public:
    explicit InstStoreLocal(InstructionGuard::reg_fn_arg_ty r, int off);
    ~InstStoreLocal() override;

private:
    bool TryJoin(InstructionList &iList) override
    {
        return true;
    }
    bool doMerge(InstructionList &iList) override;
    void emitInst(RegisterOutputTarget &target) override;
};

class InstTest : public Instruction
{
private:
    bool m_testnot;

public:
    explicit InstTest(bool testnot);
    ~InstTest();

private:
    void emitInst(RegisterOutputTarget &target) override;
};


#endif

