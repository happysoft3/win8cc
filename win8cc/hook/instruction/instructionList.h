
#ifndef INSTRUCTION_LIST_H__
#define INSTRUCTION_LIST_H__

#include <memory>
#include <list>

class Instruction;
class AbstractRegister;
class RegisterOutputTarget;

class InstructionList
{
private:
    std::list<std::unique_ptr<Instruction>> m_instlist;
    InstructionList *m_parent;
    std::unique_ptr<Instruction> m_branch;
    std::unique_ptr<AbstractRegister> m_addressTarget;
    std::unique_ptr<AbstractRegister> m_valueTarget;

public:
    InstructionList();
    explicit InstructionList(std::unique_ptr<Instruction> inst);
    ~InstructionList();

private:
    InstructionList *putBranch(std::unique_ptr<InstructionList> branch);
    void putBranchInst(std::unique_ptr<Instruction> inst);

public:
    AbstractRegister *AddressTarget() const
    {
        return m_addressTarget.get();
    }
    AbstractRegister *ValueTarget() const
    {
        return m_valueTarget.get();
    }
    InstructionList *GetParent() const
    {
        return m_parent;
    }
    void PutTarget(const AbstractRegister *address, const AbstractRegister *value);
    void ResetValue();
    void PutInst(std::unique_ptr<Instruction> &inst);

    template <class... Args>
    InstructionList *CreateBranch(Args&&... args)
    {
        return putBranch(std::make_unique<InstructionList>(std::forward<Args>(args)...));
    }
    void PreorderInst();
    bool CanConv() const;
    bool ConvertBranch(InstructionList &iList);
    void ConvertInst();
    void EmitInstAll(RegisterOutputTarget &outctx);
    void CanIgnore(std::unique_ptr<Instruction> &inst);
};

#endif

