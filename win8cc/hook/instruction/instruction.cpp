
#include "instruction.h"
#include "hook/register/registerOutputTarget.h"
#include "instructionList.h"
#include "hook/register/intregister.h"
#include "hook/register/abstractregister.h"
#include "hook/noxfunction.h"
#include "hook/noxOpcode.h"
#include "common/utils/mydebug.h"

#include <sstream>

Instruction::Instruction()
{ }

Instruction::Instruction(InstructionGuard::reg_fn_arg_ty r)
{
    replaceTargetAddress(r);
}

Instruction::~Instruction()
{ }

std::unique_ptr<AbstractRegister> Instruction::replaceTargetAddress(InstructionGuard::reg_fn_arg_ty r)
{
    std::unique_ptr<AbstractRegister> old = std::move(m_addressTarget);

    m_addressTarget = std::unique_ptr<AbstractRegister>(r.Clone());
    return old;
}

void Instruction::BindParams(InstructionList &iList)
{
    iList.PutTarget(targetAddress(), nullptr);
}

void Instruction::TryMerge(InstructionList &iList)
{
    iList.ResetValue();
}

bool Instruction::equivalentTo(const Instruction *inst) const
{
    return m_addressTarget->IsSame(*inst->m_addressTarget);
}

bool Instruction::MergeInst(InstructionList &iList)
{
    if (iList.CanConv())
        return doMerge(iList);

    return false;
}

InstPush::InstPush(InstructionGuard::reg_fn_arg_ty r)
    : Instruction(r)
{ }

InstPush::~InstPush()
{ }

bool InstPush::TryJoin(InstructionList &iList)
{
    //mov eax, 30
    //push eax
    return true;
}

bool InstPush::doMerge(InstructionList &iList)
{
    auto targ = iList.AddressTarget();

    if (targ)
    { //mov eax, 0; //push eax => push 0
        if (targ->IsSame(*targetAddress()))
        {
            replaceTargetAddress(*iList.ValueTarget());
            return true;
        }
    }
    return false;
}

void InstPush::emitInst(RegisterOutputTarget &target)
{
    targetAddress()->Value(target);
    target.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_EUDPush);    
    target.PutDebugPrettyname("push- ", targetAddress()->DebugName());
}

bool InstPush::DoIgnore(Instruction **prev)
{
    //pop eax
    //push eax
    if (dynamic_cast<InstPop *>(*prev))
    {
        if (equivalentTo(*prev))
        {
            *prev = nullptr;
            return true;
        }
    }
    return false;
}

InstPop::InstPop(InstructionGuard::reg_fn_arg_ty r)
    : Instruction(r)
{}

InstPop::~InstPop()
{}

bool InstPop::TryJoin(InstructionList &iList)
{
    if (!iList.AddressTarget())
        return true;

    auto r = targetAddress();

    //mov eax, ...
    //...
    //pop eax
    //add ecx, eax
    if (r->IsSame(*iList.AddressTarget()))
        return false;
    return true;
}

void InstPop::emitInst(RegisterOutputTarget &target)
{
    auto r = targetAddress();

    r->Address(target);
    target.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_EUDPop);
    target.PutDebugPrettyname("pop- ", r->DebugName());
}

BinaryInst::BinaryInst(InstructionGuard::reg_fn_arg_ty first, InstructionGuard::reg_fn_arg_ty secondary)
    : Instruction(first)
{
    m_secondary = std::unique_ptr<AbstractRegister>(secondary.Clone());
}

BinaryInst::~BinaryInst()
{}

void BinaryInst::replaceSecondary(InstructionGuard::reg_fn_arg_ty r)
{
    m_secondary = std::unique_ptr<AbstractRegister>(r.Clone());
}

bool BinaryInst::equivalentTo(const Instruction *inst) const
{
    const BinaryInst *bInst = dynamic_cast<const BinaryInst *>(inst);

    if (!bInst)
        return false;

    if (bInst->m_secondary->IsSame(*m_secondary))
        return Instruction::equivalentTo(inst);
    return false;
}

InstMov::InstMov(InstructionGuard::reg_fn_arg_ty src, InstructionGuard::reg_fn_arg_ty dest)
    : BinaryInst(dest, src)
{}

InstMov::~InstMov()
{}

bool InstMov::doMerge(InstructionList &iList)
{
    auto targ = iList.AddressTarget();

    if (targ)
    {
        if (targ->IsSame(*secondary()))
        {
            replaceSecondary(*iList.ValueTarget());
            return true;
        }
    }
    return false;
}

void InstMov::BindParams(InstructionList &iList)
{
    iList.PutTarget(targetAddress(), secondary());
}

void InstMov::emitInst(RegisterOutputTarget &target)
{
    auto srcReg = secondary(), destReg = targetAddress();
    int srcSize = srcReg->Size(), destSize = destReg->Size();

    int operSize = (srcSize < destSize) ? srcSize : destSize;

    if (!operSize)
        MY_THROW() << "register size == 0?";

    srcReg->ConvValue(target, operSize);
    destReg->Address(target);
    target.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::LoadSetter(operSize));    //getMem(reg)
    target.PutDebugPrettyname("mov", operSize, "- ", srcReg->DebugName(), ", ", destReg->DebugName());
}

InstMovN::InstMovN(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg, int size)
    : BinaryInst(destReg, srcReg)
{
    m_instSize = size;
}

InstMovN::~InstMovN()
{ }

void InstMovN::emitInst(RegisterOutputTarget &target)
{
    auto srcReg = secondary(), destReg = targetAddress();

    srcReg->ConvValue(target, m_instSize);
    destReg->Address(target);
    target.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::LoadSetter(m_instSize));    //getMem(reg)
    target.PutDebugPrettyname("mov", m_instSize, "- ", srcReg->DebugName(), ", ", destReg->DebugName());
}

InstMovzx::InstMovzx(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg)
    : BinaryInst(destReg, srcReg)
{}

InstMovzx::~InstMovzx()
{}

bool InstMovzx::DoIgnore(Instruction **prev)
{
    return equivalentTo(*prev);
}

void InstMovzx::emitInst(RegisterOutputTarget &target)
{
    auto srcReg = secondary(), destReg = targetAddress();

    srcReg->Value(target);
    destReg->Address(target);
    target.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::LoadSetterEx(destReg->Size() < srcReg->Size() ? destReg->Size() : srcReg->Size(), false));
    ///(the rest be filled with 0)
    target.PutDebugPrettyname("movzx- ", srcReg->DebugName(), ", ", destReg->DebugName());
}

InstMovsx::InstMovsx(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg)
    : BinaryInst(destReg, srcReg)
{}

InstMovsx::~InstMovsx()
{}

bool InstMovsx::DoIgnore(Instruction **prev)
{
    return equivalentTo(*prev);
}

void InstMovsx::emitInst(RegisterOutputTarget &target)
{
    auto srcReg = secondary(), destReg = targetAddress();

    srcReg->Value(target);
    destReg->Address(target);
    target.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::LoadSetterEx(destReg->Size() < srcReg->Size() ? destReg->Size() : srcReg->Size(), true));    //getMem(reg)
    target.PutDebugPrettyname("movsx- ", srcReg->DebugName(), ", ", destReg->DebugName());
}

InstAdd::InstAdd(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg)
    : BinaryInst(destReg, srcReg)
{}

InstAdd::~InstAdd()
{}

bool InstAdd::TryJoin(InstructionList &iList)
{
    if (!iList.AddressTarget())
        return true;

    auto r = targetAddress();

    //mov eax, ...
    //...
    //pop eax
    //add ecx, eax
    if (r->IsSame(*iList.AddressTarget()))
    {
        iList.ResetValue();
        return false;
    }
    return true;
}

void InstAdd::emitInst(RegisterOutputTarget &target)
{
    auto regValue = secondary(), destReg = targetAddress();

    regValue->Value(target);
    destReg->Address(target);
    target.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_Addition);
    target.PutDebugPrettyname("add- ", regValue->DebugName(), ' ', destReg->DebugName());
}

InstSub::InstSub(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg)
    : BinaryInst(destReg, srcReg)
{}

InstSub::~InstSub()
{}

bool InstSub::TryJoin(InstructionList &iList)
{
    if (!iList.AddressTarget())
        return true;

    auto r = targetAddress();

    //mov eax, ...
    //...
    //pop eax
    //add ecx, eax
    if (r->IsSame(*iList.AddressTarget()))
    {
        iList.ResetValue();
        return false;
    }
    return true;
}

void InstSub::emitInst(RegisterOutputTarget &target)
{
    auto regValue = secondary(), destReg = targetAddress();

    regValue->Value(target);
    destReg->Address(target);
    target.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_Subtract);
    target.PutDebugPrettyname("sub- ", regValue->DebugName(), ' ', destReg->DebugName());
}

InstMul::InstMul(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg)
    : BinaryInst(destReg, srcReg)
{}

InstMul::~InstMul()
{}

void InstMul::emitInst(RegisterOutputTarget &target)
{
    auto regValue = secondary(), destReg = targetAddress();

    regValue->Value(target);
    destReg->Address(target);
    target.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_Multiply);
    target.PutDebugPrettyname("mul- ", regValue->DebugName(), ' ', destReg->DebugName());
}

InstDiv::InstDiv(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg, bool isUnsigned)
    : BinaryInst(destReg, srcReg)
{
    m_unsigned = isUnsigned;
}

InstDiv::~InstDiv()
{}

void InstDiv::emitInst(RegisterOutputTarget &target)
{
    auto regValue = secondary(), destReg = targetAddress();

    regValue->Value(target);
    destReg->Address(target);
    target.DispatchOpcode<NoxBuiltinCall>(m_unsigned ? NoxBuiltinCall::BuiltinsFn::BUILTINS_UnsignedDiv : NoxBuiltinCall::BuiltinsFn::BUILTINS_SignedDiv); //FIXME.
    target.PutDebugPrettyname("div- ", regValue->DebugName(), ' ', destReg->DebugName());
}

InstMod::InstMod(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg, bool isUnsigned)
    : BinaryInst(destReg, srcReg)
{
    m_unsigned = isUnsigned;
}

InstMod::~InstMod()
{}

void InstMod::emitInst(RegisterOutputTarget &target)
{
    auto regValue = secondary(), destReg = targetAddress();

    regValue->Value(target);
    destReg->Address(target);
    target.DispatchOpcode<NoxBuiltinCall>(m_unsigned ? NoxBuiltinCall::BuiltinsFn::BUILTINS_UnsignedMod : NoxBuiltinCall::BuiltinsFn::BUILTINS_SignedMod); //FIXME.
    target.PutDebugPrettyname("mod- ", regValue->DebugName(), ' ', destReg->DebugName());
}

InstCallFromReg::InstCallFromReg(InstructionGuard::reg_fn_arg_ty r)
    : Instruction(r)
{}

InstCallFromReg::~InstCallFromReg()
{}

void InstCallFromReg::emitInst(RegisterOutputTarget &target)
{
    auto srcValue = targetAddress();

    srcValue->Value(target);
    target.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_UserCall);
}

InstCall::InstCall(const std::string &fn)
    : Instruction()
{
    m_fname = fn;
}

InstCall::~InstCall()
{}

void InstCall::emitInst(RegisterOutputTarget &target)
{
    target.DispatchOpcode<NoxUserCall>(m_fname);
}

InstLea::InstLea(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg)
    : BinaryInst(destReg, srcReg)
{}

InstLea::~InstLea()
{}

bool InstLea::TryJoin(InstructionList &iList)
{
    auto a = iList.AddressTarget();

    if (!a)
        return true;

    auto r = targetAddress();
    auto s = secondary();

    //mov eax, ...
    //...
    //pop eax
    //add ecx, eax
    if (r->HasReference(*a))
        return false;
    return !s->HasReference(*a);
}

void InstLea::emitInst(RegisterOutputTarget &target)
{
    auto srcRegAddr = secondary(), destReg = targetAddress();

    srcRegAddr->Address(target);
    destReg->Address(target);
    target.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_SetMemory);
    target.PutDebugPrettyname("lea- ", srcRegAddr->DebugName(), ' ', destReg->DebugName());
}

InstBinaryOp::InstBinaryOp(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg, int id)
    : BinaryInst(destReg, srcReg)
{
    m_builtinId = id;
}

InstBinaryOp::~InstBinaryOp()
{}

void InstBinaryOp::emitInst(RegisterOutputTarget &target)
{
    auto selFn = static_cast<NoxBuiltinCall::BuiltinsFn::Class>(m_builtinId);
    std::string opmark;

    switch (selFn)
    {
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_ShiftLeft: opmark = "shl"; break;
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_ShiftRight: opmark = "shr"; break;
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_XOR: opmark = "xor"; break;
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_AND: opmark = "and"; break;
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_OR: opmark = "or"; break;
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_FloatAdd: opmark = "fadd"; break;
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_FloatSub: opmark = "fsub"; break;
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_FloatMul: opmark = "fmul"; break;
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_FloatDiv: opmark = "fdiv"; break;
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_IntToFloat:
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_FloatToInt:
        break;

    default:
        MY_THROW() << "incorrect op";
    }
    auto regValue = secondary(), destReg = targetAddress();

    regValue->Value(target);
    destReg->Address(target);
    target.DispatchOpcode<NoxBuiltinCall>(selFn);
    target.PutDebugPrettyname("some- ", regValue->DebugName(), ' ', destReg->DebugName());
}

InstNot::InstNot(InstructionGuard::reg_fn_arg_ty r)
    : Instruction(r)
{}

InstNot::~InstNot()
{}

void InstNot::emitInst(RegisterOutputTarget &target)
{
    auto dest = targetAddress();

    dest->Value(target);
    //dispatchOpcode(std::make_unique<NoxNativeOp>(NoxNativeOp::NativeBitNot));
    target.DispatchOpcode<NoxNativeOp>(NoxNativeOp::NativeInvert);
    dest->Address(target);
    target.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_SetMemory);
}

InstComparison::InstComparison(InstructionGuard::reg_fn_arg_ty srcReg, InstructionGuard::reg_fn_arg_ty destReg, int id)
    : BinaryInst(destReg, srcReg)
{
    m_cmpty = id;
}

InstComparison::~InstComparison()
{}

bool InstComparison::doMerge(InstructionList &iList)
{
    auto targ = iList.AddressTarget();

    if (targ)
    { //mov eax, 0; //push eax => push 0
        if (targ->IsSame(*targetAddress()))
        {
            replaceTargetAddress(*iList.ValueTarget());
            return true;
        }
        if (targ->IsSame(*secondary()))
        {
            replaceSecondary(*iList.ValueTarget());
            return true;
        }
    }
    return false;
}

void InstComparison::emitInst(RegisterOutputTarget &target)
{
    auto selfn = static_cast<NoxBuiltinCall::BuiltinsFn::Class>(m_cmpty);

    switch (selfn)
    {
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_CompareEqual:
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_CompareNotEqual:
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_CompareLess:
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_CompareLessEqual:
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_CompareFloatLess:
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_CompareFloatLessEqual:
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_TEST:
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_TEST_NOT:
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_CompareB:
    case NoxBuiltinCall::BuiltinsFn::BUILTINS_CompareEqualB:
        break;

    default:
        MY_THROW() << "incorrect comparison";
    }
    auto cmp1 = secondary(), cmp2 = targetAddress();

    cmp2->Value(target);
    cmp1->Value(target);
    target.DispatchOpcode<NoxBuiltinCall>(selfn);
}

InstCmpResult::InstCmpResult(InstructionGuard::reg_fn_arg_ty r)
    : Instruction(r)
{}

InstCmpResult::~InstCmpResult()
{}

void InstCmpResult::emitInst(RegisterOutputTarget &target)
{
    targetAddress()->Address(target);
    target.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_LoadCompareResult);
}

InstLabel::InstLabel(const std::string &ln)
    : Instruction()
{
    m_labelName = ln;
}

InstLabel::~InstLabel()
{}

void InstLabel::emitInst(RegisterOutputTarget &target)
{
    NoxFunction *currentFunction = target.GetCtx();

    if (!currentFunction)
        MY_THROW() << "internal error";

    currentFunction->PutLabel(m_labelName);
}

InstJump::InstJump(const std::string &ln)
    : InstLabel(ln)
{}

InstJump::~InstJump()
{}

//bool InstJump::mergeInst(RegisterOutputTarget &target, Instruction *inst)
//{
//    return false;
//}

void InstJump::emitInst(RegisterOutputTarget &target)
{
    target.DispatchOpcode<NoxOpJump>(labelName(), NoxOpJump::Pass);
}

InstJumpConditional::InstJumpConditional(InstructionGuard::reg_fn_arg_ty rCmp, const std::string &ln, bool isEqual)
    : InstLabel(ln)
{
    m_isEqual = isEqual;
    replaceTargetAddress(rCmp);
}

InstJumpConditional::~InstJumpConditional()
{}

void InstJumpConditional::emitInst(RegisterOutputTarget &target)
{
    //targetAddress()->Value(target);
    target.DispatchOpcode<NoxOpJump>(labelName(), m_isEqual ? NoxOpJump::JumpEqual : NoxOpJump::JumpNotEqual);
}

InstReturn::InstReturn()
    : Instruction()
{}

InstReturn::~InstReturn()
{ }

void InstReturn::emitInst(RegisterOutputTarget &target)
{
    target.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_FunctionReturn);
    target.DispatchOpcode<NoxNativeOp>(NoxNativeOp::NativeReturn);
}

InstMemCopy::InstMemCopy(InstructionGuard::reg_fn_arg_ty src, InstructionGuard::reg_fn_arg_ty dest, InstructionGuard::reg_fn_arg_ty repeat)
    : Instruction(dest)
{
    m_repeat = std::unique_ptr<AbstractRegister>(repeat.Clone());
    m_src = std::unique_ptr<AbstractRegister>(src.Clone());
}

InstMemCopy::~InstMemCopy()
{}

void InstMemCopy::emitInst(RegisterOutputTarget &target)
{
    m_repeat->Value(target);
    targetAddress()->Address(target);
    m_src->Address(target);
    target.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_Memcopy);
}

InstZeroFiller::InstZeroFiller(InstructionGuard::reg_fn_arg_ty src, InstructionGuard::reg_fn_arg_ty dest)
    : BinaryInst(dest, src)
{ }

InstZeroFiller::~InstZeroFiller()
{}

void InstZeroFiller::emitInst(RegisterOutputTarget &target)
{
    auto end = targetAddress(), start = secondary();

    end->Value(target);
    start->Value(target);
    target.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_ZeroFiller);
}

InstStoreLocal::InstStoreLocal(InstructionGuard::reg_fn_arg_ty r, int off)
    : Instruction(r)
{
    m_off = off;
}

InstStoreLocal::~InstStoreLocal()
{}

bool InstStoreLocal::doMerge(InstructionList &iList)
{
    auto targ = iList.AddressTarget();

    if (targ)
    {
        if (targ->IsSame(*targetAddress()))
        {
            replaceTargetAddress(*iList.ValueTarget());
            return true;
        }
    }
    return false;
}

void InstStoreLocal::emitInst(RegisterOutputTarget &target)
{
    auto value = targetAddress();

    value->Value(target);
    target.DispatchOpcode<NoxNativePush>(m_off);
    target.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_StoreLocal);
    target.PutDebugPrettyname("store local- ", m_off, ' ', value->DebugName());
}

InstTest::InstTest(bool testnot)
    : Instruction()
{
    m_testnot = testnot;
}

InstTest::~InstTest()
{}

void InstTest::emitInst(RegisterOutputTarget &target)
{
    target.DispatchOpcode<NoxBuiltinCall>(m_testnot ? NoxBuiltinCall::BuiltinsFn::BUILTINS_TEST_NOT : NoxBuiltinCall::BuiltinsFn::BUILTINS_TEST);
}
