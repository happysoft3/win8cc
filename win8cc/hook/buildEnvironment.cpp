
#include "buildEnvironment.h"
#include "portable/buildSourceFile.h"
#include "systemfileparser/iniparser.h"
#include "common/utils/pathManager.h"
#include "common/utils/myDebug.h"
#include "common/utils/stringhelper.h"

constexpr char makefile_mainkey[] = "COMPILE";
static constexpr char build_opt_use_debug[] = "ENABLE_DEBUG";
static constexpr char ccwarning_is_error[] = "WarningIsError";

using namespace _StringHelper;

class BuildEnvironment::EnvParser : public IniParser
{
private:
    BuildEnvironment *m_pEnv;

public:
    explicit EnvParser(BuildEnvironment *env)
        : m_pEnv(env)
    { }

    ~EnvParser() override
    { }

private:
    void showAll() override
    { }

    void putOption(const std::string &key, const std::string &value)
    {
        if (key.empty())
            return;

        m_pEnv->setOption(key, value);
    }

    bool addKey(const std::string &sectionkey, const std::string &key, const std::string &value)
    {
        bool result = IniParser::addKey(sectionkey, key, value);

        if (sectionkey != makefile_mainkey)
            return result;

        if (key == "SOURCE")
            m_pEnv->putSourceFile(value);
        else if (key == "INITLOADER_CALL")
            m_pEnv->putInitEudCallFunctionName(value);
        else
            putOption(key, value);

        return result;
    }
};

BuildEnvironment::BuildEnvironment()
	: IniFileMan()
{
	m_exceptCapture = [](const std::exception &e) { std::cout << e.what() << std::endl; };
}

BuildEnvironment::~BuildEnvironment()
{ }

std::unique_ptr<IniParser> BuildEnvironment::loadParser()
{
    return std::make_unique<EnvParser>(this);
}

std::unique_ptr<PathManager> BuildEnvironment::loadBasepath(const std::string &altPath)
{	//@brief. 소스파일의 기준 경로를 가져옵니다(필수)
	std::string basePath;

	GetItemValue(makefile_mainkey, "BASEPATH", basePath);
	if (basePath.empty())
		basePath = altPath;		//basepath 필드가 비어있으면, 상대경로를 사용합니다

	auto pathman = std::make_unique<PathManager>();

	pathman->SetUrl(basePath);
	return pathman;
}

std::unique_ptr<PathManager> BuildEnvironment::loadOutTarget()
{	//@brief. 바이너리 출력 경로를 가져옵니다(필수)
	std::string outTarget;

	GetItemValue(makefile_mainkey, "OUT_TARGET", outTarget);
	if (outTarget.empty())
		MY_THROW() << "has no attrib 'out-target'";

	auto outpath = std::make_unique<PathManager>();

	outpath->SetUrl(outTarget);
	return outpath;
}

void BuildEnvironment::loadSourceFiles()
{	//@brief. 컴파일을 수행할, 소스 파일목록을 가져옵니다(필수)
	int counter = 1;
	std::string source = stringFormat("SOURCE%d", counter);
	std::string value;

	while (GetItemValue(makefile_mainkey, source, value))
	{
        std::transform(value.cbegin(), value.cend(), value.begin(), [](const auto &c) { return c == '/' ? '\\' : c; });
		m_sourceFiles.push_back(std::make_unique<BuildSourceFile>(value, counter));
		source = stringFormat("SOURCE%d", ++counter);
	}
	if (m_sourceFiles.empty())
		MY_THROW() << "has no attribution 'source'";
}

void BuildEnvironment::putSourceFile(const std::string &sourceUrl)
{
    if (sourceUrl.empty())
        MY_THROW() << "source url is empty";

    std::string value = sourceUrl;
    int counter = static_cast<int>(m_sourceFiles.size());

    std::transform(value.cbegin(), value.cend(), value.begin(), [](const auto &c) { return c == '/' ? '\\' : c; });
    m_sourceFiles.push_back(std::make_unique<BuildSourceFile>(value, ++counter));
}

void BuildEnvironment::putInitEudCallFunctionName(const std::string &fname)
{
    if (fname.empty())
        return;

    if (m_eudloaderCallFunctions.find(fname) == m_eudloaderCallFunctions.cend())
        m_eudloaderCallFunctions.emplace(fname);
}

void BuildEnvironment::catchException(const std::exception &except)
{
	throw except;	//한번 더 던진다
}

void BuildEnvironment::PutExceptCapture(std::function<void(const std::exception&)> &&capture)
{
	m_exceptCapture = std::move(capture);
}

bool BuildEnvironment::ReadIni(const std::string &inifile)
{
	try
	{
		IniFileMan::ReadIni(inifile);
		m_pathman = loadBasepath(inifile);
		m_outTargetPath = loadOutTarget();
	}
	catch (const std::exception &except)
	{
		m_exceptCapture(except);
		catchException(except);
		return false;
	}
	return true;
}

void BuildEnvironment::DoSourceFiles(std::function<void(const BuildSourceFile &)> &&proc)
{
	for (auto &s : m_sourceFiles)
		proc(*s);
}

std::string BuildEnvironment::OutTargetPath() const
{
	if (!m_outTargetPath)
		return {};

	return m_outTargetPath->FullPath();
}

const char *const BuildEnvironment::GetOption(const std::string &s) const
{
	auto optionIter = m_optionMap.find(s);

	if (optionIter == m_optionMap.cend())
		return "";

	return optionIter->second.c_str();
}

bool BuildEnvironment::GetOptionAsInt(const std::string &key, int &dest) const
{
    std::string str = GetOption(key);

    if (str.empty())
        return false;

    if (str.length() > 10)
        return false;

    for (const auto &c : str)
    {
        if (c < '0' && c > '9')
            return false;
    }
    std::stringstream ss(str);

    ss >> dest;
    return true;
}

void BuildEnvironment::setOption(const std::string &key, const std::string &val)
{
    if (key.empty())
        return;

    m_optionMap[key] = val;
}

bool BuildEnvironment::UseDebug() const
{
	auto optIter = m_optionMap.find(build_opt_use_debug);

	if (optIter == m_optionMap.cend())
		return false;

	return optIter->second == "TRUE";
}

void BuildEnvironment::DoPreorderEudLoaderCallTarget(std::function<void(const std::string &)> &&f)
{
    for (const auto &s : m_eudloaderCallFunctions)
        f(s);
}

