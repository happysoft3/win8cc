
#ifndef EUD_GLOBAL_POINTER_PACK_H__
#define EUD_GLOBAL_POINTER_PACK_H__

#include "hook/noxFunction.h"

class GPointerOffsetTable;

class EudGlobalPointerPack : public NoxFunction
{
private:
	std::shared_ptr<GPointerOffsetTable> m_gpointerTable;

public:
	explicit EudGlobalPointerPack(std::shared_ptr<GPointerOffsetTable> &table);
	~EudGlobalPointerPack() override;

private:
	void emitCode(ScrOutStream &scrOs) override;
};

#endif

