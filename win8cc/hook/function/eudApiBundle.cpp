
#include "eudApiBundle.h"
#include "common/utils/stringhelper.h"
#include "common/include/binaryBufferEx.h"
#include "common/utils/myDebug.h"
#include <fstream>
#include <vector>
#include <iterator>

using namespace _StringHelper;

EudApiBundle::EudApiBundle(const std::string &fName)
    : NoxFunction(fName)
{
    commonInit();
}

EudApiBundle::~EudApiBundle()
{ }

void EudApiBundle::commonInit()
{
    m_readpos = 0;
}

int EudApiBundle::emitLength(ScrOutStream &scrOs)
{
    int length = 0;

    if (!m_buffer->Getc(length, m_readpos))
        MY_THROW() << "length of stream is too less";
    m_readpos += sizeof(length);
    scrOs.Write(length + sizeof(int));  //길이 필드를 포함한 전체 필드 크기입니다
    scrOs.Write(length);    //내용 길이
    return length;
}

void EudApiBundle::emitCodeImpl(ScrOutStream &scrOs, BinaryBufferEx<char> &buffer)
{
    int n = emitLength(scrOs);  //길이가 4으로 나누어 질 수 있어야 합니다

	if (n % sizeof(int))
		MY_THROW() << "padding's length must be divisible by 4";

    int count = n / sizeof(int);

    while (--count >= 0)
    {
        int code = 0;

		if (!buffer.Getc(code, m_readpos))
			MY_THROW() << "stream ended unexpectedly";
        m_readpos += sizeof(code);
        scrOs.Write(code);
    }
}

void EudApiBundle::emitCode(ScrOutStream &scrOs)
{
	if (!m_buffer)
		MY_THROW() << "buffer is null";

	if (isDebugModeOn(scrOs))
	{
		scrOs << "this field will not show on debug mode";
		scrOs << ENDL;
		return;
	}
    emitCodeImpl(scrOs, *m_buffer);
}

bool EudApiBundle::DoReadBinary(const std::string &url)
{
    std::fstream file(url, std::ios::binary | std::ios::in);

    file << std::noskipws;
    if (!file.is_open())
        return false;

    std::vector<char> buf;

    std::transform(std::istream_iterator<char>(file), std::istream_iterator<char>(), std::insert_iterator<std::vector<char>>(buf, buf.begin()), [](const auto &c) { return static_cast<uint8_t>(c); });

    if (buf.empty())
        return false;

    m_buffer = std::make_unique<BinaryBufferEx<char>>();
    m_buffer->StreamPut(buf);
    m_readpos = 0;
    return true;
}

