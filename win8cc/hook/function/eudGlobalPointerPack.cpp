
#include "eudGlobalPointerPack.h"
#include "hook/global/gpointerOffsetTable.h"
#include "hook/link/cclink.h"
#include "common/utils/randomString.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

EudGlobalPointerPack::EudGlobalPointerPack(std::shared_ptr<GPointerOffsetTable> &table)
	: NoxFunction(stringFormat("EUDGlobal%sPointer", RandomString::Generate(8)))
{
	m_gpointerTable = table;
}

EudGlobalPointerPack::~EudGlobalPointerPack()
{ }

void EudGlobalPointerPack::emitCode(ScrOutStream &scrOs)
{
    int count = m_gpointerTable->Count();

	scrOs.Write((count + 1) * sizeof(int));
    scrOs.Write(count);
	m_gpointerTable->Preorder([this, &scrOs](CCLink &off)
	{
		scrOs << off.Link();
	});
}

