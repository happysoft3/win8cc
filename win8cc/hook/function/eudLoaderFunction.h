
#ifndef EUD_LOADER_FUNCTION_H__
#define EUD_LOADER_FUNCTION_H__

#include "eudApiBundle.h"

class EudLoaderFunction : public EudApiBundle
{
public:
    EudLoaderFunction();
    ~EudLoaderFunction() override;

private:
    void emitCodeImpl(ScrOutStream &scrOs, BinaryBufferEx<char> &buffer) override;
    void onReleaseStream(ScrOutStream &scrOs) override;
};

#endif

