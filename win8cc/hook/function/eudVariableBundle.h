
#ifndef EUD_VARIABLE_BUNDLE_H__
#define EUD_VARIABLE_BUNDLE_H__

#include "hook/noxFunction.h"

class CDataSection;
class CDataAllocator;

class EudVariableBundle : public NoxFunction
{
private:
	CDataSection *m_globalField;

public:
	EudVariableBundle(int stackSize);
	~EudVariableBundle() override;

private:
	void allocateStack(int stacksize);
	void emitCode(ScrOutStream &scrOs) override;

public:
	void PutGlobalData(CDataSection &globalField);
};

#endif

