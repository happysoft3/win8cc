
#ifndef EUD_API_BUNDLE_H__
#define EUD_API_BUNDLE_H__

#include "../noxFunction.h"

template <class Ty>
class BinaryBufferEx;

class EudApiBundle : public NoxFunction
{
private:
    std::unique_ptr<BinaryBufferEx<char>> m_buffer;
    size_t m_readpos;

public:
    explicit EudApiBundle(const std::string &fName);
    ~EudApiBundle() override;

private:
    void commonInit();
    int emitLength(ScrOutStream &scrOs);
    virtual void emitCodeImpl(ScrOutStream &scrOs, BinaryBufferEx<char> &buffer);

protected:
    void emitCode(ScrOutStream &scrOs) override;

public:
    bool DoReadBinary(const std::string &url);
};

#endif

