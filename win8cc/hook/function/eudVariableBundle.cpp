
#include "eudVariableBundle.h"
#include "hook/cdata/cDataSection.h"
#include "hook/cdata/cDataAllocator.h"
#include "common/utils/randomString.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

EudVariableBundle::EudVariableBundle(int stackSize)
	: NoxFunction("EUDStackField" + RandomString::Generate(8))
{
	m_globalField = nullptr;
	allocateStack(stackSize);
}

EudVariableBundle::~EudVariableBundle()
{ }

constexpr int stack_min_size = 131072;
constexpr int stack_max_size = stack_min_size * 10;
void EudVariableBundle::allocateStack(int stacksize)
{
	if (stacksize < stack_min_size || stacksize > stack_max_size)
		throw std::logic_error(stringFormat("invalid stack size '%d'", stacksize));

	PushNativeVar(stacksize/sizeof(int));
}

void EudVariableBundle::emitCode(ScrOutStream &scrOs)
{
	//사실은, 바이트 코드 대신에 광역변수 필드를 삽입합니다
	scrOs.Write(m_globalField->DataLength());
	m_globalField->Preorder([&scrOs](CDataAllocator &a) { a.EmitData(scrOs); });
	m_globalField = nullptr;
}

void EudVariableBundle::PutGlobalData(CDataSection &globalField)
{
	//if (!m_globalField)
	//	throw std::logic_error("global field is expired");

	m_globalField = &globalField;
}
