
#include "eudLoaderFunction.h"
#include "common/utils/randomstring.h"
#include "common/include/binaryBufferEx.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

EudLoaderFunction::EudLoaderFunction()
    : EudApiBundle(stringFormat("Initial%sEUDLoader", RandomString::Generate(8)))
{ }

EudLoaderFunction::~EudLoaderFunction()
{ }

void EudLoaderFunction::emitCodeImpl(ScrOutStream &scrOs, BinaryBufferEx<char> &buffer)
{
    if (buffer.Empty())
        throw std::exception("no stream");

    size_t pos = 0, length = buffer.Size();

    while (pos < length)
    {
        int iRead = 0;
        if (buffer.Getc(iRead, pos))
        {
            scrOs.Write(iRead);
            pos += sizeof(int);
            continue;
        }
        char cRead = 0;
        if (buffer.Getc(cRead, pos))
        {
            scrOs.Write(cRead);
            ++pos;
            continue;
        }
        throw std::exception("over stream");
    }
}

void EudLoaderFunction::onReleaseStream(ScrOutStream &scrOs)
{
    emitFunctionIdentifier(scrOs);
    emitCode(scrOs);
}

