
#ifndef JOIN_COMPILATION_H__
#define JOIN_COMPILATION_H__

#include "common/taskthread/taskThread.h"
#include <vector>

class TaskResultHash;
class BuildEnvironment;

class JoinCompilation : public TaskThread
{
private:
	std::vector<std::unique_ptr<TaskThread>> m_procVector;

public:
	JoinCompilation();
	~JoinCompilation() override;

private:
	void doMakeThread();
	bool beforeExec(std::shared_ptr<TaskQueue> queue) override;
	void afterExec(TaskResultHash &resultMap) noexcept override;
	bool setThreadCount(size_t count);

public:
	void SetDetailOption(BuildEnvironment &buildOpt);
};

#endif

