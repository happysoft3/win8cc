
#include "noxCodegen.h"
#include "register/derefRegister.h"
#include "register/globalRegister.h"
#include "register/functionRegister.h"
#include "register/intRegister.h"
#include "../parse.h"
#include "../typePool.h"
#include "../debug.h"
#include "functionTable.h"
#include "noxFunction.h"
#include "cdata/cDataSection.h"
#include "cdata/cDataAllocator.h"
#include "global/gpointerOffsetTable.h"
#include "../8cc.h"
#include "ast/astVector.h"
#include "../vector.h"
#include "../error.h"
#include "ast/astNode.h"
#include "ast/astType.h"
#include "common/mmanager/memorySweeper.h"
#include "common/utils/myDebug.h"
#include "common/utils/stringhelper.h"

#include <functional>

using namespace _StringHelper;

constexpr int ireg_max_count = 4;

//Code Options for compilation//

#define NOXCODEGEN_COPYSTRUCT_USE_MEMCOPY_			//구조체 복사 시, memcopy 을 사용합니다
//#define NOXCODEGEN_ENABLE_FLOAT_TYPE_				//float 형식을 처리합니다
//#define NOXCODEGEN_INSERT_PADDING					//스택 push-pop 작업 시, 필요할 경우, 패딩을 삽입합니다

///

static int getAlign(int n, int m)
{
    int rem = n % m;
    return (rem == 0) ? n : n - rem + m;
}

NoxCodegen::NoxCodegen(int uniqId)
{
    m_sourceFunction = std::make_unique<FunctionTable>();
	m_dataSection = std::make_unique<CDataSection>();
    m_stackpos = 0;
	m_memSweep = std::make_unique<MemorySweeper>();
}

NoxCodegen::~NoxCodegen()
{ }

void NoxCodegen::genOpPush(AbstractRegister &regValue)
{
	NoxOpGenerator::genOpPush(regValue);
	m_stackpos += NoxOpGenerator::default_bytes;
}

void NoxCodegen::genOpPop(AbstractRegister &regAddress)
{
	NoxOpGenerator::genOpPop(regAddress);
	m_stackpos -= NoxOpGenerator::default_bytes;
	LOGIC_ASSERT(m_stackpos >= 0);
}

void NoxCodegen::emitToBool(AstType *ty)
{
    genOpCompareNotEqual(IntRegister(0), EAX());
    genOpGetCompareResult(EAX());
}

void NoxCodegen::emitIntCast(AstType *ty)  //ty 형식으로부터, 형식 변환
{
    switch (ty->Kind())
    {
    case KIND_BOOL:
    case KIND_CHAR:     //eg. int->char
        ty->Unsigned() ? genOpMovzx(EAX(1), EAX()) : genOpMovsx(EAX(1), EAX());
        return;
    case KIND_SHORT:
        ty->Unsigned() ? genOpMovzx(EAX(2), EAX()) : genOpMovsx(EAX(2), EAX());
        return;
    case KIND_INT:      //no effect?
    case KIND_LONG:
    case KIND_LLONG:
        return;
    }
}

void NoxCodegen::emitToInt(AstType *fTy)
{
    if (fTy->IsKind( KIND_FLOAT) )
        genOpConvFloatToInt(EAX(), EAX());
}

void NoxCodegen::emitLoadConvert(AstType *to, AstType *from)
{
    if (from->IsIntType() && to->IsKind( KIND_FLOAT))		//INT-> float
        genOpConvIntToFloat(EAX(), EAX());  //emit("cvtsi2ss #eax, #xmm0");
    else if (from->IsIntType() && to->IsKind( KIND_DOUBLE))	//int-> double
    {
		MY_THROW() << "not support";	//emit("cvtsi2sd #eax, #xmm0");
    }
    else if (from->IsKind( KIND_FLOAT ) && to->IsKind( KIND_DOUBLE)	) //float -> double
    {
		MY_THROW() << "not support";	//emit("cvtps2pd #xmm0, #xmm0");
    }
    else if ((from->IsKind( KIND_DOUBLE) || from->IsKind( KIND_LDOUBLE)) && to->IsKind( KIND_FLOAT) )	//double->float
    {
		MY_THROW() << "not support";	//emit("cvtpd2ps #xmm0, #xmm0");
    }
    else if (to->IsKind( KIND_BOOL))	//any->bool
    {
        emitToBool(from);
    }
    else if (from->IsIntType() && to->IsIntType())	//int->int
    {
        //if (from->kind != to->kind)
            emitIntCast(from);
    }
    else if (to->IsIntType())	//any->int
        emitToInt(from);
}

void NoxCodegen::maybeConvertBool(AstType *ty)
{
    if (ty->IsKind(KIND_BOOL)) {
        genOpTestNot();
        genOpGetCompareResult(EAX());
    }
}

void NoxCodegen::emitConvert(AstUnaryOpNode *node)
{
    emitExpr(node->Operand() );
    emitLoadConvert(node->GetType(), node->Operand()->GetType() );
}

void NoxCodegen::emitDereference(AstUnaryOpNode *node)
{
    emitExpr(node->Operand());
    emitLLoad( node->Operand()->GetType()->GetPTR(), EAX(), 0);
    emitLoadConvert(node->GetType(), node->Operand()->GetType()->GetPTR());
}

void NoxCodegen::emitCast(AstUnaryOpNode *node)
{
    emitExpr(node->Operand());
    emitLoadConvert(node->GetType(), node->Operand()->GetType());
}

void NoxCodegen::doEmitAssignDereference(AstType *ty, int off)
{
    genOpMov(DerefRegister(ESP()), ECX());
    genOpMov(ECX(ty->GetSize()), DerefRegister(EAX(), off));
    genOpPop(EAX());
}

void NoxCodegen::emitAssignDeref(AstUnaryOpNode *var)
{
    genOpPush(EAX());
    emitExpr(var->Operand());
    doEmitAssignDereference( var->Operand()->GetType()->GetPTR(), 0);
}

void NoxCodegen::emitAssignStructReference(AstNode *struc, AstType *field, int off)
{
    switch (struc->Kind()) {
    case AST_LVAR:
    {
        AstVariableNode *lv = static_cast<AstVariableNode *>(struc);

        ensureLvarInit(lv);
        int loff = 0;
        lv->FetchLvarData(nullptr, &loff);
        emitLsave(field, loff + field->Offset() + off);
        break;
    }
    case AST_GVAR:
        emitGSave(static_cast<AstGlobalVariableNode *>( struc)->GlobalLabel(), field, field->Offset() + off);
        break;
    case AST_STRUCT_REF:
        emitAssignStructReference(static_cast<AstStructNode *>(struc)->Struc(), field, off + struc->GetType()->Offset());
        break;
    case AST_DEREF:
        genOpPush(EAX());
        emitExpr(static_cast<AstUnaryOpNode *>( struc)->Operand());
        doEmitAssignDereference(field, field->Offset() + off);
        break;
    default:
        THROW_ERROR_NOPOS("internal error: %s", struc->Explain());
    }
}

void NoxCodegen::emitStore(AstNode *var)
{
    switch (var->Kind()) {
    case AST_DEREF: emitAssignDeref(static_cast<AstUnaryOpNode *>( var) ); break;
    case AST_STRUCT_REF: emitAssignStructReference(static_cast<AstStructNode *>( var)->Struc(), var->GetType(), 0); break;
    case AST_LVAR:
    {
        auto lv = static_cast<AstVariableNode *>(var);

        ensureLvarInit(lv);
        int loff = 0;
        lv->FetchLvarData(nullptr, &loff);
        emitLsave(lv->GetType(), loff);
        break;
    }
    case AST_GVAR: emitGSave(static_cast<AstGlobalVariableNode *>( var)->GlobalLabel(), var->GetType(), 0); break;
    default: THROW_ERROR_NOPOS("internal error");
    }
}

void NoxCodegen::emitCompoundStatement(AstCompoundStmtNode *node)
{
    for (int i = 0; i < node->StatementLength() ; i++)
        emitExpr(node->GetStatement(i));
}

void NoxCodegen::emitLoadStructRef(AstNode *struc, AstType *field, int off)
{
    switch (struc->Kind()) {
    case AST_LVAR:
    {
        AstVariableNode *varnode = static_cast<AstVariableNode *>(struc);
        int loff = 0;

        ensureLvarInit(varnode);
        varnode->FetchLvarData(nullptr, &loff);
        emitLLoad(field, EBP(), loff + field->Offset() + off);
        break;
    }
    case AST_GVAR:
        emitGLoad(field, static_cast<AstGlobalVariableNode *>( struc)->GlobalLabel(), field->Offset() + off);
        break;
    case AST_STRUCT_REF:
        emitLoadStructRef(static_cast<AstStructNode *>( struc)->Struc(), field, struc->GetType()->Offset() + off);
        break;
    case AST_DEREF:
        emitExpr(static_cast<AstUnaryOpNode *>( struc)->Operand());
        emitLLoad(field, EAX(), field->Offset() + off);
        break;
    default:
		THROW_ERROR_NOPOS("internal error: %s", struc->Explain());
    }
}

void NoxCodegen::emitCopyStructMemcopy(AstNode *left, AstNode *right)
{
    AstType *leftTy = left->GetType();

	if (leftTy->GetSize() != right->GetType()->GetSize())
		MY_THROW() << "struct size unmatched";
	if (leftTy->GetSize() == 0)
		MY_THROW() << "empty struct";

	genOpPush(ECX());
	emitAddress(right);
	genOpMov(EAX(), ECX());
	emitAddress(left);
	genOpMemCopy(ECX(), EAX(), IntRegister(leftTy->GetSize() / NoxOpGenerator::default_bytes));
	genOpPop(ECX());
}

void NoxCodegen::emitCopyStruct(AstNode *left, AstNode *right)
{
	genOpPush(ECX());
	genOpPush(EBX());
    emitAddress(right);
	genOpMov(EAX(), ECX());
    emitAddress(left);
    int i = 0, end = left->GetType()->GetSize();
	for (; static_cast<int>( i + sizeof(int)) < end ; i += sizeof(int))
	{
		genOpMov(DerefRegister(ECX(), IntRegister(i)), EBX());
		genOpMov(EBX(), DerefRegister(EAX(), IntRegister(i)));
    }
	for (; i < end ; i += sizeof(char))
	{
		genOpMov(DerefRegister(ECX(), IntRegister(i)), EBX(sizeof(char)));
		genOpMov(EBX(sizeof(char)), DerefRegister(EAX(), IntRegister(i)));
    }
	genOpPop(EBX());
	genOpPop(ECX());
}

void NoxCodegen::emitAssign(AstBinaryOpNode *node)
{
    AstNode *left, *right;

    node->GetChild(&left, &right);
    if (left->GetType()->IsKind(KIND_STRUCT) &&
        left->GetType()->GetSize() > NoxOpGenerator::default_bytes) {

#ifdef NOXCODEGEN_COPYSTRUCT_USE_MEMCOPY_
		emitCopyStructMemcopy(left, right);
#else
        emitCopyStruct(node->left, node->right);
#endif
    }
    else {
        emitExpr(right);
        emitLoadConvert(node->GetType(), right->GetType());
        emitStore(left);
    }
}

void NoxCodegen::emitComma(AstBinaryOpNode *node)
{
    AstNode *left, *right;

    node->GetChild(&left, &right);
    emitExpr(left);
    emitExpr(right);
}

void NoxCodegen::emitPreIncOrDec(AstUnaryOpNode *node, const std::string &op)
{
    emitExpr(node->Operand());

    std::function<void(AbstractRegister&, AbstractRegister&)> work;
    if (op == "add")
        work = [this](AbstractRegister &s, AbstractRegister &d) { genOpAdd(s, d); };
    else if (op == "sub")
        work = [this](AbstractRegister &s, AbstractRegister &d) { genOpSub(s, d); };
    else
        MY_THROW() << "undefined operator";

    AstType *ptrTy = node->GetType()->GetPTR();

    work(IntRegister(ptrTy ? ptrTy->GetSize() : 1), EAX());
    emitStore(node->Operand());
}

void NoxCodegen::emitPostIncOrDec(AstUnaryOpNode *node, const std::string &op)
{
    emitExpr(node->Operand());
    genOpPush(EAX());

    std::function<void(AbstractRegister&, AbstractRegister&)> work;
    if (op == "add")
        work = [this](AbstractRegister &s, AbstractRegister &d) { genOpAdd(s, d); };
    else if (op == "sub")
        work = [this](AbstractRegister &s, AbstractRegister &d) { genOpSub(s, d); };
    else
		MY_THROW() << "undefined operator";
    AstType *ptrTy = node->GetType()->GetPTR();
    work(IntRegister(ptrTy ? ptrTy->GetSize() : 1), EAX());

    emitStore(node->Operand());
    genOpPop(EAX());
}

void NoxCodegen::emitLogNot(AstUnaryOpNode *node)
{
    emitExpr(node->Operand());
    genOpCompareEqual(IntRegister(0), EAX());
    genOpGetCompareResult(EAX());
}

void NoxCodegen::emitLogAnd(AstBinaryOpNode *node)
{
    char *end = m_parser->make_label();
    AstNode *left, *right;

    node->GetChild(&left, &right);
    emitExpr(left);
    genOpTest();
    genOpMov(IntRegister(0), EAX());
    genOpJE(end);
    emitExpr(right);
    genOpTest();
    genOpMov(IntRegister(0), EAX());
    genOpJE(end);
    genOpMov(IntRegister(1), EAX());
    emitLabel(end);
}

void NoxCodegen::emitLogOr(AstBinaryOpNode *node)
{
    char *end = m_parser->make_label();
    AstNode *left, *right;
    node->GetChild(&left, &right);
    emitExpr(left);
    genOpTest();
    genOpMov(IntRegister(1), EAX());
    genOpJNE(end);
    emitExpr(right);
    genOpTest();
    genOpMov(IntRegister(1), EAX());
    genOpJNE(end);
    genOpMov(IntRegister(0), EAX());
    emitLabel(end);
}

void NoxCodegen::emitBitAnd(AstBinaryOpNode *node)
{
    AstNode *left, *right;
    node->GetChild(&left, &right);
    emitExpr(left);
    genOpPush(EAX());
    emitExpr(right);
    genOpPop(ECX());
    genOpAnd(ECX(), EAX());
}

void NoxCodegen::emitBitOr(AstBinaryOpNode *node)
{
    AstNode *left, *right;

    node->GetChild(&left, &right);
    emitExpr(left);
    genOpPush(EAX());
    emitExpr(right);
    genOpPop(ECX());
    genOpOr(ECX(), EAX());
}

void NoxCodegen::emitBitNot(AstUnaryOpNode *node)
{
    emitExpr(node->Operand());
    genOpNot(EAX());
}

void NoxCodegen::emitJMP(const std::string &label)
{
    genOpJMP(label);
}

void NoxCodegen::emitJE(const std::string &label)
{
    genOpTest();
    genOpJE(label);
}

void NoxCodegen::emitTernary(AstIfNode *node)
{
    AstNode *cond, *then, *els;

    node->FetchBranches(&cond, &then, &els);
    emitExpr(cond);
    char *ne = m_parser->make_label();
    emitJE(ne);
    if (then)
        emitExpr(then);
    if (els) {
        char *end = m_parser->make_label();
        emitJMP(end);
        emitLabel(ne);
        emitExpr(els);
        emitLabel(end);
    }
    else {
        emitLabel(ne);
    }
}

void NoxCodegen::emitGoto(AstGotoNode *node)
{
	LOGIC_ASSERT(node->NewLabel() != nullptr)
    emitJMP(node->NewLabel() );
}

void NoxCodegen::emitLabel(const std::string &label)
{
    genOpLabel(label);
}

void NoxCodegen::emitComputedGoto(AstNode *node)
{
	MY_THROW() << "not support";
    //emitExpr(node->operand);
    //emit("jmp *#rax");
}

void NoxCodegen::emitIntCompare(AstBinaryOpNode *node, int compareKind)
{
    AstNode *left;

    node->GetChild(&left, nullptr);
    bool ignoreSigned = left->GetType()->Unsigned();
    auto binOp = loadIntCompare(compareKind, ignoreSigned);

	if (!binOp)
		MY_THROW() << "int compare is null!!";
    //여기에서 unsigned 에 따라 비교 op 코드가 다릅니다!!
    (this->*binOp)(EAX(), ECX());
}

void NoxCodegen::emitFloatCompare(AstNode *node, int compareKind)
{
    //if (node->left->ty->kind != KIND_FLOAT)
    //    MY_THROW() << "expect float";

    auto binOp = loadFloatCompare(compareKind);

	if (!binOp)
		MY_THROW() << "unknown float compare";
    (this->*binOp)(EAX(), ECX());
}

void NoxCodegen::emitCompare(AstBinaryOpNode *node, int compareKind)
{
    AstNode *left, *right;

    node->GetChild(&left, &right);
    emitExpr(left);
    genOpPush(EAX());
    emitExpr(right);
    genOpPop(ECX());
    if (left->GetType()->IsFloatType())
    {
        emitFloatCompare(node, compareKind);
    }
    else
    {
        emitIntCompare(node, compareKind);
    }

    genOpGetCompareResult(EAX());
}

void NoxCodegen::emitPointerArith(char kind, AstNode *left, AstNode *right)
{
    emitExpr(left);
    genOpPush(ECX());
    genOpPush(EAX());
    emitExpr(right);
    int size = left->GetType()->GetPTR()->GetSize();

    switch (size)
    {
    case 0:
        MY_THROW() << "assert";
    case 1:
        genOpMov(EAX(), ECX());
        break;

    case 2:
        genOpLea(DerefRegister(EAX(), EAX()), ECX());
        break;

    default:
        genOpLea(DerefRegister(size, EAX()), ECX());
    }

    //if (size > 1)
    //    genOpMul(IntRegister(size), EAX());
    //genOpMov(EAX(), ECX());
    genOpPop(EAX());
    switch (kind) {
    case '+': genOpAdd(ECX(), EAX()); break;
    case '-': genOpSub(ECX(), EAX()); break;
    default: THROW_ERROR_NOPOS("invalid operator '%d'", kind);
    }
    genOpPop(ECX());
}

void NoxCodegen::emitBinaryOpIntArith(AstBinaryOpNode *node)
{
    AstNode *left, *right;

    node->GetChild(&left, &right);
    emitExpr(left);
    genOpPush(EAX());
    emitExpr(right);
    genOpMov(EAX(), ECX());
    genOpPop(EAX());

    auto oper = loadIntArithmetic(node->Kind(), node->GetType()->Unsigned());

	if (!oper)
		THROW_ERROR_NOPOS("invalid operator '%d'", node->Kind());

    switch (node->Kind())
    {
    case OP_SAL: case OP_SAR: case OP_SHR:
        (this->*oper)(ECX(1), EAX(left->GetType()->GetSize()));
        break;

    default:
        (this->*oper)(ECX(), EAX());
    }
}

void NoxCodegen::emitBinaryOpFloatArith(AstBinaryOpNode *node)
{
    auto oper = loadFloatArithmetic(node->Kind());
    //bool isdouble = (node->ty->kind == KIND_DOUBLE);    //not support double
    if (!oper)
		THROW_ERROR_NOPOS("invalid operator '%d'", node->Kind());

    AstNode *left, *right;
    node->GetChild(&left, &right);
    emitExpr(left);
    genOpPush(EAX());
    emitExpr(right);
    genOpMov(EAX(), ECX());
    genOpPop(EAX());
    (this->*oper)(ECX(), EAX());
}

void NoxCodegen::emitBinaryOp(AstNode *node)
{
    AstBinaryOpNode *bnode = static_cast<AstBinaryOpNode *>(node);

    if (node->GetType()->IsKind(KIND_PTR)) {
        AstNode *left, *right;

        bnode->GetChild(&left, &right);
        emitPointerArith(node->Kind(), left, right);
        return;
    }

    switch (node->Kind()) {
    case '<':
    case OP_EQ:
    case OP_LE:
    case OP_NE:
        emitCompare(bnode, node->Kind());
        return;
    }
    if (node->GetType()->IsIntType())
        emitBinaryOpIntArith(bnode);
    else if (node->GetType()->IsFloatType())
        emitBinaryOpFloatArith(bnode);
    else
		THROW_ERROR_NOPOS("internal error: %s", node->Explain());
}

void NoxCodegen::emitDeclare(AstDeclNode *node)
{
    if (!node->DeclInit())
        return;

    AstVariableNode *dclvar = static_cast<AstVariableNode *>( node->DeclVar() );
    int loff;
    dclvar->FetchLvarData(nullptr, &loff, nullptr);
    emitDeclareInit(node->DeclInit(),  loff, dclvar->GetType()->GetSize());
}

void NoxCodegen::maybeBooleanizeReturnValue(AstType *ty)
{
    if (ty->IsKind( KIND_BOOL) ) {
        genOpMovzx(EAX(1), EAX());
    }
}

void NoxCodegen::restoreArgRegs(int nInts, int nFloats)
{
    //for (int i = nFloats - 1; i > 0; i--)
    //    pop_xmm(i);
    for (int i = nInts - 1; i >= 0; i--)
        genOpPop(loadREG(nInts));
}

void NoxCodegen::popIntArgs(int nints)
{
    for (int i = nints - 1; i >= 0; i--)
        genOpPop(loadREG(i));
}

int NoxCodegen::emitArgs(CCVector *vals)
{
    int r = 0;
    for (int i = 0; i < vals->Length(); i++) {
        AstNode *v = reinterpret_cast<AstNode *>(vals->Get( i));
        if (v->GetType()->IsKind( KIND_STRUCT) ) {
            emitAddress(v);
            r += pushStructMemcopy(v->GetType()->GetSize() );
        }
#ifdef NOXCODEGEN_ENABLE_FLOAT_TYPE_
        else if (is_flotype(v->ty)) {
            emitExpr(v);
            push_xmm(0);
            r += 8;
        }
#endif
        else {
            if (v->IsKind( AST_LITERAL) )
            {
                emitLiteral(v, true);
            }
            else
            {
                emitExpr(v);
                genOpPush(EAX());
            }
            r += NoxOpGenerator::default_bytes;
        }
    }
    return r;
}

void NoxCodegen::saveArgRegs(int nints, int nfloats)
{
	if (nints > ireg_max_count)
		MY_THROW() << stringFormat("assert nints <= %d", ireg_max_count);
    assert(nfloats <= 8);
    for (int i = 0; i < nints; i++)
        genOpPush(loadREG(i));
	for (int i = 1; i < nfloats; i++)
		MY_THROW() << "nop"; //push_xmm(i);
}

void NoxCodegen::classifyArgs(CCVector *ints, CCVector *floats, CCVector *rest, AstFunctionNode *fnode)
{
    int ireg = 0; // , xreg = 0;
    int imax = ireg_max_count; // , xmax = 8;
    for (int i = 0; i < fnode->ArgLength() ; i++) {
        AstNode *v = fnode->GetArg(i);
        if (v->GetType()->IsKind( KIND_STRUCT) )
            rest->Push( v);
        //else if (is_flotype(v->ty))
        //    vec_push((xreg++ < xmax) ? floats : rest, v);
		else
		{
			((ireg++ < imax) ? ints : rest)->Push( v);
		}
    }
}

void NoxCodegen::emitFunctionCall(AstFunctionNode *node)
{
    int opos = m_stackpos;
    bool isptr = (node->IsKind( AST_FUNCPTR_CALL) );
    AstType *ftype = isptr ? node->FunctionPTR()->GetType()->GetPTR() : node->FTYPE();
	CCVector *ints = m_memSweep->AllocateObject<CCVector>();
	CCVector *rest = m_memSweep->AllocateObject<CCVector>();

    classifyArgs(ints, /*floats*/nullptr, rest, node);
    //saveArgRegs(ints->Length(), 0/* vec_len(floats)*/);

#ifdef NOXCODEGEN_INSERT_PADDING_
    bool padding = m_stackpos % 16;
    if (padding) {
        genOpSub(loadINT(NoxOpGenerator::default_bytes), ESP());
        m_stackpos += NoxOpGenerator::default_bytes;
    }
#endif

    int restsize = emitArgs(rest->Reverse());
    if (isptr) {
        emitExpr(node->FunctionPTR());
		genOpMov(EAX(), EBX());
    }
    restsize += emitArgs(ints->Reverse());  //2022-11-13 22:54 반전을 하면?
    //popIntArgs(ints->Length());

    if (isptr)
        genOpCallFromReg(EBX());
	else
		genOpCallFunction(node->FunctionName());

    maybeBooleanizeReturnValue(node->GetType());
    if (restsize > 0) {
        genOpAdd(IntRegister(restsize), ESP());
        m_stackpos -= restsize;
    }

#ifdef NOXCODEGEN_INSERT_PADDING
    if (padding) {
        genOpAdd(loadINT(NoxOpGenerator::default_bytes), ESP());
        m_stackpos -= NoxOpGenerator::default_bytes;
    }
#endif
    //restoreArgRegs(ints->Length(), 0 /*vec_len(floats)*/);
	LOGIC_ASSERT(m_stackpos == opos)
}

void NoxCodegen::emitLiteral(AstNode *node, bool direct)
{
	switch (node->GetType()->Kind())
	{
	case KIND_BOOL:
	case KIND_CHAR:
	case KIND_SHORT:
	case KIND_INT:
	case KIND_LONG:
	case KIND_LLONG:
	{
        int iValue = static_cast<AstLiteralNode *>(node)->GetIntValue();

        if (direct)
            genOpPush(IntRegister(iValue));
        else
            genOpMov(IntRegister(iValue), EAX());
		break;
	}
	case KIND_FLOAT:
	{
        float fconv = static_cast<AstLiteralNode *>(node)->GetFloatValue();
        int *iConv = (int *)&fconv;

        if (direct)
            genOpPush(IntRegister(*iConv));
        else
            genOpMov(IntRegister(*iConv), EAX());
#if 0
		std::string vlabel = m_dataSection->PutVLabel(0);

		m_dataSection->Allocate<ArithAllocator<float>>(static_cast<float>(node->fval));
		genOpLea(loadGlobalReg(vlabel), EAX());
#endif
		break;
	}
	case KIND_DOUBLE:
	case KIND_LDOUBLE:
		MY_THROW() << "not support";
		break;

	case KIND_ARRAY:
    {
        AstStringNode *strNode = static_cast<AstStringNode *>(node);

        if (!strNode->StringLabel())
        {
            strNode->PutStringLabel(const_cast<char *>(m_dataSection->PutVLabel(0)));	//ensure don't change			)
            LOGIC_ASSERT( node->GetType()->GetPTR() != nullptr);
            switch ( node->GetType()->GetPTR()->Kind())
            {
            case KIND_CHAR:
                m_dataSection->Allocate<StringAllocator<char>>(strNode->StringValue());
                break;

            case KIND_SHORT:
                m_dataSection->Allocate<StringAllocator<wchar_t>>(strNode->WideStringValue()); //reinterpret_cast<const short *>(strNode->StringValue()));
                break;

            default:
                THROW_ERROR_NOPOS("undefined array target");
            }
        }
        if (direct)
        {
            genOpPush(GlobalRegister(strNode->StringLabel()));
            break;
        }
        genOpLea(GlobalRegister(strNode->StringLabel()), EAX());
        break;
    }

	default:
		THROW_ERROR_NOPOS("internal error");
	}
}

void NoxCodegen::maybeEmitBitshiftSave(AstType *ty, AbstractRegister &addr)
{
    if (ty->BitSize() <= 0)
        return;

    genOpPush(ECX());
    genOpPush(EDI());
	genOpMov(IntRegister((1 << (long)ty->BitSize()) - 1), EDI());
	genOpAnd(EDI(), EAX());
	genOpShl(IntRegister(ty->BitOffset()), EAX());
	genOpMov(addr, ECX(ty->GetSize()));
	genOpMov(IntRegister(~(((1 << (long)ty->BitSize()) - 1) << ty->BitOffset())), EDI());
	genOpAnd(EDI(), ECX());
	genOpOr(ECX(), EAX());
    genOpPop(EDI());
    genOpPop(ECX());
}

void NoxCodegen::maybeEmitBitshiftLoad(AstType *ty)
{
	if (ty->BitSize() <= 0)
		return;

	genOpShr(IntRegister(ty->BitOffset() ), EAX());
	genOpPush(ECX());
	genOpMov(IntRegister((1 << (long)ty->BitSize() ) - 1), ECX());	//emit("mov $0x%lx, #rcx", (1 << (long)ty->bitsize) - 1);
	genOpAnd(ECX(), EAX());
	genOpPop(ECX());
}

void NoxCodegen::emitLLoad(AstType *ty, AbstractRegister &base, int off)
{
	switch (ty->Kind())
	{
	case KIND_ARRAY:
		genOpLea(DerefRegister(base, off), EAX());
		break;

    case KIND_INT:
	case KIND_FLOAT:
		genOpMov(DerefRegister(base, off), EAX());
		break;

	case KIND_DOUBLE:
	case KIND_LDOUBLE:
		MY_THROW() << "FIXME";	//emit("movsd %d(#%s), #xmm0", off, base);
		break;

	default:
		////eax 을 ffffffff 으로 세팅합니다, 그 다음에 크기 만큼의 값을 eax에 대입합니다
		////예를 들어, 대입이 1바이트 0xaa 일 때, movsbq 을 수행하면, 이렇게 되어집니다
		//// eax = ffffffaa
		//char *inst = get_load_inst(ty);
		//emit("%s %d(#%s), #rax", inst, off, base);	//mov [ebp+08], eax
        if (ty->Unsigned())
            genOpMovzx(DerefRegister(base, off), EAX(ty->GetSize()));
        else
            genOpMovsx(DerefRegister(base, off), EAX(ty->GetSize() ));
		maybeEmitBitshiftLoad(ty);
	}
}

void NoxCodegen::emitGLoad(AstType *ty, const char *label, int off)
{
    if (ty->IsKind( KIND_ARRAY) )
	{
		genOpLea(GlobalRegister(label, off), EAX());
        return;
    }
	genOpMovsx(GlobalRegister(label, off), EAX(ty->GetSize()));
    maybeEmitBitshiftLoad(ty);
}

static int cmpinit(const void *x, const void *y)
{
	AstNode *a = *reinterpret_cast<AstNode **>(const_cast<void *>(x));
	AstNode *b = *reinterpret_cast<AstNode **>(const_cast<void *>(y));

    LOGIC_ASSERT(a->IsKind(AST_INIT) && b->IsKind(AST_INIT));
    return static_cast<AstInitializerNode *>(a)->InitOffset() - static_cast<AstInitializerNode *>(b)->InitOffset();
}

void NoxCodegen::emitZeroFiller(int start, int end)
{
    if (end - start >= 12)
    {
        genOpZeroFiller(IntRegister(start), IntRegister(end));
        for (; start <= end - 4; start += 4);
    }
    else
    {
        for (; start <= end - 4; start += 4)
            genOpLocalStore(IntRegister(0), start);
    }

	for (; start < end; start++)
	{
		genOpMovN(IntRegister(0), DerefRegister(EBP(), start), sizeof(char));	//1바이트만 써야합니다!!//
	}
}

void NoxCodegen::emitFillHoles(AstVector *inits, int off, int totalsize)
{
	// If at least one of the fields in a variable are initialized,
	// unspecified fields has to be initialized with 0.
	int len = inits->Length(); //m_memSweep->AllocateRaw<Node *>(len);

#ifdef USE_OLDEST_ALLOCATE_
	Node **buf = reinterpret_cast<Node **>(malloc(len * sizeof(Node *)));
#else
	AstNode **buf = m_memSweep->AllocateRaw<AstNode *>(len * sizeof(AstNode *));
#endif

	for (int i = 0; i < len; i++)
		buf[i] = static_cast<AstNode *>(inits->Get( i));
	qsort(buf, len, sizeof(AstNode *), cmpinit);

	int lastend = 0;
	for (int i = 0; i < len; i++)
	{
		AstInitializerNode *node = dynamic_cast<AstInitializerNode *>( buf[i] );

        if (!node)
            THROW_ERROR_NOPOS("bad cast");

		if (lastend < node->InitOffset())
			emitZeroFiller(lastend + off, node->InitOffset() + off);
        lastend = node->InitOffset() + node->ToType()->GetSize();
	}
	emitZeroFiller(lastend + off, totalsize + off);
}

void NoxCodegen::emitSaveLiteral(AstNode *node, AstType *totype, int off)
{
    AstLiteralNode *lnode = static_cast<AstLiteralNode *>(node);

	switch (totype->Kind())
	{
	case KIND_BOOL: genOpMovN(IntRegister(!!lnode->GetIntValue()), DerefRegister(EBP(), off), sizeof(char)); break;
	case KIND_CHAR: genOpMovN(IntRegister(lnode->GetIntValue()), DerefRegister(EBP(), off), sizeof(char)); break;
	case KIND_SHORT: genOpMovN(IntRegister(lnode->GetIntValue()), DerefRegister(EBP(), off), sizeof(short)); break;
	case KIND_INT:
	case KIND_LONG:
	case KIND_LLONG:
	case KIND_PTR:
	{   //기존8바이트->4바이트, 그러므로 포인터는 INT 처럼 처리합니다
        genOpLocalStore(IntRegister(lnode->GetIntValue()), off);
		break;
	}
	case KIND_FLOAT:
	{
		float fval = lnode->GetFloatValue();

        genOpLocalStore(IntRegister(*(int *)&fval), off);
		break;
	}
	case KIND_DOUBLE:
	case KIND_LDOUBLE:
	{
		MY_THROW() << "FIXME";
		//emit("movl $%lu, %d(#rbp)", *(uint64_t *)&node->fval & ((1L << 32) - 1), off);
		//emit("movl $%lu, %d(#rbp)", *(uint64_t *)&node->fval >> 32, off + 4);
		break;
	}
	default:
		THROW_ERROR_NOPOS("internal error: <%s> <%s> <%d>", node->Explain(), totype->Explain(), off);
	}
}

void NoxCodegen::emitLsave(AstType *ty, int off)
{
    switch (ty->Kind())
    {
    case KIND_FLOAT: case KIND_INT: case KIND_ENUM: case KIND_PTR: case KIND_ARRAY:
        genOpLocalStore(EAX(), off);
        break;

    case KIND_DOUBLE:
		MY_THROW() << "unsupport";
        
    default:
        maybeConvertBool(ty);
        genOpLocalStore(EAX(ty->GetSize()), off);
        break;
    }
}

void NoxCodegen::emitGSave(const char *varname, AstType *ty, int off)
{
    LOGIC_ASSERT(!ty->IsKind(KIND_ARRAY));

    maybeConvertBool(ty);
	//GlobalRegister globalAddr(varname, off);
	AbstractRegister &globalAddr = GlobalRegister(varname, off);

    maybeEmitBitshiftSave(ty, globalAddr);
    genOpMov(EAX(ty->GetSize()), globalAddr);
}

void NoxCodegen::emitDeclareInit(AstVector *inits, int off, int totalsize)
{
	emitFillHoles(inits, off, totalsize);
	for (int i = 0; i < inits->Length(); i++)
	{
		AstInitializerNode *node = reinterpret_cast<AstInitializerNode *>(inits->Get( i));

        LOGIC_ASSERT(node->IsKind(AST_INIT));

		bool isbitfield = (node->ToType()->BitSize() > 0);
		if (node->InitValue()->IsKind( AST_LITERAL) && !isbitfield)
		{
			emitSaveLiteral(node->InitValue(), node->ToType(), node->InitOffset() + off);
		}
		else
		{
			emitExpr(node->InitValue() );
			emitLsave(node->ToType(), node->InitOffset() + off);
		}
	}
}

void NoxCodegen::ensureLvarInit(AstVariableNode *node)
{
    LOGIC_ASSERT(node->IsKind(AST_LVAR));
    AstVector *lvarinit = nullptr;
    int loff;

    node->FetchLvarData(&lvarinit, &loff, nullptr);
	if (lvarinit)
		emitDeclareInit(lvarinit, loff, node->GetType()->GetSize());
	node->SetLvarInit(nullptr);
}

void NoxCodegen::emitLvar(AstVariableNode *node)
{
    int loff;

    node->FetchLvarData(nullptr, &loff, nullptr);
	ensureLvarInit(node);
	emitLLoad(node->GetType(), EBP(), loff);
}

void NoxCodegen::emitGvar(AstGlobalVariableNode *node)
{
    emitGLoad(node->GetType(), node->GlobalLabel(), 0);
}

void NoxCodegen::emitAddress(AstNode *node)
{
    switch (node->Kind()) {
    case AST_LVAR:
    {
        ensureLvarInit(static_cast<AstVariableNode *>(node));
        int loff;
        static_cast<AstVariableNode *>(node)->FetchLvarData(nullptr, &loff, nullptr);
        genOpLea(DerefRegister(EBP(), loff), EAX());
        break;
    }
    case AST_GVAR:
        genOpLea(GlobalRegister(static_cast<AstGlobalVariableNode *>(node)->GlobalLabel()), EAX());
        break;
    case AST_DEREF:
        emitExpr(static_cast<AstUnaryOpNode *>(node)->Operand());
        break;
    case AST_STRUCT_REF:
        emitAddress(static_cast<AstStructNode *>( node)->Struc() );
        genOpAdd(IntRegister( node->GetType()->Offset() ), EAX());
        break;
    case AST_FUNCDESG:
        genOpMov(FunctionRegister(static_cast<AstFunctionNode *>(node)->FunctionName() ), EAX());
        break;
    default:
		THROW_ERROR_NODE(node, "internal error: %s %d", node->Explain(), node->Kind());
    }
}

int NoxCodegen::pushStructMemcopy(int size)
{	//pushStruct memcopy 쓰는 버전입니다
	int aligned = getAlign(size, NoxOpGenerator::default_bytes);

	genOpSub(IntRegister(aligned), ESP());	//sub esp, aligned
	//memcopy [src: eax, dest: esp]
	genOpMemCopy(EAX(), ESP(), IntRegister(aligned / NoxOpGenerator::default_bytes));
	m_stackpos += aligned;
	return aligned;
}

int NoxCodegen::pushStruct(int size)
{
    //함수 call 전에, 인자로 삽입된 구조체를 가리키고, 새 구조체를 복사
    int aligned = getAlign(size, NoxOpGenerator::default_bytes);

    genOpSub(IntRegister(aligned), ESP());      //emit("sub $%d, #rsp", aligned);   //새 영역할당
    genOpMov(ECX(), DerefRegister(ESP(), IntRegister(-NoxOpGenerator::default_bytes)));   //emit("mov #rcx, -8(#rsp)");       //기존값 저장 인듯 한데,
    genOpMov(EBX(), DerefRegister(ESP(), IntRegister(-NoxOpGenerator::default_bytes * 2)));   //emit("mov #r11, -16(#rsp)");  //이것도.. 만약, 그렇다면 나중에 push 으로 바꿔도 될듯//
    genOpMov(EAX(), ECX());     //emit("mov #rax, #rcx");	//인수로 들어온 구조체의 시작주소를 가리킵니다
    int i = 0;

    for (; i + 4 < size; i += 4) {
		genOpMov(DerefRegister(ECX(), IntRegister(i)), EBX());		//emit("movl %d(#rcx), #r11", i);
		genOpMov(EBX(), DerefRegister(ESP(), IntRegister(i)));		//emit("movl #r11d, %d(#rsp)", i);
    }
    for (; i < size; i++) {
		//emit("movb %d(#rcx), #r11", i);	//만약에 여기에서, bytemov을 하면..? //src 을 바이트 단위로 읽어야 할 텐데..;
		genOpMov(DerefRegister(ECX(), IntRegister(i)), EBX(1));
        //emit("movb #r11b, %d(#rsp)", i);	//레지스터를 value, bytevalue, wordvalue 으로 세분화 하면, 어떤 문제가 생길지?
		genOpMov(EBX(1), DerefRegister(ESP(), IntRegister(i)));
    }
    genOpMov(DerefRegister(ESP(), IntRegister(-NoxOpGenerator::default_bytes)), ECX());       //emit("mov -8(#rsp), #rcx");
    genOpMov(DerefRegister(ESP(), IntRegister(-NoxOpGenerator::default_bytes * 2)), EBX());       //emit("mov -16(#rsp), #r11");
    m_stackpos += aligned;
    return aligned;
}

void NoxCodegen::pushFunctionParamsRewritten(const AstFunctionNode &fnode) //f->params
{		//송신측에서 인자를 pushing 했으므로, 수신측은 단지 인자에 넘버링만 합니다
	int argoff = 4;	//ebp 제외해야 하므로, 초기값은 4 입니다

	for (int i = 0; i < fnode.ParamLength() ; ++i)
	{
		AstNode *v = fnode.GetParam(i);

        LOGIC_ASSERT(v->IsKind(AST_LVAR));
		static_cast<AstVariableNode *>(v)->AdjustLocalOffset( argoff );
		if (v->GetType()->IsKind(KIND_STRUCT) )
		{
			argoff += getAlign(v->GetType()->GetSize(), NoxOpGenerator::default_bytes);
		}
		else
		{
			argoff += NoxOpGenerator::default_bytes;
		}
	}
}

void NoxCodegen::pushFunctionParams(AstVector *params, int off)
{
    int arg = 2;    //Why is it two??   //이전 ebp 값 그리고, 복귀주소를 제외하기 때문에, 2입니다/.
	int ireg = 0;

    for (int i = 0; i < params->Length(); i++)
    {
        AstNode *v = static_cast<AstNode *>(params->Get(i));

        if (v->GetType()->IsKind( KIND_STRUCT) ) {
            genOpLea(DerefRegister(EBP(), IntRegister(arg*NoxOpGenerator::default_bytes)), EAX());     //emit("lea %d(#rbp), #rax", arg * bit_wise);  //구조체 인자 시작번지 가리키기
            int size = pushStruct(v->GetType()->GetSize() );
            off -= size;
            arg += size / NoxOpGenerator::default_bytes;
        }
#ifdef NOXCODEGEN_ENABLE_FLOAT_TYPE_
        else if (CCParse::is_flotype(v->ty)) {
            genOpMov(DerefRegister(EBP(), loadINT(arg++ * NoxOpGenerator::default_bytes)), EAX());    //8바이트 형식은, 아직 모르겠음...
            genOpPush(EAX());
            off -= NoxOpGenerator::default_bytes;
        }
#endif
        else {
            if (ireg >= ireg_max_count) {
                if (v->GetType()->IsKind( KIND_BOOL) ) {
					genOpMov(DerefRegister(EBP(), IntRegister(arg++ * NoxOpGenerator::default_bytes)), EAX(1));
					genOpMovzx(EAX(1), EAX());
                }
                else {
					genOpMov(DerefRegister(EBP(), IntRegister(arg++ * NoxOpGenerator::default_bytes)), EAX());
                }
				genOpPush(EAX());
            }
            else {
                if (v->GetType()->IsKind( KIND_BOOL ))
					genOpMovzx(loadREG(ireg, 1), loadREG(ireg));
				genOpPush(loadREG(ireg++));
            }
            off -= NoxOpGenerator::default_bytes;
        }
        LOGIC_ASSERT(dynamic_cast<AstVariableNode *>(v) != nullptr);
        static_cast<AstVariableNode *>( v)->AdjustLocalOffset(off);
    }
}

void NoxCodegen::emitReturn(AstReturnNode *node)
{
	if (node->ReturnValue() )
	{
		emitExpr(node->ReturnValue() );
		maybeBooleanizeReturnValue(node->ReturnValue()->GetType() );
	}
	emitRet();
}

void NoxCodegen::emitRet()
{
	genOpReturn();
}

void NoxCodegen::emitExpr(AstNode *node)
{
	switch (node->Kind())
	{
	case AST_LITERAL: emitLiteral(node); return;
	case AST_LVAR:    emitLvar(static_cast<AstVariableNode *>( node)); return;
	case AST_GVAR:    emitGvar(static_cast<AstGlobalVariableNode *>( node) ); return;
	case AST_FUNCDESG: emitAddress(node); return;
    case AST_FUNCALL:
        // fall through
    case AST_FUNCPTR_CALL:
        emitFunctionCall(static_cast<AstFunctionNode *>( node) );
        return;
    case AST_DECL:    emitDeclare(static_cast<AstDeclNode *>( node )); return;
    case AST_CONV:    emitConvert(static_cast<AstUnaryOpNode *>( node) ); return;
    case AST_ADDR:    emitAddress(static_cast<AstUnaryOpNode *>(node)->Operand() ); return;
    case AST_DEREF:   emitDereference(static_cast<AstUnaryOpNode *>( node) ); return;
    case AST_IF:
    case AST_TERNARY:
        emitTernary(static_cast<AstIfNode *>( node) );
        return;
    case AST_GOTO:    emitGoto(static_cast<AstGotoNode *>( node) ); return;
    case AST_LABEL:
    {
        auto newlabel = static_cast<AstLabelNode *>(node)->NewLabel();

        if (newlabel)
            emitLabel(newlabel);
        return;
    }
    case AST_RETURN:  emitReturn(static_cast<AstReturnNode *>( node) ); return;
    case AST_COMPOUND_STMT: emitCompoundStatement(static_cast<AstCompoundStmtNode *>(node)); return;
    case AST_STRUCT_REF:
        emitLoadStructRef(static_cast<AstStructNode *>(node)->Struc(), node->GetType(), 0);
        return;
    case OP_PRE_INC:   emitPreIncOrDec(static_cast<AstUnaryOpNode *>( node), "add"); return;
    case OP_PRE_DEC:   emitPreIncOrDec(static_cast<AstUnaryOpNode *>(node), "sub"); return;
    case OP_POST_INC:  emitPostIncOrDec(static_cast<AstUnaryOpNode *>(node), "add"); return;
    case OP_POST_DEC:  emitPostIncOrDec(static_cast<AstUnaryOpNode *>(node), "sub"); return;
    case '!': emitLogNot(static_cast<AstUnaryOpNode *>( node)); return;
    case '&': emitBitAnd(static_cast<AstBinaryOpNode *>( node )); return;
    case '|': emitBitOr(static_cast<AstBinaryOpNode *>(node)); return;
    case '~': emitBitNot(static_cast<AstUnaryOpNode *>( node)); return; // 테스트 필요//
    case OP_LOGAND: emitLogAnd(static_cast<AstBinaryOpNode *>( node)); return;
    case OP_LOGOR:  emitLogOr(static_cast<AstBinaryOpNode *>(node)); return;
    case OP_CAST:   emitCast(static_cast<AstUnaryOpNode *>( node )); return;
    case ',': emitComma(static_cast<AstBinaryOpNode *>(node) ); return;
    case '=': emitAssign(static_cast<AstBinaryOpNode *>( node) ); return;
	case OP_LABEL_ADDR: MY_THROW() << "op_label_addr not support";
    case AST_COMPUTED_GOTO: emitComputedGoto(node); return;
    default:
        emitBinaryOp(static_cast<AstBinaryOpNode *>( node) );
	}
}

void NoxCodegen::emitFunctionPrologue(AstFunctionNode *func)
{
    genOpPush(EBP());
    genOpMov(ESP(), EBP());
    //int off = 0;

    if (static_cast<AstFunctionType *>(func->GetType())->HasVarArgs()) {
        MY_THROW() << "not support yet";
    }
    //pushFunctionParams(func->params, off);
	pushFunctionParamsRewritten(*func);
    //off -= func->params->Length() * NoxOpGenerator::default_bytes;
	int off = 0;
    int localarea = 0;

    for (int i = 0; i < func->LocalVarsCount() ; i++)
    {
        AstNode *v = func->GetLocalVars(i);
        int size = getAlign(v->GetType()->GetSize(), NoxOpGenerator::default_bytes);
        LOGIC_ASSERT(size % NoxOpGenerator::default_bytes == 0);
        off -= size;
        static_cast<AstVariableNode *>(v)->AdjustLocalOffset( off );
        localarea += size;
    }
    if (localarea)
    {
        genOpSub(IntRegister(localarea), ESP());
        m_stackpos += localarea;
    }
}

void NoxCodegen::emitZero(int size)
{
	for (; size >= 8; size -= 8) m_dataSection->Allocate<ArithAllocator<uint64_t>>(0);
	for (; size >= 4; size -= 4) m_dataSection->Allocate<ArithAllocator<uint32_t>>(0);
	for (; size > 0; size--)     m_dataSection->Allocate<ArithAllocator<uint8_t>>(0);
}

void NoxCodegen::emitPadding(AstInitializerNode *node, int off)
{
    int diff = node->InitOffset() - off;

    if (diff < 0)
        MY_THROW() << "incorrect padding length";

    emitZero(diff);
}

void NoxCodegen::emitDataCharptr(const AstNode *node, int depth)
{
    const AstStringNode *strNode = static_cast<const AstStringNode *>(node);
	std::string vlabel = m_dataSection->PutVLabel(depth + 1);

	switch (node->GetType()->GetPTR()->Kind())
	{
	case KIND_CHAR:
		m_dataSection->AllocateWithDepth<StringAllocator<char>>(depth + 1, strNode->StringValue());
		break;

	case KIND_SHORT:
        m_dataSection->AllocateWithDepth<StringAllocator<wchar_t>>(depth + 1, strNode->WideStringValue()); //reinterpret_cast<const short *>(strNode->StringValue()));
		break;

	default:
		THROW_ERROR_NOPOS("cannot allocate strings");
	}
	m_dataSection->Allocate<PointerAllocator>(vlabel, m_gpointerTable);
	//나중에 링커에서, 레이블이 가리키는 영역의 주소로 치환해 주고, op 코드에서 그 주소를 가리키게 만들면 되겠죠//
}

void NoxCodegen::emitDataAddr(AstNode *operand, int depth)
{
    switch (operand->Kind())
    {
    case AST_LVAR:
    {
		std::string vlabel = m_dataSection->PutVLabel(depth + 1);
        AstVector *lvarinit = nullptr;
        
        static_cast<AstVariableNode *>(operand)->FetchLvarData(&lvarinit);
		doEmitData(lvarinit, operand->GetType()->GetSize(), 0, depth + 1);
		m_dataSection->Allocate<PointerAllocator>(vlabel, m_gpointerTable);
        return;
    }
    case AST_GVAR:
    {
        m_dataSection->AllocateWithDepth<PointerAllocator>(depth, static_cast<AstGlobalVariableNode *>( operand)->GlobalLabel(), m_gpointerTable);
        return;
    }

	case AST_FUNCDESG:	//operand->fname 에서 함수 이름을 얻을 수 있어요
		m_dataSection->Allocate<FunctionPointerAllocator>(static_cast<AstFunctionNode *>( operand)->FunctionName() );
		break;

    default:
		THROW_ERROR_NOPOS("internal error");
    }
}

void NoxCodegen::emitDataPrimaryType(AstType *ty, AstNode *val, int depth)
{
    switch (ty->Kind())
    {
    case KIND_FLOAT:
		m_dataSection->Allocate<ArithAllocator<float>>(static_cast<float>(static_cast<AstLiteralNode *>( val)->GetFloatValue()));
        break;

    case KIND_DOUBLE:
    {
        float fval = static_cast<AstLiteralNode *>(val)->GetFloatValue();

        m_dataSection->Allocate<ArithAllocator<uint64_t>>(*reinterpret_cast<uint64_t *>(&fval));
        break;
    }

    case KIND_BOOL:
		m_dataSection->Allocate<ArithAllocator<uint8_t>>(!!val->EvaluateIntExpr( nullptr));
        break;

    case KIND_CHAR:
		m_dataSection->Allocate<ArithAllocator<uint8_t>>(val->EvaluateIntExpr( nullptr));
        break;

    case KIND_SHORT:
		m_dataSection->Allocate<ArithAllocator<uint16_t>>(val->EvaluateIntExpr( nullptr));
        break;

    case KIND_INT:
    case KIND_LONG:
		m_dataSection->Allocate<ArithAllocator<uint32_t>>(val->EvaluateIntExpr( nullptr));
        break;

    case KIND_LLONG:
    case KIND_PTR:
    {
		if (val->IsKind(AST_LITERAL))
			THROW_ERROR_NOPOS("expect ptr but got literal");
        if (val->IsKind(OP_LABEL_ADDR))
        {
			MY_THROW() << "no attribution op_label_addr";        //FIXME
        }

        //bool isCharPtr = (val->operand->ty->kind == KIND_ARRAY && val->operand->ty->ptr->kind == KIND_CHAR);
        AstNode *oprand = static_cast<AstUnaryOpNode *>(val)->Operand();
		bool isCharPtr = oprand->GetType()->IsString() && oprand->IsKind(AST_LITERAL);

        if (isCharPtr)
        {
            emitDataCharptr(oprand, depth);
        }
        else if (val->IsKind(AST_GVAR))
        {
			m_dataSection->Allocate<PointerAllocator>(static_cast<AstGlobalVariableNode *>( val)->GlobalLabel(), m_gpointerTable);
        }
        else
        {
            AstNode *base = nullptr;
            int v = val->EvaluateIntExpr( &base);

            if (base == nullptr)
            {
				m_dataSection->Allocate<ArithAllocator<uint32_t>>(v);
                break;
            }
            AstType *ty = base->GetType();
            if (base->IsKind( AST_CONV) || base->IsKind(AST_ADDR))
                base = static_cast<AstUnaryOpNode *>( base)->Operand();
            if (!base->IsKind( AST_GVAR) )
				THROW_ERROR_NOPOS("global variable expected, but got %s", base->Explain());

            if (!ty->GetPTR())
                MY_THROW() << "ty->ptr is null";
            
			m_dataSection->Allocate<PointerAllocator>(static_cast<AstGlobalVariableNode *>( base)->GlobalLabel(), m_gpointerTable, v * ty->GetPTR()->GetSize());
        }
        break;
    }
    default:
        throw std::logic_error(stringFormat("don't know how to handle\n  <%s>\n  <%s>", ty->Explain(), val->Explain()));
    }
}

void NoxCodegen::doEmitData(AstVector *inits, int size, int off, int depth)
{
    for (int i = 0; i < inits->Length() && 0 < size; i++)
    {
        AstInitializerNode *node = reinterpret_cast<AstInitializerNode *>(inits->Get( i));
        AstNode *v = node->InitValue();

        emitPadding(node , off);
        if (node->ToType()->BitSize() > 0) {        //비트필드입니다, 지원안합니다
            MY_THROW() << "not support feature yet";

            uint32_t data = v->EvaluateIntExpr( nullptr);
            AstType *totype = node->ToType();
            for (i++ ; i < inits->Length(); i++) {
                node = reinterpret_cast<AstInitializerNode *>(inits->Get( i));
                if (node->ToType()->BitSize() <= 0) {
                    break;
                }
                v = node->InitValue();
                totype = node->ToType();
                data |= ((((long)1 << totype->BitSize()) - 1) & v->EvaluateIntExpr( nullptr)) << totype->BitOffset();
            }
            AstIntNode nodeTmp(totype, data);

            emitDataPrimaryType(totype, &nodeTmp, depth);
            off += totype->GetSize();
            size -= totype->GetSize();
            if (i == inits->Length())
                break;
        }
        else {
            off += node->ToType()->GetSize();
            size -= node->ToType()->GetSize();
        }
        if (v->IsKind( AST_ADDR)) {
            emitDataAddr(static_cast<AstUnaryOpNode *>( v)->Operand(), depth);
            continue;
        }
        if (v->IsKind(AST_LVAR))
        {
            AstVariableNode *lvnode = static_cast<AstVariableNode *>(v);
            AstVector *lvarinit = nullptr;
            lvnode->FetchLvarData(&lvarinit, nullptr, nullptr);
            if (lvarinit)
            {
                doEmitData(lvarinit, v->GetType()->GetSize(), 0, depth);
                continue;
            }
        }
        emitDataPrimaryType(node->ToType(), node->InitValue(), depth);
    }
    emitZero(size);
}

void NoxCodegen::emitData(AstDeclNode *v, int off, int depth)      //Brief. depth 이 제로가 아닌 경우가 없다라고 간주합니다//
{
    auto declvar = v->DeclVar();

	m_dataSection->PutLabel(static_cast<AstGlobalVariableNode *>(declvar)->GlobalLabel() , declvar->GetType()->IsStatic() );
    doEmitData(v->DeclInit() , declvar->GetType()->GetSize(), off, depth);
}

//광역변수를 초기화하지 않으면, 크기만큼 제로로서 초기화 되어집니다
void NoxCodegen::emitBss(AstDeclNode *v)
{
	//m_dataSection->PutLabel(v->declvar->glabel, v->declvar->ty->isstatic);
	//m_dataSection->Allocate<BssDataAllocator>(v->declvar->ty->size);
    auto var = v->DeclVar();

	LOGIC_ASSERT(var->GetType()->GetSize() > 0);    
	m_dataSection->PutBssLabel(static_cast<AstGlobalVariableNode *>(var)->GlobalLabel(), var->GetType()->GetSize(), var->GetType()->IsStatic() );
}

void NoxCodegen::emitGlobalVariable(AstDeclNode *v)
{
    if (v->DeclInit() )
        emitData(v, 0, 0);
    else
        emitBss(v);
}

void NoxCodegen::EmitToplevel(AstNode *v)
{
    m_stackpos = NoxOpGenerator::default_bytes;
	if (v->IsKind(AST_FUNC) )
	{
        AstFunctionNode *fnNode = static_cast<AstFunctionNode *>(v);

		attachFunction(std::make_unique<NoxFunction>(fnNode->FunctionName(), v->GetType()->IsStatic()));    //아직, static 여부는 판단하지 않음//
		emitFunctionPrologue(fnNode);
		emitExpr(fnNode->FunctionBody());
		emitRet();     //WHAT? 리턴 코드가 두번 만들어짐//
		m_sourceFunction->Push(detachFunction());
	}
	else if (v->IsKind(AST_DECL))
		emitGlobalVariable(static_cast<AstDeclNode *>(v) );
	else
		MY_THROW() << "internal error";
}

std::unique_ptr<FunctionTable> NoxCodegen::ReleaseFunctionTable()
{
	m_parser.reset();

	return std::move(m_sourceFunction);
}

std::unique_ptr<CDataSection> NoxCodegen::ReleaseField()
{
	return std::move(m_dataSection);
}
