
#include "compileUnit.h"
#include "hook/noxCodegen.h"
#include "hook/cCResult.h"
#include "hook/functionTable.h"
#include "hook/noxFunction.h"
#include "buildEnvironment.h"
#include "portable/buildSourceFile.h"
#include "parse.h"
//#include "vector.h"
#include "hook/ast/astVector.h"
#include "hook/ast/astNode.h"
#include "common/utils/myDebug.h"
#include "common/utils/printUtil.h"
#include "common/utils/stringhelper.h"
#include "debug.h"

using namespace _StringHelper;

CompileUnit::CompileUnit()
	: TaskUnit()
{ }

CompileUnit::~CompileUnit()
{ }

void CompileUnit::compileImpl()
{
	auto toplevels = m_parser->read_toplevels();
	int nodeLength = toplevels->Length();
/*
    MY_PRINT() << "print ast";
    printAST(toplevels);
    MY_PRINT() << "print ast";
*/
	m_codegen->AttachParser(std::move(m_parser));
	for (int i = 0; i < nodeLength; ++i)
	{
		AstNode *v = reinterpret_cast<AstNode *>(toplevels->Get( i));

		m_codegen->EmitToplevel(v);
	}
    m_codegen->DetachParser();
}

std::unique_ptr<CCResult> CompileUnit::tryCompile()
{
	auto cResult = std::make_unique<CCResult>(*this);
	
	try
	{
        showBeforeCompile(*cResult);
		compileImpl();
		cResult->MakeResult(*m_codegen);
	}
	catch (std::logic_error &except)
	{
		cResult->PutError(except);
	}
	return cResult;
}

void CompileUnit::showBeforeCompile(CCResult &res)
{
    auto fn = m_parser->ThisFileName();

	PrintUtil::PrintMessage(PrintUtil::ConsoleColor::COLOR_GREY, "compile ", fn);
    res.PutSourceFile(fn);
}

void CompileUnit::PutSource(const BuildSourceFile &sourceFile, BuildEnvironment &env, int uniqId)
{
	m_parser = std::make_unique<CCParse>();

	m_parser->RegistPath(env.GetPath());
	m_parser->SetInitialFilename(sourceFile.Name());
	m_parser->InitializeUnit();

	m_codegen = std::make_unique<NoxCodegen>(uniqId);
    m_codegen->PutParams(env);
}

void CompileUnit::PutGPointerTable(std::shared_ptr<GPointerOffsetTable> &table)
{
	if (!m_codegen)
		MY_THROW() << "m_codegen == nullptr";

	m_codegen->PutGPointerTable(table);
}

void CompileUnit::Run()
{
	auto predict = std::async([this]()
	{
		return this->tryCompile();
	});

	m_compileOutput = std::make_unique<std::decay_t<decltype(predict)>>(std::move(predict));
}

std::unique_ptr<TaskResult> CompileUnit::GetResult()
{
	if (!m_compileOutput)
		MY_THROW() << stringFormat("%s:null output", __FUNCDNAME__);

	std::unique_ptr<CCResult> result = m_compileOutput->get();

	m_compileOutput.reset();
	return result;
}
