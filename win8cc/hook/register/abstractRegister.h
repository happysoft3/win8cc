
#ifndef ABSTRACT_REGISTER_H__
#define ABSTRACT_REGISTER_H__

#include <list>
#include <memory>
#include <string>

class NoxOpcode;
class NoxFunction;
class RegisterOutputTarget;

class AbstractRegister
{
private:
    int m_offset;
	int m_size;
    std::string m_regDbgName;

public:
    AbstractRegister(int value, int size = 4);
    virtual ~AbstractRegister();

protected:
    void putDebugName(const std::string &alias)
    {
        m_regDbgName = alias;
    }
	void setOffset(int value)
	{
		m_offset = value;
	}
    int getOffset() const;

private:
    virtual void releaseValue(RegisterOutputTarget &targ, int reqSize) = 0;

public:
    virtual std::unique_ptr<int> ConstValue() const {
        return nullptr;
    }
    bool IsSame(const AbstractRegister &r) const;
    virtual bool HasReference(const AbstractRegister &r) const {
        return IsSame(r);
    }
    const char *const DebugName() const
    {
        return m_regDbgName.empty() ? "(empty)" : m_regDbgName.c_str();
    }
	int Size() const
	{
		return m_size;
	}
    virtual void Address(RegisterOutputTarget &targ) = 0;
    virtual void Value(RegisterOutputTarget &targ);
    virtual void ConvValue(RegisterOutputTarget &targ, int requestSize);
    virtual AbstractRegister *Clone() const = 0;
};

#endif


