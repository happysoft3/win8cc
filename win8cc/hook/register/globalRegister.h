
#ifndef GLOBAL_REGISTER_H__
#define GLOBAL_REGISTER_H__

#include "abstractRegister.h"

class GlobalRegister : public AbstractRegister
{
private:
	std::string m_label;

public:
	GlobalRegister(const std::string &label, int off = 0);
	~GlobalRegister() override;

private:
	void Address(RegisterOutputTarget &targ) override;
    void releaseValue(RegisterOutputTarget &targ, int reqSize) override;
    AbstractRegister *Clone() const override;
};

#endif

