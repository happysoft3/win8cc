
#include "functionRegister.h"
#include "registerOutputTarget.h"
#include "../noxFunction.h"
#include "hook/noxOpcode.h"
#include "common/utils/mydebug.h"

FunctionRegister::FunctionRegister(const std::string &fn)
	: AbstractRegister(0)
{
	m_link = fn;
}

FunctionRegister::~FunctionRegister()
{ }

void FunctionRegister::Address(RegisterOutputTarget &targ)
{
	MY_THROW() << "function register has no attrib 'address'";
}

void FunctionRegister::releaseValue(RegisterOutputTarget &targ, int)
{
	targ.DispatchOpcode<NoxNativeOp>(NoxNativeOp::NativePush);
	targ.DispatchOpcode<NoxUserCallAddress>(m_link);
}

AbstractRegister *FunctionRegister::Clone() const
{
    return new FunctionRegister(*this);
}

