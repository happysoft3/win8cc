
#ifndef REGISTER_OUTPUT_TARGET_H__
#define REGISTER_OUTPUT_TARGET_H__

#include <memory>
#include <list>

class NoxFunction;
class AbstractRegister;
class Instruction;
class InstructionList;
class NoxOpcode;

class RegisterOutputTarget
{
private:
	std::unique_ptr<NoxFunction> m_outctx;
    std::unique_ptr<InstructionList> m_iFirstList;
    InstructionList *m_pICurList;
    bool m_showDebugMark;

public:
	RegisterOutputTarget();
	~RegisterOutputTarget();

private:
    void pushInst(std::unique_ptr<Instruction> inst);
    void emitInst();
    void pushOpcode(std::unique_ptr<NoxOpcode> opcode);

public:
    void ShowDebugPrint(bool show)
    {
        m_showDebugMark = show;
    }
    template <class Ty, class... Args>
    void MakeInstruction(Args&&... args)
    {
        pushInst(std::make_unique<Ty>(std::forward<Args>(args)...));
    }

    template <class Ty, class... Args>
    void DispatchOpcode(Args&&... args)
    {
        pushOpcode(std::make_unique<Ty>(std::forward<Args>(args)...));
    }

private:
    template <class Ty>
    static void pushStringStream(std::stringstream &ss, Ty &&val)
    {
        ss << val;
    }

    template <class Ty, class... Args>
    static void pushStringStream(std::stringstream &ss, Ty &&val, Args&&... args)
    {
        pushStringStream(ss, val);
        pushStringStream(ss, std::forward<Args>(args)...);
    }

public:
    template <class... Args>
    void PutDebugPrettyname(Args&&... args)
    {
        if (!m_showDebugMark)
            return;

        std::stringstream ss;

        pushStringStream(ss, std::forward<Args>(args)...);
        DispatchOpcode<NoxOpDebug>(ss.str());
    }

    std::unique_ptr<NoxFunction> Detach();

	void Attach(std::unique_ptr<NoxFunction> ctx)
	{
		m_outctx = std::move(ctx);
	}

	NoxFunction *GetCtx() const
	{
		return m_outctx.get();
	}
};

#endif

