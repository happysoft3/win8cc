
#include "register.h"
#include "registerOutputTarget.h"
#include "../noxFunction.h"
#include "../noxOpcode.h"
#include "common/utils/mydebug.h"

Register::Register(int offset)
    : AbstractRegister(offset)
{ }

Register::Register(int offset, int size, const std::string &debugName)
	: AbstractRegister(offset, size)
{
    if (debugName.size())
        putDebugName(debugName);
}

Register::~Register()
{ }

//주소를 참조하는 경우, 그대로 내보냅니다(아마..)
void Register::Address(RegisterOutputTarget &targ)
{
    targ.DispatchOpcode<NoxNativePush>(getOffset());          //네이티브 스크립트 큐에 삽입
}

//레지스터로 부터, 값을 꺼내와야 합니다
void Register::releaseValue(RegisterOutputTarget &targ, int reqSize)
{
    if (reqSize != Size())
        MY_THROW() << "register size mismatch";

    auto bIterator = m_loadvalMap.find(Size());

    if (bIterator != m_loadvalMap.cend())
    {
        targ.DispatchOpcode<NoxBuiltinCall>(static_cast<NoxBuiltinCall::BuiltinsFn::Class>(bIterator->second));
        return;
    }
    //scrpush offset
    //builtcall getmem
    targ.DispatchOpcode<NoxNativePush>(getOffset());       //scrpush offset
    targ.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::LoadGetter(Size()));
}

AbstractRegister *Register::Clone() const
{
    return new Register(*this);
}

void Register::PutValueMethod(int size, int bid)
{
    m_loadvalMap[size] = bid;
}
