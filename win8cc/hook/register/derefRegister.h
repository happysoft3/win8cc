
#ifndef DEREF_REGISTER_H__
#define DEREF_REGISTER_H__

#include "abstractRegister.h"

class DerefRegister : public AbstractRegister
{
private:
    std::unique_ptr<AbstractRegister> m_regTarget;
    std::unique_ptr<AbstractRegister> m_adderTarget;
    //std::unique_ptr<AbstractRegister> m_coreAdder;
    std::unique_ptr<AbstractRegister> m_multiply;

public:
	explicit DerefRegister(const AbstractRegister &target);
    explicit DerefRegister(const AbstractRegister &target, int offset);
	explicit DerefRegister(const AbstractRegister &target, const AbstractRegister &adder);
    explicit DerefRegister(const AbstractRegister &target, const AbstractRegister &adder, int multiply);
    explicit DerefRegister(int multiply, const AbstractRegister &target);
    ~DerefRegister() override;

private:
    void Address(RegisterOutputTarget &targ) override;
    void releaseValue(RegisterOutputTarget &targ, int reqSize) override;
    bool HasReference(const AbstractRegister &r) const override;

public:
    template <class... Args>
    static std::shared_ptr<AbstractRegister> Make(Args&&... args)
    {
        return std::make_shared<DerefRegister>(std::forward<Args>(args)...);
    }

private:
    AbstractRegister *Clone() const override;
};

#endif

