
#include "abstractRegister.h"
#include "registerOutputTarget.h"
#include "../noxFunction.h"
#include "../noxOpcode.h"

AbstractRegister::AbstractRegister(int value, int size)
{
	m_offset = value;
	m_size = size;
}

AbstractRegister::~AbstractRegister()
{ }

int AbstractRegister::getOffset() const
{
	return m_offset;
}

bool AbstractRegister::IsSame(const AbstractRegister &r) const
{
    if (m_offset != r.m_offset)
        return false;
    if (m_size != r.m_size)
        return false;
    if (m_regDbgName.empty() || r.m_regDbgName.empty())
        return false;

    return m_regDbgName == r.m_regDbgName;
}

void AbstractRegister::Value(RegisterOutputTarget &targ)
{
    releaseValue(targ, m_size);
}

void AbstractRegister::ConvValue(RegisterOutputTarget &targ, int requestSize)
{
    switch (requestSize)
    {
    case 1: case 2: case 4:
        releaseValue(targ, requestSize);
        break;

    default:
        throw std::exception("undefined size");
    }
}
