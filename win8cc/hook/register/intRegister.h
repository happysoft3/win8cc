
#ifndef INT_REGISTER_H__
#define INT_REGISTER_H__

#include "abstractRegister.h"

class IntRegister : public AbstractRegister
{
public:
    explicit IntRegister(int value);
    ~IntRegister() override;

private:
    void Address(RegisterOutputTarget &targ) override;
    void releaseValue(RegisterOutputTarget &targ, int reqSize) override;

public:
	void SetValue(int value)
	{
		setOffset(value);
	}

    static std::shared_ptr<AbstractRegister> Make(int value)
    {
        return std::make_shared<IntRegister>(value);
    }

private:
    AbstractRegister *Clone() const override;
    std::unique_ptr<int> ConstValue() const override;
};

#endif

