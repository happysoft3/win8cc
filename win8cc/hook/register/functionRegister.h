
#ifndef FUNCTION_REGISTER_H__
#define FUNCTION_REGISTER_H__

#include "abstractRegister.h"

class FunctionRegister : public AbstractRegister
{
private:
	std::string m_link;

public:
	FunctionRegister(const std::string &fn);
	~FunctionRegister() override;

private:
	void Address(RegisterOutputTarget &targ) override;
    void releaseValue(RegisterOutputTarget &targ, int) override;
    AbstractRegister *Clone() const override;
};

#endif

