
#ifndef REGISTER_H__
#define REGISTER_H__

#include "abstractRegister.h"
#include <map>

class Register : public AbstractRegister
{
private:
    std::map<int, int> m_loadvalMap;

public:
    explicit Register(int offset);
    explicit Register(int offset, int size, const std::string &dbgName = { });
    ~Register() override;

private:
    void Address(RegisterOutputTarget &targ) override;
    void releaseValue(RegisterOutputTarget &targ, int reqSize) override;
    AbstractRegister *Clone() const override;

public:
    void PutValueMethod(int size, int bid);
};

#endif

