
#include "globalRegister.h"
#include "registerOutputTarget.h"
#include "../noxFunction.h"
#include "hook/noxOpcode.h"
#include "common/utils/mydebug.h"

GlobalRegister::GlobalRegister(const std::string &label, int off)
	: AbstractRegister(off)
{
	m_label = label;
    if (label.empty())
    {
        MY_THROW() << "removeme";
    }
}

GlobalRegister::~GlobalRegister()
{ }

void GlobalRegister::Address(RegisterOutputTarget &targ)
{
	//아예 실제 global field base 주소까지 더해주면 좋을 것 같다 -> 그렇게 하도록 설계를 함
	targ.DispatchOpcode<NoxNativeOp>(NoxNativeOp::NativePush);	//오프셋을 인자로 받고,
	targ.DispatchOpcode<NoxOpGlobalLoader>(m_label, getOffset());	//글로벌 주소를 리턴합니다
	targ.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_LoadGlobalAddress);
}

void GlobalRegister::releaseValue(RegisterOutputTarget &targ, int reqSize)
{
	targ.DispatchOpcode<NoxNativeOp>(NoxNativeOp::NativePush);
	targ.DispatchOpcode<NoxOpGlobalLoader>(m_label, getOffset());	//마찬가지
	targ.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_LoadGlobalValue);
    if (reqSize != sizeof(int))
        MY_PRINT() << __FUNCSIG__ << " got size " << reqSize;
}

AbstractRegister *GlobalRegister::Clone() const
{
    return new GlobalRegister(*this);
}
