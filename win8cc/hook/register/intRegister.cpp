
#include "intRegister.h"
#include "registerOutputTarget.h"
#include "../noxFunction.h"
#include "../noxOpcode.h"
#include "common/utils/mydebug.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

IntRegister::IntRegister(int value)
    : AbstractRegister(value)
{
    putDebugName(stringFormat("%d", value));
}

IntRegister::~IntRegister()
{ }

//push 50   //value
//mov [00507250], eax
void IntRegister::Address(RegisterOutputTarget &targ)
{
    MY_THROW() << "invalid address";
}

void IntRegister::releaseValue(RegisterOutputTarget &targ, int reqSize)
{
    //push 50
    //mov eax, 00751004
	int val = getOffset(), mask = 0;

	switch (reqSize)
	{
	case sizeof(char) : mask = 0xff; break;
	case sizeof(short) : mask = 0xffff; break;
	case sizeof(int) : mask = 0xffffffff; break;
	default:
		MY_THROW() << "error intreg::value";
	}
    targ.DispatchOpcode<NoxNativePush>(val & mask);
}

AbstractRegister *IntRegister::Clone() const
{
    return new IntRegister(*this);
}

std::unique_ptr<int> IntRegister::ConstValue() const
{
    return std::make_unique<int>(getOffset());
}
