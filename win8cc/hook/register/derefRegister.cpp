
#include "derefRegister.h"
#include "registerOutputTarget.h"
#include "intRegister.h"
#include "../noxFunction.h"
#include "../noxOpcode.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

DerefRegister::DerefRegister(const AbstractRegister &target)
	: AbstractRegister(0), m_regTarget(target.Clone())
{
    putDebugName(stringFormat("[%s]", target.DebugName()));
}

DerefRegister::DerefRegister(const AbstractRegister &target, int offset)
    : AbstractRegister(0), m_regTarget(target.Clone())
{
    if (offset)
        m_adderTarget = std::make_unique<IntRegister>(offset);
    else
        m_adderTarget = nullptr;
    putDebugName(stringFormat("[%s + %d]", target.DebugName(), offset));
}

DerefRegister::DerefRegister(const AbstractRegister &target, const AbstractRegister &adder)
	: AbstractRegister(0), m_regTarget(target.Clone()), m_adderTarget(adder.Clone())
{
    putDebugName(stringFormat("[%s + %s]", target.DebugName(), adder.DebugName()));
}

DerefRegister::DerefRegister(const AbstractRegister &target, const AbstractRegister &adder, int multiply)
    : AbstractRegister(0), m_regTarget(target.Clone()), m_adderTarget(adder.Clone())
{
    m_multiply = std::make_unique<IntRegister>(multiply);
    putDebugName(stringFormat("[%s+%s*%d]", target.DebugName(), adder.DebugName(), multiply));
}

DerefRegister::DerefRegister(int multiply, const AbstractRegister &target)
    : AbstractRegister(0), m_regTarget(target.Clone())
{
    m_multiply = std::make_unique<IntRegister>(multiply);
    putDebugName(stringFormat("[%s*%d]", target.DebugName(), multiply));
}

DerefRegister::~DerefRegister()
{ }

//무조건 4바이트
void DerefRegister::Address(RegisterOutputTarget &targ)
{
	m_regTarget->ConvValue(targ, Size());

	if ((!m_adderTarget) && (!m_multiply))
		return;

	//mov "[ebp+04]", ecx
    if (m_adderTarget)
	    m_adderTarget->ConvValue(targ, Size());
    if (m_multiply)
    {
        m_multiply->ConvValue(targ, Size());
        targ.DispatchOpcode<NoxNativeOp>(NoxNativeOp::NativeMultiplyInt);
    }
    if (m_adderTarget)
	    targ.DispatchOpcode<NoxNativeOp>(NoxNativeOp::NativeAddInt);
}

//무조건 4바이트
void DerefRegister::releaseValue(RegisterOutputTarget &targ, int reqSize)
{
    //mov eax, "[00507250]"

    m_regTarget->ConvValue(targ, Size());   //00507250 아니면, eax 주소 같은 것이 올것입니다

    if ((!m_adderTarget) && (!m_multiply))
    {
        targ.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::LoadGetter(Size()));       //getMem(reg)
        return;
    }

    //mov eax, "[eax+04]"
    if (m_adderTarget)
        m_adderTarget->ConvValue(targ, Size());     //마찬가지...
    if (m_multiply)
    {
        m_multiply->ConvValue(targ, Size());
        targ.DispatchOpcode<NoxNativeOp>(NoxNativeOp::NativeMultiplyInt);
    }
    if (m_adderTarget)
        targ.DispatchOpcode<NoxNativeOp>(NoxNativeOp::NativeAddInt);
    targ.DispatchOpcode<NoxBuiltinCall>(NoxBuiltinCall::LoadGetter(Size()));       //getMem(reg)
}

bool DerefRegister::HasReference(const AbstractRegister &r) const
{
    if (m_regTarget)
    {
        if (m_regTarget->HasReference(r))
            return true;
    }
    if (m_adderTarget)
    {
        if (m_adderTarget->HasReference(r))
            return true;
    }
    if (m_multiply)
    {
        if (m_multiply->HasReference(r))
            return true;
    }
    return false;
}

AbstractRegister *DerefRegister::Clone() const
{
    DerefRegister *clone = new DerefRegister(*m_regTarget);

    clone->putDebugName(DebugName());
    clone->m_adderTarget = m_adderTarget ? std::unique_ptr<AbstractRegister>(m_adderTarget->Clone()) : nullptr;
    clone->m_multiply = m_multiply ? std::unique_ptr<AbstractRegister>(m_multiply->Clone()) : nullptr;
    return clone;
}
