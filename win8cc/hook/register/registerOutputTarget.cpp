
#include "registerOutputTarget.h"
#include "hook/instruction/instructionList.h"
#include "hook/instruction/instruction.h"
#include "../noxOpcode.h"
#include "abstractRegister.h"
#include "../noxFunction.h"
#include "common/utils/mydebug.h"

RegisterOutputTarget::RegisterOutputTarget()
{
    m_showDebugMark = false;
    m_iFirstList = std::make_unique<InstructionList>();
    m_pICurList = m_iFirstList.get();
}

RegisterOutputTarget::~RegisterOutputTarget()
{ }

void RegisterOutputTarget::pushInst(std::unique_ptr<Instruction> inst)
{
    //m_instList.push_back(std::move(inst));  //나중에 다 구현되면, 이 코드는 지우세요

    m_pICurList->CanIgnore(inst);

    if (!inst)
        return;

    //새구현 시작
    if (inst->IsBranch())
    {
        m_pICurList = m_pICurList->CreateBranch(std::move(inst));
        return;
    }
    do
    {
        m_pICurList->PutInst(std::move(inst));
        if (!inst)  //OK
            return;

        auto parent = m_iFirstList.get(); //m_pICurList->GetParent();

        if (!parent)
            MY_THROW() << "internal error";
        m_pICurList = parent;
    }
    while (true);

    //새구현 끝
}

void RegisterOutputTarget::emitInst()
{
    m_iFirstList->ConvertInst();
    m_iFirstList->EmitInstAll(*this);
#if 0
    std::unique_ptr<Instruction> inst;

    while (m_instList.size())
    {
        inst = std::move(m_instList.front());

        m_instList.pop_front();
        inst->emitInst(*this);
    }
#endif
}

void RegisterOutputTarget::pushOpcode(std::unique_ptr<NoxOpcode> opcode)
{
    if (m_outctx)
        m_outctx->PushOp(std::move(opcode));
}

std::unique_ptr<NoxFunction> RegisterOutputTarget::Detach()
{
    if (m_outctx)
    {
        emitInst();
        return std::move(m_outctx);
    }
    return{ };
}


