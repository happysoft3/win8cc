
#include "noxFunction.h"
#include "noxOpcode.h"
#include "link/cclink.h"
#include "common/utils/myDebug.h"
#include "common/utils/randomString.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

NoxFunction::NoxFunction(const std::string &fn, bool isStatic)
	: ScrOutStream()
{
    m_name = fn;
	m_uniqId = std::make_shared<CCLink>();
	m_isStatic = isStatic;
	m_latestOpcode = nullptr;
	m_hasReturn = false;
	m_nativeArgCount = 0;
}

NoxFunction::~NoxFunction()
{ }

void NoxFunction::emitNativeVarInfo(ScrOutStream &scrOs)
{
	scrOs.Write(m_hasReturn ? 1 : 0);
	scrOs.Write(m_nativeArgCount);
	scrOs << "SYMB";
	scrOs.Write(m_nativeVarInfo.size());
	scrOs.Write(0);
	for (const auto &n : m_nativeVarInfo)
		scrOs.Write(n);
}

void NoxFunction::preEmitCode(int &wholeLength)
{
	int off = 0;

	for (auto &op : m_opcodes)
	{
		op->MakeOffsetMark(off);
		off += op->Length();
	}
	wholeLength = off;
}

void NoxFunction::emitCode(ScrOutStream &scrOs)
{
	int curOff = 0, fieldLength = 0;

	preEmitCode(fieldLength);
	scrOs.Write(fieldLength * sizeof(int));	//여기에서 코드 필드 크기를 내보냅니다
	for (auto &op : m_opcodes)
	{
		if (isDebugModeOn(scrOs))
		{
            if (op->Length())
            {
                scrOs << stringFormat("%d: ", curOff);
                scrOs << op->ToString();
            }
			op->Emit(scrOs);
			scrOs << ScrOutStream::ENDL;
		}
		else if (op->Length())
			op->Emit(scrOs);
        curOff += op->Length();
	}
}

void NoxFunction::emitFunctionIdentifier(ScrOutStream &scrOs)
{
    scrOs << "FUNC";
    scrOs.Write(m_name.length());
    scrOs << m_name;
    scrOs << ScrOutStream::ENDL;
}

void NoxFunction::onReleaseStream(ScrOutStream &scrOs)
{
    emitFunctionIdentifier(scrOs);
	emitNativeVarInfo(scrOs);
	scrOs << "DATA";
	scrOs << ScrOutStream::ENDL;
	emitCode(scrOs);
	scrOs << ScrOutStream::ENDL;
}

const char *NoxFunction::Name() const
{
    if (m_name.empty())
        return nullptr;

    return m_name.c_str();
}

void NoxFunction::ChangeName(const std::string &name)
{
    m_name = name;
}

void NoxFunction::PushOp(std::unique_ptr<NoxOpcode> opcode)
{
	m_latestOpcode = opcode.get();
    m_opcodes.push_back(std::move(opcode));
}

void NoxFunction::PushFrontOp(std::unique_ptr<NoxOpcode> opcode)
{
    if (m_opcodes.empty())
        m_latestOpcode = opcode.get();
    m_opcodes.push_front(std::move(opcode));
}

std::unique_ptr<NoxOpcode> NoxFunction::PopOp()
{
	if (m_opcodes.empty())
	{
		if (m_latestOpcode)
			m_latestOpcode = nullptr;
		return {};
	}

	std::unique_ptr<NoxOpcode> op;

	op.swap(m_opcodes.front());
	m_opcodes.pop_front();
	return op;
}

void NoxFunction::SetUniqueId(int uniqId)
{
	if (m_uniqId->Linked())
		throw std::logic_error(stringFormat("function '%s' linked twice", m_name));

	m_uniqId->MakeLink(uniqId);
}

std::shared_ptr<CCLink> NoxFunction::UniqueId() const
{
	return m_uniqId;
}

void NoxFunction::PutLabel(const std::string &label)
{
	if (m_labels.find(label) != m_labels.cend())
		throw std::logic_error(stringFormat("label '%s' already defined", label));

	if (!m_latestOpcode)
		MY_THROW() << "no opcode";

	m_labels[label] = m_latestOpcode->MarkCodePos();
}

std::weak_ptr<CCLink> NoxFunction::GetLabelAddress(const std::string &label) const
{
	auto labelPick = m_labels.find(label);

	if (labelPick == m_labels.cend())
		MY_THROW() << stringFormat("function '%s' has no label '%s'", m_name, label);

	return labelPick->second;
}

void NoxFunction::OpcodePreorder(std::function<void(NoxOpcode&)> &&fn)
{
	for (auto &op : m_opcodes)
		fn(*op);
}

void NoxFunction::generateStaticTag()
{
	std::string rndgen = "XXXXXXXX";

	RandomString::Generate(rndgen);
	m_staticTag = stringFormat("--%s%s", rndgen, m_name);
}

const char *const NoxFunction::StaticTag()
{
	if (m_staticTag.empty())
		generateStaticTag();

	return m_staticTag.c_str();
}

void NoxFunction::PushNativeVar(int size, bool isArg)
{
	if (isArg) ++m_nativeArgCount;
	m_nativeVarInfo.push_back(isArg ? 1 : size);
}

