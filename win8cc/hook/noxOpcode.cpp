
#include "noxOpcode.h"
#include "functionTable.h"
#include "noxFunction.h"
#include "cdata/cDataSection.h"
#include "cdata/cDataLabel.h"
#include "link/codeOffset.h"
#include "common/utils/myDebug.h"
#include "hook/output/scrOutStream.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

NoxOpcode::NoxOpcode(NoxOpType ty)
{
	m_opType = ty;
}

NoxOpcode::~NoxOpcode()
{ }

std::weak_ptr<CCLink> NoxOpcode::MarkCodePos()
{
	if (!m_codepos)
		m_codepos = std::make_shared<CodeOffset>();
	
	return m_codepos;
}

void NoxOpcode::MakeOffsetMark(int off)
{
	if (m_codepos)
		m_codepos->Change(off + Length());
}

NoxNativePush::NoxNativePush(int value)
    : NoxOpcode(NoxOpType::NativePush)
{
	m_pushValue = value;
}

NoxNativePush::~NoxNativePush()
{ }

void NoxNativePush::Emit(ScrOutStream &scrOs)
{
	scrOs.Write(static_cast<int>(NoxNativeOp::NativePush));
	scrOs.Write(m_pushValue);
}

NoxNativeOp::NoxNativeOp(NoxNativeOp::OpID opid)
    : NoxOpcode(NoxOpType::NativeOperator)
{
    m_opId = opid;
}

NoxNativeOp::~NoxNativeOp()
{ }

const char *const NoxNativeOp::nativeOperatorName() const
{
	switch (m_opId)
	{
#define DEF_NATIVE_OP(id, _nop) case id: return #id;
#include "hook/ident/nativeOperator.inc"
#undef DEF_NATIVE_OP
	default:
		return "unknown";
	}
}

void NoxNativeOp::Emit(ScrOutStream &scrOs)
{
	if (scrOs.UseDebug())
		scrOs << stringFormat("-%s-", nativeOperatorName());
	scrOs.Write(static_cast<int>(m_opId));
}

NoxBuiltinCall::NoxBuiltinCall(NoxBuiltinCall::BuiltinsFn::Class fnId)
    : NoxOpcode(NoxOpType::BuiltinCall)
{
    m_fnId = fnId;
}

NoxBuiltinCall::~NoxBuiltinCall()
{ }

NoxBuiltinCall::BuiltinsFn::Class NoxBuiltinCall::LoadGetter(int size)
{
	switch (size)
	{
	case sizeof(char): return BuiltinsFn::BUILTINS_GetMemoryByte;
	case sizeof(short) : return BuiltinsFn::BUILTINS_GetMemoryWord;
	case sizeof(int) : return BuiltinsFn::BUILTINS_GetMemory;
	default: return BuiltinsFn::BUILTINS_Invalid;
	}
}

NoxBuiltinCall::BuiltinsFn::Class NoxBuiltinCall::LoadSetter(int size)
{
	switch (size)
	{
	case sizeof(char) : return BuiltinsFn::BUILTINS_SetMemoryByte;
	case sizeof(short) : return BuiltinsFn::BUILTINS_SetMemoryWord;
	case sizeof(int) : return BuiltinsFn::BUILTINS_SetMemory;
	default: return BuiltinsFn::BUILTINS_Invalid;
	}
}

NoxBuiltinCall::BuiltinsFn::Class NoxBuiltinCall::LoadSetterEx(int size, bool needSigned)
{
	switch (size)
	{
	case sizeof(char) : return needSigned ? BuiltinsFn::BUILTINS_SetMemorySByte : BuiltinsFn::BUILTINS_SetMemoryZByte;
	case sizeof(short) : return needSigned ? BuiltinsFn::BUILTINS_SetMemorySWord : BuiltinsFn::BUILTINS_SetMemoryZWord;
    case sizeof(int) : return BuiltinsFn::BUILTINS_SetMemory; //return needSigned ? BuiltinsFn::BUILTINS_SetMemoryS : BuiltinsFn::BUILTINS_SetMemoryZ;
	default: return BuiltinsFn::BUILTINS_Invalid;
	}
}

const char *NoxBuiltinCall::builtinsName() const
{
	switch (m_fnId)
	{
#define DEF_IMPL_BUILTINS_FUNCTION(id, _discard) case BuiltinsFn::id: return #id;
#include "hook/ident/implBuiltinsFunctions.inc"
#undef DEF_IMPL_BUILTINS_FUNCTION
	default:
		return "unknown";
	}
}

int NoxBuiltinCall::builtinsExecuteBase() const
{
    switch (m_fnId)
    {
#define DEF_IMPL_BUILTINS_FUNCTION(id, offset) case BuiltinsFn::id: return offset;
#include "hook/ident/implBuiltinsFunctions.inc"
#undef DEF_IMPL_BUILTINS_FUNCTION
    default:
        return 0;
    }
}

void NoxBuiltinCall::Emit(ScrOutStream &scrOs)
{
	if (scrOs.UseDebug())
		scrOs << stringFormat("-%s-", builtinsName());
	scrOs.Write(69);

    constexpr int scr_builtins_base = 0x5c308c;
    int base = builtinsExecuteBase();

    if (!base)
        throw std::exception("execbase is null");
    if (base % 4)
        throw std::exception("execbase must be divided by 4");
	scrOs.Write((base - scr_builtins_base) / 4);
}

NoxNeedLink::NoxNeedLink(const std::string &tag, NoxOpType type)
	: NoxOpcode(type)
{
	m_labelTag = tag;
}

NoxNeedLink::~NoxNeedLink()
{ }

void NoxNeedLink::SetLinkResult(std::shared_ptr<CCLink> &link)
{
	if (m_linkResult)
		MY_THROW() << "opcode linked twice";

	m_linkResult = link;
}

void NoxNeedLink::Emit(ScrOutStream &scrOs)
{
	scrOs.Write(LinkedValue());
}

int NoxNeedLink::LinkedValue() const
{
	for (;;)
	{
		if (!m_linkResult)
			break;
		if (!m_linkResult->Linked())
			break;

		return m_linkResult->Link();
	}
	MY_THROW() << "unlinked opcode";
}

NoxUserCallBase::NoxUserCallBase(const std::string &tag, NoxOpType type)
    : NoxNeedLink(tag, type)
{ }

NoxUserCallBase::~NoxUserCallBase()
{ }

void NoxUserCallBase::staticLink(NoxFunction *target)
{
    if (!target)
        return;

    if (target->IsStatic())	//static 함수만 링크합니다
        SetLinkResult(target->UniqueId());
}

void NoxUserCallBase::globalLink(NoxFunction *target)
{
    if (!target)
        MY_THROW() << stringFormat("unresolved external symbol '%s'", Link());
    SetLinkResult(target->UniqueId());
}

void NoxUserCallBase::DoLinking(FunctionTable *fnTable, CDataSection *data, NoxFunction *fn)
{
    if (Linked())
        return;

    NoxFunction *invokeTarget = fnTable->Retrieve(Link(), true);

    if (fn)
    {
        staticLink(invokeTarget);
        return;
    }
    globalLink(invokeTarget);
}

NoxUserCall::NoxUserCall(const std::string &fn)
	: NoxUserCallBase(fn, NoxOpType::UserCall)
{ }

NoxUserCall::~NoxUserCall()
{ }

void NoxUserCall::Emit(ScrOutStream &scrOs)
{
	scrOs.Write(70);
	NoxNeedLink::Emit(scrOs);
}

NoxUserCallAddress::NoxUserCallAddress(const std::string &fn)
	: NoxUserCallBase(fn, NoxOpType::LoadUserCall)
{ }

NoxUserCallAddress::~NoxUserCallAddress()
{ }

NoxOpJump::NoxOpJump(const std::string &label, NoxOpJump::Conditional cond)
	:NoxNeedLink(label, NoxOpType::Jump)
{
	m_jmpCond = cond;
	printJumpDebug();
}

NoxOpJump::~NoxOpJump()
{ }

void NoxOpJump::printJumpDebug()
{
	auto jmpClassify = [](int ty)->const char*
	{
		switch (ty)
		{
		case Conditional::Pass: return "jmp";
		case Conditional::JumpEqual: return "je";
		case Conditional::JumpNotEqual: return "jne";
		default: return "error";
		}
	};
	m_jmpDebug = stringFormat("jump:%s:%s", jmpClassify(m_jmpCond), Link());
}

int NoxOpJump::Length() const
{
    switch (m_jmpCond)
    {
    case Conditional::JumpNotEqual:
    case Conditional::JumpEqual: return 3;
    case Conditional::ComputedJump:
    case Conditional::Pass:
    default:
        return 2;
    }
}

void NoxOpJump::Emit(ScrOutStream &scrOs)
{
	switch (m_jmpCond)
	{
    case Conditional::Pass:
        scrOs.Write(static_cast<int>(NoxNativeOp::NativeJump));
        break;     //op = NoxNativeOp::NativeJump; break;

    case Conditional::JumpEqual: // op = NoxNativeOp::NativeJumpIfNot; break;  //19, Nov 2022 21:43 반대로 바꿈
    {
        std::unique_ptr<NoxOpcode> je = std::make_unique<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_JumpIfNot);

        je->Emit(scrOs);
        break;
    }
    case Conditional::JumpNotEqual:  // op = NoxNativeOp::NativeJumpIf; break;  //19, Nov 2022 21:43 반대로 바꿈
    {
        std::unique_ptr<NoxOpcode> jne = std::make_unique<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_JumpIf);

        jne->Emit(scrOs);
        break;
    }
    case Conditional::ComputedJump:
    {
        std::unique_ptr<NoxOpcode> cjmp = std::make_unique<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_ComputedGoto);

        cjmp->Emit(scrOs);
        return; //break;
    }
	default:
        MY_THROW() << "unknown jump";
	}
	scrOs.Write(LinkedValue());
}

const char *const NoxOpJump::ToString() const
{
	return m_jmpDebug.c_str();
}

void NoxOpJump::DoLinking(FunctionTable *fnTable, CDataSection *data, NoxFunction *fn)
{
    auto labelName = Link();
    auto destinationLabel = fn->GetLabelAddress(labelName).lock();

    if (!destinationLabel)
        MY_THROW() << "jump error " << labelName;
    SetLinkResult(destinationLabel);
}

NoxOpGlobalLoader::NoxOpGlobalLoader(const std::string &link, int offset)
	: NoxNeedLink(link, NoxOpType::Global)
{
    m_staticSymb = nullptr;
}

NoxOpGlobalLoader::~NoxOpGlobalLoader()
{ }

void NoxOpGlobalLoader::SetLinkResult(std::shared_ptr<CCLink> &link)
{
	link->Change(link->Link() + m_offset);
	NoxNeedLink::SetLinkResult(link);
}

void NoxOpGlobalLoader::globalLink(CDataSection *global)
{
    auto glabel = global->Retrieve(Link());

    if (!glabel)
        MY_THROW() << stringFormat("glabel '%s' is missing", Link());

    SetLinkResult(glabel->TryLoadOffset());
}

void NoxOpGlobalLoader::staticLink(CDataSection *scope)
{
    auto slabel = scope->Retrieve(Link());

    if (slabel)
    {
        if (slabel->IsGlobal())
            return;

        m_staticSymb = slabel;
    }
}

void NoxOpGlobalLoader::DoLinking(FunctionTable *fnTable, CDataSection *data, NoxFunction *fn)
{
    if (!fn)
    {
        if (m_staticSymb)
        {
            SetLinkResult(m_staticSymb->TryLoadOffset());
            return;
        }
        globalLink(data);
        return;
    }
    staticLink(data);
}

NoxOpDebug::NoxOpDebug(const std::string &dbgString)
    : NoxOpcode(NoxOpType::Debug)
{
    m_dbgString = dbgString;
}

NoxOpDebug::~NoxOpDebug()
{ }

void NoxOpDebug::Emit(ScrOutStream &scrOs)
{
    if (scrOs.UseDebug())
    {
        scrOs << stringFormat("  //%s", m_dbgString);
        scrOs << ScrOutStream::ENDL;
    }
}

