
#include "noxOpGenerator.h"
#include "noxOpcode.h"
#include "hook/instruction/instruction.h"
#include "register/register.h"
#include "register/intRegister.h"
#include "register/derefRegister.h"
#include "register/functionRegister.h"
#include "register/globalRegister.h"
#include "register/registerOutputTarget.h"
#include "noxFunction.h"
#include "buildEnvironment.h"
#include "8cc.h"
#include "coreExecutor/coreRegisterDef.h"
#include "common/utils/mydebug.h"

#include <sstream>

using op_reg_ty = std::shared_ptr<AbstractRegister>;
using fn_ret_reg_ty = AbstractRegister &;

NoxOpGenerator::NoxOpGenerator()
{
	m_regOutputTarget = std::make_unique<RegisterOutputTarget>();

	m_regEspVector = initRegisters(CORE_REG_ESP, "esp");
	m_regEbpVector = initRegisters(CORE_REG_EBP, "ebp");
	m_regEaxVector = initRegisters(CORE_REG_EAX, "eax");
	m_regEbxVector = initRegisters(CORE_REG_EBX, "ebx");
	m_regEcxVector = initRegisters(CORE_REG_ECX, "ecx");
	m_regEdxVector = initRegisters(CORE_REG_EDX, "edx");
	m_esiRegs = initRegisters(CORE_REG_ESI, "esi");
	m_ediRegs = initRegisters(CORE_REG_EDI, "edi");
    
    m_regCmpResult = std::make_unique<Register>(CORE_REG_FLAG);
    myHack();
}

NoxOpGenerator::~NoxOpGenerator()
{ }

void NoxOpGenerator::myHack()
{
    ///도무지 안되서, 임시조치 입니다//
    //완벽한 방법을 찾으면 없애십시오//
    Register *pEbp = dynamic_cast<Register *>(&EBP());

    if (pEbp)
        pEbp->PutValueMethod(sizeof(int), NoxBuiltinCall::BuiltinsFn::BUILTINS_EBP);

    Register *pEax = dynamic_cast<Register *>(&EAX());

    if (pEax)
        pEax->PutValueMethod(sizeof(int), NoxBuiltinCall::BuiltinsFn::BUILTINS_EAX);

    Register *pEcx = dynamic_cast<Register *>(&ECX());

    if (pEcx)
        pEcx->PutValueMethod(sizeof(int), NoxBuiltinCall::BuiltinsFn::BUILTINS_ECX);
}

std::vector<std::unique_ptr<AbstractRegister>> NoxOpGenerator::initRegisters(int baseOffset, const std::string &dbgName)
{
	std::vector<std::unique_ptr<AbstractRegister>> regis; // = { nullptr, nullptr, nullptr };

	regis.push_back(std::make_unique<Register>(baseOffset, sizeof(char), dbgName));
	regis.push_back(std::make_unique<Register>(baseOffset, sizeof(short), dbgName));
	regis.push_back(std::make_unique<Register>(baseOffset, sizeof(int), dbgName));
	return regis;
}

fn_ret_reg_ty NoxOpGenerator::fetchRegist(std::vector<std::unique_ptr<AbstractRegister>> &regs, int align) const
{
	switch (align)
	{
	case 1: case 2: case 4:
		break;

	default:
		MY_THROW() << "invalid align";
	}
	return *regs[align >> 1];
}

fn_ret_reg_ty NoxOpGenerator::loadREG(int order, int align)
{
	switch (order)
	{
	case 0: return EDI(align);
	case 1: return ESI(align);
	case 2: return EDX(align);
	case 3: return ECX(align);
	case 4:
	case 5:
	default: MY_THROW() << "out of range from register pool";
	}
    return EAX();
}

//AbstractRegister &NoxOpGenerator::loadFunctionReg(const std::string &link)
//{
//	m_funcLinkReg = std::make_unique<FunctionRegister>(link);
//
//	return *m_funcLinkReg;
//}

//AbstractRegister &NoxOpGenerator::loadGlobalReg(const std::string &vTag, int off)
//{
//	m_globalLinkReg = std::make_unique<GlobalRegister>(vTag, off);
//
//	return *m_globalLinkReg;
//}

void NoxOpGenerator::attachFunction(std::unique_ptr<NoxFunction> boundary)
{
    if (!boundary)
        return;

	m_regOutputTarget->Attach(std::move(boundary));
}

std::unique_ptr<NoxFunction> NoxOpGenerator::detachFunction()
{
	auto detach = m_regOutputTarget->Detach();

    if (!detach)
        MY_THROW() << "has no attribution: detach function";
	return detach;
}

NoxOpGenerator::binary_oper_ty NoxOpGenerator::loadIntCompare(int compareKind, bool ignoreSignedBit) const
{
	switch (compareKind)
	{
	case '<': return ignoreSignedBit ? &NoxOpGenerator::genOpCompareUnsignedB : &NoxOpGenerator::genOpCompareLess;
	case OP_EQ: return &NoxOpGenerator::genOpCompareEqual;
	case OP_LE: return ignoreSignedBit ? &NoxOpGenerator::genOpCompareUnsignedBEqual : &NoxOpGenerator::genOpCompareLessEqual;
	case OP_NE: return &NoxOpGenerator::genOpCompareNotEqual;
	}
	return nullptr;
}

NoxOpGenerator::binary_oper_ty NoxOpGenerator::loadFloatCompare(int compareKind) const
{
	switch (compareKind)
	{
	case '<': return &NoxOpGenerator::genOpCompareFloatLess;
	case OP_EQ: return &NoxOpGenerator::genOpCompareEqual;
	case OP_LE: return &NoxOpGenerator::genOpCompareFloatLessEqual;
	case OP_NE: return &NoxOpGenerator::genOpCompareNotEqual;
	}
	return nullptr;
}

NoxOpGenerator::binary_oper_ty NoxOpGenerator::loadIntArithmetic(int compareKind, bool ignoreSignedBit) const
{
	switch (compareKind)
	{
	case '+': return &NoxOpGenerator::genOpAdd;
	case '-': return &NoxOpGenerator::genOpSub;
	case '*': return &NoxOpGenerator::genOpMul;
	case '^': return &NoxOpGenerator::genOpXor;
	case OP_SAL: return &NoxOpGenerator::genOpShl;    //ensure equivalent?
	case OP_SAR: return &NoxOpGenerator::genOpShr;
	case OP_SHR: return &NoxOpGenerator::genOpShr;    //WARNING //weakness point
	case '/': return ignoreSignedBit ? &NoxOpGenerator::genOpUnsignedDiv : &NoxOpGenerator::genOpDiv; break;
	case '%': return ignoreSignedBit ? &NoxOpGenerator::genOpUnsignedMod : &NoxOpGenerator::genOpMod; break;
	default: return nullptr;
	}
}

NoxOpGenerator::binary_oper_ty NoxOpGenerator::loadFloatArithmetic(int compareKind) const
{
	switch (compareKind)
	{
	case '+': return &NoxOpGenerator::genOpFloatAdd;
	case '-': return &NoxOpGenerator::genOpFloatSub;
	case '*': return &NoxOpGenerator::genOpFloatMul;
	case '/': return &NoxOpGenerator::genOpFloatDiv;
	default: return nullptr;
	}
}

template <class Ty>
static void pushStringStream(std::stringstream &ss, Ty &&val)
{
    ss << val;
}

template <class Ty, class... Args>
static void pushStringStream(std::stringstream &ss, Ty &&val, Args&&... args)
{
    pushStringStream(ss, val);
    pushStringStream(ss, std::forward<Args>(args)...);
}

template <class... Args>
void NoxOpGenerator::putDebugOpcode(Args&&... args)
{    
    if (!m_putDebugCode)
        return;

    std::stringstream ss;

    pushStringStream(ss, std::forward<Args>(args)...);
    dispatchOpcode(std::make_unique<NoxOpDebug>(ss.str()));
}

//스택에 값을 삽입합니다
void NoxOpGenerator::genOpPush(fn_ret_reg_ty regValue)
{
    //regValue.Value();
    //dispatchOpcode(std::make_unique<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_EUDPush));
    //putDebugOpcode("push- ", regValue.DebugName());
    m_regOutputTarget->MakeInstruction<InstPush>(regValue);
}

void NoxOpGenerator::genOpPop(fn_ret_reg_ty regAddress)
{
    //regAddress.Address();
    //dispatchOpcode(std::make_unique<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_EUDPop));
    //putDebugOpcode("pop- ", regAddress.DebugName());
    m_regOutputTarget->MakeInstruction<InstPop>(regAddress);
}

void NoxOpGenerator::genOpMovN(fn_ret_reg_ty srcReg, fn_ret_reg_ty destReg, int size)
{
    //srcReg.ConvValue(size);
    //destReg.Address();
    //dispatchOpcode(std::make_unique<NoxBuiltinCall>(NoxBuiltinCall::LoadSetter(size)));    //getMem(reg)
    //putDebugOpcode("mov", size, "- ", srcReg.DebugName(), ", ", destReg.DebugName());
    if (size == sizeof(int))
    {
        m_regOutputTarget->MakeInstruction<InstMov>(srcReg, destReg);
        return;
    }
    m_regOutputTarget->MakeInstruction<InstMovN>(srcReg, destReg, size);
}

/*
* 결국 mov 명령은, 메모리에 쓸 때, 몇 byte 을 쓸 것인지 결정합니다
* 그러면 레지스터 eax, ax, al 같은 경우는, 몇 byte 을 읽을 것인지(얼마나 읽은 것인지)? 아마 그런듯;;
*/
void NoxOpGenerator::genOpMov(fn_ret_reg_ty srcReg, fn_ret_reg_ty destReg)
{
    // 기본적으로, 레지스터 간 사이즈가 같아야 합니다
    // 예외적으로, deref 와 레지스터는 크기가 달라도 됩니다
    // mov al, [ebp-04] //1바이트만 쓰기
    // mov [ebp-04], al //1바이트만 쓰기
    // mov eax, al  //문법오류!
    //int srcSize = srcReg.Size(), destSize = destReg.Size();

    //int operSize = (srcSize < destSize) ? srcSize : destSize;

    //if (!operSize)
    //    MY_THROW() << "register size == 0?";

    //srcReg.ConvValue(operSize);
    //destReg.Address();
    //dispatchOpcode(std::make_unique<NoxBuiltinCall>(NoxBuiltinCall::LoadSetter(operSize)));    //getMem(reg)
    //putDebugOpcode("mov", operSize, "- ", srcReg.DebugName(), ", ", destReg.DebugName());
    m_regOutputTarget->MakeInstruction<InstMov>(srcReg, destReg);
}

//#define GEN_MOV_PRIVIOUS_ROUTINE
void NoxOpGenerator::genOpMovzx(fn_ret_reg_ty srcReg, fn_ret_reg_ty destReg)
{
	//남는 필드는 0으로 채워야 합니다!
#ifdef GEN_MOV_PRIVIOUS_ROUTINE
	loadINT(0).Value();
	destReg.Address();
	dispatchOpcode(std::make_unique<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_SetMemory));

	//이제 값 복사
	genOpMov(srcReg, destReg, destReg.Size());
#else
	//srcReg.Value();
	//destReg.Address();
	//dispatchOpcode(std::make_unique<NoxBuiltinCall>(NoxBuiltinCall::LoadSetterEx(destReg.Size(), false)));
#endif
    //putDebugOpcode("movzx ", "(the rest be filled with 0)");
    if (srcReg.Size() == destReg.Size())
    {
        genOpMovN(srcReg, destReg, srcReg.Size());
        return;
    }
    m_regOutputTarget->MakeInstruction<InstMovzx>(srcReg, destReg);
}

void NoxOpGenerator::genOpMovsx(fn_ret_reg_ty srcReg, fn_ret_reg_ty destReg)
{
	//남는 필드는 1으로 채워야 합니다!
#ifdef GEN_MOV_PRIVIOUS_ROUTINE
	loadINT(-1).Value();
	destReg.Address();
	dispatchOpcode(std::make_unique<NoxBuiltinCall>(NoxBuiltinCall::BuiltinsFn::BUILTINS_SetMemory));
	genOpMov(srcReg, destReg, destReg.Size());
#else
	//srcReg.Value();
	//destReg.Address();
	//dispatchOpcode(std::make_unique<NoxBuiltinCall>(NoxBuiltinCall::LoadSetterEx(destReg.Size(), true)));    //getMem(reg)
#endif
    //putDebugOpcode("movsx");
    if (srcReg.Size() == destReg.Size())
    {
        genOpMovN(srcReg, destReg, srcReg.Size());
        return;
    }
    m_regOutputTarget->MakeInstruction<InstMovsx>(srcReg, destReg);
}

void NoxOpGenerator::genOpAdd(fn_ret_reg_ty regValue, fn_ret_reg_ty destReg)
{
    m_regOutputTarget->MakeInstruction<InstAdd>(regValue, destReg);
}

void NoxOpGenerator::genOpSub(fn_ret_reg_ty regValue, fn_ret_reg_ty destReg)
{
    m_regOutputTarget->MakeInstruction<InstSub>(regValue, destReg);
}

void NoxOpGenerator::genOpMul(fn_ret_reg_ty src, fn_ret_reg_ty dest)
{
    m_regOutputTarget->MakeInstruction<InstMul>(src, dest);
}

void NoxOpGenerator::genOpDiv(fn_ret_reg_ty src, fn_ret_reg_ty dest)
{
    m_regOutputTarget->MakeInstruction<InstDiv>(src, dest, false);
}

void NoxOpGenerator::genOpMod(fn_ret_reg_ty src, fn_ret_reg_ty dest)
{
    m_regOutputTarget->MakeInstruction<InstMod>(src, dest, false);
}

void NoxOpGenerator::genOpUnsignedDiv(fn_ret_reg_ty src, fn_ret_reg_ty dest)
{
    m_regOutputTarget->MakeInstruction<InstDiv>(src, dest, true);
}

void NoxOpGenerator::genOpUnsignedMod(fn_ret_reg_ty src, fn_ret_reg_ty dest)
{
    m_regOutputTarget->MakeInstruction<InstMod>(src, dest, true);
}

void NoxOpGenerator::genOpCallFromReg(fn_ret_reg_ty srcValue)
{
    m_regOutputTarget->MakeInstruction<InstCallFromReg>(srcValue);
}

void NoxOpGenerator::genOpCallFunction(const std::string &fn)
{
    m_regOutputTarget->MakeInstruction<InstCall>(fn);
}

void NoxOpGenerator::genOpLea(fn_ret_reg_ty srcRegAddr, fn_ret_reg_ty destReg)
{
    m_regOutputTarget->MakeInstruction<InstLea>(srcRegAddr, destReg);
}

void NoxOpGenerator::genOpShr(fn_ret_reg_ty srcValue, fn_ret_reg_ty destReg)
{
    m_regOutputTarget->MakeInstruction<InstBinaryOp>(srcValue, destReg, NoxBuiltinCall::BuiltinsFn::BUILTINS_ShiftRight);
}

void NoxOpGenerator::genOpShl(fn_ret_reg_ty srcValue, fn_ret_reg_ty dest)
{
    m_regOutputTarget->MakeInstruction<InstBinaryOp>(srcValue, dest, NoxBuiltinCall::BuiltinsFn::BUILTINS_ShiftLeft);
}

void NoxOpGenerator::genOpXor(fn_ret_reg_ty srcValue, fn_ret_reg_ty dest)
{
    m_regOutputTarget->MakeInstruction<InstBinaryOp>(srcValue, dest, NoxBuiltinCall::BuiltinsFn::BUILTINS_XOR);
}

void NoxOpGenerator::genOpAnd(fn_ret_reg_ty srcValue, fn_ret_reg_ty destReg)
{
    m_regOutputTarget->MakeInstruction<InstBinaryOp>(srcValue, destReg, NoxBuiltinCall::BuiltinsFn::BUILTINS_AND);
}

void NoxOpGenerator::genOpOr(fn_ret_reg_ty src, fn_ret_reg_ty dest)
{
    m_regOutputTarget->MakeInstruction<InstBinaryOp>(src, dest, NoxBuiltinCall::BuiltinsFn::BUILTINS_OR);
}

void NoxOpGenerator::genOpNot(fn_ret_reg_ty dest)
{
    m_regOutputTarget->MakeInstruction<InstNot>(dest);
}

void NoxOpGenerator::genOpFloatAdd(fn_ret_reg_ty src, fn_ret_reg_ty dest)
{
    m_regOutputTarget->MakeInstruction<InstBinaryOp>(src, dest, NoxBuiltinCall::BuiltinsFn::BUILTINS_FloatAdd);
}

void NoxOpGenerator::genOpFloatSub(fn_ret_reg_ty src, fn_ret_reg_ty dest)
{
    m_regOutputTarget->MakeInstruction<InstBinaryOp>(src, dest, NoxBuiltinCall::BuiltinsFn::BUILTINS_FloatSub);
}

void NoxOpGenerator::genOpFloatMul(fn_ret_reg_ty src, fn_ret_reg_ty dest)
{
    m_regOutputTarget->MakeInstruction<InstBinaryOp>(src, dest, NoxBuiltinCall::BuiltinsFn::BUILTINS_FloatMul);
}

void NoxOpGenerator::genOpFloatDiv(fn_ret_reg_ty src, fn_ret_reg_ty dest)
{
    m_regOutputTarget->MakeInstruction<InstBinaryOp>(src, dest, NoxBuiltinCall::BuiltinsFn::BUILTINS_FloatDiv);
}

void NoxOpGenerator::genOpConvIntToFloat(fn_ret_reg_ty src, fn_ret_reg_ty dest)
{
    m_regOutputTarget->MakeInstruction<InstBinaryOp>(src, dest, NoxBuiltinCall::BuiltinsFn::BUILTINS_IntToFloat);
}

//src에서 받은, float 값을 int 값으로 변환 후, dest에 대입합니다
void NoxOpGenerator::genOpConvFloatToInt(fn_ret_reg_ty src, fn_ret_reg_ty dest)
{
    m_regOutputTarget->MakeInstruction<InstBinaryOp>(src, dest, NoxBuiltinCall::BuiltinsFn::BUILTINS_FloatToInt);
}

void NoxOpGenerator::placeComparison(fn_ret_reg_ty cmp1, fn_ret_reg_ty cmp2,
	NoxOpGenerator::CompareOperator::Type cmpTy)
{
	auto nativeCompToBuiltin = [this](int comp)
	{
		switch (comp)
		{
		case CompareOperator::CompareEqual: return NoxBuiltinCall::BuiltinsFn::BUILTINS_CompareEqual;
		case CompareOperator::CompareEqualNot: return NoxBuiltinCall::BuiltinsFn::BUILTINS_CompareNotEqual;
		case CompareOperator::CompareLess: return NoxBuiltinCall::BuiltinsFn::BUILTINS_CompareLess;
		case CompareOperator::CompareLessEqual: return NoxBuiltinCall::BuiltinsFn::BUILTINS_CompareLessEqual;
		case CompareOperator::CompareFloatLess: return NoxBuiltinCall::BuiltinsFn::BUILTINS_CompareFloatLess;
		case CompareOperator::CompareFloatLessEqual: return NoxBuiltinCall::BuiltinsFn::BUILTINS_CompareFloatLessEqual;
		case CompareOperator::CompareTest: return NoxBuiltinCall::BuiltinsFn::BUILTINS_TEST;
		case CompareOperator::CompareTestNot:	return NoxBuiltinCall::BuiltinsFn::BUILTINS_TEST_NOT;
		case CompareOperator::CompareB: return NoxBuiltinCall::BuiltinsFn::BUILTINS_CompareB;
		case CompareOperator::CompareBEqual:	return NoxBuiltinCall::BuiltinsFn::BUILTINS_CompareEqualB;
		default: MY_THROW() << "unknown comparer";
		}
	};
	auto builtinMethod = nativeCompToBuiltin(cmpTy);

	//cmp2.Value();
	//cmp1.Value();
	//dispatchOpcode(std::make_unique<NoxBuiltinCall>(builtinMethod));
    m_regOutputTarget->MakeInstruction<InstComparison>(cmp1, cmp2, builtinMethod);
}

void NoxOpGenerator::genOpCompareNotEqual(fn_ret_reg_ty cmp1, fn_ret_reg_ty cmp2)
{
	placeComparison(cmp1, cmp2, CompareOperator::CompareEqualNot);
}

void NoxOpGenerator::genOpCompareEqual(fn_ret_reg_ty cmp1, fn_ret_reg_ty cmp2)
{
	placeComparison(cmp1, cmp2, CompareOperator::CompareEqual);
}

void NoxOpGenerator::genOpCompareLess(fn_ret_reg_ty cmp1, fn_ret_reg_ty cmp2)
{
	placeComparison(cmp1, cmp2, CompareOperator::CompareLess);
}

void NoxOpGenerator::genOpCompareLessEqual(fn_ret_reg_ty cmp1, fn_ret_reg_ty cmp2)
{
	placeComparison(cmp1, cmp2, CompareOperator::CompareLessEqual);
}

void NoxOpGenerator::genOpCompareUnsignedB(fn_ret_reg_ty cmp1, fn_ret_reg_ty cmp2)
{
	placeComparison(cmp1, cmp2, CompareOperator::CompareB);
}

void NoxOpGenerator::genOpCompareUnsignedBEqual(fn_ret_reg_ty cmp1, fn_ret_reg_ty cmp2)
{
	placeComparison(cmp1, cmp2, CompareOperator::CompareBEqual);
}

void NoxOpGenerator::genOpTest()
{
    m_regOutputTarget->MakeInstruction<InstTest>(false);
}

void NoxOpGenerator::genOpTestNot()
{
    m_regOutputTarget->MakeInstruction<InstTest>(true);
}

void NoxOpGenerator::genOpCompareFloatLess(fn_ret_reg_ty fcmp1, fn_ret_reg_ty fcmp2)
{
	placeComparison(fcmp1, fcmp2, CompareOperator::CompareFloatLess);
}

void NoxOpGenerator::genOpCompareFloatLessEqual(fn_ret_reg_ty fcmp1, fn_ret_reg_ty fcmp2)
{
	placeComparison(fcmp1, fcmp2, CompareOperator::CompareFloatLessEqual);
}

void NoxOpGenerator::genOpGetCompareResult(fn_ret_reg_ty dest)
{
    m_regOutputTarget->MakeInstruction<InstCmpResult>(dest);
}

void NoxOpGenerator::genOpLabel(const std::string &label)
{
    m_regOutputTarget->MakeInstruction<InstLabel>(label);
}

void NoxOpGenerator::genOpJMP(const std::string &label)
{
    m_regOutputTarget->MakeInstruction<InstJump>(label);
}

void NoxOpGenerator::genOpJE(const std::string &label)
{
    m_regOutputTarget->MakeInstruction<InstJumpConditional>(*m_regCmpResult, label, true);
}

void NoxOpGenerator::genOpJNE(const std::string &label)
{
    m_regOutputTarget->MakeInstruction<InstJumpConditional>(*m_regCmpResult, label, false);
}

void NoxOpGenerator::genOpReturn()
{
    m_regOutputTarget->MakeInstruction<InstReturn>();
}

void NoxOpGenerator::genOpMemCopy(fn_ret_reg_ty srcAddress, fn_ret_reg_ty destAddress, fn_ret_reg_ty repeat)
{
    m_regOutputTarget->MakeInstruction<InstMemCopy>(srcAddress, destAddress, repeat);
}

void NoxOpGenerator::genOpZeroFiller(fn_ret_reg_ty start, fn_ret_reg_ty end)
{
    m_regOutputTarget->MakeInstruction<InstZeroFiller>(start, end);
}

void NoxOpGenerator::genOpLocalStore(fn_ret_reg_ty value, int off)
{
    m_regOutputTarget->MakeInstruction<InstStoreLocal>(value, off);
}

void NoxOpGenerator::PutParams(BuildEnvironment &env)
{
    m_regOutputTarget->ShowDebugPrint(env.UseDebug());
}
