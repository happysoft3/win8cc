
#ifndef NOX_OP_GENERATOR_H__
#define NOX_OP_GENERATOR_H__

#include <memory>
#include <vector>
#include <list>

class AbstractRegister;
class NoxOpcode;
class Register;
class NoxFunction;
class IntRegister;
class RegisterOutputTarget;
class BuildEnvironment;

class NoxOpGenerator
{
	struct CompareOperator
	{
		enum Type
		{
			CompareEqual,
			CompareEqualNot,
			CompareLess,
			CompareLessEqual,
			CompareB,
			CompareBEqual,
			CompareFloatLess,
			CompareFloatLessEqual,
			CompareTest,
			CompareTestNot,
		};
	};
protected:
    static constexpr int default_bytes = sizeof(int);  //8     32bit or 64bit
	using op_reg_ty = std::unique_ptr<AbstractRegister>;

private:
	std::vector<op_reg_ty> m_regEspVector;
	std::vector<op_reg_ty> m_regEbpVector;
	std::vector<op_reg_ty> m_regEaxVector;
	std::vector<op_reg_ty> m_regEbxVector;		//instead r11
	std::vector<op_reg_ty> m_regEcxVector;
	std::vector<op_reg_ty> m_regEdxVector;
	std::vector<op_reg_ty> m_ediRegs;
	std::vector<op_reg_ty> m_esiRegs;

    //Introduced compare result register
    op_reg_ty m_regCmpResult;

	std::shared_ptr<RegisterOutputTarget> m_regOutputTarget;
    std::list<op_reg_ty> m_tempRegList;

	//std::unique_ptr<AbstractRegister> m_funcLinkReg;
	//std::unique_ptr<AbstractRegister> m_globalLinkReg;

public:
    NoxOpGenerator();
    virtual ~NoxOpGenerator();

private:
    void myHack();
	std::vector<op_reg_ty> initRegisters(int baseOffset, const std::string &dbgName);

protected:
    template <class Ty, class... Args>
    Ty *storeTempReg(Args&&... args)
    {
        auto p = std::make_unique<Ty>(std::forward<Args>(args)...);
        auto ret = p.get();

        p->PropagateCtx(ESP());
        m_tempRegList.push_back(std::move(p));
        if (m_tempRegList.size() > sizeof(int))
            m_tempRegList.pop_front();
        return ret;
    }

    using fn_ret_reg_ty = AbstractRegister &;

    fn_ret_reg_ty ESP(int align = default_bytes)
    {
		return fetchRegist(m_regEspVector, align);
    }
    fn_ret_reg_ty &EBP(int align = default_bytes)
    {
		return fetchRegist(m_regEbpVector, align);
    }

private:
    fn_ret_reg_ty fetchRegist(std::vector<op_reg_ty> &regs, int align) const;

protected:
    fn_ret_reg_ty EAX(int align = default_bytes)
	{
		return fetchRegist(m_regEaxVector, align);
	}
    fn_ret_reg_ty EBX(int align = default_bytes)
	{
		return fetchRegist(m_regEbxVector, align);
	}
    fn_ret_reg_ty ECX(int align = default_bytes)
	{
		return fetchRegist(m_regEcxVector, align);
	}
    fn_ret_reg_ty EDX(int align = default_bytes)
	{
		return fetchRegist(m_regEdxVector, align);
	}
    fn_ret_reg_ty ESI(int align = default_bytes)
	{
		return fetchRegist(m_esiRegs, align);
	}
    fn_ret_reg_ty EDI(int align = default_bytes)
	{
		return fetchRegist(m_ediRegs, align);
	}
    fn_ret_reg_ty loadREG(int order, int align = default_bytes);
	//AbstractRegister &loadFunctionReg(const std::string &link);
	//AbstractRegister &loadGlobalReg(const std::string &vTag, int off = 0);
    void attachFunction(std::unique_ptr<NoxFunction> boundary);
    std::unique_ptr<NoxFunction> detachFunction();

	using binary_oper_ty = void(NoxOpGenerator::*)(fn_ret_reg_ty, fn_ret_reg_ty);
	binary_oper_ty loadIntCompare(int compareKind, bool ignoreSignedBit) const;
	binary_oper_ty loadFloatCompare(int compareKind) const;
	binary_oper_ty loadIntArithmetic(int compareKind, bool ignoreSignedBit) const;
	binary_oper_ty loadFloatArithmetic(int compareKind) const;

private:
    template <class... Args>
    void putDebugOpcode(Args&&... args);

protected:  //compiler->ENGINE 배선도
    virtual void genOpPush(fn_ret_reg_ty regValue);
    virtual void genOpPop(fn_ret_reg_ty regAddress);
    void genOpMovN(fn_ret_reg_ty srcReg, fn_ret_reg_ty destReg, int size);
    void genOpMov(fn_ret_reg_ty srcReg, fn_ret_reg_ty destReg);

	//unsigned version -- 확장된 비트를 제로로 채웁니다
	void genOpMovzx(fn_ret_reg_ty srcReg, fn_ret_reg_ty destReg);

	//signed version -- 확장된 비트를 1 으로 채웁니다
	void genOpMovsx(fn_ret_reg_ty srcReg, fn_ret_reg_ty destReg);
    void genOpAdd(fn_ret_reg_ty regValue, fn_ret_reg_ty destReg);
    void genOpSub(fn_ret_reg_ty regValue, fn_ret_reg_ty destReg);
    //두개를 곱하여, dest에 저장
    void genOpMul(fn_ret_reg_ty src, fn_ret_reg_ty dest);

    //나누기 연산, native byte code 으로 직결
    void genOpDiv(fn_ret_reg_ty src, fn_ret_reg_ty dest);
    //모듈러스 연산, native byte code 으로 직결
    void genOpMod(fn_ret_reg_ty src, fn_ret_reg_ty dest);
    //부호없는, 나누기 연산 -> 직접 구현
    void genOpUnsignedDiv(fn_ret_reg_ty src, fn_ret_reg_ty dest);
    //부호없는, 모듈러스 -> 직접 구현
    void genOpUnsignedMod(fn_ret_reg_ty src, fn_ret_reg_ty dest);

    void genOpCallFromReg(fn_ret_reg_ty srcValue);
    void genOpCallFunction(const std::string &fn);
    void genOpLea(fn_ret_reg_ty srcRegAddr, fn_ret_reg_ty destReg);
	void genOpShr(fn_ret_reg_ty srcValue, fn_ret_reg_ty destReg);
	void genOpShl(fn_ret_reg_ty srcValue, fn_ret_reg_ty dest);
    void genOpXor(fn_ret_reg_ty srcValue, fn_ret_reg_ty dest);
	void genOpAnd(fn_ret_reg_ty srcValue, fn_ret_reg_ty destReg);
	void genOpOr(fn_ret_reg_ty src, fn_ret_reg_ty dest);
    void genOpNot(fn_ret_reg_ty dest);

    ///Float arithmetic
    void genOpFloatAdd(fn_ret_reg_ty src, fn_ret_reg_ty dest);
    void genOpFloatSub(fn_ret_reg_ty src, fn_ret_reg_ty dest);
    void genOpFloatMul(fn_ret_reg_ty src, fn_ret_reg_ty dest);
    void genOpFloatDiv(fn_ret_reg_ty src, fn_ret_reg_ty dest);
    ////

    //src에서 받은, int 값을 float 값으로 변환 후, dest에 대입합니다
    void genOpConvIntToFloat(fn_ret_reg_ty src, fn_ret_reg_ty dest);

    //src에서 받은, float 값을 int 값으로 변환 후, dest에 대입합니다
    void genOpConvFloatToInt(fn_ret_reg_ty src, fn_ret_reg_ty dest);

private:
	void placeComparison(fn_ret_reg_ty cmp1, fn_ret_reg_ty cmp2, CompareOperator::Type cmpTy);

protected:
    //compare 결과를, cmp_result 레지스터에 저장합니다
    void genOpCompareNotEqual(fn_ret_reg_ty cmp1, fn_ret_reg_ty cmp2);  //녹스 native byte code 으로 직결
    void genOpCompareEqual(fn_ret_reg_ty cmp1, fn_ret_reg_ty cmp2); //녹스 native byte code 으로 직결
    void genOpCompareLess(fn_ret_reg_ty cmp1, fn_ret_reg_ty cmp2);  //signed compare 입니다, native 으로 직결
    void genOpCompareLessEqual(fn_ret_reg_ty cmp1, fn_ret_reg_ty cmp2); //maybe a<=b

    void genOpCompareUnsignedB(fn_ret_reg_ty cmp1, fn_ret_reg_ty cmp2); //직접 구현이 필요할 듯,,
    void genOpCompareUnsignedBEqual(fn_ret_reg_ty cmp1, fn_ret_reg_ty cmp2);    //also

    void genOpTest();   ////녹스 native byte code:: and 으로 직결->결과를 compare result 레지스터로
    void genOpTestNot();    //test 결과의 not 을 저장

    //float 비교, 적으면 jump?
    void genOpCompareFloatLess(fn_ret_reg_ty fcmp1, fn_ret_reg_ty fcmp2);
    void genOpCompareFloatLessEqual(fn_ret_reg_ty fcmp1, fn_ret_reg_ty fcmp2);

    //compare 결과를, dest 레지스터에 저장합니다
    void genOpGetCompareResult(fn_ret_reg_ty dest);

    //label 은, 코드단락에는 포함되지 않지만
    //어쨌든 gen 을 해야합니다, 오프셋을 계산해야 하기 때문에...
    void genOpLabel(const std::string &label);
    void genOpJMP(const std::string &label);
    void genOpJE(const std::string &label);
    void genOpJNE(const std::string &label);

	void genOpReturn();
	void genOpMemCopy(fn_ret_reg_ty srcAddress, fn_ret_reg_ty destAddress, fn_ret_reg_ty repeat);
    void genOpZeroFiller(fn_ret_reg_ty start, fn_ret_reg_ty end);
    void genOpLocalStore(fn_ret_reg_ty value, int off);

public:
    void PutParams(BuildEnvironment &env);
};

#endif

