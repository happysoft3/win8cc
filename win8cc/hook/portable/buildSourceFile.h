
#ifndef BUILD_SOURCE_FILE_H__
#define BUILD_SOURCE_FILE_H__

#include <string>

class BuildSourceFile
{
private:
	std::string m_name;
	int m_order;

public:
	BuildSourceFile(const std::string &name, int order);
	~BuildSourceFile();

	const char *const Name() const
	{
		return m_name.c_str();
	}

	int Order() const
	{
		return m_order;
	}
};

#endif

