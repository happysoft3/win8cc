
#ifndef GENERIC_DATA_H__
#define GENERIC_DATA_H__

#include <memory>

class BuildSourceFile;

class GenericData
{
private:
	std::shared_ptr<BuildSourceFile> m_locate;

public:
	GenericData(std::shared_ptr<BuildSourceFile> locate);
	virtual ~GenericData();
};

#endif

