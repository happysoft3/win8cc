
#ifndef FUNCTION_TABLE_MAP_H__
#define FUNCTION_TABLE_MAP_H__

#include <map>
#include <memory>
#include <string>

class FunctionTable;

class FunctionTableMap
{
private:
	std::map<std::string, std::unique_ptr<FunctionTable>> m_tableMap;

public:
	FunctionTableMap();
	~FunctionTableMap();

	void Put(std::unique_ptr<FunctionTable> table);
};

#endif

