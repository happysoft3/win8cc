
#ifndef COMPILE_LINKER_H__
#define COMPILE_LINKER_H__

#include <memory>
#include <list>
#include <map>

class FunctionTable;
class NoxFunction;
class NoxOpcode;
class CDataSection;
class CCResult;
class CDataAllocator;

class CompileLinker
{
private:
	std::unique_ptr<FunctionTable> m_sourceFnTable;
	NoxFunction *m_currentFunction;
	std::unique_ptr<CDataSection> m_globalData;
    std::unique_ptr<CDataSection> m_staticField;
	std::shared_ptr<FunctionTable> m_globalFnTable;
	int m_functionUniqId;

public:
	CompileLinker();
	~CompileLinker();

private:
	void fixUsercallGlobalAddress(NoxOpcode &op);
	void fixUsercallStaticAddress(NoxOpcode &op);
	void fixJumpDestination(NoxOpcode &op);
	void fixGlobalVariable(NoxOpcode &op);
	void preLinkingCode(NoxOpcode &opcode);
	void moveFunctionToGlobal();
	void linkFunction(NoxFunction &fn);
	void prelinkFunctionCode(NoxFunction &fn);

public:
	void Prelink(CCResult &cResult);

private:
	void postMakeCodeRepeat(NoxOpcode &op);
	void postlinkImpl(NoxFunction &fn);

public:
	void Postlink();
	std::shared_ptr<FunctionTable> ReleaseGlobalFunctionTable();
	std::unique_ptr<CDataSection> ReleaseGlobalField();
};

#endif

