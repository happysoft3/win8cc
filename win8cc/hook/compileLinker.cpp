
#include "compileLinker.h"
#include "hook/cCResult.h"
#include "link/cclink.h"
#include "portableLinkData.h"
#include "hook/global/functionGlobalTable.h"
#include "hook/noxFunction.h"
#include "hook/noxOpcode.h"
#include "hook/cdata/cDataSection.h"
#include "hook/cdata/cDataLabel.h"
#include "hook/cdata/cDataAllocator.h"
#include "common/utils/mydebug.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

CompileLinker::CompileLinker()
{
	m_globalData = std::make_unique<CDataSection>();
	m_globalFnTable = std::make_shared<FunctionGlobalTable>();
	m_functionUniqId = 7;
}

CompileLinker::~CompileLinker()
{ }

void CompileLinker::fixUsercallGlobalAddress(NoxOpcode &op)
{
    op.DoLinking(m_globalFnTable.get(), nullptr, nullptr);
}

void CompileLinker::fixUsercallStaticAddress(NoxOpcode &op)
{
    op.DoLinking(m_sourceFnTable.get(), nullptr, m_currentFunction);
}

void CompileLinker::fixJumpDestination(NoxOpcode &op)
{
    op.DoLinking(nullptr, nullptr, m_currentFunction);
}

//로컬에서 찾아서, 글로벌에 넣어요
void CompileLinker::fixGlobalVariable(NoxOpcode &op)
{
    op.DoLinking(nullptr, m_globalData.get(), nullptr);
}

void CompileLinker::preLinkingCode(NoxOpcode &opcode)
{	//함수 호출 링크- static 함수일 때만, 점프레이블 링크
	switch (opcode.Type())
	{
	case NoxOpType::LoadUserCall:
	case NoxOpType::UserCall:
		fixUsercallStaticAddress(opcode);
		break;

	case NoxOpType::Jump:
		fixJumpDestination(opcode);
		break;

	//static 및 광역 변수를 링크해야 합니다	//여기에서 처리할 수 없어서, post 링크에서 처리합니다...
	case NoxOpType::Global:
        opcode.DoLinking(nullptr, m_staticField.get(), m_currentFunction); //스태틱변수만 링크
        break;

    case NoxOpType::NativeOperator: //pass
    case NoxOpType::NativePush:
    case NoxOpType::BuiltinCall:
    case NoxOpType::Debug:
        break;

	default:
		MY_THROW() << stringFormat("unknown op '%s' code, but the app will not pass here", opcode.ToString());
	}
}

void CompileLinker::moveFunctionToGlobal()
{
	std::unique_ptr<NoxFunction> noxFn;

	do
	{
		noxFn = m_sourceFnTable->Pop();

		if (!noxFn)
			break;
        if (!noxFn->IsStatic())
        {
            if (m_globalFnTable->Retrieve(noxFn->Name()))
                MY_THROW() << stringFormat("%s::multiple definition of '%s'", __FUNCDNAME__, noxFn->Name());
        }
        m_globalFnTable->Push(std::move(noxFn));
	} while (true);
}

//광역함수는 이 클래스가 관리하는, 함수테이블에 모아서 관리합니다
//하나의 소스안에 있는, static 함수가 아니면, 이름 중복이 발생하면 안됩니다
void CompileLinker::linkFunction(NoxFunction &fn)
{	//처리할 함수로 등록합니다
    fn.SetUniqueId(m_functionUniqId++);
}

void CompileLinker::prelinkFunctionCode(NoxFunction &fn)
{
	m_currentFunction = &fn;
	fn.OpcodePreorder([this](NoxOpcode &op) { this->preLinkingCode(op); });
}

void CompileLinker::Prelink(CCResult &cResult)
{
	m_sourceFnTable = cResult.DetachTable();
	m_sourceFnTable->SetParentTable(m_globalFnTable);	//여기에서 linkData 을 보내면, 무슨일이 생길까...

	auto sourceField = cResult.DetachField();

    sourceField->PreLinking(*m_sourceFnTable);

    m_staticField = std::make_unique<CDataSection>();

	//필드에서 레이블 노드를 찾고,
	//레이블 노드일 때, 오프셋을 정합니다
	//depth 값이 같을 때, 거기에 링크됩니다
	//sourceField->MoveToGlobal(*m_globalData); //여기에서 variable 을 링크해야 할텐데...
    sourceField->MoveToGlobal(*m_staticField);
	m_sourceFnTable->Preorder([this](NoxFunction &fn) { this->prelinkFunctionCode(fn); });	//Resolve 로컬함수 호출, 점프문
    m_globalData->Exchange(*m_staticField);

    //Todo. staticField -> globalData
	//함수 하나씩 코드를 생성합니다
	//소스필드에 있는 함수를, 글로벌로 옮깁니다
	moveFunctionToGlobal();
}

void CompileLinker::postMakeCodeRepeat(NoxOpcode &op)
{
	switch (op.Type())
	{
	case NoxOpType::LoadUserCall:
	case NoxOpType::UserCall:
		fixUsercallGlobalAddress(op);
		break;

		//static 및 광역 변수를 링크해야 합니다	//여기에서 처리할 수 없어서, post 링크에서 처리합니다...
	case NoxOpType::Global:
		fixGlobalVariable(op);
		break;

	case NoxOpType::Jump:
	case NoxOpType::NativeOperator: //pass
	case NoxOpType::NativePush:
	case NoxOpType::BuiltinCall:
		break;
	}
}

void CompileLinker::postlinkImpl(NoxFunction &fn)
{
	using namespace std::placeholders;
	fn.OpcodePreorder(std::bind(&CompileLinker::postMakeCodeRepeat, this, _1));

	linkFunction(fn);		//여기에서 넘버링
}

void CompileLinker::Postlink()
{
	//여기에서 global 패키징?
	//여기까지 왔을 때에는, CDataLabel객체가 완전한 데이터를 가리키고 있는 상태입니다
    m_globalData->PostLinking(*m_globalFnTable);
	//여기에서, 코드에 박힌 글로벌 변수를 링크해야 할 지 싶네요;;
	using namespace std::placeholders;
	m_globalFnTable->Preorder(std::bind(&CompileLinker::postlinkImpl, this, _1));
}

std::shared_ptr<FunctionTable> CompileLinker::ReleaseGlobalFunctionTable()
{
    if (!m_globalFnTable)
        return{ };

    return std::move(m_globalFnTable);

}

std::unique_ptr<CDataSection> CompileLinker::ReleaseGlobalField()
{
    if (!m_globalData)
        return{ };

    return std::move(m_globalData);
}

