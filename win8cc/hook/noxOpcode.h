
#ifndef NOX_OPCODE_H__
#define NOX_OPCODE_H__

#include <string>
#include <memory>

class CCLink;
class CDataSection;
class CDataLabel;
class FunctionTable;
class NoxFunction;

enum class NoxOpType
{
	NONE,
	NativePush,
	NativeOperator,
	BuiltinCall,
	UserCall,
	LoadUserCall,
	Jump,
	Global,
    Debug,
};

class ScrOutStream;

class NoxOpcode
{
private:
	NoxOpType m_opType;
	std::shared_ptr<CCLink> m_codepos;

public:
    NoxOpcode(NoxOpType ty = NoxOpType::NONE);
    virtual ~NoxOpcode();

	NoxOpType Type() const
	{
		return m_opType;
	}
	virtual int Length() const = 0;
    virtual const char *const ToString() const = 0; //디버그 용도입니다
	virtual void Emit(ScrOutStream &scrOs) = 0;
	std::weak_ptr<CCLink> MarkCodePos();
	void MakeOffsetMark(int off);
    virtual void DoLinking(FunctionTable *fnTable, CDataSection *data, NoxFunction *fn) { }
};

class NoxNativePush : public NoxOpcode
{
private:
	int m_pushValue;

public:
    NoxNativePush(int value = 0);
    ~NoxNativePush() override;

private:
	int Length() const override
	{
		return 2;
	}
    const char *const ToString() const override {
        return "NoxNativePush";
    }
	void Emit(ScrOutStream &scrOs) override;
};

class NoxNativeOp : public NoxOpcode
{
public:
	//게임 엔진에서 지원하는 메서드
    enum OpID
    {
#define DEF_NATIVE_OP(id, code) id = code,
#include "hook/ident/nativeOperator.inc"
#undef DEF_NATIVE_OP
    };

private:
    int m_opId;

public:
    NoxNativeOp(OpID opid);
    ~NoxNativeOp() override;

private:
	const char *const nativeOperatorName() const;
	void Emit(ScrOutStream &scrOs) override;
	int Length() const override { return 1; }
    const char *const ToString() const override {
        return "NoxNativeOP";
    }
};

class NoxBuiltinCall : public NoxOpcode
{
public:
	//직접 구현해야 하는 메서드
    struct BuiltinsFn
    {
        enum Class
        {
            BUILTINS_Invalid,
#define DEF_IMPL_BUILTINS_FUNCTION(id, _discard) id,
#include "hook/ident/implBuiltinsFunctions.inc"
#undef DEF_IMPL_BUILTINS_FUNCTION
        };
    };

private:
    BuiltinsFn::Class m_fnId;

public:
    explicit NoxBuiltinCall(BuiltinsFn::Class fnId);
    ~NoxBuiltinCall() override;

	static BuiltinsFn::Class LoadGetter(int size);
	static BuiltinsFn::Class LoadSetter(int size);
	static BuiltinsFn::Class LoadSetterEx(int size, bool needSigned = false);

private:
	const char *builtinsName() const;
    int builtinsExecuteBase() const;
	void Emit(ScrOutStream &scrOs) override;
	int Length() const override { return 2; }
    const char *const ToString() const override {
        return "NoxBuiltinCall";
    }
};

class NoxNeedLink : public NoxOpcode
{
private:
	std::string m_labelTag;
	std::shared_ptr<CCLink> m_linkResult;

public:
	NoxNeedLink(const std::string &tag, NoxOpType type);
	~NoxNeedLink() override;

private:
	int Length() const override { return 1; }

public:
	const char *Link() const
	{
		return m_labelTag.c_str();
	}
	bool Linked() const
	{
		return m_linkResult ? true : false;
	}
	void Emit(ScrOutStream &scrOs) override;
	virtual void SetLinkResult(std::shared_ptr<CCLink> &link);
	int LinkedValue() const;
};

class NoxUserCallBase : public NoxNeedLink
{
public:
    NoxUserCallBase(const std::string &tag, NoxOpType type);
    ~NoxUserCallBase() override;

private:
    void staticLink(NoxFunction *target);
    void globalLink(NoxFunction *target);
    void DoLinking(FunctionTable *fnTable, CDataSection *data, NoxFunction *fn) override;
};

//사용자 정의 함수 호출
class NoxUserCall : public NoxUserCallBase
{
public:
	NoxUserCall(const std::string &fn);
	~NoxUserCall() override;

private:
	int Length() const override { return 2; }
	void Emit(ScrOutStream &scrOs) override;
    const char *const ToString() const override {
        return "NoxUserCall";
    }
};

//링커에서 함수번호로 변환되어 집니다
class NoxUserCallAddress : public NoxUserCallBase
{
public:
	NoxUserCallAddress(const std::string &link);
	~NoxUserCallAddress() override;

private:
    const char *const ToString() const override {
        return "NoxUserCallAddress";
    }
};

class NoxOpJump : public NoxNeedLink
{
public:
	enum Conditional
	{
		Pass,
		JumpEqual,
		JumpNotEqual,
        ComputedJump,
	};

private:
	Conditional m_jmpCond;
	std::string m_jmpDebug;

public:
	explicit NoxOpJump(const std::string &label, Conditional cond);
	~NoxOpJump() override;

private:
	void printJumpDebug();
    int Length() const override;
	void Emit(ScrOutStream &scrOs) override;
	const char *const ToString() const override;
    void DoLinking(FunctionTable *fnTable, CDataSection *data, NoxFunction *fn) override;
};

class NoxOpGlobalLoader : public NoxNeedLink
{
private:
	int m_offset;
    CDataLabel *m_staticSymb;

public:
	NoxOpGlobalLoader(const std::string &link, int offset = 0);
	~NoxOpGlobalLoader() override;

private:
	void SetLinkResult(std::shared_ptr<CCLink> &link) override;
    const char *const ToString() const override {
        return "NoxOpGlobalLoader";
    }
    void globalLink(CDataSection *global);
    void staticLink(CDataSection *scope);
    void DoLinking(FunctionTable *fnTable, CDataSection *data, NoxFunction *fn) override;
};

class NoxOpDebug : public NoxOpcode
{
private:
    std::string m_dbgString;

public:
    NoxOpDebug(const std::string &dbgString);
    ~NoxOpDebug() override;

private:
    int Length() const override
    {
        return 0;
    }
    const char *const ToString() const override
    {
        return "NoxOpDebug";
    }
    void Emit(ScrOutStream &scrOs) override;
};

#endif

