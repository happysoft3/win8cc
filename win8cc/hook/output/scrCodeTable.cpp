
#include "scrCodeTable.h"
#include "hook/compileLinker.h"
#include "hook/buildEnvironment.h"
#include "hook/functionTable.h"
#include "../noxFunction.h"
#include "hook/function/eudVariableBundle.h"
#include "hook/function/eudApiBundle.h"
#include "hook/function/eudLoaderFunction.h"
#include "hook/function/eudGlobalPointerPack.h"
#include "hook/cdata/cDataSection.h"
#include "hook/link/cclink.h"
#include "hook/noxOpcode.h"
#include "common/utils/randomString.h"
#include "common/utils/myDebug.h"
#include "common/utils/stringhelper.h"
#include <sstream>

using namespace _StringHelper;

static constexpr auto load_binary_eud_loader = "binary\\impleudloader.bin";
static constexpr auto load_binary_eud_core_api_table = "binary\\implTable.bin";
static constexpr auto load_binary_eud_core_api = "binary\\implbuiltins.bin";

ScrCodeTable::ScrCodeTable()
    : ScrOutStream()
{
	m_stackSize = 0;
    m_globalAllocSize = 0;
    m_eudLoaderFn = nullptr;
}

ScrCodeTable::~ScrCodeTable()
{ }

int ScrCodeTable::emitFunctionCount()
{
	if (!m_globalFnTable)
		MY_THROW() << "has no attrib 'function table'";

	int fnCount = m_globalFnTable->Count();

	if (!fnCount)
		MY_THROW() << "function table is empty";
	return fnCount	//그대로 내보낼 수 없고, GLOBAL 2개 그리고, InitialEUDLoader, StackZone 을 포함해야 합니다
		+ m_defaultFnTable->Count();
}

void ScrCodeTable::emitDefaultFunctionTable(ScrOutStream &scrOs)
{
	///GLOBAL 2개 그리고, InitialEUDLoader 함수를 여기에서 내보냅니다
	std::unique_ptr<NoxFunction> fn;

	for (;;)
	{
		fn = m_defaultFnTable->Pop();

		if (!fn)
			break;

		scrOs << *fn;
	}
}

void ScrCodeTable::emitFunctionTable(ScrOutStream &scrOs)
{
	///나머지 함수를 여기에서 내보냅니다
	std::unique_ptr<NoxFunction> fn;

	for (;;)
	{
		fn = m_globalFnTable->Pop();

		if (!fn)
			break;

		scrOs << *fn;
	}
}

void ScrCodeTable::onReleaseStream(ScrOutStream &scrOs)
{
	scrOs << "CODE";
	scrOs.Write(emitFunctionCount());	//함수 개수를 내보냅니다
	emitDefaultFunctionTable(scrOs);
	emitFunctionTable(scrOs);
}

NoxUserCall *ScrCodeTable::emitDefaultFunctionTwoGlobal(FunctionTable &fnTable, int &uniqId)
{
	auto firstGlobal = std::make_unique<NoxFunction>("GLOBAL");

	firstGlobal->SetUniqueId(uniqId++);
	firstGlobal->PushOp(std::make_unique<NoxNativeOp>(NoxNativeOp::NativeReturn));
	fnTable.Push("FIRSTGLOBAL", std::move(firstGlobal));

	auto secondGlobal = std::make_unique<NoxFunction>("GLOBAL");

	secondGlobal->SetUniqueId(uniqId++);
	secondGlobal->PushNativeVar(1);
	secondGlobal->PushNativeVar(1);
	secondGlobal->PushNativeVar(1);
	secondGlobal->PushNativeVar(1);
    if (m_globalAllocSize)
    {
        if (m_globalAllocSize > 16384)
        {
            m_globalAllocSize = 16384;
            MY_PRINTC(MyCOut::Green) << "g-alloc size is more longer than 16384";
        }
        secondGlobal->PushNativeVar(m_globalAllocSize);
    }
	auto callLoader = new NoxUserCall("");

	secondGlobal->PushOp(std::unique_ptr<NoxOpcode>(callLoader));
	secondGlobal->PushOp(std::make_unique<NoxNativeOp>(NoxNativeOp::NativeReturn));
	fnTable.Push("SECONDGLOBAL", std::move(secondGlobal));
	return callLoader;
}

void ScrCodeTable::emitDefaultFunctionEUDLoader(FunctionTable &fnTable, int &uniqId, NoxUserCall *initCall)
{
    auto eudLoader = std::make_unique<EudLoaderFunction>();

    m_eudLoaderFn = eudLoader.get();
	eudLoader->SetUniqueId(uniqId++);
	initCall->SetLinkResult(eudLoader->UniqueId());
	eudLoader->DoReadBinary(load_binary_eud_loader);
	fnTable.Push(std::move(eudLoader));
}

void ScrCodeTable::emitDefaultFunctionEUDStack(FunctionTable &fnTable, int &uniqId)
{
	auto eudStack = std::make_unique<EudVariableBundle>(m_stackSize);

	eudStack->SetUniqueId(uniqId++);
	eudStack->PutGlobalData(*m_globalField);
	fnTable.Push(std::move(eudStack));
}

void ScrCodeTable::emitDefaultFunctionEUDCoreApiTable(FunctionTable &fnTable, int &uniqId)
{
	auto apiTableFn = std::make_unique<EudApiBundle>(stringFormat("CoreApi%sTable", RandomString::Generate(6)));

	apiTableFn->SetUniqueId(uniqId++);
	apiTableFn->DoReadBinary(load_binary_eud_core_api_table);
	fnTable.Push(std::move(apiTableFn));
}

void ScrCodeTable::emitDefaultFunctionEUDImplApi(FunctionTable &fnTable, int &uniqId)
{
	auto apiImpl = std::make_unique<EudApiBundle>(stringFormat("CoreApi%sImpl", RandomString::Generate(6)));

	apiImpl->SetUniqueId(uniqId++);
	apiImpl->DoReadBinary(load_binary_eud_core_api);
	fnTable.Push(std::move(apiImpl));
}

void ScrCodeTable::emitDefaultFunctionGlobalPointer(FunctionTable &fnTable, int &uniqId)
{
	if (!m_gpointerFn)
		MY_THROW() << "g-pointer is missing";

	m_gpointerFn->SetUniqueId(uniqId++);
	fnTable.Push(std::move(m_gpointerFn));
}

void ScrCodeTable::ensureValidGlobalEntry(FunctionTable &fnTable)
{
	auto firstGlobalFn = m_globalFnTable->Front();

	if (firstGlobalFn)
	{
		auto firstId = firstGlobalFn->UniqueId();

		if (!firstId)
			MY_THROW() << "error";

		if (fnTable.Count() != firstId->Link())
			MY_THROW() << "initial function unique id mismatched";
	}
}

//EMIT-EX
///(0) +GLOBAL
///(1) +GLOBAL
///(2) +InitialEUDLoaderXXXXXXXX
///(3) +EUDStackFieldXXXXXXXX
///(4) +EUDApiTableCopy
///(5) +EUDCoreApiImpl
///(6) +EUDGlobalXXXPointer
std::unique_ptr<FunctionTable> ScrCodeTable::createDefaultFunctionTable()
{
    int uniqId = 0;
	auto fnTable = std::make_unique<FunctionTable>();	
	auto callLoader = emitDefaultFunctionTwoGlobal(*fnTable, uniqId);

	if (!callLoader)
		MY_THROW() << "invalid callee";
	emitDefaultFunctionEUDLoader(*fnTable, uniqId, callLoader);
	emitDefaultFunctionEUDStack(*fnTable, uniqId);
	emitDefaultFunctionEUDCoreApiTable(*fnTable, uniqId);
	emitDefaultFunctionEUDImplApi(*fnTable, uniqId);
	emitDefaultFunctionGlobalPointer(*fnTable, uniqId);
	ensureValidGlobalEntry(*fnTable);
	return fnTable;
}

static constexpr int use_default_size = 1310720;
int ScrCodeTable::determineStackSize(BuildEnvironment &env)
{
    int nsize = 0;

    if (!env.GetOptionAsInt("CCStackSize", nsize))
        return use_default_size;

	return (nsize < 65536) ? use_default_size : nsize;
}

void ScrCodeTable::insertInitLoaderInvokeCode(const std::string &fname, NoxFunction &loader)
{
    if (fname.empty())
        return;

    NoxFunction *target = m_globalFnTable->Retrieve(fname);

    if (!target)
        MY_THROW() << "cannot find function " << fname;

    auto callOp = std::make_unique<NoxUserCall>("");

    callOp->SetLinkResult(loader.UniqueId());
    target->PushFrontOp(std::move(callOp));
}

void ScrCodeTable::PutOutputParam(CompileLinker &linker, BuildEnvironment &env)
{
    //TODO. 여기에서 ini 에 등록된 특정함수에 initial 함수 호출코드를 넣어줍니다//
	m_globalFnTable = linker.ReleaseGlobalFunctionTable();
	m_globalField = linker.ReleaseGlobalField();


	if (!m_globalFnTable)
		MY_THROW() << "got empty function table";

	m_stackSize = determineStackSize(env);
    if (!env.GetOptionAsInt("GALLOC_SIZE", m_globalAllocSize))
        m_globalAllocSize = 0;
	m_defaultFnTable = createDefaultFunctionTable();

    if (!m_eudLoaderFn)
        MY_THROW() << "eudLoaderFn==nullptr";

    env.DoPreorderEudLoaderCallTarget([this](const std::string &f) { insertInitLoaderInvokeCode(f, *m_eudLoaderFn); });
}

void ScrCodeTable::PutGPointerTable(std::shared_ptr<GPointerOffsetTable> &table)
{
	if (m_gpointerFn)
		MY_THROW() << "already exist yet";
	if (!table)
		MY_THROW() << "invalid g-pointer table";
	m_gpointerFn = std::make_unique<EudGlobalPointerPack>(table);
}

