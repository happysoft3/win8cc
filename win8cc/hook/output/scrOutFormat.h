
#ifndef SCR_OUT_FORMAT_H__
#define SCR_OUT_FORMAT_H__

#include "scrOutStream.h"
#include <memory>

class BuildEnvironment;

namespace std
{
	template<class _Elem,
		class _Traits>
		class basic_streambuf;
}

class ScrOutFormat : public ScrOutStream
{
	class Core;
private:
	std::unique_ptr<Core> m_core;
	using ff_buff = std::basic_streambuf<char>;
	ff_buff *m_oldBuffer;

public:
	ScrOutFormat();
	~ScrOutFormat();
	
private:
	bool resetBuffer();
	bool putHeaderData();
	virtual std::string loadOutUrl(BuildEnvironment &env);

public:
	//파일 먼저 생성
	bool Create(BuildEnvironment &env);
};

#endif

