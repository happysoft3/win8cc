
#include "scrStringTable.h"

ScrStringTable::ScrStringTable()
	: ScrOutStream()
{ }

ScrStringTable::~ScrStringTable()
{ }

void ScrStringTable::releaseStrings(ScrOutStream &scrOs, const std::string &str)
{
	scrOs.Write(str.length());
	scrOs.Write(str);
	scrOs << ScrOutStream::ENDL;
}

void ScrStringTable::onReleaseStream(ScrOutStream &scrOs)
{
	scrOs << "STRG";
	scrOs.Write(m_strTable.size());
	scrOs << ScrOutStream::ENDL;

	for (const auto &s : m_strTable)
	{
		releaseStrings(scrOs, s);
	}
}

void ScrStringTable::Append(const std::string &s)
{
	if (m_strMap.find(s) == m_strMap.cend())
	{
		m_strTable.push_back(s);
		m_strMap[s] = std::prev(m_strTable.end());
	}
}

void ScrStringTable::Replace(const std::string &prev, const std::string &change)
{
	auto strIterator = m_strMap.find(prev);

	if (strIterator == m_strMap.cend())
		return;

	if (m_strMap.find(change) != m_strMap.cend())
		return;

	auto tableIterator = strIterator->second;

	*tableIterator = change;
	m_strMap.erase(strIterator);
	m_strMap[change] = tableIterator;
}

void ScrStringTable::Erase(const std::string &s)
{
	auto strIterator = m_strMap.find(s);

	if (strIterator == m_strMap.cend())
		return;

	m_strTable.erase(strIterator->second);
	m_strMap.erase(strIterator);
}
