
#ifndef SCR_OUT_STREAM_H__
#define SCR_OUT_STREAM_H__

#include <ostream>

class ScrOutStream : public std::ostream
{
private:
	bool m_useDebug;

public:
	ScrOutStream();
	ScrOutStream(std::streambuf *buf);
	~ScrOutStream() override;

private:
	virtual void writeStreamPrivate(const char *const s, const size_t &length);
	template <bool IsArithmetic, class Ty>
	struct streamWriter;

	template <class Ty>
	struct streamWriter<true, Ty>
	{
		static void writeStream(ScrOutStream &output, const Ty &value)
		{
			union
			{
				Ty source;
				char data[sizeof(Ty)];
			} p;

			p.source = value;
			output.writeStreamPrivate(p.data, sizeof(Ty));
		}
	};

	template <class Cont>
	struct streamWriter<false, Cont>
	{
		static void writeStream(ScrOutStream &output, const Cont &cont)
		{
			output.writeStreamPrivate(cont.data(), cont.size());
		}
	};
	virtual void onReleaseStream(ScrOutStream &scrOs);

public:
	template <class Ty>
	void Write(const Ty &any)
	{
		streamWriter<std::is_arithmetic<Ty>::value, Ty>::writeStream(*this, any);
	}

	template <class Ty, class = typename std::enable_if<std::is_arithmetic<Ty>::value, Ty>::type>
	ScrOutStream &operator<<(const Ty &v)
	{
		Write(v);
		return *this;
	}

	ScrOutStream &operator<<(ScrOutStream &scrOs)
	{
		scrOs.onReleaseStream(*this);
		return *this;
	}

protected:
	void enableDebugMode()
	{
		m_useDebug = true;
	}
	bool isDebugModeOn(ScrOutStream &scrOs) const
	{
		return scrOs.m_useDebug;
	}

public:
	bool UseDebug() const
	{
		return m_useDebug;
	}
	ScrOutStream& __CLR_OR_THIS_CALL operator<<(ScrOutStream& (__cdecl *_Pfn)(ScrOutStream&))
	{	// call basic_ostream manipulator
		return ((*_Pfn)(*this));
	}
	static ScrOutStream &ENDL(ScrOutStream &scrOs);
};

#endif

