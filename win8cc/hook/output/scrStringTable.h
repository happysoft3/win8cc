
#ifndef SCR_STRING_TABLE_H__
#define SCR_STRING_TABLE_H__

#include "scrOutStream.h"
#include <string>
#include <list>
#include <map>

//Outcontext 같은 거 상속받아서, 파일에 쓰게 해야합니다
class ScrStringTable : public ScrOutStream
{
private:
	using str_table_ty = std::list<std::string>;
	str_table_ty m_strTable;
	using str_table_iter = str_table_ty::iterator;
	std::map<std::string, str_table_iter> m_strMap;

public:
	ScrStringTable();
	~ScrStringTable() override;

private:
	void releaseStrings(ScrOutStream &scrOs, const std::string &str);
	void onReleaseStream(ScrOutStream &scrOs) override;

public:
	void Append(const std::string &s);
	void Replace(const std::string &prev, const std::string &change);
	void Erase(const std::string &s);
};

#endif

