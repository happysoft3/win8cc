
#ifndef SCR_DEBUG_FORMAT_H__
#define SCR_DEBUG_FORMAT_H__

#include "scrOutFormat.h"

class BuildEnvironment;

class ScrDebugFormat : public ScrOutFormat
{
public:
	ScrDebugFormat();
	~ScrDebugFormat() override;

private:
	void writeStreamPrivate(const char *const s, const size_t &length) override;
    std::string loadDefaultUrl(const std::string &prefix, const std::string &dir);
	std::string loadOutUrl(BuildEnvironment &env) override;
};

#endif

