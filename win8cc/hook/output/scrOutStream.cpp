
#include "scrOutStream.h"
#include "common/utils/randomString.h"
#include "common/utils/stringhelper.h"
#include <iostream>

using namespace _StringHelper;

ScrOutStream::ScrOutStream()
	: std::ostream(std::cout.rdbuf())
{
	m_useDebug = false;
}

ScrOutStream::ScrOutStream(std::streambuf *buf)
	: std::ostream(buf)
{ }

ScrOutStream::~ScrOutStream()
{ }

void ScrOutStream::writeStreamPrivate(const char *const s, const size_t &length)
{
	write(s, length);
}

void ScrOutStream::onReleaseStream(ScrOutStream &scrOs)
{
	std::string rndGen = "XXXXXXXX";

	RandomString::Generate(rndGen);
	scrOs << "ERROR";
	std::cout << "don't calling this method" << std::endl;
	throw std::logic_error(stringFormat("[%s:%s:isn't error, but no effect]", __FUNCDNAME__, rndGen));
}

ScrOutStream &ScrOutStream::ENDL(ScrOutStream &scrOs)
{
    if (scrOs.m_useDebug)
        scrOs << "\r\n";
	scrOs.flush();
	return scrOs;
}

