
#include "scrOutFormat.h"
#include "hook/buildEnvironment.h"
#include <fstream>

class ScrOutFormat::Core
{
private:
	std::ofstream m_file;

public:
	Core(const std::string &url)
	{
		m_file = std::ofstream(url, std::ios::binary | std::ios::out);
	}
	std::ofstream *operator->()
	{
		return &m_file;
	}
};

constexpr char noxscr_header_key[] = "SCRIPT03";

ScrOutFormat::ScrOutFormat()
	: ScrOutStream()
{
	m_oldBuffer = nullptr;
}

ScrOutFormat::~ScrOutFormat()
{ }

bool ScrOutFormat::resetBuffer()
{
	if (m_oldBuffer)
	{
		set_rdbuf(m_oldBuffer);
		m_oldBuffer = nullptr;
	}
	return false;
}

bool ScrOutFormat::putHeaderData()
{
	(*this) << noxscr_header_key;

	if (good())
	{
		(*m_core)->flush();
		return true;
	}
	return resetBuffer();
}

std::string ScrOutFormat::loadOutUrl(BuildEnvironment &env)
{
	return env.OutTargetPath();
}

bool ScrOutFormat::Create(BuildEnvironment &env)
{
	std::string url = loadOutUrl(env);

	if (url.empty())
		return resetBuffer();

	std::unique_ptr<Core> core = std::make_unique<Core>(url);
	if (!(*core)->is_open())
		return resetBuffer();

	m_core = std::move(core);
	m_oldBuffer = rdbuf();
	set_rdbuf((*m_core)->rdbuf());

	return putHeaderData();
}

