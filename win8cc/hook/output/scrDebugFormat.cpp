
#include "scrDebugFormat.h"
#include "hook/buildEnvironment.h"
#include "common/utils/stringhelper.h"
#include <chrono>

using namespace _StringHelper;

static const std::vector<const char *> s_typeNameVector = {"??", "char", "short", "??", "int"};

ScrDebugFormat::ScrDebugFormat()
	: ScrOutFormat()
{
	enableDebugMode();
}

ScrDebugFormat::~ScrDebugFormat()
{ }

void ScrDebugFormat::writeStreamPrivate(const char *const s, const size_t &length)
{
	const char *tyName = nullptr;

	switch (length)
	{
	case sizeof(char):
	case sizeof(short):
	case sizeof(int):
		tyName = s_typeNameVector[length];
		break;

	default:
		(*this) << s;
		return;
	}
	union
	{
		int dwValue;
		char byteArray[4];
	} field = {};

	for (size_t u = 0; u < length; u++)
		field.byteArray[u] = s[u];
	(*this) << stringFormat("[%s: %d]", tyName, field.dwValue);
}

std::string ScrDebugFormat::loadDefaultUrl(const std::string &prefix, const std::string &dir)
{
    auto timeData = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    struct tm currentTime;

    localtime_s(&currentTime, &timeData);
    int year = currentTime.tm_year % 100;
    return stringFormat("%s%s_%02d%02d%02d%02d%02d%02d.txt", dir, prefix, year, currentTime.tm_mon, currentTime.tm_mday, currentTime.tm_hour, currentTime.tm_min, currentTime.tm_sec);
}

std::string ScrDebugFormat::loadOutUrl(BuildEnvironment &env)
{
	std::string debugFileUrl = env.GetOption("DEBUG_FILEURL");

    if (debugFileUrl.empty())
        return loadDefaultUrl("dbg", { });

	std::transform(debugFileUrl.cbegin(), debugFileUrl.cend(), debugFileUrl.begin(), [](const auto &c) { return (c == '/') ? '\\' : c; });
    if (debugFileUrl.back() == '\\')    //디렉터리 경로로서 간주합니다
        return loadDefaultUrl("dbg", debugFileUrl);
	return debugFileUrl;
}


