
#ifndef SCR_CODE_TABLE_H__
#define SCR_CODE_TABLE_H__

#include "scrOutStream.h"
#include <memory>

class CompileLinker;
class BuildEnvironment;
class FunctionTable;
class CDataSection;
class NoxUserCall;
class GPointerOffsetTable;
class NoxFunction;

class ScrCodeTable : public ScrOutStream
{
private:
	std::shared_ptr<FunctionTable> m_globalFnTable;
	std::unique_ptr<FunctionTable> m_defaultFnTable;
	std::unique_ptr<CDataSection> m_globalField;
	int m_stackSize;
    int m_globalAllocSize;
	std::unique_ptr<NoxFunction> m_gpointerFn;
    NoxFunction *m_eudLoaderFn;

public:
	ScrCodeTable();
	~ScrCodeTable() override;

private:
	int emitFunctionCount();
	void emitDefaultFunctionTable(ScrOutStream &scrOs);
	void emitFunctionTable(ScrOutStream &scrOs);
	void onReleaseStream(ScrOutStream &scrOs) override;

private:
	NoxUserCall *emitDefaultFunctionTwoGlobal(FunctionTable &fnTable, int &uniqId);
	void emitDefaultFunctionEUDLoader(FunctionTable &fnTable, int &uniqId, NoxUserCall *initCall);
	void emitDefaultFunctionEUDStack(FunctionTable &fnTable, int &uniqId);
	void emitDefaultFunctionEUDCoreApiTable(FunctionTable &fnTable, int &uniqId);
	void emitDefaultFunctionEUDImplApi(FunctionTable &fnTable, int &uniqId);
	void emitDefaultFunctionGlobalPointer(FunctionTable &fnTable, int &uniqId);

private:
	void ensureValidGlobalEntry(FunctionTable &fnTable);
	std::unique_ptr<FunctionTable> createDefaultFunctionTable();
	int determineStackSize(BuildEnvironment &env);
    void insertInitLoaderInvokeCode(const std::string &fname, NoxFunction &loader);

public:
	void PutOutputParam(CompileLinker &linker, BuildEnvironment &env);
	void PutGPointerTable(std::shared_ptr<GPointerOffsetTable> &table);
};

#endif

