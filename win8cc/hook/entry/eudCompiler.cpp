
#include "eudCompiler.h"
#include "hook/compilation.h"
#include "hook/buildEnvironment.h"
#include "common/utils/mydebug.h"
#include "common/utils/stringhelper.h"
#include "common/utils/runclock.h"

#include <iostream>

using namespace _StringHelper;

static constexpr auto app_version = "V1.121";

EudCompiler::EudCompiler()
{ }

EudCompiler::~EudCompiler()
{ }

void EudCompiler::loadBuildEnvironment()
{
	m_procName = __FUNCDNAME__;
	std::unique_ptr<BuildEnvironment> env = std::make_unique<BuildEnvironment>();
	std::shared_ptr<bool> error(new bool(false));

	env->PutExceptCapture([error = error](const std::exception &except) { *error = true; MY_PRINT() << except.what(); });
	env->ReadIni(m_makeFileUrl);

	m_env = std::move(env);
}

void EudCompiler::compilationUnit()
{
	m_procName = __FUNCSIG__;
	auto comp = std::make_unique<Compilation>();

	comp->PutEnvironment(std::move(m_env));
	comp->CompileAll();

	m_compile = std::move(comp);
}

void EudCompiler::linking()
{
	m_procName = __FUNCSIG__;
	m_compile->Linking();
}

void EudCompiler::createOutput()
{
	m_procName = __FUNCSIG__;
	m_compile->CreateOutput();
}

void EudCompiler::ShowInfo()
{
    MY_PRINTC(MyCOut::Pink) << "EUD-CC " << app_version;
    MY_PRINTC(MyCOut::Green) << "Build on " << __TIMESTAMP__;
}

bool EudCompiler::DoCompile(const std::string &makeFile)
{
    RunClock clock;

    clock.SetFormat("elapsed time %s clock per seconds");
	m_makeFileUrl = makeFile;

	try
	{
		loadBuildEnvironment();
		compilationUnit();
		linking();
		createOutput();
	}
	catch (const std::exception &except)
	{
        MY_PRINTC(MyCOut::Red) << "ERROR:" << m_procName << ':' << except.what();
		return false;
	}
    MY_PRINTC(MyCOut::Cyan) << clock.Get();
	return true;
}

