
#ifndef EUD_COMPILER_H__
#define EUD_COMPILER_H__

#include <string>
#include <memory>

class BuildEnvironment;
class Compilation;

class EudCompiler
{
private:
	std::string m_makeFileUrl;
	std::string m_procName;
	std::unique_ptr<BuildEnvironment> m_env;
	std::unique_ptr<Compilation> m_compile;

public:
	EudCompiler();
	~EudCompiler();

private:
	void loadBuildEnvironment();
	void compilationUnit();
	void linking();
	void createOutput();

public:
    void ShowInfo();
	bool DoCompile(const std::string &makeFile);
};

#endif

