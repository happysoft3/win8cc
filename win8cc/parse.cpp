// Copyright 2012 Rui Ueyama. Released under the MIT license.

/*
* Recursive descendent parser for C.
*/
#include "parse.h"
#include "vector.h"
#include "hook/ast/astVector.h"
#include "hook/ast/astDict.h"
#include "hook/ast/astMap.h"
#include "dict.h"
#include "map.h"
#include "error.h"
#include "debug.h"
#include "encoding.h"
#include "buffer.h"
#include "file.h"
#include "typePool.h"
#include "sourceLocation.h"
#include "8cc.h"
#include "hook/ast/astBuiltinType.h"
#include "hook/ast/astNode.h"
#include "hook/ast/astType.h"
#include "common/mmanager/memorySweeper.h"
#include "common/utils/stringhelper.h"

#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>

using namespace _StringHelper;

// The largest alignment requirement on x86-64. When we are allocating memory
// for an array whose type is unknown, the array will be aligned to this
// boundary.
#define MAX_ALIGN 16

#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))


// Objects representing basic types. All variables will be of one of these types
// or a derived type from one of them. Note that (typename){initializer} is C99
// feature to write struct literals.

//#define DECLARE_TYPE_INSTANCE(_typeId, _typeEnum, _tySize, _tyAlign, _tyUnsigned) \
//static Type s_##_typeId = {_typeEnum, _tySize, _tyAlign, _tyUnsigned}; \
//Type *_typeId = &s_##_typeId;
//
//DECLARE_TYPE_INSTANCE(type_void, KIND_VOID, 0, 0, false)
//DECLARE_TYPE_INSTANCE(type_bool, KIND_BOOL, sizeof(char), sizeof(char), true)
//DECLARE_TYPE_INSTANCE(type_char, KIND_CHAR, sizeof(char), sizeof(char), false)
//DECLARE_TYPE_INSTANCE(type_short, KIND_SHORT, sizeof(short), sizeof(short), false)
//DECLARE_TYPE_INSTANCE(type_int, KIND_INT, sizeof(int), sizeof(int), false)
//DECLARE_TYPE_INSTANCE(type_long, KIND_LONG, sizeof(long), sizeof(long), false)
//DECLARE_TYPE_INSTANCE(type_llong, KIND_LLONG, sizeof(long long), sizeof(long long), false)
//DECLARE_TYPE_INSTANCE(type_uchar, KIND_CHAR, sizeof(char), sizeof(char), true)
//DECLARE_TYPE_INSTANCE(type_ushort, KIND_SHORT, sizeof(short), sizeof(short), true)
//DECLARE_TYPE_INSTANCE(type_uint, KIND_INT, sizeof(int), sizeof(int), true)
//DECLARE_TYPE_INSTANCE(type_ulong, KIND_LONG, sizeof(long), sizeof(long), true) //DECLARE_TYPE_INSTANCE(type_ulong, KIND_LONG, 8, 8, true)
//DECLARE_TYPE_INSTANCE(type_ullong, KIND_LLONG, sizeof(long long), sizeof(long long), true)
//DECLARE_TYPE_INSTANCE(type_float, KIND_FLOAT, sizeof(float), sizeof(float), false)
//DECLARE_TYPE_INSTANCE(type_double, KIND_DOUBLE, sizeof(double), sizeof(double), false)
//DECLARE_TYPE_INSTANCE(type_ldouble, KIND_LDOUBLE, sizeof(long double), sizeof(long double), false)
//DECLARE_TYPE_INSTANCE(type_enum, KIND_ENUM, sizeof(int), sizeof(int), false)
//
//#undef DECLARE_TYPE_INSTANCE

class Case
{
private:
	int m_begin;
	int m_end;
	char *m_label;

public:
	Case(int beg, int end, char *label)
		: m_begin(beg), m_end(end), m_label(label)
	{ }

	int Begin() const
	{
		return m_begin;
	}
	int End() const
	{
		return m_end;
	}
	char *Label() const
	{
		return m_label;
	}
};

enum {
    S_TYPEDEF = 1,
    S_EXTERN,
    S_STATIC,
    S_AUTO,
    S_REGISTER,
};

enum {
    DECL_BODY = 1,
    DECL_PARAM,
    DECL_PARAM_TYPEONLY,
    DECL_CAST,
};

/*
* Source location
*/

CCParse::CCParse()
    : CCPreprocessor()
{
    m_toplevels = nullptr;
    m_tempnameCounter = 0;
    m_labelCounter = 0;
    m_staticLabelCounter = 0;
    m_globalenv = new AstMap;
    m_localenv = nullptr;
	m_sourceLocator = std::make_unique<SourceLocation>();
    m_tags = new AstMap;
	m_parseBuffer = std::make_unique<CCBufferAllocator>();
	m_parseSweep = std::make_unique<MemorySweeper>();
	m_typegen = std::make_unique<TypePool>();
}

CCParse::~CCParse()
{
    delete m_globalenv;
    delete m_tags;
}

/*
* Constructors
*/

char *CCParse::make_tempname() {
    return m_parseBuffer->Format(".T%d", m_tempnameCounter++);
}

char *CCParse::make_label() {
    return m_parseBuffer->Format(".L%d", m_labelCounter++);
}

char *CCParse::make_static_label(const char *name) {
    return m_parseBuffer->Format(".S%d.%s", m_staticLabelCounter++, name);
}

AstMap *CCParse::env() {
    return m_localenv ? m_localenv : m_globalenv;
}

template <class Ty, class... Args>
Ty *CCParse::makeAstNode(Args&&... args)
{
    Ty *ast = m_parseSweep->AllocateObject<Ty>(std::forward<Args>(args)...);

    ast->PutSourceLocation(*m_sourceLocator);
    return ast;
}

AstNode *CCParse::astLvarNode(AstType *ty, const std::string &name, AstVector *init)
{
    AstNode *r = makeAstNode<AstVariableNode>(ty, name, init, m_localenv);

    if (m_localvars)
        m_localvars->Push(r);

    return r;
}

AstNode *CCParse::astStaticLvarNode(AstType *ty, const char *name)
{
    LOGIC_ASSERT(m_localenv);
    return makeAstNode<AstGlobalVariableNode>(ty, name, make_static_label(name), m_localenv);
}

/*
* Predicates and kind checking routines
*/

void CCParse::expect(char id) {
    Token *tok = get();
    if (!is_keyword(tok, id))
        THROW_ERROR_TOKEN(tok, "'%c' expected, but got %s", id, CCHelper::ToString(tok));
}

AstType *CCParse::copy_incomplete_type(AstType *ty)
{
    if (!ty) return nullptr;

    //LOGIC_ASSERT(ty->IsKind(KIND_ARRAY));
    if (!ty->IsKind(KIND_ARRAY))
        return ty;

    AstArrayType *arrTy = static_cast<AstArrayType *>(ty);

    return (arrTy->GetLength() == -1) ? ty->Clone() : ty;
}

void CCParse::typeChecking(AstType *ty, AstNode *target, int cop)
{
    AstType *targetTy = target->GetType();

    target->PutSourceLocation(*m_sourceLocator);
    if (! ty->EvalSpec(target, cop))
        THROW_WARNING_NODE(target, "type mismatched %s<>%s", ty->Explain(), targetTy->Explain());
}

AstType *CCParse::get_typedef(const char *name)
{
    AstNode *node = reinterpret_cast<AstNode *>(env()->Get( name));

    return (node && node->IsKind(AST_TYPEDEF)) ? node->GetType() : nullptr;
}

bool CCParse::is_type(Token *tok) {
    if (tok->kind == TIDENT)
        return get_typedef(tok->sval) != nullptr;
    if (tok->kind != TKEYWORD)
        return false;
    switch (tok->id) {
#define op(x, y)
#define keyword(id, _, istype) case id: return istype;
#include "keyword.inc"
#undef keyword
#undef op
    default:
        return false;
    }
}

bool CCParse::next_token(int kind, Token **discard)
{
    Token *tok = get();
	if (is_keyword(tok, kind))
	{
		if (discard != nullptr)
			*discard = tok;
		return true;
	}
    unget_token(tok);
    return false;
}

void *CCParse::make_pair(void *first, void *second)
{
	auto r = m_parseSweep->AllocateRaw<void *>(sizeof(void *) * 2);
    r[0] = first;
    r[1] = second;
    return r;
}

/*
* Type conversion
*/

AstNode *CCParse::conv(AstNode *node) {
    if (!node)
        return nullptr;
    AstType *ty = node->GetType();
    switch (ty->Kind()) {
    case KIND_ARRAY:
        // C11 6.3.2.1p3: An array of T is converted to a pointer to T.
        //return ast_uop(AST_CONV, m_typegen->MakePtrType(ty->ptr), node);
        return makeAstNode<AstUnaryOpNode>(AST_CONV, m_typegen->MakeType<AstPointerType>(ty->GetPTR()), node);
    case KIND_FUNC:
        // C11 6.3.2.1p4: A function designator is converted to a pointer to the function.
        return makeAstNode<AstUnaryOpNode>(AST_ADDR, m_typegen->MakeType<AstPointerType>(ty), node);
    case KIND_SHORT: case KIND_CHAR: case KIND_BOOL:
        // C11 6.3.1.1p2: The integer promotions
        return makeAstNode<AstConvNode>(AstBuiltinType::IntType(), node);
    case KIND_INT:
        if (ty->BitSize() > 0)
            return makeAstNode<AstConvNode>(AstBuiltinType::IntType(), node);
    }
    return node;
}

AstNode *CCParse::wrap(AstType *t, AstNode *node)
{
    if (t->SameArithType(*node->GetType()))
        return node;

    return makeAstNode<AstUnaryOpNode>(AST_CONV, t, node);
}

// C11 6.3.1.8: Usual arithmetic conversions
AstType *CCParse::usual_arith_conv(AstType *t, AstType *u)
{
    LOGIC_ASSERT(t->IsArithtype());
    LOGIC_ASSERT(u->IsArithtype());
    if (t->Kind() < u->Kind()) {
        // Make t the larger type
        AstType *tmp = t;
        t = u;
        u = tmp;
    }
    if (t->IsFloatType())
        return t;
    LOGIC_ASSERT(t->IsIntType() && t->GetSize() >= AstBuiltinType::IntType()->GetSize());
    LOGIC_ASSERT(u->IsIntType() && u->GetSize() >= AstBuiltinType::IntType()->GetSize());
    if (t->GetSize() > u->GetSize())
        return t;
    LOGIC_ASSERT(t->GetSize() == u->GetSize());
    if (t->Unsigned() == u->Unsigned())
        return t;
    AstType *r = t->Clone();
    r->SwitchingUnsigned(true);
    return r;
}

static bool valid_pointer_binop(int op) {
    switch (op) {
    case '-': case '<': case '>': case OP_EQ:
    case OP_NE: case OP_GE: case OP_LE:
        return true;
    default:
        return false;
    }
}

AstNode *CCParse::binopLiteral(int op, AstNode *lhs, AstNode *rhs, AstType *altTy)
{
    AstNode *operand = makeAstNode<AstBinaryOpNode>(altTy, op, wrap(altTy, lhs), wrap(altTy, rhs));
    int val = operand->EvaluateIntExpr(nullptr);

    return makeAstNode<AstIntNode>(altTy, val);
}

AstNode *CCParse::binopLiteralFloat(int op, AstNode *lhs, AstNode *rhs, AstType *altTy)
{
    float lhsfval = static_cast<AstLiteralNode *>(lhs)->GetFloatValue();
    float rhsfval = static_cast<AstLiteralNode *>(rhs)->GetFloatValue();

    switch (op)
    {
    case '+': return makeAstNode<AstFloatNode>(altTy, lhsfval + rhsfval);
    case '-': return makeAstNode<AstFloatNode>(altTy, lhsfval - rhsfval);
    case '*': return makeAstNode<AstFloatNode>(altTy, lhsfval * rhsfval);
    case '/': return makeAstNode<AstFloatNode>(altTy, lhsfval / rhsfval);
    default: return nullptr;
    }
}

AstNode *CCParse::binop(int op, AstNode *lhs, AstNode *rhs)
{
    AstType *lhsTy = lhs->GetType();
    AstType *rhsTy = rhs->GetType();

    if (lhsTy->IsKind( KIND_PTR ) && rhsTy->IsKind( KIND_PTR)) {
		if (!valid_pointer_binop(op))
			THROW_ERROR(m_sourceLocator->LoadProfileString(), "invalid pointer arith");
        // C11 6.5.6.9: Pointer subtractions have type ptrdiff_t.
        if (op == '-')
            return makeAstNode<AstBinaryOpNode>(AstBuiltinType::LongType(), op, lhs, rhs);
        // C11 6.5.8.6, 6.5.9.3: Pointer comparisons have type int.
        return makeAstNode<AstBinaryOpNode>(AstBuiltinType::IntType(), op, lhs, rhs);
    }
    if (lhsTy->IsKind(KIND_PTR))
        return makeAstNode<AstBinaryOpNode>(lhsTy, op, lhs, rhs);
    if (rhsTy->IsKind( KIND_PTR))
        return makeAstNode<AstBinaryOpNode>(rhsTy, op, rhs, lhs);
    if (!lhsTy->IsArithtype()) lhs->ErrorIfNotArithtype();       //assert(is_arithtype(lhs->ty));
    if (!rhsTy->IsArithtype()) rhs->ErrorIfNotArithtype();       //assert(is_arithtype(rhs->ty));
    AstType *r = usual_arith_conv(lhsTy, rhsTy);

    if (lhs->IsKind(AST_LITERAL) && rhs->IsKind(AST_LITERAL))
    {
        AstNode *computed = nullptr;

        if (r->IsIntType())
            computed = binopLiteral(op, lhs, rhs, r);
        else if (r->IsFloatType())
            computed = binopLiteralFloat(op, lhs, rhs, r);

        if (computed)
            return computed;
    }
    return makeAstNode<AstBinaryOpNode>(r, op, wrap(r, lhs), wrap(r, rhs));
}

/*
* Integer constant expression
*/

int CCParse::read_intexpr()
{
    AstNode *n = read_conditional_expr();

    LOGIC_ASSERT(n != nullptr);
    return n->EvaluateIntExpr(nullptr);
}

/*
* Numeric literal
*/

static AstType *read_int_suffix(char *s) {
    if (!_stricmp(s, "u"))
        return AstBuiltinType::UIntType();
    if (!_stricmp(s, "l"))
        return AstBuiltinType::LongType();
    if (!_stricmp(s, "ul") || !_stricmp(s, "lu"))
        return AstBuiltinType::ULongType();
    if (!_stricmp(s, "ll"))
        return AstBuiltinType::LLongType();
    if (!_stricmp(s, "ull") || !_stricmp(s, "llu"))
        return AstBuiltinType::ULLongType();
    return NULL;
}

AstNode *CCParse::read_int(Token *tok) {
    char *s = tok->sval;
    char *end;
    long v = !_strnicmp(s, "0b", 2)
        ? strtoul(s + 2, &end, 2) : strtoul(s, &end, 0);
    AstType *ty = read_int_suffix(end);
    if (ty)
        return makeAstNode<AstIntNode>(ty, v);
    if (*end != '\0')
        THROW_ERROR_TOKEN(tok, "invalid character '%c': %s", *end, s);

    // C11 6.4.4.1p5: Decimal constant type is int, long, or long long.
    // In 8cc, long and long long are the same size.
    bool base10 = (*s != '0');
    if (base10) {
        ty = !(v & ~(long)INT_MAX) ? AstBuiltinType::IntType() : AstBuiltinType::LongType();
        return makeAstNode<AstIntNode>(ty, v);
    }
    // Octal or hexadecimal constant type may be unsigned.
    ty = !(v & ~(unsigned long)INT_MAX) ? AstBuiltinType::IntType()
        : !(v & ~(unsigned long)UINT_MAX) ? AstBuiltinType::UIntType()
        : !(v & ~(unsigned long)LONG_MAX) ? AstBuiltinType::LongType()
        : AstBuiltinType::ULongType();
    return makeAstNode<AstIntNode>(ty, v);
}

AstNode *CCParse::read_float(Token *tok) {
    char *s = tok->sval;
    char *end;
    double v = strtod(s, &end);
    // C11 6.4.4.2p4: The default type for flonum is double.
    if (!_stricmp(end, "l"))
        return makeAstNode<AstFloatNode>(AstBuiltinType::LDoubleType(), static_cast<float>(v));
    if (!_stricmp(end, "f"))
        return makeAstNode<AstFloatNode>(AstBuiltinType::FloatType(), static_cast<float>(v) );
    if (*end != '\0')
        THROW_ERROR_TOKEN(tok, "invalid character '%c': %s", *end, s);
    return makeAstNode<AstFloatNode>(AstBuiltinType::DoubleType(), static_cast<float>(v) );
}

AstNode *CCParse::read_number(Token *tok) {
    char *s = tok->sval;
    bool isfloat = strpbrk(s, ".pP") || (_strnicmp(s, "0x", 2) && strpbrk(s, "eE"));
    return isfloat ? read_float(tok) : read_int(tok);
}

/*
* Sizeof operator
*/

AstType *CCParse::read_sizeof_operand_sub() {
    Token *tok = get();
    if (is_keyword(tok, '(') && is_type(peek())) {
        AstType *r = read_cast_type();
        expect(')');
        return r;
    }
    unget_token(tok);
    return read_unary_expr()->GetType();
}

AstNode *CCParse::read_sizeof_operand() {
    AstType *ty = read_sizeof_operand_sub();
    // Sizeof on void or function type is GNU extension
    int size = (ty->IsKind( KIND_VOID ) || ty->IsKind ( KIND_FUNC)) ? 1 : ty->GetSize();
    LOGIC_ASSERT(0 <= size);
    return makeAstNode<AstIntNode>(AstBuiltinType::ULongType(), size);
}

/*
* Alignof operator
*/

AstNode *CCParse::read_alignof_operand() {
    expect('(');
    AstType *ty = read_cast_type();
    expect(')');
    return makeAstNode<AstIntNode>(AstBuiltinType::ULongType(), ty->GetAlign());
}

/*
* Function arguments
*/

AstVector *CCParse::read_func_args(AstVector *params) {
    AstVector *args = m_parseSweep->AllocateObject<AstVector>();
    int i = 0;
	Token *restTok = nullptr;

    for (;;) {
        if (next_token(')')) break;
		if (next_token(',', &restTok))	//예외 처리기 추가 2022-11-28 09:26
			THROW_ERROR_TOKEN(restTok, "token error ','");
		AstNode *pure = read_assignment_expr();
        AstNode *arg = conv(pure);
        AstType *paramtype;
        if (i < params->Length()) {
            paramtype = reinterpret_cast<AstType *>(params->Get(i++));
        }
        else {
            paramtype = arg->GetType()->IsFloatType() ? AstBuiltinType::DoubleType() :
                arg->GetType()->IsIntType() ? AstBuiltinType::IntType() : arg->GetType();
        }
        paramtype->EnsureAssignable(arg->GetType());
        typeChecking(paramtype, pure);
		if (!paramtype->IsKind(arg->GetType()->Kind()))
			arg = makeAstNode<AstConvNode>(paramtype, arg);
        args->Push(arg);
        Token *tok = get();
        if (is_keyword(tok, ')')) break;
        if (!is_keyword(tok, ','))
            THROW_ERROR_TOKEN(tok, "unexpected token: '%s'", CCHelper::ToString(tok));
    }
	if (args->Length() != params->Length())
		THROW_WARNING(m_sourceLocator->LoadProfileString(),
			"too many or few arguments to function (def: %d<> got: %d)", params->Length(), args->Length());
    return args;
}

AstNode *CCParse::read_funcall(AstNode *fp) {
    if (fp->IsKind( AST_ADDR ) && static_cast<AstUnaryOpNode *>(fp)->Operand()->IsKind( AST_FUNCDESG))
    {
        AstFunctionDesgNode *desg = static_cast<AstFunctionDesgNode *>( static_cast<AstUnaryOpNode *>(fp)->Operand() );
        AstVector *args = read_func_args(desg->GetType()->Params() );

        return makeAstNode<AstFunctionCall>(desg->GetType(), desg->FunctionName(), args);
    }
    AstVector *args = read_func_args(fp->GetType()->GetPTR()->Params());

    return makeAstNode<AstFunctionPtrCall>(fp, args);
}

/*
* _Generic
*/

CCVector *CCParse::read_generic_list(AstNode **defaultexpr) {
	CCVector *r = m_parseSweep->AllocateObject<CCVector>();
    for (;;) {
        if (next_token(')'))
            return r;
        Token *tok = peek();
        if (next_token(KDEFAULT)) {
            if (*defaultexpr)
                THROW_ERROR_TOKEN(tok, "default expression specified twice");
            expect(':');
            *defaultexpr = read_assignment_expr();
        }
        else {
            AstType *ty = read_cast_type();
            expect(':');
            AstNode *expr = read_assignment_expr();
            r->Push( make_pair(ty, expr));
        }
        m_sourceLocator->Markup(peek());
        next_token(',');
    }
}

AstNode *CCParse::read_generic() {
    expect('(');
    Token *tok = peek();
    AstNode *contexpr = read_assignment_expr();

    m_sourceLocator->Markup(peek());
    expect(',');
    AstNode *defaultexpr = nullptr;
    CCVector *list = read_generic_list(&defaultexpr);
    for (int i = 0; i < list->Length(); i++) {
        void **pair = reinterpret_cast<void **>(list->Get(i));
        AstType *ty = reinterpret_cast<AstType *>(pair[0]);
        AstNode *expr = reinterpret_cast<AstNode *>(pair[1]);

        if (contexpr->GetType()->Compatible(ty))
            return expr;
    }
    if (!defaultexpr)
        THROW_ERROR_TOKEN(tok, "no matching generic selection for %s: %s", contexpr->Explain(), contexpr->GetType()->Explain());
    return defaultexpr;
}

/*
* _Static_assert
*/

void CCParse::read_static_assert() {
    expect('(');
    int val = read_intexpr();
    expect(',');
    Token *tok = get();
    if (tok->kind != TSTRING)
        THROW_ERROR_TOKEN(tok, "string expected as the second argument for _Static_assert, but got %s", CCHelper::ToString(tok));
    expect(')');
    expect(';');
    if (!val)
		THROW_ERROR_TOKEN(tok, "_Static_assert failure: %s", tok->sval);
}

/*
* Expression
*/

AstNode *CCParse::read_var_or_func(const char *name) {
    AstNode *v = reinterpret_cast<AstNode *>(env()->Get( name));
    if (!v) {
        Token *tok = peek();
        if (!is_keyword(tok, '('))
			THROW_ERROR_TOKEN(tok, "undefined variable: %s", name);
        AstType *ty = m_typegen->MakeType<AstFunctionType>(AstBuiltinType::IntType(), m_parseSweep->AllocateObject<AstVector>(), true, false);
        THROW_WARNING_TOKEN(tok, "assume returning int: %s()", name);
        return makeAstNode<AstFunctionDesgNode>(ty, name);
    }
    if (v->GetType()->IsKind( KIND_FUNC))
        return makeAstNode<AstFunctionDesgNode>(v->GetType(), name);
    return v;
}

static int get_compound_assign_op(Token *tok) {
    if (tok->kind != TKEYWORD)
        return 0;
    switch (tok->id) {
    case OP_A_ADD: return '+';
    case OP_A_SUB: return '-';
    case OP_A_MUL: return '*';
    case OP_A_DIV: return '/';
    case OP_A_MOD: return '%';
    case OP_A_AND: return '&';
    case OP_A_OR:  return '|';
    case OP_A_XOR: return '^';
    case OP_A_SAL: return OP_SAL;
    case OP_A_SAR: return OP_SAR;
    case OP_A_SHR: return OP_SHR;
    default: return 0;
    }
}

AstNode *CCParse::read_stmt_expr() {
    AstNode *r = read_compound_stmt();
    AstVector *stmts = static_cast<AstCompoundStmtNode *>(r)->Statements();

    expect(')');
    AstType *rtype = AstBuiltinType::VoidType();
    if (stmts->Length() > 0) {
        AstNode *lastexpr = reinterpret_cast<AstNode *>(stmts->Tail());
        if (lastexpr->GetType())
            rtype = lastexpr->GetType();
    }
    r->SetType(rtype);
    return r;
}

static AstType *char_type(int enc) {
    switch (enc) {
    case ENC_NONE:
    case ENC_WCHAR:
        return AstBuiltinType::IntType();
    case ENC_CHAR16:
        return AstBuiltinType::UShortType();
    case ENC_CHAR32:
        return AstBuiltinType::UIntType();
    }
    THROW_ERROR_NOPOS("internal error");
    return nullptr;
}

AstNode *CCParse::read_primary_expr() {
    Token *tok = get();
    if (!tok) return NULL;
    if (is_keyword(tok, '(')) {
        if (next_token('{'))
            return read_stmt_expr();
        AstNode *r = read_expr();
        expect(')');
        return r;
    }
    if (is_keyword(tok, KGENERIC)) {
        return read_generic();
    }
    switch (tok->kind) {
    case TIDENT:
        return read_var_or_func(tok->sval);
    case TNUMBER:
        return read_number(tok);
    case TCHAR:
        return makeAstNode<AstIntNode>(char_type(tok->enc), tok->c);
    case TSTRING:
        return makeAstNode<AstStringNode>(tok->enc, tok->sval, tok->slen);
    case TKEYWORD:
        unget_token(tok);
        return NULL;
    default:
        THROW_ERROR_NOPOS("internal error: unknown token kind: %d", tok->kind);
        return NULL;
    }
}

AstNode *CCParse::read_subscript_expr(AstNode *node) {
    Token *tok = peek();
    AstNode *sub = read_expr();
    if (!sub)
        THROW_ERROR_TOKEN(tok, "subscription expected");
    expect(']');
    AstNode *t = binop('+', conv(node), conv(sub));
    return makeAstNode<AstUnaryOpNode>(AST_DEREF, t->GetType()->GetPTR(), t);
}

AstNode *CCParse::read_postfix_expr_tail(AstNode *node) {
    if (!node) return nullptr;
    for (;;) {
        if (next_token('(')) {
            Token *tok = peek();
            node = conv(node);
            AstType *t = node->GetType();

            do
            {
                if (t->IsKind(KIND_PTR)) // || !t->GetPTR()->IsKind(KIND_FUNC))
                {
                    if (t->GetPTR()->IsKind(KIND_FUNC))
                        break;
                }
                THROW_ERROR_TOKEN(tok, "function expected, but got %s", node->Explain());
            }
            while (false);
            node = read_funcall(node);
            continue;
        }
        if (next_token('[')) {
			///배열이나 포인터가 아닌 변수를 배열로 참조할 때, 충돌나는 문제로 추가//
			///2022-11-07 17:37
            node->EnsureSafeDereference();
			////
            node = read_subscript_expr(node);
            continue;
        }
        if (next_token('.')) {
            node = read_struct_field(node);
            continue;
        }
        if (next_token(OP_ARROW)) {
            if (!node->GetType()->IsKind( KIND_PTR) )
                THROW_ERROR_NOPOS("pointer type expected, but got %s %s",
					node->GetType()->Explain(), node->Explain());
            node = makeAstNode<AstUnaryOpNode>(AST_DEREF, node->GetType()->GetPTR(), node);
            node = read_struct_field(node);
            continue;
        }
        Token *tok = peek();
        if (next_token(OP_INC) || next_token(OP_DEC)) {
            node->EnsureLValue();
            int op = is_keyword(tok, OP_INC) ? OP_POST_INC : OP_POST_DEC;
            return makeAstNode<AstUnaryOpNode>(op, node->GetType(), node);
        }
        return node;
    }
}

AstNode *CCParse::read_postfix_expr() {
    AstNode *node = read_primary_expr();
    return read_postfix_expr_tail(node);
}

AstNode *CCParse::read_unary_incdec(int op) {
    AstNode *operand = read_unary_expr();
    operand = conv(operand);
    operand->EnsureLValue();
    return makeAstNode<AstUnaryOpNode>(op, operand->GetType(), operand);
}

AstNode *CCParse::read_label_addr(Token *tok) {
    // [GNU] Labels as values. You can get the address of the a label
    // with unary "&&" operator followed by a label name.
    Token *tok2 = get();
    if (tok2->kind != TIDENT)
        THROW_ERROR_TOKEN(tok, "label name expected after &&, but got %s", CCHelper::ToString(tok2));
    AstNode *r = makeAstNode<AstLabelAddrNode>(tok2->sval);
    m_gotos->Push(r);
    return r;
}

AstNode *CCParse::read_unary_addr() {
    AstNode *operand = read_cast_expr();
    if (operand->IsKind(AST_FUNCDESG) )
        return conv(operand);
    operand->EnsureLValue();
    return makeAstNode<AstUnaryOpNode>(AST_ADDR, m_typegen->MakeType<AstPointerType>(operand->GetType()), operand);
}

AstNode *CCParse::read_unary_deref(Token *tok) {
    AstNode *operand = conv(read_cast_expr());
    if (!operand->GetType()->IsKind( KIND_PTR) )
        THROW_ERROR_TOKEN(tok, "pointer type expected, but got %s", operand->Explain());
    if (operand->GetType()->GetPTR()->IsKind(KIND_FUNC))
        return operand;
    return makeAstNode<AstUnaryOpNode>(AST_DEREF, operand->GetType()->GetPTR(), operand);
}

AstNode *CCParse::read_unary_minus() {
    AstNode *expr = read_cast_expr();

    expr->EnsureArithtype();
    if (expr->GetType()->IsIntType())
        return binop('-', conv(makeAstNode<AstIntNode>(expr->GetType(), 0)), conv(expr));
    return binop('-', makeAstNode<AstFloatNode>(expr->GetType(), 0.0f), expr);
}

AstNode *CCParse::read_unary_bitnot(Token *tok) {
    AstNode *expr = read_cast_expr();
    expr = conv(expr);
    if (!expr->GetType()->IsIntType())
        THROW_ERROR_TOKEN(tok, "invalid use of ~: %s", expr->Explain());
    return makeAstNode<AstUnaryOpNode>('~', expr->GetType(), expr);
}

AstNode *CCParse::read_unary_lognot() {
    AstNode *operand = read_cast_expr();
    operand = conv(operand);
    return makeAstNode<AstUnaryOpNode>('!', AstBuiltinType::IntType(), operand);
}

AstNode *CCParse::read_unary_expr() {
    Token *tok = get();
    if (tok->kind == TKEYWORD) {
        switch (tok->id) {
        case KSIZEOF: return read_sizeof_operand();
        case KALIGNOF: return read_alignof_operand();
        case OP_INC: return read_unary_incdec(OP_PRE_INC);
        case OP_DEC: return read_unary_incdec(OP_PRE_DEC);
        case OP_LOGAND: return read_label_addr(tok);
        case '&': return read_unary_addr();
        case '*': return read_unary_deref(tok);
        case '+': return read_cast_expr();
        case '-': return read_unary_minus();
        case '~': return read_unary_bitnot(tok);
        case '!': return read_unary_lognot();
        }
    }
    unget_token(tok);
    return read_postfix_expr();
}

AstNode *CCParse::read_compound_literal(AstType *ty) {
    char *name = make_label();
    AstVector *init = read_decl_init(ty);

    return astLvarNode(ty, name, init);
}

AstType *CCParse::read_cast_type() {
    return read_abstract_declarator(read_decl_spec(nullptr));
}

AstNode *CCParse::read_cast_expr() {
    Token *tok = get();
    if (is_keyword(tok, '(') && is_type(peek())) {
        AstType *ty = read_cast_type();
        expect(')');
        if (is_keyword(peek(), '{')) {
            AstNode *node = read_compound_literal(ty);
            return read_postfix_expr_tail(node);
        }
        return makeAstNode<AstUnaryOpNode>(OP_CAST, ty, read_cast_expr());
    }
    unget_token(tok);
    return read_unary_expr();
}

AstNode *CCParse::read_multiplicative_expr() {
    AstNode *node = read_cast_expr();
    for (;;) {
        if (next_token('*'))      node = binop('*', conv(node), conv(read_cast_expr()));
        else if (next_token('/')) node = binop('/', conv(node), conv(read_cast_expr()));
        else if (next_token('%')) node = binop('%', conv(node), conv(read_cast_expr()));
        else    return node;
    }
}

AstNode *CCParse::read_additive_expr() {
    AstNode *node = read_multiplicative_expr();
    for (;;) {
        if (next_token('+')) node = binop('+', conv(node), conv(read_multiplicative_expr()));
        else if (next_token('-')) node = binop('-', conv(node), conv(read_multiplicative_expr()));
        else    return node;
    }
}

AstNode *CCParse::read_shift_expr() {
    AstNode *node = read_additive_expr();

    for (;;) {
        int op;
        if (next_token(OP_SAL))
            op = OP_SAL;
        else if (next_token(OP_SAR))
            op = node->GetType()->Unsigned() ? OP_SHR : OP_SAR;
        else
            break;
        AstNode *right = read_additive_expr();
        node->EnsureIntType();
        right->EnsureIntType();
        node = makeAstNode<AstBinaryOpNode>(node->GetType(), op, conv(node), conv(right));
    }
    return node;
}

AstNode *CCParse::read_relational_expr() {
    AstNode *node = read_shift_expr();
    for (;;) {
        if (next_token('<'))   node = binop('<', conv(node), conv(read_shift_expr()));
        else if (next_token('>'))   node = binop('<', conv(read_shift_expr()), conv(node));
        else if (next_token(OP_LE)) node = binop(OP_LE, conv(node), conv(read_shift_expr()));
        else if (next_token(OP_GE)) node = binop(OP_LE, conv(read_shift_expr()), conv(node));
        else    return node;
        node->SetType(AstBuiltinType::IntType());
    }
}

AstNode *CCParse::read_equality_expr() {
    AstNode *node = read_relational_expr();
    AstNode *r;
    if (next_token(OP_EQ)) {
        r = binop(OP_EQ, conv(node), conv(read_equality_expr()));
    }
    else if (next_token(OP_NE)) {
        r = binop(OP_NE, conv(node), conv(read_equality_expr()));
    }
    else {
        return node;
    }
    r->SetType(AstBuiltinType::IntType());
    return r;
}

AstNode *CCParse::read_bitand_expr() {
    AstNode *node = read_equality_expr();
    while (next_token('&'))
        node = binop('&', conv(node), conv(read_equality_expr()));
    return node;
}

AstNode *CCParse::read_bitxor_expr() {
    AstNode *node = read_bitand_expr();
    while (next_token('^'))
        node = binop('^', conv(node), conv(read_bitand_expr()));
    return node;
}

AstNode *CCParse::read_bitor_expr() {
    AstNode *node = read_bitxor_expr();
    while (next_token('|'))
        node = binop('|', conv(node), conv(read_bitxor_expr()));
    return node;
}

AstNode *CCParse::read_logand_expr() {
    AstNode *node = read_bitor_expr();
    while (next_token(OP_LOGAND))
        node = makeAstNode<AstBinaryOpNode>(AstBuiltinType::IntType(), OP_LOGAND, node, read_bitor_expr());
    return node;
}

AstNode *CCParse::read_logor_expr() {
    AstNode *node = read_logand_expr();
    while (next_token(OP_LOGOR))
        node = makeAstNode<AstBinaryOpNode>(AstBuiltinType::IntType(), OP_LOGOR, node, read_logand_expr());
    return node;
}

AstNode *CCParse::do_read_conditional_expr(AstNode *cond) {
    AstNode *then = conv(read_comma_expr());
    expect(':');
    AstNode *els = conv(read_conditional_expr());
    // [GNU] Omitting the middle operand is allowed.
    AstType *t = then ? then->GetType() : cond->GetType();
    AstType *u = els->GetType();
    // C11 6.5.15p5: if both types are arithemtic type, the result
    // type is the result of the usual arithmetic conversions.
    if (t->IsArithtype() && u->IsArithtype()) {
        AstType *r = usual_arith_conv(t, u);
        return makeAstNode<AstTernaryNode>(r, cond, (then ? wrap(r, then) : nullptr), wrap(r, els));
    }
    return makeAstNode<AstTernaryNode>(u, cond, then, els);
}

AstNode *CCParse::read_conditional_expr()
{
    AstNode *cond = read_logor_expr();
    if (!next_token('?'))
        return cond;
    return do_read_conditional_expr(cond);
}

AstNode *CCParse::read_assignment_expr() {
    AstNode *node = read_logor_expr();
    Token *tok = get();
    if (!tok)
        return node;
    if (is_keyword(tok, '?'))
        return do_read_conditional_expr(node);
    int cop = get_compound_assign_op(tok);

    if (is_keyword(tok, '=') || cop) {
		AstNode *pure = read_assignment_expr();
        typeChecking(node->GetType(), pure, cop);

        if (is_keyword(tok, '=') || cop)
            node->EnsureLValue();
		AstNode *value = conv(pure);
        AstNode *right = cop ? binop(cop, conv(node), value) : value;

        return makeAstNode<AstBinaryOpNode>(node->GetType(), '=', node, right);
    }
    unget_token(tok);
    return node;
}

AstNode *CCParse::read_comma_expr() {
    AstNode *node = read_assignment_expr();
    while (next_token(',')) {
        m_sourceLocator->Markup(peek());
        AstNode *expr = read_assignment_expr();
        node = makeAstNode<AstBinaryOpNode>(expr->GetType(), ',', node, expr);
    }
    return node;
}

AstNode *CCParse::read_expr() {
    Token *tok = peek();
    AstNode *r = read_comma_expr();
    if (!r)
        THROW_ERROR_TOKEN(tok, "expression expected");
    return r;
}

AstNode *CCParse::read_expr_opt() {
    return read_comma_expr();
}

/*
* Struct or union
*/

AstNode *CCParse::read_struct_field(AstNode *struc) {
    if (!struc->GetType()->IsKind( KIND_STRUCT) )
        THROW_ERROR_NOPOS("struct expected, but got %s", struc->Explain());
    Token *name = get();
    if (name->kind != TIDENT)
        THROW_ERROR_NOPOS("field name expected, but got %s", CCHelper::ToString(name));
    AstType *field = static_cast<AstStructType *>( struc->GetType())->GetFieldType( name->sval);
    if (!field)
        THROW_ERROR_NOPOS("struct has no such field: %s", CCHelper::ToString(name));
    return makeAstNode<AstStructNode>(field, struc, name->sval);
}

char *CCParse::read_rectype_tag() {
    Token *tok = get();
    if (tok->kind == TIDENT)
        return tok->sval;
    unget_token(tok);
    return nullptr;
}

static int compute_padding(int offset, int align) {
    return (offset % align == 0) ? 0 : align - offset % align;
}

void CCParse::squash_unnamed_struct(AstDict *dict, AstType *unnamed, int offset)
{
    AstStructType *strtTy = static_cast<AstStructType *>(unnamed);
    auto keys = strtTy->GetFieldKeys(); // unnamed->fields->Keys();
    for (int i = 0; i < static_cast<int>(keys.size()) ; i++) {
        auto name = keys[i]; //reinterpret_cast<char *>(keys->Get(i));
        AstType *tyBuff = strtTy->GetFieldType(name.c_str());// reinterpret_cast<AstType *>(unnamed->fields->Get(name));

		if (!tyBuff)
			THROW_ERROR_NOPOS("tyBuff == null");

        AstType *t = tyBuff->Clone(); // m_typegen->Copy(*tyBuff);

        t->IncreaseOffset( offset );
        dict->Put( name, t);
    }
}

int CCParse::read_bitsize(const char *name, AstType *ty) {
    if (!ty->IsIntType())
        THROW_ERROR_NOPOS("non-integer type cannot be a bitfield: %s", CCHelper::ToString(ty));
    Token *tok = peek();
    int r = read_intexpr();
    int maxsize = ty->IsKind( KIND_BOOL) ? 1 : ty->GetSize() * 8;
    if (r < 0 || maxsize < r)
        THROW_ERROR_TOKEN(tok, "invalid bitfield size for %s: %d", CCHelper::ToString(ty), r);
    if (r == 0 && name != nullptr)
        THROW_ERROR_TOKEN(tok, "zero-width bitfield needs to be unnamed: %s", name);
    return r;
}

CCVector *CCParse::read_rectype_fields_sub() {
	CCVector *r = m_parseSweep->AllocateObject<CCVector>();
    for (;;) {
        if (next_token(KSTATIC_ASSERT)) {
            read_static_assert();
            continue;
        }
        if (!is_type(peek()))
            break;
        AstType *basetype = read_decl_spec(NULL);
        if (basetype->IsKind(KIND_STRUCT) && next_token(';')) {
            r->Push( make_pair(NULL, basetype));
            continue;
        }
        for (;;) {
            char *name = NULL;
            AstType *fieldtype = read_declarator(&name, basetype, NULL, DECL_PARAM_TYPEONLY);

            fieldtype->EnsureNotVoid();
            fieldtype = fieldtype->Clone(); // m_typegen->Copy(*fieldtype);
            fieldtype->SetBitSize( next_token(':') ? read_bitsize(name, fieldtype) : -1 );
            r->Push(make_pair(name, fieldtype));
            if (next_token(','))
                continue;
            if (is_keyword(peek(), '}'))
                THROW_WARNING_TOKEN(peek(), "missing ';' at the end of field list");
            else
                expect(';');
            break;
        }
    }
    expect('}');
    return r;
}

static void fix_rectype_flexible_member(CCVector *fields) {
    for (int i = 0; i < fields->Length(); i++) {
        void **pair = reinterpret_cast<void **>(fields->Get(i));
        char *name = reinterpret_cast<char *>(pair[0]);
        AstType *ty = reinterpret_cast<AstType *>(pair[1]);
        if (!ty->IsKind(KIND_ARRAY))
            continue;

        AstArrayType *arrTy = static_cast<AstArrayType *>(ty);

        if (arrTy->GetLength() == -1) {
            if (i != fields->Length() - 1)
                THROW_ERROR_NOPOS("flexible member may only appear as the last member: %s %s", ty->Explain(), name);
            if (fields->Length() == 1)
                THROW_ERROR_NOPOS("flexible member with no other fields: %s %s", ty->Explain(), name);
            arrTy->SetLength(0);
            arrTy->SetSize(0);
        }
    }
}

static void finish_bitfield(int *off, int *bitoff) {
    *off += (*bitoff + 7) / 8;
    *bitoff = 0;
}

AstDict *CCParse::update_struct_offset(int *rsize, int *align, CCVector *fields) {
    int off = 0, bitoff = 0;
	AstDict *r = m_parseSweep->AllocateObject<AstDict>();
    for (int i = 0; i < fields->Length(); i++) {
        void **pair = reinterpret_cast<void **>(fields->Get(i));
        char *name = reinterpret_cast<char *>(pair[0]);
        AstType *fieldtype = reinterpret_cast<AstType *>(pair[1]);
        // C11 6.7.2.1p14: Each member is aligned to its natural boundary.
        // As a result the entire struct is aligned to the largest among its members.
        // Unnamed fields will never be accessed, so they shouldn't be taken into account
        // when calculating alignment.

        if (name)
            *align = MAX(*align, fieldtype->GetAlign());

        if (name == NULL && fieldtype->IsKind(KIND_STRUCT)) {
            // C11 6.7.2.1p13: Anonymous struct
            finish_bitfield(&off, &bitoff);
            off += compute_padding(off, fieldtype->GetAlign());
            squash_unnamed_struct(r, fieldtype, off);
            off += fieldtype->GetSize();
            continue;
        }
        if (fieldtype->BitSize() == 0) {
            // C11 6.7.2.1p12: The zero-size bit-field indicates the end of the
            // current run of the bit-fields.
            finish_bitfield(&off, &bitoff);
            off += compute_padding(off, fieldtype->GetAlign());
            bitoff = 0;
            continue;
        }
        if (fieldtype->BitSize() > 0) {
            int bit = fieldtype->GetSize() * 8;
            int room = bit - (off * 8 + bitoff) % bit;
            if (fieldtype->BitSize() <= room) {
                fieldtype->SetOffset( off );
                fieldtype->SetBitOffset( bitoff );
            }
            else {
                finish_bitfield(&off, &bitoff);
                off += compute_padding(off, fieldtype->GetAlign());
                fieldtype->SetOffset(off);
                fieldtype->SetBitOffset( 0 );
            }
            bitoff += fieldtype->BitSize();
        }
        else {
            finish_bitfield(&off, &bitoff);
            off += compute_padding(off, fieldtype->GetAlign());
            fieldtype->SetOffset(off);
            off += fieldtype->GetSize();
        }
        if (name)
            r->Put( name, fieldtype);
    }
    finish_bitfield(&off, &bitoff);
    *rsize = off + compute_padding(off, *align);
    return r;
}

AstDict *CCParse::update_union_offset(int *rsize, int *align, CCVector *fields) {
    int maxsize = 0;
	AstDict *r = m_parseSweep->AllocateObject<AstDict>();
    for (int i = 0; i < fields->Length(); i++) {
        void **pair = reinterpret_cast<void **>(fields->Get(i));
        char *name = reinterpret_cast<char *>(pair[0]);
        AstType *fieldtype = reinterpret_cast<AstType *>(pair[1]);
        maxsize = MAX(maxsize, fieldtype->GetSize());
        *align = MAX(*align, fieldtype->GetAlign());
        if (name == nullptr && fieldtype->IsKind( KIND_STRUCT) ) {
            squash_unnamed_struct(r, fieldtype, 0);
            continue;
        }
        LOGIC_ASSERT(fieldtype->IsKind(KIND_STRUCT));
        static_cast<AstStructType *>(fieldtype)->SetOffset(0);
        if (fieldtype->BitSize() >= 0)
            fieldtype->SetBitOffset( 0 );
        if (name)
            r->Put( name, fieldtype);
    }
    *rsize = maxsize + compute_padding(maxsize, *align);
    return r;
}

AstDict *CCParse::read_rectype_fields(int *rsize, int *align, bool is_struct) {
    if (!next_token('{'))
        return NULL;
    CCVector *fields = read_rectype_fields_sub();
    fix_rectype_flexible_member(fields);
    if (is_struct)
        return update_struct_offset(rsize, align, fields);
    return update_union_offset(rsize, align, fields);
}

AstType *CCParse::read_rectype_def(bool is_struct) {
    char *tag = read_rectype_tag();
    AstType *r;

    if (tag)
    {
        r = reinterpret_cast<AstType *>(m_tags->Get( tag));
        while (r)
        {
            if (!r->IsKind(KIND_ENUM))
            {
                LOGIC_ASSERT(r->IsKind(KIND_STRUCT));
                if (static_cast<AstStructType *>(r)->IsStruct() == is_struct)
                    break;
            }
            THROW_ERROR_NOPOS("declarations of %s does not match", tag);
            break;
        }

        if (!r) {
            r = m_typegen->MakeType<AstStructType>(is_struct);
            m_tags->Put( tag, r);
        }
    }
    else {
        r = m_typegen->MakeType<AstStructType>(is_struct);
    }
    int size = 0, align = 1;
    AstDict *fields = read_rectype_fields(&size, &align, is_struct);

    r->SetAlign(align);
    if (fields)
    {
        static_cast<AstStructType *>(r)->SetField(fields);
        r->SetSize(size);
    }
    return r;
}

AstType *CCParse::read_struct_def() {
    return read_rectype_def(true);
}

AstType *CCParse::read_union_def() {
    return read_rectype_def(false);
}

/*
* Enum
*/

AstType *CCParse::read_enum_def() {
    char *tag = nullptr;
    Token *tok = get();

    // Enum is handled as a synonym for int. We only check if the enum
    // is declared.
    if (tok->kind == TIDENT) {
        tag = tok->sval;
        tok = get();
    }
    if (tag) {
        AstType *ty = reinterpret_cast<AstType *>(m_tags->Get( tag));
        if (ty && !ty->IsKind( KIND_ENUM))
            THROW_ERROR_TOKEN(tok, "declarations of %s does not match", tag);
    }
    if (!is_keyword(tok, '{')) {
        if (!tag || !m_tags->Get( tag))
            THROW_ERROR_TOKEN(tok, "enum tag %s is not defined", tag);
        unget_token(tok);
        return AstBuiltinType::IntType();
    }
    if (tag)
        m_tags->Put( tag, AstBuiltinType::EnumType());

    int val = 0;
    for (;;) {
        tok = get();
        if (is_keyword(tok, '}'))
            break;
        if (tok->kind != TIDENT)
            THROW_ERROR_TOKEN(tok, "identifier expected, but got %s", CCHelper::ToString(tok));
        char *name = tok->sval;

        if (next_token('='))
            val = read_intexpr();
        AstNode *constval = makeAstNode<AstIntNode>(AstBuiltinType::IntType(), val++);
        env()->Put( name, constval);
        if (next_token(','))
            continue;
        if (next_token('}'))
            break;
        THROW_ERROR_TOKEN(peek(), "',' or '}' expected, but got %s", CCHelper::ToString(peek()));
    }
    return AstBuiltinType::IntType();
}

/*
* Initializer
*/

void CCParse::assign_string(AstVector *inits, AstType *ty, const char *p, int off) {
    LOGIC_ASSERT(ty->IsKind(KIND_ARRAY));
    AstArrayType *realTy = static_cast<AstArrayType *>(ty);
    int tyLength = realTy->GetLength();

    if (tyLength == -1)
    {
        int toLength = strlen(p) + 1;

        realTy->SetLength(toLength);
        ty->SetSize(toLength);
    }
    int i = 0;
    for (; i < tyLength && *p; i++)
        inits->Push(makeAstNode<AstInitializerNode>(makeAstNode<AstIntNode>(AstBuiltinType::CharType(), *p++), AstBuiltinType::CharType(), off + i));
    for (; i < tyLength; i++)
        inits->Push(makeAstNode<AstInitializerNode>(makeAstNode<AstIntNode>(AstBuiltinType::CharType(), 0), AstBuiltinType::CharType(), off + i));
}

template <class T, typename std::enable_if<std::is_integral<T>::value, bool>::type = true>
struct ParseIntType;

template <>
struct ParseIntType<char>
{
	static AstType *GetType()
	{
		return AstBuiltinType::CharType();
	}
};

template <>
struct ParseIntType<short>
{
	static AstType *GetType()
	{
		return AstBuiltinType::ShortType();
	}
};

template <class T>
void CCParse::makeStringInitializer(AstVector *inits, const AstNode *str, int off)
{
    const AstStringNode *realNode = static_cast<const AstStringNode *>(str);
    AstArrayType *realTy = static_cast<AstArrayType *>(str->GetType());
	const T *p = reinterpret_cast<const T *>(realNode->StringValue());
	auto initTy = ParseIntType<T>::GetType();
	int i = 0, length = realTy->GetLength();

    for (; i < length && *p; ++i)
        inits->Push(makeAstNode<AstInitializerNode>(makeAstNode<AstIntNode>(initTy, *p++), initTy, off + (i*sizeof(T))));
		//inits->Push(ast_init(ast_inttype(initTy, *p++), initTy, off + (i * sizeof(T))));
    for (; i < length; ++i)
        inits->Push(makeAstNode<AstInitializerNode>(makeAstNode<AstIntNode>(initTy, 0), initTy, off + (i*sizeof(T))));
		//inits->Push(ast_init(ast_inttype(initTy, 0), initTy, off + (i * sizeof(T))));
}

void CCParse::assignStringEx(AstVector *inits, AstType *ty, const AstNode *str, int off)
{
    LOGIC_ASSERT(ty->IsKind(KIND_ARRAY));
    AstArrayType *realTy = static_cast<AstArrayType *>(ty);

    if (realTy->GetLength() == -1)
	{
        realTy->SetLengthIfArray(str->GetType());
        ty->SetSize(str->GetType()->GetSize());
	}
    AstType *realTyPtr = realTy->GetPTR();
	LOGIC_ASSERT(realTyPtr != nullptr);
	AstType *candidate = nullptr;

	switch (realTyPtr->Kind())
	{
	case KIND_CHAR: makeStringInitializer<char>(inits, str, off); break;
	case KIND_SHORT: makeStringInitializer<short>(inits, str, off); break;
	default: THROW_ERROR_NOPOS("internal error");
	}
}

bool CCParse::maybe_read_brace() {
    return next_token('{');
}

void CCParse::maybe_skip_comma() {
    next_token(',');
}

void CCParse::skip_to_brace() {
    for (;;) {
        if (next_token('}'))
            return;
        if (next_token('.')) {
            get();
            expect('=');
        }
        Token *tok = peek();
        AstNode *ignore = read_assignment_expr();
        if (!ignore)
            return;
        THROW_WARNING_TOKEN(tok, "excessive initializer: %s", ignore->Explain());
        maybe_skip_comma();
    }
}

void CCParse::read_initializer_elem(AstVector *inits, AstType *ty, int off, bool designated) {
    next_token('=');
    if (ty->IsKind(KIND_ARRAY) || ty->IsKind(KIND_STRUCT)) {
        read_initializer_list(inits, ty, off, designated);
    }
    else if (next_token('{')) {
        read_initializer_elem(inits, ty, off, true);
        expect('}');
    }
    else {
        AstNode *expr = conv(read_assignment_expr());

        typeChecking(ty, expr);
        ty->EnsureAssignable(expr->GetType());
        inits->Push( makeAstNode<AstInitializerNode>(expr, ty, off));
    }
}

static int comp_init(const void *p, const void *q) {
    int x = (*(AstInitializerNode **)p)->InitOffset();
    int y = (*(AstInitializerNode **)q)->InitOffset();
    if (x < y) return -1;
    if (x > y) return 1;
    return 0;
}

//static void sort_inits(CCVector *inits) {
//    qsort(inits->Body(), inits->Length(), sizeof(void *), comp_init);
//}

void CCParse::read_struct_initializer_sub(AstVector *inits, AstStructType *ty, int off, bool designated) {
    bool has_brace = maybe_read_brace();
    std::vector<std::string> keys = ty->GetFieldKeys();
    int i = 0;
    for (;;) {
        Token *tok = get();
        if (is_keyword(tok, '}')) {
            if (!has_brace)
                unget_token(tok);
            return;
        }
        std::string fieldname;
        AstType *fieldtype;
        if ((is_keyword(tok, '.') || is_keyword(tok, '[')) && !has_brace && !designated) {
            unget_token(tok);
            return;
        }
        if (is_keyword(tok, '.')) {
            tok = get();
            if (!tok || tok->kind != TIDENT)
                THROW_ERROR_TOKEN(tok, "malformed desginated initializer: %s", CCHelper::ToString(tok));
            fieldname = tok->sval;
            fieldtype = ty->GetFieldType(fieldname.c_str());
            if (!fieldtype)
                THROW_ERROR_TOKEN(tok, "field does not exist: %s", CCHelper::ToString(tok));
            keys = ty->GetFieldKeys();
            i = 0;
            while (i < static_cast<int>(keys.size())  ) {
                auto s = keys.at(i++); // reinterpret_cast<char *>(keys->Get(i++));
                if (s == fieldname) // strcmp(fieldname, s) == 0)
                    break;
            }
            designated = true;
        }
        else {
            unget_token(tok);
            if (i == keys.size() )
                break;
            fieldname = keys[i++].c_str(); //reinterpret_cast<char *>(keys->Get(i++));
            fieldtype = ty->GetFieldType( fieldname.c_str() );
        }
        read_initializer_elem(inits, fieldtype, off + fieldtype->Offset(), designated);
        maybe_skip_comma();
        designated = false;
        if (!ty->IsStruct())
            break;
    }
    if (has_brace)
        skip_to_brace();
}

void CCParse::read_struct_initializer(AstVector *inits, AstStructType *ty, int off, bool designated) {
    read_struct_initializer_sub(inits, ty, off, designated);
    inits->Sorting(comp_init);
}

void CCParse::read_array_initializer_sub(AstVector *inits, AstArrayType *ty, int off, bool designated) {
    bool has_brace = maybe_read_brace();
    bool flexible = (ty->GetLength() <= 0);
    int elemsize = ty->GetPTR()->GetSize();
    int i;
    for (i = 0; flexible || i < ty->GetLength(); i++) {
        Token *tok = get();
        if (is_keyword(tok, '}')) {
            if (!has_brace)
                unget_token(tok);
            goto finish;
        }
        if ((is_keyword(tok, '.') || is_keyword(tok, '[')) && !has_brace && !designated) {
            unget_token(tok);
            return;
        }
        if (is_keyword(tok, '[')) {
            Token *tok = peek();
            int idx = read_intexpr();
            if (idx < 0 || (!flexible && ty->GetLength() <= idx))
                THROW_ERROR_TOKEN(tok, "array designator exceeds array bounds: %d", idx);
            i = idx;
            expect(']');
            designated = true;
        }
        else {
            unget_token(tok);
        }
        read_initializer_elem(inits, ty->GetPTR(), off + elemsize * i, designated);
        maybe_skip_comma();
        designated = false;
    }
    if (has_brace)
        skip_to_brace();
finish:
    if (ty->GetLength() < 0) {
        ty->SetLength(i);
        ty->SetSize( elemsize * i );
    }
}

void CCParse::read_array_initializer(AstVector *inits, AstArrayType *ty, int off, bool designated) {
    read_array_initializer_sub(inits, ty, off, designated);
    //sort_inits(inits);
    inits->Sorting(comp_init);
}

void CCParse::read_initializer_list(AstVector *inits, AstType *ty, int off, bool designated) {
    Token *tok = get();
    if (ty->IsString())
    {
        if (tok->kind == TSTRING) {
			assignStringEx(inits, ty, makeAstNode<AstStringNode>(tok->enc, tok->sval, tok->slen), off);
            return;
        }
        if (is_keyword(tok, '{') && peek()->kind == TSTRING) {
            tok = get();
            assign_string(inits, ty, tok->sval, off);
            expect('}');
            return;
        }
    }
    unget_token(tok);
    if (ty->IsKind( KIND_ARRAY)) {
        read_array_initializer(inits, static_cast<AstArrayType *>(ty), off, designated);
    }
    else if (ty->IsKind( KIND_STRUCT)) {
        read_struct_initializer(inits, static_cast<AstStructType *>(ty), off, designated);
    }
    else {
        AstArrayType *arraytype = m_typegen->MakeType<AstArrayType>(ty, 1);

        read_array_initializer(inits, arraytype, off, designated);
    }
}

AstVector *CCParse::read_decl_init(AstType *ty) {
	AstVector *r = m_parseSweep->AllocateObject<AstVector>();
    if (is_keyword(peek(), '{') || ty->IsString()) {
        read_initializer_list(r, ty, 0, false);
    }
    else {
		AstNode *pure = read_assignment_expr();
        typeChecking(ty, pure);
		AstNode *init = conv(pure);

        if (init->GetType()->IsArithtype() && init->GetType()->Kind() != ty->Kind())
            init = makeAstNode<AstConvNode>(ty, init);
		r->Push(makeAstNode<AstInitializerNode>(init, ty, 0));
    }
    return r;
}

/*
* Declarator
*
* C's syntax for declaration is not only hard to read for humans but also
* hard to parse for hand-written parsers. Consider the following two cases:
*
*   A: int *x;
*   B: int *x();
*
* A is of type pointer to int, but B is not a pointer type B is of type
* function returning a pointer to an integer. The meaning of the first half
* of the declaration ("int *" part) is different between them.
*
* In 8cc, delcarations are parsed by two functions: read_declarator
* and read_declarator_tail. The former function parses the first half of a
* declaration, and the latter parses the (possibly nonexistent) parentheses
* of a function or an array.
*/

AstType *CCParse::read_func_param(char **name, bool optional) {
    int sclass = 0;
    AstType *basety = AstBuiltinType::IntType();
    if (is_type(peek())) {
        basety = read_decl_spec(&sclass);
    }
    else if (optional) {
        THROW_ERROR_TOKEN(peek(), "type expected, but got %s", CCHelper::ToString(peek()));
    }
    AstType *ty = read_declarator(name, basety, nullptr, optional ? DECL_PARAM_TYPEONLY : DECL_PARAM);
    // C11 6.7.6.3p7: Array of T is adjusted to pointer to T
    // in a function parameter list.
    if (ty->IsKind(KIND_ARRAY))
    {
        //return m_typegen->MakePtrType(ty->ptr);
        return m_parseSweep->AllocateObject<AstPointerType>(ty->GetPTR());
    }
    // C11 6.7.6.3p8: Function is adjusted to pointer to function
    // in a function parameter list.
    if (ty->IsKind(KIND_FUNC))
        return m_parseSweep->AllocateObject<AstPointerType>(ty);
    return ty;
}

// Reads an ANSI-style prototyped function parameter list.
void CCParse::read_declarator_params(AstVector *types, AstVector *vars, bool *ellipsis) {
    bool typeonly = !vars;
    *ellipsis = false;
    for (;;) {
        Token *tok = peek();
        if (next_token(KELLIPSIS)) {
            if (types->Length() == 0)
                THROW_ERROR_TOKEN(tok, "at least one parameter is required before \"...\"");
            expect(')');
            *ellipsis = true;
            return;
        }
        char *name;
        AstType *ty = read_func_param(&name, typeonly);

        ty->EnsureNotVoid();
        types->Push( ty);
        if (!typeonly)
            vars->Push( astLvarNode(ty, name));
        tok = get();
        if (is_keyword(tok, ')'))
            return;
        if (!is_keyword(tok, ','))
            THROW_ERROR_TOKEN(tok, "comma expected, but got %s", CCHelper::ToString(tok));
    }
}

// Reads a K&R-style un-prototyped function parameter list.
void CCParse::read_declarator_params_oldstyle(AstVector *vars) {
    for (;;) {
        Token *tok = get();
        if (tok->kind != TIDENT)
            THROW_ERROR_TOKEN(tok, "identifier expected, but got %s", CCHelper::ToString(tok));
        vars->Push(astLvarNode(AstBuiltinType::IntType(), tok->sval));
        if (next_token(')'))
            return;
        if (!next_token(','))
            THROW_ERROR_TOKEN(tok, "comma expected, but got %s", CCHelper::ToString(get()));
    }
}

AstType *CCParse::read_func_param_list(AstVector *paramvars, AstType *rettype) {
    // C11 6.7.6.3p10: A parameter list with just "void" specifies that
    // the function has no parameters.
    Token *tok = get();
    if (is_keyword(tok, KVOID) && next_token(')'))
    {
        return m_parseSweep->AllocateObject<AstFunctionType>(rettype, m_parseSweep->AllocateObject<AstVector>(), false, false);
    }

    // C11 6.7.6.3p14: K&R-style un-prototyped declaration or
    // function definition having no parameters.
    // We return a type representing K&R-style declaration here.
    // If this is actually part of a declartion, the type will be fixed later.
    if (is_keyword(tok, ')'))
        return m_parseSweep->AllocateObject<AstFunctionType>(rettype, m_parseSweep->AllocateObject<AstVector>(), true, true);

    unget_token(tok);

    Token *tok2 = peek();
    if (next_token(KELLIPSIS))
        THROW_ERROR_TOKEN(tok2, "at least one parameter is required before \"...\"");
    if (is_type(peek())) {
        bool ellipsis;
        AstVector *paramtypes = m_parseSweep->AllocateObject<AstVector>();
        read_declarator_params(paramtypes, paramvars, &ellipsis);
        return m_parseSweep->AllocateObject<AstFunctionType>(rettype, paramtypes, ellipsis, false);
    }
    if (!paramvars)
        THROW_ERROR_TOKEN(tok, "invalid function definition");
    read_declarator_params_oldstyle(paramvars);
    AstVector *paramtypes = m_parseSweep->AllocateObject<AstVector>();
    for (int i = 0; i < paramvars->Length(); i++)
        paramtypes->Push( AstBuiltinType::IntType() );
    return m_parseSweep->AllocateObject<AstFunctionType>(rettype, paramtypes, false, true);
}

AstType *CCParse::read_declarator_array(AstType *basety) {
    int len;
    if (next_token(']')) {
        len = -1;
    }
    else {
        len = read_intexpr();
        expect(']');
    }
    Token *tok = peek();
    AstType *t = read_declarator_tail(basety, nullptr);
    if (t->IsKind(KIND_FUNC))
        THROW_ERROR_TOKEN(tok, "array of functions");
    return m_parseSweep->AllocateObject<AstArrayType>(t, len);
}

AstType *CCParse::read_declarator_func(AstType *basety, AstVector *param) {
    if (basety->IsKind(KIND_FUNC))
		THROW_ERROR_NOPOS("function returning a function");
    if (basety->IsKind(KIND_ARRAY))
		THROW_ERROR_NOPOS("function returning an array");
    return read_func_param_list(param, basety);
}

AstType *CCParse::read_declarator_tail(AstType *basety, AstVector *params) {
    if (next_token('['))
        return read_declarator_array(basety);
    if (next_token('('))
        return read_declarator_func(basety, params);
    return basety;
}

void CCParse::skip_type_qualifiers() {
    while (next_token(KCONST) || next_token(KVOLATILE) || next_token(KRESTRICT));
}

// C11 6.7.6: Declarators
AstType *CCParse::read_declarator(char **rname, AstType *basety, AstVector *params, int ctx) {
    if (next_token('(')) {
        // '(' is either beginning of grouping parentheses or of a function parameter list.
        // If the next token is a type name, a parameter list must follow.
        if (is_type(peek()))
            return read_declarator_func(basety, params);
        // If not, it's grouping. In that case we have to read from outside.
        // For example, consider int (*)(), which is "pointer to function returning int".
        // We have only read "int" so far. We don't want to pass "int" to
        // a recursive call, or otherwise we would get "pointer to int".
        // Here, we pass a dummy object to get "pointer to <something>" first,
        // continue reading to get "function returning int", and then combine them.
        AstType *stub = m_parseSweep->AllocateObject<AstStubType>();
        AstType *t = read_declarator(rname, stub, params, ctx);
        expect(')');
        //*stub = *read_declarator_tail(basety, params); //컴파일 오류나서 아래것으로 변경함
        auto repStub = read_declarator_tail(basety, params)->Clone();

        if (t->GetPTR() == stub)
        {
            t->SetPTR(repStub);
            m_parseSweep->Delete(stub);
        }
        return t;
    }
    if (next_token('*')) {
        skip_type_qualifiers();
        return read_declarator(rname, m_parseSweep->AllocateObject<AstPointerType>(basety), params, ctx);
    }
    Token *tok = get();
    if (tok->kind == TIDENT) {
        if (ctx == DECL_CAST)
            THROW_ERROR_TOKEN(tok, "identifier is not expected, but got %s", CCHelper::ToString(tok));
        *rname = tok->sval;
        return read_declarator_tail(basety, params);
    }
    if (ctx == DECL_BODY || ctx == DECL_PARAM)
        THROW_ERROR_TOKEN(tok, "identifier, ( or * are expected, but got %s", CCHelper::ToString(tok));
    unget_token(tok);
    return read_declarator_tail(basety, params);
}

// C11 6.7.7: Type names
// read_abstract_declarator reads a type name.
// A type name is a declaration that omits the identifier.
// A few examples are int* (pointer to int), int() (function returning int),
// int*() (function returning pointer to int),
// or int(*)() (pointer to function returning int). Used for casting.
AstType *CCParse::read_abstract_declarator(AstType *basety) {
    return read_declarator(nullptr, basety, nullptr, DECL_CAST);
}

/*
* typeof()
*/

AstType *CCParse::read_typeof() {
    expect('(');
    AstType *r = is_type(peek())
        ? read_cast_type()
        : read_comma_expr()->GetType();
    expect(')');
    return r;
}

/*
* Declaration specifier
*/

static bool is_poweroftwo(int x) {
    // If there's only one bit set in x, the value is a power of 2.
    return (x <= 0) ? false : !(x & (x - 1));
}

int CCParse::read_alignas() {
    // C11 6.7.5. Valid form of _Alignof is either _Alignas(type-name) or
    // _Alignas(constant-expression).
    expect('(');
    int r = is_type(peek())
        ? read_cast_type()->GetAlign()
        : read_intexpr();
    expect(')');
    return r;
}

AstType *CCParse::read_decl_spec(int *rsclass) {
    int sclass = 0;
    Token *tok = peek();
    if (!is_type(tok))
        THROW_ERROR_TOKEN(tok, "type name expected, but got %s", CCHelper::ToString(tok));

    AstType *usertype = nullptr;
    enum {
        kvoid = 1, kbool, kchar, kint, kfloat, kdouble
	} kind = {};
    enum {
        kshort = 1, klong, kllong
	} size = {};
    enum {
        ksigned = 1, kunsigned
	} sig = {};
    int align = -1;

    for (;;) {
        tok = get();
        if (tok->kind == EOF)
		THROW_ERROR_TOKEN(tok, "premature end of input");
        if (kind == 0 && tok->kind == TIDENT && !usertype) {
            AstType *def = get_typedef(tok->sval);
            if (def) {
                if (usertype) goto err;
                usertype = def;
                goto errcheck;
            }
        }
        if (tok->kind != TKEYWORD) {
            unget_token(tok);
            break;
        }
        switch (tok->id) {
        case KTYPEDEF:  if (sclass) goto err; sclass = S_TYPEDEF; break;
        case KEXTERN:   if (sclass) goto err; sclass = S_EXTERN; break;
        case KSTATIC:   if (sclass) goto err; sclass = S_STATIC; break;
        case KAUTO:     if (sclass) goto err; sclass = S_AUTO; break;
        case KREGISTER: if (sclass) goto err; sclass = S_REGISTER; break;
        case KCONST:    break;
        case KVOLATILE: break;
        case KINLINE:   break;
        case KNORETURN: break;
        case KVOID:     if (kind) goto err; kind = kvoid; break;
        case KBOOL:     if (kind) goto err; kind = kbool; break;
        case KCHAR:     if (kind) goto err; kind = kchar; break;
        case KINT:      if (kind) goto err; kind = kint; break;
        case KFLOAT:    if (kind) goto err; kind = kfloat; break;
        case KDOUBLE:   if (kind) goto err; kind = kdouble; break;
        case KSIGNED:   if (sig) goto err; sig = ksigned; break;
        case KUNSIGNED: if (sig) goto err; sig = kunsigned; break;
        case KSHORT:    if (size) goto err; size = kshort; break;
        case KSTRUCT:   if (usertype) goto err; usertype = read_struct_def(); break;
        case KUNION:    if (usertype) goto err; usertype = read_union_def(); break;
        case KENUM:     if (usertype) goto err; usertype = read_enum_def(); break;
        case KALIGNAS:
        {
            int val = read_alignas();
            if (val < 0)
                THROW_ERROR_TOKEN(tok, "negative alignment: %d", val);
            // C11 6.7.5p6: alignas(0) should have no effect.
            if (val == 0)
                break;
            if (align == -1 || val < align)
                align = val;
            break;
        }
        case KLONG:
        {
            if (size == 0) size = klong;
            else if (size == klong) size = kllong;
            else goto err;
            break;
        }
        case KTYPEOF:
        {
            if (usertype) goto err;
            usertype = read_typeof();
            break;
        }
        default:
            unget_token(tok);
            goto done;
        }
errcheck:
        if (kind == kbool && (size != 0 && sig != 0))
            goto err;
        if (size == kshort && (kind != 0 && kind != kint))
            goto err;
        if (size == klong && (kind != 0 && kind != kint && kind != kdouble))
            goto err;
        if (sig != 0 && (kind == kvoid || kind == kfloat || kind == kdouble))
            goto err;
        if (usertype && (kind != 0 || size != 0 || sig != 0))
            goto err;
    }
done:
    if (rsclass)
        *rsclass = sclass;
    if (usertype)
        return usertype;
    if (align != -1 && !is_poweroftwo(align))
        THROW_ERROR_TOKEN(tok, "alignment must be power of 2, but got %d", align);
    AstType *ty;
    switch (kind) {
    case kvoid:   ty = AstBuiltinType::VoidType(); goto end;
    case kbool:   ty = m_typegen->MakeType<AstType>(KIND_BOOL, 0,0, false); goto end;
    case kchar:   ty = m_typegen->MakeType<AstType>(KIND_CHAR, sizeof(char), sizeof(char), sig == kunsigned); goto end;
    case kfloat:  ty = m_typegen->MakeType<AstType>(KIND_FLOAT, sizeof(float), sizeof(float), false); goto end;
    case kdouble: ty = m_typegen->MakeType<AstType>(size == klong ? KIND_LDOUBLE : KIND_DOUBLE, sizeof(double), sizeof(double), false); goto end;   //!FIXME!//longdouble에 관해 처리누락
    default: break;
    }
    switch (size) {
    case kshort: ty = m_typegen->MakeType<AstType>(KIND_SHORT, sizeof(short), sizeof(short), sig == kunsigned); goto end;
    case klong:  ty = m_typegen->MakeType<AstType>(KIND_LONG, sizeof(long), sizeof(long), sig == kunsigned); goto end;
    case kllong: ty = m_typegen->MakeType<AstType>(KIND_LLONG, sizeof(long long), sizeof(long long), sig == kunsigned); goto end;
    default:     ty = m_typegen->MakeType<AstType>(KIND_INT, sizeof(int), sizeof(int), sig == kunsigned); goto end;
    }
    THROW_ERROR_NOPOS("internal error: kind: %d, size: %d", kind, size);
end:
    if (align != -1)
        ty->SetAlign(align);
    return ty;
err:
    THROW_ERROR_TOKEN(tok, "type mismatch: %s", CCHelper::ToString(tok));
    return NULL;
}

/*
* Declaration
*/

void CCParse::read_static_local_var(AstType *ty, const char *name) {
    AstNode *var = astStaticLvarNode(ty, name);
    AstVector *init = nullptr;
    if (next_token('=')) {
        AstMap *orig = m_localenv;
        m_localenv = nullptr;
        init = read_decl_init(ty);
        m_localenv = orig;
    }
    m_toplevels->Push( makeAstNode<AstDeclNode>(var, init));
}

AstType *CCParse::read_decl_spec_opt(int *sclass) {
    if (is_type(peek()))
        return read_decl_spec(sclass);
    THROW_WARNING_TOKEN(peek(), "type specifier missing, assuming int");
    return AstBuiltinType::IntType();
}

void CCParse::read_decl(AstVector *block, bool isglobal) {
    int sclass = 0;
    AstType *basetype = read_decl_spec_opt(&sclass);
    if (next_token(';'))
        return;
    for (;;) {
        char *name = NULL;
        AstType *ty = read_declarator(&name, copy_incomplete_type(basetype), nullptr, DECL_BODY);

        ty->SwitchingStatic(sclass == S_STATIC);
        if (sclass == S_TYPEDEF) {
            makeAstNode<AstTypedefNode>(ty, name, env());
        }
        else if (ty->IsStatic() && !isglobal)
        {
            ty->EnsureNotVoid();
            read_static_local_var(ty, name);
        }
        else {
            ty->EnsureNotVoid();
            AstNode *var = isglobal ? makeAstNode<AstGlobalVariableNode>(ty, name, m_globalenv) : astLvarNode(ty, name);
            if (next_token('=')) {
                m_sourceLocator->Markup(peek());
                block->Push( makeAstNode<AstDeclNode>(var, read_decl_init(ty)));
            }
            else if (sclass != S_EXTERN && !ty->IsKind(KIND_FUNC)) {
                block->Push( makeAstNode<AstDeclNode>(var, nullptr));
            }
        }
        if (next_token(';'))
            return;
        if (!next_token(','))
            THROW_ERROR_TOKEN(peek(), "';' or ',' are expected, but got %s", CCHelper::ToString(peek()));
    }
}

/*
* K&R-style parameter types
*/

AstVector *CCParse::read_oldstyle_param_args() {
    AstMap *orig = m_localenv;
    m_localenv = nullptr;
    AstVector *r = m_parseSweep->AllocateObject<AstVector>();
    for (;;) {
        if (is_keyword(peek(), '{'))
            break;
        if (!is_type(peek()))
            THROW_ERROR_TOKEN(peek(), "K&R-style declarator expected, but got %s", CCHelper::ToString(peek()));
        read_decl(r, false);
    }
    m_localenv = orig;
    return r;
}

static void update_oldstyle_param_type(AstVector *params, AstVector *vars)
{
    for (int i = 0; i < vars->Length(); i++)
    {
        AstNode *decl = reinterpret_cast<AstNode *>(vars->Get( i));
        LOGIC_ASSERT(decl->IsKind( AST_DECL) );
        AstNode *var = static_cast<AstDeclNode *>(decl)->DeclVar();
        LOGIC_ASSERT(var->IsKind( AST_LVAR) );
        for (int j = 0; j < params->Length(); j++)
        {
            AstNode *param = static_cast<AstNode *>(params->Get(j));
            LOGIC_ASSERT(param->IsKind( AST_LVAR) );
            if (strcmp(static_cast<AstVariableNode *>(param)->VarName() , static_cast<AstVariableNode *>(var )->VarName() ))
                continue;
            param->SetType(var->GetType());
            goto found;
        }
        THROW_ERROR_NODE(var, "missing parameter: %s", static_cast<AstVariableNode *>(var)->VarName());
found:;
    }
}

void CCParse::read_oldstyle_param_type(AstVector *params) {
    AstVector *vars = read_oldstyle_param_args();
    update_oldstyle_param_type(params, vars);
}

AstVector *CCParse::param_types(AstVector *params)
{
	AstVector *r = m_parseSweep->AllocateObject<AstVector>();

    for (int i = 0; i < params->Length(); i++) {
        AstNode *param = static_cast<AstNode *>(params->Get( i));
        r->Push( param->GetType());
    }
    return r;
}

/*
* Function definition
*/

AstNode *CCParse::read_func_body(AstType *functype, char *fname, AstVector *params)
{
    m_localenv = m_parseSweep->AllocateObject<AstMap>(m_localenv);
	m_localvars = m_parseSweep->AllocateObject<AstVector>();
    m_current_func_type = functype;
    AstNode *funcname = makeAstNode<AstStringNode>(ENC_NONE, fname, strlen(fname) + 1);
    m_localenv->Put( "__func__", funcname);
    m_localenv->Put( "__FUNCTION__", funcname);
    AstNode *body = read_compound_stmt();
    AstNode *r = makeAstNode<AstFunctionNode>(functype, fname, params, body, m_localvars);
    m_current_func_type = nullptr;
    m_localenv = nullptr;
    m_localvars = nullptr;
    return r;
}

void CCParse::skip_parentheses(CCVector *buf) {
    for (;;) {
        Token *tok = get();
        if (tok->kind == TEOF)
            THROW_ERROR_NOPOS("premature end of input");
        buf->Push( tok);
        if (is_keyword(tok, ')'))
            return;
        if (is_keyword(tok, '('))
            skip_parentheses(buf);
    }
}

// is_funcdef returns true if we are at beginning of a function definition.
// The basic idea is that if we see '{' or a type keyword after a closing
// parenthesis of a function parameter list, we were reading a function
// definition. (Usually '{' comes after a closing parenthesis.
// A type keyword is allowed for K&R-style function definitions.)
bool CCParse::is_funcdef() {
	CCVector *buf = m_parseSweep->AllocateObject<CCVector>();
    bool r = false;
    for (;;) {
        Token *tok = get();
        buf->Push( tok);
        if (tok->kind == TEOF)
            THROW_ERROR_NOPOS("premature end of input");
        if (is_keyword(tok, ';'))
            break;
        if (is_type(tok))
            continue;
        if (is_keyword(tok, '(')) {
            skip_parentheses(buf);
            continue;
        }
        if (tok->kind != TIDENT)
            continue;
        if (!is_keyword(peek(), '('))
            continue;
        buf->Push( get());
        skip_parentheses(buf);
        r = (is_keyword(peek(), '{') || is_type(peek()));
        break;
    }
    while (buf->Length() > 0)
        unget_token(reinterpret_cast<Token *>(buf->Pop()));
    return r;
}

void CCParse::backfill_labels() {
    for (int i = 0; i < m_gotos->Length(); i++) {
        AstNode *src = static_cast<AstNode *>(m_gotos->Get(i));

        LOGIC_ASSERT(src->IsKind(AST_GOTO));
        static_cast<AstGotoNode *>(src)->BackfillLabel(*m_labels, [this]() { return make_label(); });
    }
}

AstNode *CCParse::read_funcdef() {
    int sclass = 0;
    AstType *basetype = read_decl_spec_opt(&sclass);
    m_localenv = m_parseSweep->AllocateObject<AstMap>(m_globalenv);
	m_gotos = m_parseSweep->AllocateObject<AstVector>();
	m_labels = m_parseSweep->AllocateObject<AstMap>();
    char *name;
	AstVector *params = m_parseSweep->AllocateObject<AstVector>();
    AstType *functype = read_declarator(&name, basetype, params, DECL_BODY);
    AstFunctionType *realFuncTy = dynamic_cast<AstFunctionType *>(functype);

    LOGIC_ASSERT(realFuncTy != nullptr);

    if (realFuncTy->IsOldStyle())
    {
        if (params->Length() == 0)
            realFuncTy->SetVarArgs(false);
        read_oldstyle_param_type(params);
        realFuncTy->SetParams(param_types(params));
    }
    functype->SwitchingStatic(sclass == S_STATIC);
    //ast_gvar(functype, name);
    m_parseSweep->AllocateObject<AstGlobalVariableNode>(functype, name, m_globalenv);
    expect('{');
    AstNode *r = read_func_body(functype, name, params);
    backfill_labels();
    m_localenv = nullptr;
    return r;
}

/*
* If
*/

AstNode *CCParse::read_boolean_expr()
{
    AstNode *cond = read_expr();

    return cond->GetType()->IsFloatType() ? makeAstNode<AstConvNode>(AstBuiltinType::BoolType(), cond) : cond;
}

AstNode *CCParse::read_if_stmt() {
    expect('(');
    AstNode *cond = read_boolean_expr();
    expect(')');
    AstNode *then = read_stmt();
    if (!next_token(KELSE))
        return makeAstNode<AstIfNode>(cond, then, nullptr);
    AstNode *els = read_stmt();
    return makeAstNode<AstIfNode>(cond, then, els);
}

/*
* For
*/

AstNode *CCParse::read_opt_decl_or_stmt() {
    if (next_token(';'))
        return NULL;
    AstVector *list = m_parseSweep->AllocateObject<AstVector>();
    read_decl_or_stmt(list);
    return makeAstNode<AstCompoundStmtNode>(list);
}

#define SET_JUMP_LABELS(cont, brk)              \
    char *ocontinue = m_lcontinue;                \
    char *obreak = m_lbreak;                      \
    m_lcontinue = cont;                           \
    m_lbreak = brk

#define RESTORE_JUMP_LABELS()                   \
    m_lcontinue = ocontinue;                      \
    m_lbreak = obreak

AstNode *CCParse::read_for_stmt() {
    expect('(');
    char *beg = make_label();
    char *mid = make_label();
    char *end = make_label();
    AstMap *orig = m_localenv;
    m_localenv = m_parseSweep->AllocateObject<AstMap>(m_localenv);
    AstNode *init = read_opt_decl_or_stmt();
    AstNode *cond = read_expr_opt();
    if (cond && cond->GetType()->IsFloatType())
        cond = makeAstNode<AstConvNode>(AstBuiltinType::BoolType(), cond);
    expect(';');
    AstNode *step = read_expr_opt();
    expect(')');
    SET_JUMP_LABELS(mid, end);
    AstNode *body = read_stmt();
    RESTORE_JUMP_LABELS();
    m_localenv = orig;

	AstVector *v = m_parseSweep->AllocateObject<AstVector>();
    if (init)
        v->Push(init);
    v->Push( makeAstNode<AstDestNode>(beg) );
    if (cond)
        v->Push(makeAstNode<AstIfNode>(cond, nullptr, makeAstNode<AstJumpNode>(end)));
    if (body)
        v->Push( body);
    v->Push( makeAstNode<AstDestNode>(mid));
    if (step)
        v->Push( step);
    v->Push( makeAstNode<AstJumpNode>(beg));
    v->Push( makeAstNode<AstDestNode>(end));
    return makeAstNode<AstCompoundStmtNode>(v);
}

/*
* While
*/

AstNode *CCParse::read_while_stmt() {
    expect('(');
    AstNode *cond = read_boolean_expr();
    expect(')');

    char *beg = make_label();
    char *end = make_label();
    SET_JUMP_LABELS(beg, end);
    AstNode *body = read_stmt();
    RESTORE_JUMP_LABELS();

	auto v = m_parseSweep->AllocateObject<AstVector>();
    v->Push( makeAstNode<AstDestNode>(beg));
    v->Push( makeAstNode<AstIfNode>(cond, body, makeAstNode<AstJumpNode>(end)));
    v->Push( makeAstNode<AstJumpNode>(beg));
    v->Push( makeAstNode<AstDestNode>(end));
    return makeAstNode<AstCompoundStmtNode>(v);
}

/*
* Do
*/

AstNode *CCParse::read_do_stmt() {
    char *beg = make_label();
    char *end = make_label();
    SET_JUMP_LABELS(beg, end);
    AstNode *body = read_stmt();
    RESTORE_JUMP_LABELS();
    Token *tok = get();
    if (!is_keyword(tok, KWHILE))
        THROW_ERROR_TOKEN(tok, "'while' is expected, but got %s", CCHelper::ToString(tok));
    expect('(');
    AstNode *cond = read_boolean_expr();
    expect(')');
    expect(';');

	auto v = m_parseSweep->AllocateObject<AstVector>();
    v->Push( makeAstNode<AstDestNode>(beg));
    if (body)
        v->Push( body);
    v->Push( makeAstNode<AstIfNode>(cond, makeAstNode<AstJumpNode>(beg), nullptr));
    v->Push( makeAstNode<AstDestNode>(end));
    return makeAstNode<AstCompoundStmtNode>(v);
}

/*
* Switch
*/

AstNode *CCParse::make_switch_jump(AstNode *var, Case *c) {
    AstNode *cond;
    if (c->Begin() == c->End()) {
        cond = makeAstNode<AstBinaryOpNode>(AstBuiltinType::IntType(), OP_EQ, var, makeAstNode<AstIntNode>(AstBuiltinType::IntType(), c->Begin()));
    }
    else {
        // [GNU] case i ... j is compiled to if (i <= cond && cond <= j) goto <label>.
        AstNode *x = makeAstNode<AstBinaryOpNode>(AstBuiltinType::IntType(), OP_LE, makeAstNode<AstIntNode>(AstBuiltinType::IntType(), c->Begin()), var);
        AstNode *y = makeAstNode<AstBinaryOpNode>(AstBuiltinType::IntType(), OP_LE, var, makeAstNode<AstIntNode>(AstBuiltinType::IntType(), c->End()));
        cond = makeAstNode<AstBinaryOpNode>(AstBuiltinType::IntType(), OP_LOGAND, x, y);
    }
    return makeAstNode<AstIfNode>(cond, makeAstNode<AstJumpNode>(c->Label()), nullptr);
}

// C11 6.8.4.2p3: No two case constant expressions have the same value.
static void check_case_duplicates(CCVector *cases) {
    int len = cases->Length();
    Case *x = reinterpret_cast<Case *>(cases->Get(len - 1));
    for (int i = 0; i < len - 1; i++) {
        Case *y = reinterpret_cast<Case *>(cases->Get( i));
        if (x->End() < y->Begin() || y->End() < x->Begin())
            continue;
        if (x->Begin() == x->End())
            THROW_ERROR_NOPOS("duplicate case value: %d", x->Begin());
        THROW_ERROR_NOPOS("duplicate case value: %d ... %d", x->Begin(), x->End());
    }
}

#define SET_SWITCH_CONTEXT(brk)                 \
    CCVector *ocases = m_cases;                     \
    char *odefaultcase = m_defaultcase;           \
    char *obreak = m_lbreak;                      \
    m_cases = m_parseSweep->AllocateObject<CCVector>();                      \
    m_defaultcase = NULL;                         \
    m_lbreak = brk

#define RESTORE_SWITCH_CONTEXT()                \
    m_cases = ocases;                             \
    m_defaultcase = odefaultcase;                 \
    m_lbreak = obreak

AstNode *CCParse::read_switch_stmt() {
    expect('(');
    AstNode *expr = conv(read_expr());
    expr->EnsureIntType();
    expect(')');

    char *end = make_label();
    SET_SWITCH_CONTEXT(end);
    AstNode *body = read_stmt();
	auto v = m_parseSweep->AllocateObject<AstVector>();
    AstNode *var = astLvarNode( expr->GetType(), make_tempname());
    v->Push( makeAstNode<AstBinaryOpNode>(expr->GetType(), '=', var, expr));
    for (int i = 0; i < m_cases->Length(); i++)
        v->Push( make_switch_jump(var, reinterpret_cast<Case *>(m_cases->Get( i))));
    v->Push( makeAstNode<AstJumpNode>(m_defaultcase ? m_defaultcase : end));
    if (body)
        v->Push( body);
    v->Push( makeAstNode<AstDestNode>(end));
    RESTORE_SWITCH_CONTEXT();
    return makeAstNode<AstCompoundStmtNode>(v);
}

AstNode *CCParse::read_label_tail(AstNode *label) {
    AstNode *stmt = read_stmt();
	auto v = m_parseSweep->AllocateObject<AstVector>(label);

    if (stmt)
        v->Push( stmt);
    return makeAstNode<AstCompoundStmtNode>(v);
}

AstNode *CCParse::read_case_label(Token *tok) {
    if (!m_cases)
		THROW_ERROR_TOKEN(tok, "stray case label");
    char *label = make_label();
    int beg = read_intexpr();
    if (next_token(KELLIPSIS)) {
        int end = read_intexpr();
        expect(':');
        if (beg > end)
            THROW_ERROR_TOKEN(tok, "case region is not in correct order: %d ... %d", beg, end);
        m_cases->Push( m_parseSweep->AllocateObject<Case>(beg, end, label));
    }
    else {
        expect(':');
        m_cases->Push( m_parseSweep->AllocateObject<Case>(beg, beg, label));
    }
    check_case_duplicates(m_cases);
    return read_label_tail(makeAstNode<AstDestNode>(label));
}

AstNode *CCParse::read_default_label(Token *tok) {
    expect(':');
    if (m_defaultcase)
        THROW_ERROR_TOKEN(tok, "duplicate default");
    m_defaultcase = make_label();
    return read_label_tail(makeAstNode<AstDestNode>(m_defaultcase));
}

/*
* Jump statements
*/

AstNode *CCParse::read_break_stmt(Token *tok) {
    expect(';');
    if (!m_lbreak)
        THROW_ERROR_TOKEN(tok, "stray break statement");
    return makeAstNode<AstJumpNode>(m_lbreak);
}

AstNode *CCParse::read_continue_stmt(Token *tok) {
    expect(';');
    if (!m_lcontinue)
        THROW_ERROR_TOKEN(tok, "stray continue statement");
    return makeAstNode<AstJumpNode>(m_lcontinue);
}

AstNode *CCParse::read_return_stmt() {
    AstNode *retval = read_expr_opt();
    AstFunctionType *cfty = static_cast<AstFunctionType *>(m_current_func_type);

    expect(';');
	if (retval)
	{
        typeChecking(cfty->ReturnType(), retval);
		return makeAstNode<AstReturnNode>(makeAstNode<AstConvNode>(cfty->ReturnType(), retval));
	}
	else
	{
        if (!cfty->ReturnType()->IsKind(KIND_VOID))
			THROW_WARNING(m_sourceLocator->LoadProfileString(), "return with no value, in function returning non-void");
	}
    return makeAstNode<AstReturnNode>(nullptr);
}

AstNode *CCParse::read_goto_stmt() {
    if (next_token('*')) {
        // [GNU] computed goto. "goto *p" jumps to the address pointed by p.
        Token *tok = peek();
        AstNode *expr = read_cast_expr();
        if (!expr->GetType()->IsKind( KIND_PTR) )
            THROW_ERROR_TOKEN(tok, "pointer expected for computed goto, but got %s", expr->Explain());
        return makeAstNode<AstComputedGoto>(expr);
    }
    Token *tok = get();
    if (!tok || tok->kind != TIDENT)
		THROW_ERROR_TOKEN(tok, "identifier expected, but got %s", CCHelper::ToString(tok));
    expect(';');
    AstNode *r = makeAstNode<AstGotoNode>(tok->sval);
    m_gotos->Push( r);
    return r;
}

AstNode *CCParse::read_label(Token *tok) {
    char *label = tok->sval;
    if (m_labels->Get( label))
		THROW_ERROR_TOKEN(tok, "duplicate label: %s", CCHelper::ToString(tok));
    AstNode *r = makeAstNode<AstLabelNode>(label);
    m_labels->Put( label, r);
    return read_label_tail(r);
}

/*
* Statement
*/

AstNode *CCParse::read_stmt()
{
    Token *tok = get();
    if (tok->kind == TKEYWORD) {
        switch (tok->id) {
        case '{':       return read_compound_stmt();
        case KIF:       return read_if_stmt();
        case KFOR:      return read_for_stmt();
        case KWHILE:    return read_while_stmt();
        case KDO:       return read_do_stmt();
        case KRETURN:   return read_return_stmt();
        case KSWITCH:   return read_switch_stmt();
        case KCASE:     return read_case_label(tok);
        case KDEFAULT:  return read_default_label(tok);
        case KBREAK:    return read_break_stmt(tok);
        case KCONTINUE: return read_continue_stmt(tok);
        case KGOTO:     return read_goto_stmt();
        }
    }
    if (tok->kind == TIDENT && next_token(':'))
        return read_label(tok);
    unget_token(tok);
    AstNode *r = read_expr_opt();
    expect(';');
    return r;
}

AstNode *CCParse::read_compound_stmt() {
    AstMap *orig = m_localenv;
    m_localenv = m_parseSweep->AllocateObject<AstMap>(m_localenv);
	AstVector *list = m_parseSweep->AllocateObject<AstVector>();
    for (;;) {
        if (next_token('}'))
            break;
        read_decl_or_stmt(list);
    }
    m_localenv = orig;
    return makeAstNode<AstCompoundStmtNode>(list);
}

void CCParse::read_decl_or_stmt(AstVector *list) {
    Token *tok = peek();
    if (tok->kind == TEOF)
        THROW_ERROR_NOPOS("premature end of input");
	m_sourceLocator->Markup(peek());
    if (is_type(tok)) {
        read_decl(list, false);
    }
    else if (next_token(KSTATIC_ASSERT)) {
        read_static_assert();
    }
    else {
        AstNode *stmt = read_stmt();
        if (stmt)
            list->Push( stmt);
    }
}

/*
* Compilation unit
*/

AstVector *CCParse::read_toplevels() {
	m_toplevels = m_parseSweep->AllocateObject<AstVector>();
    for (;;) {
        if (peek()->kind == TEOF)
            return m_toplevels;
        if (is_funcdef())
            m_toplevels->Push( read_funcdef());
        else
            read_decl(m_toplevels, true);
    }
}

/*
* Token handling
*/

// C11 5.1.1.2p6 Adjacent string literal tokens are concatenated.
void CCParse::concatenate_string(Token *tok) {
    int enc = tok->enc;
    CCBuffer *b = m_parseBuffer->MakeBuffer();
    b->Append( tok->sval, tok->slen - 1);
    while (peek()->kind == TSTRING) {
        Token *tok2 = read_token();
        b->Append( tok2->sval, tok2->slen - 1);
        int enc2 = tok2->enc;
        if (enc != ENC_NONE && enc2 != ENC_NONE && enc != enc2)
			THROW_ERROR_TOKEN(tok2, "unsupported non-standard concatenation of string literals: %s", CCHelper::ToString(tok2));
        if (enc == ENC_NONE)
            enc = enc2;
    }
    b->Write( '\0');
    tok->sval = b->Body();
    tok->slen = b->Length();
    tok->enc = enc;
}

Token *CCParse::get() {
    Token *r = read_token();
    if (r->kind == TINVALID)
		THROW_ERROR_TOKEN(r, "stray character in program: '%c'", r->c);
    if (r->kind == TSTRING && peek()->kind == TSTRING)
        concatenate_string(r);
    return r;
}

Token *CCParse::peek() {
    return peek_token();
}

/*
* Initializer
*/

void CCParse::define_builtin(const char *name, AstType *rettype, AstVector *paramtypes) {
    makeAstNode<AstGlobalVariableNode>(m_typegen->MakeType<AstFunctionType>(rettype, paramtypes, true, false), name, m_globalenv);
}

void CCParse::InitializeUnit()
{
	CCPreprocessor::InitializeUnit();

	parse_init();
}

void CCParse::parse_init() {
    auto voidptr = m_parseSweep->AllocateObject<AstVector>(m_typegen->MakeType<AstPointerType>(AstBuiltinType::VoidType()));
	auto two_voidptrs = m_parseSweep->AllocateObject<AstVector>();

    two_voidptrs->Push(m_typegen->MakeType<AstPointerType>(AstBuiltinType::VoidType()));
    two_voidptrs->Push(m_typegen->MakeType<AstPointerType>(AstBuiltinType::VoidType()) );
    define_builtin("__builtin_return_address", m_typegen->MakeType<AstPointerType>(AstBuiltinType::VoidType()), voidptr);
    define_builtin("__builtin_reg_class", AstBuiltinType::IntType(), voidptr);
    define_builtin("__builtin_va_arg", AstBuiltinType::VoidType(), two_voidptrs);
    define_builtin("__builtin_va_start", AstBuiltinType::VoidType(), voidptr);
}
