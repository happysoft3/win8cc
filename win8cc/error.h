
#ifndef ERROR_H__
#define ERROR_H__

// error.c
#include "common/utils/stringhelper.h"

struct Token;
class AstNode;

class CCError
{
private:
	CCError(){ }
	~CCError(){ }

public:
	CCError(const CCError &) = delete;
	CCError(CCError &&) = delete;
	CCError &operator=(const CCError &) = delete;
	CCError &operator=(CCError &&) = delete;

private:
	static void pronounceError();
	static void printMessage(const char *line, const std::string &pos, const std::string &detail, bool canIgnore = false);

public:
	template <class... Args>
	static void AnnounceError(const char *line, const std::string &pos, const char *fmt, Args&& ...args)
	{
		auto detail = _StringHelper::stringFormat(fmt, std::forward<Args>(args)...);

		printMessage(line, pos, detail);
	}

	template <class... Args>
	static void AnnounceWarning(const char *line, const std::string &pos, const char *fmt, Args&& ...args)
	{
		auto detail = _StringHelper::stringFormat(fmt, std::forward<Args>(args)...);

		printMessage(line, pos, detail, true);
	}

	static std::string TokenPosition(Token *tok);
    static std::string NodePosition(const AstNode *node);
};

#define CCERROR_STR(s)	#s
#define CCERROR_TO_STR(s) CCERROR_STR(s)
#define CCERROR_TOKEN_POS(tok) CCError::TokenPosition(tok)
#define CCERROR_NODE_POS(node) CCError::NodePosition(node)
#define CCERROR_LINE_COMMON_FMT __FILE__ ":" CCERROR_TO_STR(__LINE__)
#define THROW_ERROR(pos, ...)	CCError::AnnounceError(CCERROR_LINE_COMMON_FMT, pos, __VA_ARGS__)
#define THROW_ERROR_NOPOS(...)	THROW_ERROR("(null)", __VA_ARGS__)
#define THROW_ERROR_TOKEN(tok, ...)	THROW_ERROR(CCERROR_TOKEN_POS(tok), __VA_ARGS__)
#define THROW_ERROR_NODE(node, ...)	THROW_ERROR(CCERROR_NODE_POS(node), __VA_ARGS__)
#define THROW_WARNING(pos, ...)	CCError::AnnounceWarning(CCERROR_LINE_COMMON_FMT, pos, __VA_ARGS__)
#define THROW_WARNING_NOPOS(...)	THROW_WARNING("(null)", __VA_ARGS__)
#define THROW_WARNING_TOKEN(tok, ...) THROW_WARNING(CCERROR_TOKEN_POS(tok), __VA_ARGS__)
#define THROW_WARNING_NODE(node, ...) THROW_WARNING(CCERROR_NODE_POS(node), __VA_ARGS__)
#define LOGIC_ASSERT(condition) \
    if ((condition) == false) THROW_ERROR_NOPOS("assert fail: %s", #condition);

#endif
