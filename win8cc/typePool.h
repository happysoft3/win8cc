
#ifndef TYPE_POOL_H__
#define TYPE_POOL_H__

#include <list>
#include <memory>

class AstType;

class TypePool
{
private:
	std::list<std::unique_ptr<AstType>> m_typeList;

public:
	TypePool();
	~TypePool();

private:
	AstType *createType(AstType *ty);

public:
    template <class Ty, class... Args>
    Ty *MakeType(Args&&... args)
    {
        Ty *n = new Ty(std::forward<Args>(args)...);

        createType(n);
        return n;
    }
};

#endif
