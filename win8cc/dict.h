
#ifndef DICT_H__
#define DICT_H__

class CCVector;
class CCMap;

class CCDict
{
private:
	CCMap *m_map;
	CCVector *m_key;

public:
	CCDict();
	~CCDict();

	void *Get(const char *key);
	void *GetWithIndex(size_t index);
	void Put(const char *key, void *val);
	bool Equal(const CCDict &other) const;
	bool CompareKey(const CCDict &other) const;
	size_t Count() const;
	CCVector *Keys() const
	{
		return m_key;
	}
};

#endif

