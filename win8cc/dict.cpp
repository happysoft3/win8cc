// Copyright 2012 Rui Ueyama. Released under the MIT license.

#include <stdlib.h>
#include "dict.h"
#include "map.h"
#include "vector.h"
#include "error.h"

///CCDict Class ////

CCDict::CCDict()
{
	m_map = new CCMap;
	m_key = new CCVector;
}

CCDict::~CCDict()
{
	delete m_map;
	delete m_key;
}

void *CCDict::Get(const char *key)
{
	return m_map->Get(key);
}

void *CCDict::GetWithIndex(size_t index)
{
	LOGIC_ASSERT(index < static_cast<size_t>(m_key->Length()));
	return m_map->Get(reinterpret_cast<const char *>(m_key->Get(index)));
}

void CCDict::Put(const char *key, void *val)
{
	m_map->Put(key, val);
	m_key->Push(const_cast<char *>(key));
}

bool CCDict::Equal(const CCDict &other) const
{
	if (m_map->Length() != other.m_map->Length())
		return false;

	return m_key->EqualLength(*other.m_key);
}

bool CCDict::CompareKey(const CCDict &other) const
{
	CCVector *otherKeys = other.m_key;

	for (int i = 0; i < m_key->Length(); ++i)
	{
		if (strcmp(reinterpret_cast<const char *>(m_key->Get(i)), reinterpret_cast<const char *>(otherKeys->Get(i))))
			return false;
	}
	return true;
}

size_t CCDict::Count() const
{
	size_t keyLength = static_cast<size_t>(m_key->Length());
	size_t mapLength = m_map->Length();

	LOGIC_ASSERT(keyLength == mapLength);
	return mapLength;
}
