// Copyright 2012 Rui Ueyama. Released under the MIT license.

/*
* Tokenizer
*
* This is a translation phase after the phase 1 and 2 in file.c.
* In this phase, the source code is decomposed into preprocessing tokens.
*
* Each comment is treated as if it were a space character.
* Space characters are removed, but the presence of the characters is
* recorded to the token that immediately follows the spaces as a boolean flag.
* Newlines are converted to newline tokens.
*
* Note that the pp-token is different from the regular token.
* A keyword, such as "if", is just an identifier at this stage.
* The definition of the pp-token is usually more relaxed than
* the regular one. For example, ".32e." is a valid pp-number.
* Pp-tokens are converted to regular tokens by the C preprocesor
* (and invalid tokens are rejected by that).
* Some tokens are removed by the preprocessor (e.g. newline).
* For more information about pp-tokens, see C11 6.4 "Lexical Elements".
*/

#include "lex.h"
#include "8cc.h"
#include "error.h"
//#include "encoding.h"
#include "vector.h"
#include "buffer.h"
#include "file.h"
#include "common/mmanager/memorySweeper.h"

#include <ctype.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <cstdio>

static Token s_spaceToken = { TSPACE };
static Token *space_token = &s_spaceToken;

static Token s_newlineToken = { TNEWLINE };
static Token *newline_token = &s_newlineToken;

static Token s_eofToken = { TEOF };
static Token *eof_token = &s_eofToken;

using namespace _StringHelper;

std::string CCLex::Pos::ToString() const
{
	File *f = m_owner->m_filestack->current_file();

	return stringFormat("%s:%d:%d", f ? f->name : "(unknown)", m_line, m_column);
}

CCLex::CCLex()
    : m_pos({ })
{
    m_buffers = new CCVector;
	m_lexBuffer = std::make_unique<CCBufferAllocator>();
	m_lexSweep = std::make_unique<MemorySweeper>();
}

CCLex::~CCLex()
{
    delete m_buffers;
}

const char *const CCLex::ThisFileName() const
{
    LOGIC_ASSERT(m_filestack != nullptr);
    File *f = m_filestack->current_file();

    return f->name;
}

void CCLex::InitializeUnit()
{
	lex_init();
}

//#define errorp(p, ...) errorf(__FILE__ ":" STR(__LINE__), pos_string(&p), __VA_ARGS__)
//#define warnp(p, ...)  warnf(__FILE__ ":" STR(__LINE__), pos_string(&p), __VA_ARGS__)

void CCLex::lex_init()
{
    m_filestack = std::make_unique<CCFile>();
    onInitFileStack(m_filestack.get());

	m_buffers->Push(new CCVector);
    if (!strcmp(m_initialFileName.c_str(), "-")) {
        m_filestack->stream_push(CCFile::make_file(stdin, "-"));
        return;
    }
    FILE *fp = fopen(m_initialFileName.c_str(), "r");
	if (!fp)
		THROW_ERROR_NOPOS("Cannot open %s: %s", m_initialFileName.c_str(), strerror(errno));
    m_filestack->stream_push(CCFile::make_file(fp, m_initialFileName.c_str()));
}

CCLex::Pos CCLex::get_pos(int delta) {
    File *f = m_filestack->current_file();

	return Pos(this, f->line, f->column + delta);
}

void CCLex::mark() {
    m_pos = get_pos(0);
}

Token *CCLex::make_token(Token *tmpl) {
	Token *r = m_lexSweep->AllocateObject<Token>(*tmpl);
    r->hideset = NULL;
    File *f = m_filestack->current_file();
    r->file = f;
    r->line = m_pos.Line();
    r->column = m_pos.Column();
    r->count = f->ntok++;
    return r;
}

Token *CCLex::make_ident(char *p) {
	Token tok = { TIDENT };

	tok.sval = p;
	return make_token(&tok);
}

Token *CCLex::make_strtok(char *s, int len, int enc)
{
	Token tok = { TSTRING };

	tok.sval = s;
	tok.slen = len;
	tok.enc = enc;
	return make_token(&tok);
}

Token *CCLex::make_keyword(int id)
{
	Token tok = { TKEYWORD };

	tok.id = id;
	return make_token(&tok);
}

Token *CCLex::make_number(char *s) {
	Token tok = { TNUMBER };

	tok.sval = s;
	return make_token(&tok);
}

Token *CCLex::make_invalid(char c) {
	Token tok = { TINVALID };

	tok.c = c;
	return make_token(&tok);
}

Token *CCLex::make_char(int c, int enc) {
	Token tok = { TCHAR };

	tok.c = c;
	tok.enc = enc;
	return make_token(&tok);
}

static bool iswhitespace(int c) {
    return c == ' ' || c == '\t' || c == '\f' || c == '\v';
}

int CCLex::peek() {
    int r = m_filestack->readc();
    m_filestack->unreadc(r);
    return r;
}

bool CCLex::next(int expect) {
    int c = m_filestack->readc();
    if (c == expect)
        return true;
    m_filestack->unreadc(c);
    return false;
}

void CCLex::skip_line() {
    for (;;) {
        int c = m_filestack->readc();
        if (c == EOF)
            return;
        if (c == '\n') {
            m_filestack->unreadc(c);
            return;
        }
    }
}

bool CCLex::do_skip_space() {
    int c = m_filestack->readc();
    if (c == EOF)
        return false;
    if (iswhitespace(c))
        return true;
    if (c == '/') {
        if (next('*')) {
            skip_block_comment();
            return true;
        }
        if (next('/')) {
            skip_line();
            return true;
        }
    }
    m_filestack->unreadc(c);
    return false;
}

// Skips spaces including comments.
// Returns true if at least one space is skipped.
bool CCLex::skip_space() {
    if (!do_skip_space())
        return false;
    while (do_skip_space());
    return true;
}

void CCLex::skip_char() {
    if (m_filestack->readc() == '\\')
        m_filestack->readc();
    int c = m_filestack->readc();
    while (c != EOF && c != '\'')
        c = m_filestack->readc();
}

void CCLex::skip_string() {
    for (int c = m_filestack->readc(); c != EOF && c != '"'; c = m_filestack->readc())
        if (c == '\\')
            m_filestack->readc();
}

// Skips a block of code excluded from input by #if, #ifdef and the like.
// C11 6.10 says that code within #if and #endif needs to be a sequence of
// valid tokens even if skipped. However, in reality, most compilers don't
// tokenize nor validate contents. We don't do that, too.
// This function is to skip code until matching #endif as fast as we can.
void CCLex::skip_cond_incl() {
    int nest = 0;
    for (;;) {
        bool bol = (m_filestack->current_file()->column == 1);
        skip_space();
        int c = m_filestack->readc();
        if (c == EOF)
            return;
        if (c == '\'') {
            skip_char();
            continue;
        }
        if (c == '\"') {
            skip_string();
            continue;
        }
        if (c != '#' || !bol)
            continue;
        int column = m_filestack->current_file()->column - 1;
        Token *tok = lex();
        if (tok->kind != TIDENT)
            continue;
        if (!nest && (is_ident(tok, "else") || is_ident(tok, "elif") || is_ident(tok, "endif"))) {
            unget_token(tok);
            Token *hash = make_keyword('#');
            hash->bol = true;
            hash->column = column;
            unget_token(hash);
            return;
        }
        if (is_ident(tok, "if") || is_ident(tok, "ifdef") || is_ident(tok, "ifndef"))
            nest++;
        else if (nest && is_ident(tok, "endif"))
            nest--;
        skip_line();
    }
}

// Reads a number literal. Lexer's grammar on numbers is not strict.
// Integers and floating point numbers and different base numbers are not distinguished.
Token *CCLex::read_number(char c) {
	CCBuffer *b = m_lexBuffer->MakeBuffer();

	b->Write(c);
    char last = c;
    for (;;) {
        int c = m_filestack->readc();
        bool flonum = strchr("eEpP", last) && strchr("+-", c);
        if (!isdigit(c) && !isalpha(c) && c != '.' && !flonum) {
            m_filestack->unreadc(c);
			b->Write('\0');
            return make_number(b->Body());
        }
		b->Write(c);
        last = c;
    }
}

bool CCLex::nextoct() {
    int c = peek();
    return '0' <= c && c <= '7';
}

// Reads an octal escape sequence.
int CCLex::read_octal_char(int c) {
    int r = c - '0';
    if (!nextoct())
        return r;
    r = (r << 3) | (m_filestack->readc() - '0');
    if (!nextoct())
        return r;
    return (r << 3) | (m_filestack->readc() - '0');
}

// Reads a \x escape sequence.
int CCLex::read_hex_char() {
    Pos p = get_pos(-2);
    int c = m_filestack->readc();
	if (!isxdigit(c))
		THROW_ERROR(p.ToString(), "\\x is not followed by a hexadecimal character: %c", c);
    int r = 0;
    for (;; c = m_filestack->readc()) {
        switch (c) {
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            r = (r << 4) | (c - '0');
            continue;

        case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
            r = (r << 4) | (c - 'a' + 10);
            continue;

        case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
            r = (r << 4) | (c - 'A' + 10);
            continue;

        default: m_filestack->unreadc(c); return r;
        }
    }
}

static bool is_valid_ucn(unsigned int c) {
    // C11 6.4.3p2: U+D800 to U+DFFF are reserved for surrogate pairs.
    // A codepoint within the range cannot be a valid character.
    if (0xD800 <= c && c <= 0xDFFF)
        return false;
    // It's not allowed to encode ASCII characters using \U or \u.
    // Some characters not in the basic character set (C11 5.2.1p3)
    // are allowed as exceptions.
    return 0xA0 <= c || c == '$' || c == '@' || c == '`';
}

// Reads \u or \U escape sequences. len is 4 or 8, respecitvely.
int CCLex::read_universal_char(int len) {
    Pos p = get_pos(-2);
    unsigned int r = 0;
    for (int i = 0; i < len; i++) {
        char c = m_filestack->readc();
        switch (c) {
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            r = (r << 4) | (c - '0');
            continue;

        case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
            r = (r << 4) | (c - 'a' + 10);
            continue;

        case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
            r = (r << 4) | (c - 'A' + 10);
            continue;

        default: THROW_ERROR(p.ToString(), "invalid universal character: %c", c);
        }
    }
    if (!is_valid_ucn(r))
        THROW_ERROR(p.ToString(), "invalid universal character: \\%c%0*x", (len == 4) ? 'u' : 'U', len, r);
    return r;
}

int CCLex::read_escaped_char() {
    Pos p = get_pos(-1);
    int c = m_filestack->readc();
    // This switch-cases is an interesting example of magical aspects
    // of self-hosting compilers. Here, we teach the compiler about
    // escaped sequences using escaped sequences themselves.
    // This is a tautology. The information about their real character
    // codes is not present in the source code but propagated from
    // a compiler compiling the source code.
    // See "Reflections on Trusting Trust" by Ken Thompson for more info.
    // http://cm.bell-labs.com/who/ken/trust.html
    switch (c) {
    case '\'': case '"': case '?': case '\\':
        return c;
    case 'a': return '\a';
    case 'b': return '\b';
    case 'f': return '\f';
    case 'n': return '\n';
    case 'r': return '\r';
    case 't': return '\t';
    case 'v': return '\v';
    case 'e': return '\033';  // '\e' is GNU extension
    case 'x': return read_hex_char();
    case 'u': return read_universal_char(4);
    case 'U': return read_universal_char(8);
    case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7':
        return read_octal_char(c);
    }
	THROW_WARNING(p.ToString(), "unknown escape character: \\%c", c);
    return c;
}

Token *CCLex::read_char(int enc) {
    int c = m_filestack->readc();
    int r = (c == '\\') ? read_escaped_char() : c;
    c = m_filestack->readc();
	if (c != '\'')
		THROW_ERROR(m_pos.ToString(), "unterminated char");
    if (enc == ENC_NONE)
        return make_char((char)r, enc);
    return make_char(r, enc);
}

// Reads a string literal.
Token *CCLex::read_string(int enc) {
	CCBuffer *b = m_lexBuffer->MakeBuffer();
    for (;;) {
        int c = m_filestack->readc();
        if (c == EOF)
            THROW_ERROR(m_pos.ToString(), "unterminated string");
        if (c == '"')
            break;
        if (c != '\\') {
			b->Write(c);
            continue;
        }
        bool isucs = (peek() == 'u' || peek() == 'U');
        c = read_escaped_char();
        if (isucs) {
			b->WriteUtf8(c);
            continue;
        }
		b->Write(c);
    }
    b->Write('\0');
    return make_strtok(b->Body(), b->Length(), enc);
}

Token *CCLex::read_ident(char c) {
	CCBuffer *b = m_lexBuffer->MakeBuffer();
    
	b->Write( c);
    for (;;) {
        c = m_filestack->readc();
        if (isalnum(c) || (c & 0x80) || c == '_' || c == '$') {
            b->Write( c);
            continue;
        }
        // C11 6.4.2.1: \u or \U characters (universal-character-name)
        // are allowed to be part of identifiers.
        if (c == '\\' && (peek() == 'u' || peek() == 'U')) {
            b->WriteUtf8( read_escaped_char());
            continue;
        }
        m_filestack->unreadc(c);
        b->Write( '\0');
        return make_ident(b->Body());
    }
}

void CCLex::skip_block_comment() {
    Pos p = get_pos(-2);
    bool maybe_end = false;
    for (;;) {
        int c = m_filestack->readc();
        if (c == EOF)
            THROW_ERROR(p.ToString(), "premature end of block comment");
        if (c == '/' && maybe_end)
            return;
        maybe_end = (c == '*');
    }
}

// Reads a digraph starting with '%'. Digraphs are alternative spellings
// for some punctuation characters. They are useless in ASCII.
// We implement this just for the standard compliance.
// See C11 6.4.6p3 for the spec.
Token *CCLex::read_hash_digraph() {
    if (next('>'))
        return make_keyword('}');
    if (next(':')) {
        if (next('%')) {
            if (next(':'))
                return make_keyword(KHASHHASH);
            m_filestack->unreadc('%');
        }
        return make_keyword('#');
    }
    return NULL;
}

Token *CCLex::read_rep(char expect, int t1, int els) {
    return make_keyword(next(expect) ? t1 : els);
}

Token *CCLex::read_rep2(char expect1, int t1, char expect2, int t2, char els) {
    if (next(expect1))
        return make_keyword(t1);
    return make_keyword(next(expect2) ? t2 : els);
}

Token *CCLex::do_read_token() {
    if (skip_space())
        return space_token;
    mark();
    int c = m_filestack->readc();
    switch (c) {
    case '\n': return newline_token;
    case ':': return make_keyword(next('>') ? ']' : ':');
    case '#': return make_keyword(next('#') ? KHASHHASH : '#');
    case '+': return read_rep2('+', OP_INC, '=', OP_A_ADD, '+');
    case '*': return read_rep('=', OP_A_MUL, '*');
    case '=': return read_rep('=', OP_EQ, '=');
    case '!': return read_rep('=', OP_NE, '!');
    case '&': return read_rep2('&', OP_LOGAND, '=', OP_A_AND, '&');
    case '|': return read_rep2('|', OP_LOGOR, '=', OP_A_OR, '|');
    case '^': return read_rep('=', OP_A_XOR, '^');
    case '"': return read_string(ENC_NONE);
    case '\'': return read_char(ENC_NONE);
    case '/': return make_keyword(next('=') ? OP_A_DIV : '/');
    case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j': case 'k': case 'l':
    case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': /*case 'u'*/ case 'v': case 'w': case 'x': case 'y': case 'z':
    case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H': case 'I': case 'J': case 'K':
    //case 'a' ... 't': case 'v' ... 'z': case 'A' ... 'K':
    case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T':
    case 'V': case 'W': case 'X': case 'Y': case 'Z': case '_': case '$':
    //case 'M' ... 'T': case 'V' ... 'Z': case '_': case '$':
    /*case 0x80 ... 0xFD:*/
        return read_ident(c);
    case '0': case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':
        return read_number(c);
    case 'L': case 'U':
    {
        // Wide/char32_t character/string literal
        int enc = (c == 'L') ? ENC_WCHAR : ENC_CHAR32;
        if (next('"'))  return read_string(enc);
        if (next('\'')) return read_char(enc);
        return read_ident(c);
    }
    case 'u':
        if (next('"')) return read_string(ENC_CHAR16);
        if (next('\'')) return read_char(ENC_CHAR16);
        // C11 6.4.5: UTF-8 string literal
        if (next('8')) {
            if (next('"'))
                return read_string(ENC_UTF8);
            m_filestack->unreadc('8');
        }
        return read_ident(c);
    case '.':
        if (isdigit(peek()))
            return read_number(c);
        if (next('.')) {
            if (next('.'))
                return make_keyword(KELLIPSIS);
            return make_ident("..");
        }
        return make_keyword('.');
    case '(': case ')': case ',': case ';': case '[': case ']': case '{':
    case '}': case '?': case '~':
        return make_keyword(c);
    case '-':
        if (next('-')) return make_keyword(OP_DEC);
        if (next('>')) return make_keyword(OP_ARROW);
        if (next('=')) return make_keyword(OP_A_SUB);
        return make_keyword('-');
    case '<':
        if (next('<')) return read_rep('=', OP_A_SAL, OP_SAL);
        if (next('=')) return make_keyword(OP_LE);
        if (next(':')) return make_keyword('[');
        if (next('%')) return make_keyword('{');
        return make_keyword('<');
    case '>':
        if (next('=')) return make_keyword(OP_GE);
        if (next('>')) return read_rep('=', OP_A_SAR, OP_SAR);
        return make_keyword('>');
    case '%':
    {
        Token *tok = read_hash_digraph();
        if (tok)
            return tok;
        return read_rep('=', OP_A_MOD, '%');
    }
    case EOF:
        return eof_token;
    default: 
        if (c >= 0x80 && c <= 0xfd)
            return read_ident(c);
        return make_invalid(c);
    }
}

bool CCLex::buffer_empty()
{
	auto h = reinterpret_cast<CCVector *>(m_buffers->Head());

	return m_buffers->Length() == 1 && h->Length() == 0;
}

// Reads a header file name for #include.
//
// Filenames after #include need a special tokenization treatment.
// A filename string may be quoted by < and > instead of "".
// Even if it's quoted by "", it's still different from a regular string token.
// For example, \ in this context is not interpreted as a quote.
// Thus, we cannot use lex() to read a filename.
//
// That the C preprocessor requires a special lexer behavior only for
// #include is a violation of layering. Ideally, the lexer should be
// agnostic about higher layers status. But we need this for the C grammar.
char *CCLex::read_header_file_name(bool *std) {
    if (!buffer_empty())
        return NULL;
    skip_space();
    Pos p = get_pos(0);
    char close;
    if (next('"')) {
        *std = false;
        close = '"';
    }
    else if (next('<')) {
        *std = true;
        close = '>';
    }
    else {
        return NULL;
    }
	CCBuffer *b = m_lexBuffer->MakeBuffer();
    while (!next(close)) {
        int c = m_filestack->readc();
        if (c == EOF || c == '\n')
            THROW_ERROR(p.ToString(), "premature end of header name");
        b->Write( c);
    }
    if (b->Length() == 0)
        THROW_ERROR(p.ToString(), "header name should not be empty");
    b->Write( '\0');
    return b->Body();
}

bool CCLex::is_ident(Token *tok, char *s) {
    return tok->kind == TIDENT && !strcmp(tok->sval, s);
}

bool CCLex::is_keyword(Token *tok, int c) {
    return (tok->kind == TKEYWORD) && (tok->id == c);
}

// Temporarily switches the input token stream to given list of tokens,
// so that you can get the tokens as return values of lex() again.
// After the tokens are exhausted, EOF is returned from lex() until
// "unstash" is called to restore the original state.
void CCLex::token_buffer_stash(CCVector *buf) {
	m_buffers->Push(buf);
}

void CCLex::token_buffer_unstash() {
	m_buffers->Pop();
}

void CCLex::unget_token(Token *tok) {
    if (tok->kind == TEOF)
        return;
    CCVector *buf = reinterpret_cast<CCVector *>(m_buffers->Tail());

	buf->Push(tok);
}

// Reads a token from a given string.
// This function temporarily switches the main input stream to
// a given string and reads one token.
Token *CCLex::lex_string(char *s) {
    m_filestack->stream_stash(m_filestack->make_file_string(s));
    Token *r = do_read_token();
    next('\n');
    Pos p = get_pos(0);
    if (peek() != EOF)
        THROW_ERROR(p.ToString(), "unconsumed input: %s", s);
    m_filestack->stream_unstash();
    return r;
}

Token *CCLex::lex() {
    CCVector *buf = reinterpret_cast<CCVector *>(m_buffers->Tail());
    if (buf->Length() > 0)
        return reinterpret_cast<Token *>(buf->Pop());
    if (m_buffers->Length() > 1)
        return eof_token;
    bool bol = (m_filestack->current_file()->column == 1);
    Token *tok = do_read_token();
    while (tok->kind == TSPACE) {
        tok = do_read_token();
        tok->space = true;
    }
    tok->bol = bol;
    return tok;
}
