
#ifndef LEX_H__
#define LEX_H__

#include <memory>
#include <string>

class CCVector;
struct Token;

class CCFile;
class CCBufferAllocator;
class MemorySweeper;

//typedef struct {
//    int line;
//    int column;
//} Pos;

// lex.c
class CCLex
{
	class Pos
	{
	private:
		int m_line;
		int m_column;
		CCLex *m_owner;

	public:
		Pos(CCLex *lex, int l, int c)
			: m_owner(lex)
		{
			m_line = l;
			m_column = c;
		}
		Pos(CCLex *lex)
			: m_owner(lex), m_line(0), m_column(0)
		{ }

		std::string ToString() const;
		int Line() const
		{
			return m_line;
		}
		int Column() const
		{
			return m_column;
		}
	};
private:
    CCVector *m_buffers;
    std::unique_ptr<CCFile> m_filestack;
    Pos m_pos;
	std::string m_initialFileName;

	std::unique_ptr<CCBufferAllocator> m_lexBuffer;
	std::unique_ptr<MemorySweeper> m_lexSweep;

public:
    CCLex();
    virtual ~CCLex();

private:
    virtual void onInitFileStack(CCFile *file) {}

public:
	virtual void SetInitialFilename(const std::string &fileName)
	{
		m_initialFileName = fileName;
	}
    const char *const ThisFileName() const;

protected:
	virtual void InitializeUnit();

private:
    void lex_init();
    Pos get_pos(int delta);
    void mark();
    Token *make_token(Token *tmpl);
    Token *make_ident(char *p);
    Token *make_strtok(char *s, int len, int enc);
    Token *make_keyword(int id);
    Token *make_number(char *s);
    Token *make_invalid(char c);
    Token *make_char(int c, int enc);
    int peek();
    bool next(int expect);
    void skip_line();
    bool do_skip_space();
    bool skip_space();
    void skip_char();
    void skip_string();

public:
    //char *get_base_file(void);
    void skip_cond_incl(void);

private:
    Token *read_number(char c);
    bool nextoct();
    int read_octal_char(int c);
    int read_hex_char();
    int read_universal_char(int len);
    int read_escaped_char();
    Token *read_char(int enc);
    Token *read_string(int enc);
    Token *read_ident(char c);
    void skip_block_comment();
    Token *read_hash_digraph();
    Token *read_rep(char expect, int t1, int els);
    Token *read_rep2(char expect1, int t1, char expect2, int t2, char els);
    Token *do_read_token();
    bool buffer_empty();

public:
    char *read_header_file_name(bool *std);
    static bool is_ident(Token *tok, char *s);
    static bool is_keyword(Token *tok, int c);
    void token_buffer_stash(CCVector *buf);
    void token_buffer_unstash();
    void unget_token(Token *tok);
    Token *lex_string(char *s);
    Token *lex(void);
};

#endif
