// Copyright 2014 Rui Ueyama. Released under the MIT license.

// This is an implementation of hash table.

#include <stdlib.h>
#include <string.h>
#include <cstdint>
#include "map.h"

#define INIT_SIZE 16
#define TOMBSTONE ((void *)-1)

static uint32_t hash(const char *p) {
    // FNV hash
    uint32_t r = 2166136261;
    for (; *p; p++) {
        r ^= *p;
        r *= 16777619;
    }
    return r;
}

///// CCMap Class //////

CCMap::CCMap(CCMap *parent)
{
	initialize(parent, INIT_SIZE);
}

CCMap::~CCMap()
{
	if (m_key)
		free(m_key);
	if (m_val)
		free(m_val);
}

void CCMap::initialize(CCMap *parent, size_t size)
{
	m_parent = parent;
	m_key = reinterpret_cast<const char **>(calloc(size, sizeof(char *)));
	m_val = reinterpret_cast<void **>(calloc(size, sizeof(void *)));
	m_size = size;
	m_nelem = 0;
	m_nused = 0;
}

void CCMap::maybeRehash()
{
	if (!m_key)
	{
		m_key = reinterpret_cast<const char **>(calloc(INIT_SIZE, sizeof(char *)));
		m_val = reinterpret_cast<void **>(calloc(INIT_SIZE, sizeof(void *)));
		m_size = INIT_SIZE;
		return;
	}
	if (m_nused < m_size * 0.7)
		return;
	int newsize = (m_nelem < m_size * 0.35) ? m_size : m_size * 2;
	const char **k = reinterpret_cast<const char **>(calloc(newsize, sizeof(char *)));
	void **v = reinterpret_cast<void **>(calloc(newsize, sizeof(void *)));
	int mask = newsize - 1;
	for (int i = 0; i < m_size; i++)
	{
		if (m_key[i] == nullptr || m_key[i] == TOMBSTONE)
			continue;
		int j = hash(m_key[i]) & mask;
		for (;; j = (j + 1) & mask)
		{
			if (k[j] != nullptr)
				continue;
			k[j] = m_key[i];
			v[j] = m_val[i];
			break;
		}
	}
	if (m_key) free(m_key);
	m_key = k;
	if (m_val) free(m_val);
	m_val = v;
	m_size = newsize;
	m_nused = m_nelem;
}

void *CCMap::getNostack(const char *key)
{
	if (!m_key)
		return nullptr;
	int mask = m_size - 1;
	int i = hash(key) & mask;
	for (; m_key[i] != nullptr; i = (i + 1) & mask)
		if (m_key[i] != TOMBSTONE && !strcmp(m_key[i], key))
			return m_val[i];
	return nullptr;
}

void *CCMap::Get(const char *key)
{
	void *r = getNostack(key);

	if (r)
		return r;
	// Map is stackable. If no value is found,
	// continue searching from the parent.
	if (m_parent)
		return m_parent->Get(key);
	return nullptr;
}

void CCMap::Put(const char *key, void *val)
{
	maybeRehash();
	int mask = m_size - 1;
	int i = hash(key) & mask;
	for (;; i = (i + 1) & mask)
	{
		const char *k = m_key[i];
		if (k == nullptr || k == TOMBSTONE)
		{
			m_key[i] = key;
			m_val[i] = val;
			m_nelem++;
			if (k == nullptr)
				m_nused++;
			return;
		}
		if (!strcmp(k, key))
		{
			m_val[i] = val;
			return;
		}
	}
}

void CCMap::Remove(const char *key)
{
	if (!m_key)
		return;
	int mask = m_size - 1;
	int i = hash(key) & mask;
	for (; m_key[i] != nullptr; i = (i + 1) & mask)
	{
		if (m_key[i] == TOMBSTONE || strcmp(m_key[i], key))
			continue;
		m_key[i] = reinterpret_cast<char *>(TOMBSTONE);
		m_val[i] = nullptr;
		m_nelem--;
		return;
	}
}
