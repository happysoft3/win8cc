// Copyright 2014 Rui Ueyama. Released under the MIT license.

// Sets are containers that store unique strings.
//
// The data structure is functional. Because no destructive
// operation is defined, it's guranteed that a set will never
// change once it's created.
//
// A null pointer represents an empty set.
//
// Set is designed with simplicity in mind.
// It should be very fast for small number of items.
// However, if you plan to add a lot of items to a set,
// you should consider using Map as a set.

#include "set.h"
#include <stdlib.h>
#include <string.h>

CCSetAllocator::CCSetAllocator()
{ }

CCSetAllocator::~CCSetAllocator()
{ }

Set *CCSetAllocator::createSet()
{
	auto p = new Set({});

	m_setList.push_back(std::unique_ptr<Set>(p));
	return p;
}

Set *CCSetAllocator::SetAdd(Set *s, const char *v)
{
	Set *r = createSet();
	r->next = s;
	r->v = v;
	return r;
}

bool CCSetAllocator::SetHas(Set *s, const char *v)
{
	for (; s; s = s->next)
	{
		if (!strcmp(s->v, v))
			return true;
	}
	return false;
}

Set *CCSetAllocator::SetUnion(Set *a, Set *b)
{
	Set *r = b;
	for (; a; a = a->next)
	{
		if (!SetHas(b, a->v))
			r = SetAdd(r, a->v);
	}
	return r;
}

Set *CCSetAllocator::SetIntersection(Set *a, Set *b)
{
	Set *r = nullptr;
	for (; a; a = a->next)
	{
		if (SetHas(b, a->v))
			r = SetAdd(r, a->v);
	}
	return r;
}

