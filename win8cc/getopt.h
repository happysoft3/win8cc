
#ifndef GET_OPT_H__
#define GET_OPT_H__

#include <string.h>
#include <stdio.h>

static int     opterr = 1,             /* if error message should be printed */
optind = 1,             /* index into parent argv vector */
optopt = 0,                 /* character checked for validity */
optreset = 0;               /* reset getopt */
static char    *optarg;                /* argument associated with option */

#define BADCH   (int)'?'
#define BADARG  (int)':'
#define EMSG    ""

                                /*
                                * getopt --
                                *      Parse argc/argv argument vector.
                                */
int getopt(int nargc, char * const nargv[], const char *ostr);

#endif

