
#ifndef FILE_H__
#define FILE_H__

#include <ctime>
#include <cstdio>
#include <memory>

class CCVector;
class CCBufferAllocator;

typedef struct File
{
	FILE *file;  // stream backed by FILE *
	char *p;     // stream backed by string
	char *name;
	int line;
	int column;
	int ntok;     // token counter
	int last;     // the last character read from file
	int buf[3];   // push-back buffer for unread operations
	int buflen;   // push-back buffer size
	time_t mtime; // last modified time. 0 if string-backed file
} File;

// file.c
class CCFile
{
private:
    CCVector *m_files;
    CCVector *m_stashed;
	std::unique_ptr<CCBufferAllocator> m_buffer;

public:
    CCFile();
    ~CCFile();

public:
    static File *make_file(FILE *file, const char *name);
    static File *make_file_string(char *s);

private:
    static void close_file(File *f);
    static int readc_file(File *f);
    static int readc_string(File *f);
    int get();

public:
    int readc(void);
    void unreadc(int c);
    File *current_file(void);
    void stream_push(File *file);
    int stream_depth(void);
    char *input_position(void);
    void stream_stash(File *f);
    void stream_unstash(void);
};

#endif
