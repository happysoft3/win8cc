
#ifndef MAP_H__
#define MAP_H__

class CCMap
{
private:
	CCMap *m_parent;
	const char **m_key;
	void **m_val;
	int m_size;
	int m_nelem;
	int m_nused;

public:
	CCMap(CCMap *parent = nullptr);
	~CCMap();

private:
	void initialize(CCMap *parent, size_t size);
	void maybeRehash();
	void *getNostack(const char *key);

public:
	void *Get(const char *key);
	void Put(const char *key, void *val);
	void Remove(const char *key);
	size_t Length() const
	{
		return m_nelem;
	}
};

#endif
