
#ifndef SET_H__
#define SET_H__

#include <list>
#include <memory>

struct Set
{
	const char *v;
	Set *next;
};

class CCSetAllocator
{
private:
	std::list<std::unique_ptr<Set>> m_setList;

public:
	CCSetAllocator();
	~CCSetAllocator();

private:
	Set *createSet();

public:
	Set *SetAdd(Set *s, const char *v);
	bool SetHas(Set *s, const char *v);
	Set *SetUnion(Set *a, Set *b);
	Set *SetIntersection(Set *a, Set *b);
};

#endif

